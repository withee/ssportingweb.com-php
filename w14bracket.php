<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ผลบอลสด อัพเดทรวดเร็วที่สุดและแม่นยำที่สุด';
$meta = '<meta name="description" content="เช็คผลบอลสด ผลบอลเมื่อคืนและผลบอลย้อนหลังได้ที่นี่ ข้อมูลแม่นยำอัพเดทผลรวดเร็ว ซึ่งจะทำให้คุณไม่พลาดทุกวินาทีสำคัญ">' . "\n";
$meta .= '<meta name="keyword" content="ผลบอลสด,ผลบอล,ผลบอลเมื่อคืน,ผลบอลย้อนหลัง,ไฮไลท์ฟุตบอล">' . "\n";

$service_w14 = Services::getW14();

//echo '<pre>';
//print_r($service_w14->bracket);
//echo '</pre>';
//exit;

$footerScript .= '<script src="scripts/w14bracket.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php');
?>


    <div id="news-top-slide-box" class="wrapper-slide-comment-top" style="display: none;">

        <div class="box-comment-top" ng-repeat="item in newsTopSlide">
            <a href="/news.php?id={{ news.ontimelines[item.newsId]}}">
                <table>
                    <tr>
                        <td><img ng-src="{{ item.imageLink}}"></td>
                        <td>
                            <b ng-bind="news.titles[item.newsId]"></b>
                            <span class="detail-news" ng-bind="news.desc[item.newsId]"></span>
                        </td>
                    </tr>
                </table>
            </a>
        </div>
    </div>

    <div class="wrapper-content content-profile">
    <div class="banner" style="padding-left: 5px;">
        <a href="/w14index.php"><img src="images/banner.jpg"></a>
    </div>

    <div class="tab-heading-title">Bracket</div>
    <div class="wrapper-box-bracket">
    <?php for($i=0; $i<8 ; $i++): ?>
    <div class="box-bracket">
        <div class="nameGroup"><?php echo preg_replace("#-#", ' ', $service_w14->tables[$i][0]->subLeagueNamePk); ?></div>
        <table>
            <thead>
            <tr>
                <th>Team</th>
                <th>Pts</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($service_w14->tables[$i] as $item): ?>
            <tr>
                <td>
                    <?php if($item->tid): ?>
                        <img src="http://ws.1ivescore.com/worldcup/<?php echo $item->tid; ?>_.png">
                    <?php else: ?>
                        <img src="images/default-bal70l.png">
                    <?php endif; ?>
                    <?php echo $service_w14->team->{$item->tid}->{__LANGUAGE__}; ?></td>
                <td><?php echo $item->pts; ?></td>
            </tr>
            <?php endforeach; ?>
            </tbody>

        </table>
    </div>
    <?php endfor; ?>
    </div>

    <div style="clear: both;"></div>
    <div class="tab-round" style="text-align: center; color: #000;">Round of 32</div>
    <div class="wrapper-box-feed-expand bg-wordcup">
    <div class="wrap-boxRace">
    <div class="roundOf-16">
        <?php foreach ($service_w14->bracket->round_of_16->top as $item): ?>
        <div class="box-team-raceRound">
            <table>
                <?php if($item->mid === 0): ?>
                    <tr>
                        <td>
                            <?php if($item->hid): ?>
                                <img src="http://ws.1ivescore.com/worldcup/<?php echo $item->hid; ?>_.png">
                                <?php echo $service_w14->team->{$item->hid}->{__LANGUAGE__}; ?>
                            <?php else: ?>
                                <img src="images/default-bal70l.png">
                                <?php echo $item->s1; ?>
                            <?php endif; ?>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <?php if($item->gid): ?>
                                <img src="http://ws.1ivescore.com/worldcup/<?php echo $item->gid; ?>_.png">
                                <?php echo $service_w14->team->{$item->gid}->{__LANGUAGE__}; ?>
                            <?php else: ?>
                                <img src="images/default-bal70l.png">
                                <?php echo $item->s2; ?>
                            <?php endif; ?>
                        </td>
                        <td></td>
                    </tr>
                <?php else: ?>
                    <tr>
                        <td><img src="images/icon/english-flag.gif"> <?php echo $service_w14->team->{$item->hid}->{__LANGUAGE__}; ?></td>
                        <td><?php echo Utils::getScore($item->s1, 'h'); ?></td>
                    </tr>
                    <tr>
                        <td><img src="images/icon/english-flag.gif"> <?php echo $service_w14->team->{$item->gid}->{__LANGUAGE__}; ?></td>
                        <td><?php echo Utils::getScore($item->s1, 'h'); ?></td>
                    </tr>
                <?php endif; ?>
            </table>
            <div class="TimeRacing"><?php echo $item->date; ?></div>
        </div>
        <?php endforeach; ?>
    </div>

    <div style="clear: both;"></div>
    <div class="Rounding">
        <table>
            <tr>
                <td></td>
                <td class="title-round">Round of 16</td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="QuarterRound">
        <table>
            <tr>
                <td class="Round1">
                    <div class="box-team-raceRound" style="float: right;">
                        <table>
                            <?php if ($service_w14->bracket->quarter_finals->top[0]->mid === 0): ?>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->top[0]->hid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->quarter_finals->top[0]->hid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->top[0]->hid}->{__LANGUAGE__} ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Winner 1A/2B
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->top[0]->gid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->quarter_finals->top[0]->gid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->top[0]->gid}->{__LANGUAGE__} ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Winner 1C/2D
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                            <?php else: ?>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->top[0]->hid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $item->hid; ?>_.png">
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                        <?php endif; ?>
                                        <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->top[0]->hid}->{__LANGUAGE__}; ?></td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->top[0]->gid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->quarter_finals->top[0]->gid; ?>_.png">
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                        <?php endif; ?>
                                        <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->top[0]->gid}->{__LANGUAGE__}; ?></td>
                                    <td>0</td>
                                </tr>
                            <?php endif; ?>
                        </table>
                        <div class="TimeRacing"><?php echo $service_w14->bracket->quarter_finals->top[0]->date; ?></div>
                    </div>
                </td>
                <td class="title-round">Quarter-Final</td>
                <td class="Round2">
                    <div class="box-team-raceRound">
                        <table>
                            <?php if ($service_w14->bracket->quarter_finals->top[1]->mid === 0): ?>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->top[1]->hid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->quarter_finals->top[1]->hid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->top[1]->hid}->{__LANGUAGE__} ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Winner 1E/2F
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->top[1]->gid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->quarter_finals->top[1]->gid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->top[1]->gid}->{__LANGUAGE__} ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Winner 1G/2H
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                            <?php else: ?>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->top[1]->hid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->quarter_finals->top[1]->hid; ?>_.png">
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                        <?php endif; ?>
                                        <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->top[1]->hid}->{__LANGUAGE__}; ?></td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->top[1]->gid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->quarter_finals->top[1]->gid; ?>_.png">
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                        <?php endif; ?>
                                        <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->top[1]->gid}->{__LANGUAGE__}; ?></td>
                                    <td>0</td>
                                </tr>
                            <?php endif; ?>
                        </table>
                        <div class="TimeRacing"><?php echo $service_w14->bracket->quarter_finals->top[1]->date; ?></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <div class="line-center">
        <table>
            <tr>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="semiFinal-round">
        <table>
            <tr>
                <td class="titie-semi">Semi-Final</td>
                <td>
                    <div class="box-team-raceRound">
                        <table>
                            <?php if ($service_w14->bracket->semi_finals->top[0]->mid === 0): ?>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->semi_finals->top[0]->hid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->semi_finals->top[0]->hid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->semi_finals->top[0]->hid}->{__LANGUAGE__}; ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Winner AB/CD
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->semi_finals->top[0]->gid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->semi_finals->top[0]->gid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->semi_finals->top[0]->gid}->{__LANGUAGE__}; ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Winner EF/GH
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                            <?php else: ?>
                            <tr>
                                <td>
                                    <?php if($service_w14->bracket->semi_finals->top[0]->hid): ?>
                                        <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->semi_finals->top[0]->hid; ?>_.png">
                                    <?php else: ?>
                                        <img src="images/default-bal70l.png">
                                    <?php endif; ?>
                                    <?php echo $service_w14->team->{$service_w14->bracket->semi_finals->top[0]->hid}->{__LANGUAGE__}; ?></td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td>
                                    <?php if($service_w14->bracket->semi_finals->top[0]->gid): ?>
                                        <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->semi_finals->top[0]->gid; ?>_.png">
                                    <?php else: ?>
                                        <img src="images/default-bal70l.png">
                                    <?php endif; ?>
                                    <?php echo $service_w14->team->{$service_w14->bracket->semi_finals->top[0]->gid}->{__LANGUAGE__}; ?><</td>
                                <td>0</td>
                            </tr>
                            <?php endif; ?>
                        </table>
                        <div class="TimeRacing"><?php echo $service_w14->bracket->semi_finals->top[0]->date; ?></div>
                    </div>
                </td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="final-round">
        <h4>Final</h4>
        <table>
            <tr>
                <td>
                    <div class="box-team-raceRound box-first-race" style="float: right;">
                        <div style="text-align: center; color: #fff; font-weight: bold; background-color: #009c3a;">Final</div>
                        <table>
                            <?php if ($service_w14->bracket->finals->_1st[0]->mid === 0): ?>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->finals->_1st[0]->hid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->finals->_1st[0]->hid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->finals->_1st[0]->hid}->{__LANGUAGE__}; ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Winner Top
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->finals->_1st[0]->gid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->finals->_1st[0]->gid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->finals->_1st[0]->gid}->{__LANGUAGE__}; ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Winner Bottom
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                            <?php else: ?>
                            <tr>
                                <td>
                                    <?php if($service_w14->bracket->finals->_1st[0]->hid): ?>
                                        <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->finals->_1st[0]->hid; ?>_.png">
                                    <?php else: ?>
                                        <img src="images/default-bal70l.png">
                                    <?php endif; ?>
                                    <?php echo $service_w14->team->{$service_w14->bracket->finals->_1st[0]->hid}->{__LANGUAGE__}; ?></td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td>
                                    <?php if($service_w14->bracket->finals->_1st[0]->gid): ?>
                                        <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->finals->_1st[0]->gid; ?>_.png">
                                    <?php else: ?>
                                        <img src="images/default-bal70l.png">
                                    <?php endif; ?>
                                    <?php echo $service_w14->team->{$service_w14->bracket->finals->_1st[0]->gid}->{__LANGUAGE__}; ?></td>
                                <td>0</td>
                            </tr>
                            <?php endif; ?>
                        </table>
                        <div class="TimeRacing"><?php echo $service_w14->bracket->finals->_1st[0]->date; ?></div>
                    </div>
                </td>
                <td class="fifa-round">
                    <img src="images/logo-worldcup.png">
                                        <?php if ($service_w14->bracket->award->_1st != 0): ?>
                                        <span class="tab-champion">
                                            1. <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->award->_1st; ?>_.png"> <?php echo $service_w14->team->{$service_w14->bracket->award->_1st}->{__LANGUAGE__}; ?>
                                        </span>
                                        <?php endif; ?>
                                        <?php if ($service_w14->bracket->award->_2nd != 0): ?>
                                        <span class="tab-champion">
                                            2. <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->award->_2nd; ?>_.png"> <?php echo $service_w14->team->{$service_w14->bracket->award->_2nd}->{__LANGUAGE__}; ?>
                                        </span>
                                        <?php endif; ?>
                                        <?php if ($service_w14->bracket->award->_3nd != 0): ?>
                                            <span class="tab-champion">
                                            3. <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->award->_3ndt; ?>_.png"> <?php echo $service_w14->team->{$service_w14->bracket->award->_3nd}->{__LANGUAGE__}; ?>
                                            </span>
                                        <?php endif; ?>
                </td>
                <td>
                    <div class="box-team-raceRound">
                        <table>
                            <?php if ($service_w14->bracket->finals->_3nd[0]->mid === 0): ?>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->finals->_3nd[0]->hid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->finals->_3nd[0]->hid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->finals->_3nd[0]->hid}->{__LANGUAGE__}; ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Loser Top
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->finals->_3nd[0]->gid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->finals->_3nd[0]->gid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->finals->_3nd[0]->gid}->{__LANGUAGE__}; ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Loser Bottom
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                            <?php else: ?>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->finals->_3nd[0]->hid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->finals->_3nd[0]->hid; ?>_.png">
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                        <?php endif; ?>
                                        <?php echo $service_w14->team->{$service_w14->bracket->finals->_3nd[0]->hid}->{__LANGUAGE__}; ?></td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->finals->_3nd[0]->gid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->finals->_3nd[0]->gid; ?>_.png">
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                        <?php endif; ?>
                                        <?php echo $service_w14->team->{$service_w14->bracket->finals->_3nd[0]->gid}->{__LANGUAGE__}; ?></td>
                                    <td>0</td>
                                </tr>
                            <?php endif; ?>
                        </table>
                        <div class="TimeRacing"><?php echo $service_w14->bracket->finals->_3nd[0]->date; ?></div>
                    </div>
                    <div style="clear: both;"></div>
                    <b style="padding-left: 40px;">3ed Place</b>
                </td>
            </tr>
        </table>
    </div>

    <div class="semiFinal-round">
        <table>
            <tr>
                <td class="titie-semi">Semi-Final</td>
                <td>
                    <div class="box-team-raceRound">
                        <table>
                            <?php if ($service_w14->bracket->semi_finals->bottom[0]->mid === 0): ?>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->semi_finals->bottom[0]->hid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->semi_finals->bottom[0]->hid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->semi_finals->bottom[0]->hid}->{__LANGUAGE__}; ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Winner BA/DC
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->semi_finals->bottom[0]->gid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->semi_finals->bottom[0]->gid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->semi_finals->bottom[0]->gid}->{__LANGUAGE__}; ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Winner FE/HG
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                            <?php else: ?>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->semi_finals->bottom[0]->hid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->semi_finals->bottom[0]->hid; ?>_.png">
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                        <?php endif; ?>
                                        <?php echo $service_w14->team->{$service_w14->bracket->semi_finals->bottom[0]->hid}->{__LANGUAGE__}; ?></td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->semi_finals->bottom[0]->gid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->semi_finals->bottom[0]->gid; ?>_.png">
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                        <?php endif; ?>
                                        <?php echo $service_w14->team->{$service_w14->bracket->semi_finals->bottom[0]->gid}->{__LANGUAGE__}; ?><</td>
                                    <td>0</td>
                                </tr>
                            <?php endif; ?>
                        </table>
                        <div class="TimeRacing"><?php echo $service_w14->bracket->semi_finals->bottom[0]->date; ?></div>
                    </div>
                </td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="line-center2">
        <table>
            <tr>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="QuarterRound">
        <table>
            <tr>
                <td class="Round1">
                    <div class="box-team-raceRound" style="float: right;">
                        <table>
                            <?php if ($service_w14->bracket->quarter_finals->bottom[0]->mid === 0): ?>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->bottom[0]->hid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->quarter_finals->bottom[0]->hid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->bottom[0]->hid}->{__LANGUAGE__}; ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Winner 1B/2A
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->bottom[0]->gid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->quarter_finals->bottom[0]->gid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->bottom[0]->gid}->{__LANGUAGE__}; ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Winner 1D/2C
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                            <?php else: ?>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->bottom[0]->hid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->quarter_finals->bottom[0]->hid; ?>_.png">
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                        <?php endif; ?>
                                        <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->bottom[0]->hid}->{__LANGUAGE__}; ?></td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->bottom[0]->gid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->quarter_finals->bottom[0]->gid; ?>_.png">
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                        <?php endif; ?>
                                        <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->bottom[0]->gid}->{__LANGUAGE__}; ?></td>
                                    <td>0</td>
                                </tr>
                            <?php endif; ?>
                        </table>
                        <div class="TimeRacing"><?php echo $service_w14->bracket->quarter_finals->bottom[0]->date; ?></div>
                    </div>
                </td>
                <td class="title-round">Quarter-Final</td>
                <td class="Round2">
                    <div class="box-team-raceRound">
                        <table>
                            <?php if ($service_w14->bracket->quarter_finals->bottom[1]->mid === 0): ?>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->bottom[1]->hid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->quarter_finals->bottom[1]->hid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->bottom[1]->hid}->{__LANGUAGE__}; ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Winner 1F/2E
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->bottom[1]->gid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->quarter_finals->bottom[1]->gid; ?>_.png">
                                            <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->bottom[1]->gid}->{__LANGUAGE__}; ?>
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                            Winner 1H/2G
                                        <?php endif; ?>
                                        </td>
                                    <td></td>
                                </tr>
                            <?php else: ?>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->bottom[1]->hid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->quarter_finals->bottom[1]->hid; ?>_.png">
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                        <?php endif; ?>
                                        <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->bottom[1]->hid}->{__LANGUAGE__}; ?></td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php if($service_w14->bracket->quarter_finals->bottom[1]->gid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $service_w14->bracket->quarter_finals->bottom[1]->gid; ?>_.png">
                                        <?php else: ?>
                                            <img src="images/default-bal70l.png">
                                        <?php endif; ?>
                                        <?php echo $service_w14->team->{$service_w14->bracket->quarter_finals->bottom[1]->gid}->{__LANGUAGE__}; ?></td>
                                    <td>0</td>
                                </tr>
                            <?php endif; ?>
                        </table>
                        <div class="TimeRacing"><?php echo $service_w14->bracket->quarter_finals->bottom[1]->date; ?></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <div class="Rounding2">
        <table>
            <tr>
                <td></td>
                <td class="title-round">Round of 16</td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="roundOf-16">
        <?php foreach ($service_w14->bracket->round_of_16->bottom as $item): ?>
            <div class="box-team-raceRound">
                <table>
                    <?php if($item->mid === 0): ?>
                        <tr>
                            <td>
                                <?php if($item->hid): ?>
                                    <img src="http://ws.1ivescore.com/worldcup/<?php echo $item->hid; ?>_.png">
                                    <?php echo $service_w14->team->{$item->hid}->{__LANGUAGE__}; ?>
                                <?php else: ?>
                                    <img src="images/default-bal70l.png">
                                    <?php echo $item->s1; ?>
                                <?php endif; ?>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <?php if($item->gid): ?>
                                    <img src="http://ws.1ivescore.com/worldcup/<?php echo $item->gid; ?>_.png">
                                    <?php echo $service_w14->team->{$item->gid}->{__LANGUAGE__}; ?>
                                <?php else: ?>
                                    <img src="images/default-bal70l.png">
                                    <?php echo $item->s2; ?>
                                <?php endif; ?>
                            </td>
                            <td></td>
                        </tr>
                    <?php else: ?>
                        <tr>
                            <td>
                                <?php if($item->hid): ?>
                                    <img src="http://ws.1ivescore.com/worldcup/<?php echo $item->hid; ?>_.png">
                                <?php else: ?>
                                    <img src="images/default-bal70l.png">
                                <?php endif; ?>
                                <?php echo $service_w14->team->{$item->hid}->{__LANGUAGE__}; ?></td>
                            <td><?php echo Utils::getScore($item->s1, 'h'); ?></td>
                        </tr>
                        <tr>
                            <td>
                                <?php if($item->gid): ?>
                                    <img src="http://ws.1ivescore.com/worldcup/<?php echo $item->gid; ?>_.png">
                                <?php else: ?>
                                    <img src="images/default-bal70l.png">
                                <?php endif; ?>
                                <?php echo $service_w14->team->{$item->gid}->{__LANGUAGE__}; ?></td>
                            <td><?php echo Utils::getScore($item->s1, 'h'); ?></td>
                        </tr>
                    <?php endif; ?>
                </table>
                <div class="TimeRacing"><?php echo $item->date; ?></div>
            </div>
        <?php endforeach; ?>
    </div>
    </div>
    <div style="clear: both;"></div>
    </div>

    </div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>