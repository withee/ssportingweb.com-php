<?php
require_once(dirname(__FILE__) . '/_init_.php');

define('__FB_UID__', isset($_REQUEST['id']) ? $_REQUEST['id'] : false);

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();
$service_profileFeed = Services::getProfileFeed(__FB_UID__);
?>

<?php foreach (@$service_profileFeed->matchcommentlist->data as $index => $item): ?>
    <div class="box-feed">

        <table>
            <tr class="head-feed">      
                <td class="time-date-post"> <?php echo $item->match->date; ?></td>
                <td class="league-name-title"><?php echo $item->match->LeagueName; ?></td>
            </tr>

            <tr class="teamplay">
                <td colspan="2" class="team-playing">
                    <table>
                        <tr>
                            <td class="sec-team1"><img src="<?php echo $item->logos->{$item->match->hid}->{'256x256'}; ?>"/></td>
                            <td class="hdpr">
                                <b><?php echo $item->match->hdp; ?></b><br/> <span class="macth-hdp">( <?php echo $item->match->hdp_home; ?> ) : (<?php echo $item->match->hdp_away; ?>)</span></td>
                            <td class="sec-team2"><img src="<?php echo $item->logos->{$item->match->gid}->{'256x256'}; ?>"/></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table>
                                    <tr>
                                        <td class="teams1"><span
                                                ng-class="{'team-favored': item.match.hdp < 0}"><?php echo $item->match->HomeName; ?></span>
                                            <div class="last-stat-fivematch"> <span class="text-green">W</span> | <span class="text-red">L</span> | <span class="text-green">W</span> | <span class="text-red">L</span> | D</div>
                                        </td>
                                        <td class="teams2"><span
                                                ng-class="{'team-favored': item.match.hdp > 0}"><?php echo $item->match->AwayName; ?></span>
                                            <div class="last-stat-fivematch"> <span class="text-green">W</span> | <span class="text-red">L</span> | <span class="text-green">W</span> | <span class="text-red">L</span> | D</div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td class="sec-team1"><span
                                    ng-class="{'team-favored': item.match.hdp < 0}"><?php echo $item->match->HomeName; ?></span>
                            </td>
                            <td class="hdp-r"></td>
                            <td class="sec-team2"><span
                                    ng-class="{'team-favored': item.match.hdp > 0}"><?php echo $item->match->AwayName; ?></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>


            <!--
            <tr>
                <td colspan="2">{{ item.message }}</td>
            </tr>
            <tr>
                <td colspan="2" class="team-playing">
                    <a href="#!/match/{{ item.mid }}">
                        {{ item.oddsTimestamp | parseOdds:'home' }} <span ng-class="{'team-selected': item.choose == 'home'}">{{ allTeams[item.hid].name }}</span> <img src="{{ logoUrl }}/{{ item.hid }}_32x32.png"/>
                        {{ item.betHdp }}
                        <img src="{{ logoUrl }}/{{ item.gid }}_32x32.png"/> <span ng-class="{'team-selected': item.choose == 'away'}">{{ allTeams[item.gid].name }}</span> {{ item.oddsTimestamp | parseOdds:'away' }}
                    </a>
                </td>
            </tr>
            <tr>
                <td class="more" style="display: none;"><span class="more-info">แสดง</span></td>
            </tr>
            -->


        </table>


        <div class="wrapper-box-feed-comment" style="display: snone;">
            <div class="box-feed-comment">
                <table>
                    <tr>
                        <td class="people-like"><?php echo $item->beton->home->count; ?></td>
                        <td class="user-like">
                            <span class="img-people-like">
                                <?php for ($i = 0; $i < 5; $i++): ?>
                                    <?php if (!isset($item->beton->home->list[$i])) break; ?>
                                    <a href="/profile.php?id=<?php echo $item->beton->home->list[$i]->fb_uid; ?>"><img src="https://graph.facebook.com/<?php echo $item->beton->home->list[$i]->fb_uid; ?>/picture"/></a>
                                <?php endfor; ?>
                            </span>
                        </td>
                        <td class="vs-like">VS</td>
                        <td class="user-like">
                            <span class="img-people-like">
                                <?php for ($i = 0; $i < 5; $i++): ?>
                                    <?php if (!isset($item->beton->away->list[$i])) break; ?>
                                    <a href="/profile.php?id=<?php echo $item->beton->away->list[$i]->fb_uid; ?>"><img src="https://graph.facebook.com/<?php echo $item->beton->away->list[$i]->fb_uid; ?>/picture"/></a>
                                <?php endfor; ?>
                            </span>
                        </td>
                        <td class="people-like"><?php echo $item->beton->away->count; ?></td>
                    </tr>
                </table>
            </div>


            <div class="tab-more-feed-comment" style="display: none;">
                <img src="images/icon/commented.png"/> <a href="">ดูอีก 5 ความคิดเห็น</a>
            </div>
            <div class="box-show-comment">
                <?php foreach ($item->betlist as $idx => $itm): ?>
                    <table>
                        <tr>
                            <!--<td class="user-comment-img"></td>-->
                            <!-- <td class="user-comment-img"><a href="/profile.php?id={{ comment.fb_uid }}"><img ng-src="https://graph.facebook.com/{{ comment.fb_uid }}/picture"/></a></td>-->
                            <td class="user-comment"><a href="/profile.php?id=<?php echo $itm->fb_uid; ?>"><img
                                        src="https://graph.facebook.com/<?php echo $itm->fb_uid; ?>/picture"/></a>

                                <div class="commented-box" style="width: 440px; float: left;"><b><?php echo $itm->displayname; ?></b> <span class="team-user-selected">
                                        <?php if ($itm->choose === 'home'): ?>
                                            <span>@<?php echo $item->match->HomeName; ?></span>
                                        <?php else: ?>
                                            <span>@<?php echo $item->match->AwayName; ?></span>
                                        <?php endif; ?>
                                    </span> <?php echo $itm->message; ?>
                                    <span class="time-post">
                                        <ul>
                                            <li class="time-post-comment-user"><?php echo $itm->comment_at; ?></li>
                                            <li class="user-liked"><img src="images/icon/heart.png"/>
                                                (<?php echo $itm->like; ?>)
                                            </li>
                                        </ul>
                                    </span>
                                </div>
                            </td>
                        </tr>
                    </table>
                <?php endforeach; ?>
            </div>

            <!-- comment input -->

            <div class="box-show-comment">
                <table>
                    <tr>
                        <!-- <td class="user-comment-img"></td>-->
                        <td class="comment">
                            <div class="upimages-box"><img src="images/icon/cam15.png" title="แนบรูปภาพ"/></div>
                            <img style="width: 25px; height: 25px;" src="https://graph.facebook.com/<?php echo __FB_UID__; ?>/picture"/>
                            <input type="text" placeholder="comment..." ng-click="redirectToGamePage('<?php echo $item->match->mid; ?>')" disabled/>
                        </td>
                    </tr>
                </table>
            </div>

            <!-- end -->

        </div>

    </div>

<?php endforeach; ?>




<div style="clear: both;"></div>





