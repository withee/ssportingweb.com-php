<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$footerScript .= '<script src="scripts/games/card-black-red.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<!--Content-->
<div class="wrapper-content content-profile">

    <div class="wrapper-miniGames">
        <div class="tab-heading-title tabTypeGames"><img src="images/mini-game/card/title_icon_g3.png" width="40px;"
                                                         style="float: left;"> เกมส์ไพ่ ดำ-แดง
        </div>

        <div class="Card-redwhite">
            <table>
                <tr>
                    <td>
                        <span ng-switch="cardResult">
                            <img ng-switch-when="b" src="/images/mini-game/card/1_card_01_spade.png">
                            <img ng-switch-when="r" src="/images/mini-game/card/2_card_01_Hearts.png">
                            <img ng-switch-default src="/images/mini-game/card/card_red_front.png">
                        </span>

                    </td>
                </tr>
            </table>
<!--            <div ng-class="{'wrong':result === 'lose'}" class="resultAlert">-->
<!--                <span class="alert alert-success" ng-if="result === 'win'"><img src="images/icon/right-sign-icon.png"> คุณทายถูก</span>-->
<!--                <span class="alert alert-danger" ng-if="result === 'lose'"><img src="images/icon/wrong-sign-icon.png"> คุณทายผิด</span>-->
<!--            </div>-->
        </div>
        <div class="selectCoin">
            <table>
                <tr>
                    <td ng-class="{'activeCoin-100':scoin === 100}" ng-click="scoin = 100" class="selectCoin-100"></td>
                    <td ng-class="{'activeCoin-200':scoin === 200}" ng-click="scoin = 200" class="selectCoin-200"></td>
                    <td ng-class="{'activeCoin-500':scoin === 500}" ng-click="scoin = 500" class="selectCoin-500"></td>
                </tr>
            </table>
        </div>

        <div class="selectCard-Color">
            <table>
                <tr>
                    <td ng-click="play('b')" class="blackcard-selected">
<!--                        <img src="/images/mini-game/card/2_card_01_Hearts.png">-->
                    </td>
                    <td ng-click="play('r')" class="redcard-selected">
<!--                        <img src="/images/mini-game/card/1_card_01_spade.png">-->
                    </td>
                </tr>
            </table>
        </div>

        <div class="titleGames" style="display: none;">สติถิการเล่นล่าสุด</div>
        <div class="boxStatPlay-Games gameCards" style="display: none;">
            <table>
                <tr>
                    <td><img src="images/mini-game/card/1_card_01_spade.png"></td>
                    <td><img src="images/mini-game/card/2_card_01_Hearts.png"></td>
                    <td><img src="images/mini-game/card/2_card_01_Hearts.png"></td>
                    <td><img src="images/mini-game/card/2_card_01_Hearts.png"></td>
                    <td><img src="images/mini-game/card/1_card_01_spade.png"></td>
                    <td><img src="images/mini-game/card/1_card_01_spade.png"></td>
                    <td><img src="images/mini-game/card/2_card_01_Hearts.png"></td>
                    <td><img src="images/mini-game/card/2_card_01_Hearts.png"></td>
                    <td><img src="images/mini-game/card/1_card_01_spade.png"></td>
                    <td><img src="images/mini-game/card/2_card_01_Hearts.png"></td>
                    <td><img src="images/mini-game/card/2_card_01_Hearts.png"></td>
            </table>
        </div>

        <div class="titleGames">วิธีการเล่นเกมส์</div>
        <div class="boxHowToPlay-Games">
            <ul>
                <li><b>เกมส์ทายสีไพ่ จะเป็นเกมส์ที่ผู้เล่นทายว่าไพ่ที่จะออกเป็นไพ่สีแดงหรือสีดำ โดยมีวิธีการเล่นดังนี้</b></li>
                <li>- เลือกจำนวนเหรียญที่ต้องการเดิมพัน โดยมีให้เลือก 100, 200, 500 เหรียญ Scoin</li>
                <li>- จากนั้นเลือกทายว่าไพ่จะออกสีอะไร โดยมีให้เลือก คือ ไพ่สีแดง และไพ่สีดำ</li>
                <li>- ผลการทายจะแสดงให้คุณว่าเราทายถูกหรือทายผิด โดยสามารถดูสถิติการเล่นได้ที่ตารางสรุปผลการเล่นเกมส์</li>
            </ul>
        </div>

        <!--            ตารางสรุปผลการเล่นเกมส์-->
        <div class="titleGames">ตารางสรุปผลการเล่นเกมส์</div>

        <div class="table-ResultMiniGames">
            <div class="tabs-tableResult">
                <ul>
                    <li ng-click="stateTab = 'user'" ng-class="{'active':stateTab === 'user'}">เฉพาะฉัน</li>
                    <li ng-click="stateTab = 'all'" ng-class="{'active':stateTab === 'all'}">ทุกคน</li>
                </ul>
                <div style="clear: both;"></div>
            </div>
            <table>
                <thead>
                    <tr>
                        <th>ชื่อผู้เล่น</th>
                        <th>ชื่อเกมส์</th>
                        <th>ผู้เล่น</th>
                        <th>คอมพ์</th>
                        <th>ผลการเล่น</th>
                        <th>จำนวน</th>
                        <th>ได้/เสีย</th>
                        <th>วันที่/เวลา</th>
                    </tr>
                </thead>
                <tbody ng-show="stateTab === 'user'">
                    <tr ng-repeat="item in statement.user">
                        <td><a href="profile.php?id={{ item.fb_uid}}"><img
                                    ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"> {{ item.display_name ||
                            item.fb_firstname }}</a></td>
                        <td>ไพ่ ดำแดง</td>
                        <td>
                            <span ng-switch="item.player1">
                                <img ng-switch-when="b" src="/images/mini-game/card/1_card_01_spade.png">
                                <img ng-switch-when="r" src="/images/mini-game/card/2_card_01_Hearts.png">
                            </span>
                        </td>
                        <td>
                            <span ng-switch="item.player2">
                                <img ng-switch-when="b" src="/images/mini-game/card/1_card_01_spade.png">
                                <img ng-switch-when="r" src="/images/mini-game/card/2_card_01_Hearts.png">
                            </span>
                        </td>
                        <td>
                            <span ng-switch="item.result">
                                <span class="correct" ng-switch-when="win">ทายถูก</span>
                                <span class="wrong" ng-switch-when="lose">ทายผิด</span>
                            </span>
                        </td>
                        <td><img src="images/icon/scoin30.png"> {{ item.scoin_amount}}</td>
                        <td><img src="images/icon/sgold30.png"> <span ng-class="{'correct':item.scoin_result_amount > 0, 'wrong':item.scoin_result_amount < 0 }">{{ item.scoin_result_amount}}</span></td>
                        <td>{{ item.play_timestamp | timeagoByDateTime }}</td>
                    </tr>
                </tbody>
                <tbody ng-show="stateTab === 'all'">
                    <tr ng-repeat="item in statement.all">
                        <td><a href="profile.php?id={{ item.fb_uid}}"><img
                                    ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"> {{ item.display_name ||
                            item.fb_firstname }}</a></td>
                        <td>ไพ่ ดำแดง</td>
                        <td>
                            <span ng-switch="item.player1">
                                <img ng-switch-when="b" src="/images/mini-game/card/1_card_01_spade.png">
                                <img ng-switch-when="r" src="/images/mini-game/card/2_card_01_Hearts.png">
                            </span>
                        </td>
                        <td>
                            <span ng-switch="item.player2">
                                <img ng-switch-when="b" src="/images/mini-game/card/1_card_01_spade.png">
                                <img ng-switch-when="r" src="/images/mini-game/card/2_card_01_Hearts.png">
                            </span>
                        </td>
                        <td>
                            <span ng-switch="item.result">
                                <span class="correct" ng-switch-when="win">ทายถูก</span>
                                <span class="wrong" ng-switch-when="lose">ทายผิด</span>
                            </span>
                        </td>
                        <td><img src="images/icon/scoin30.png"> {{ item.scoin_amount}}</td>
                        <td><img src="images/icon/sgold30.png"> <span ng-class="{'correct':item.scoin_result_amount > 0, 'wrong':item.scoin_result_amount < 0 }">{{ item.scoin_result_amount}}</span></td>
                        <td>{{ item.play_timestamp | timeagoByDateTime }}</td>
                    </tr>
                </tbody>
            </table>
        </div>



        <!--alert กรณีทายถูก -->
        <div id="winModal" class="Modal-resultGame" style="display: none;">
            <div class="fadeGame"></div>
            <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="false" style="display: block; z-index: 5; top: 200px; background: none; box-shadow: none; border: 0;">

                <div class="alertCorrect">      
                    <div class="titleModal"></div>
                    <div class="bodygameCard" style="padding: 30px;">
                        <img src="images/mini-game/paoyingchoob/modal_bg_lose_sgold.png">
                    </div>
                    <div class="resultPlay">คุณได้รับ</div>
                    <h3>{{ scoin}} <span class="goldText">S</span> COIN</h3>
                    <div class="titleModal" style="cursor: pointer;"><img ng-click="close()" src="images/mini-game/allgames/modal_bg_win_btn.png"></div>

                </div>

            </div>
        </div>



        <!--alert กรณีทายผิด -->
        <div id="loseModal" class="Modal-resultGame" style="display: none;">
            <div class="fadeGame"></div>
            <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="false" style="display: block; z-index: 5; top: 200px; background: none; box-shadow: none; border: 0;">

                <div class="alertWrong">      
                    <div class="titleModal"></div>
                    <div class="bodygameCard" style="padding: 30px;">
                        <img src="images/mini-game/paoyingchoob/modal_bg_lose_sgold.png">
                    </div>
                    <div class="resultPlay">คุณเสีย</div>
                    <h3>- <span class="text-red">{{ scoin}}</span> <span class="goldText">S</span> COIN</h3>
                    <div class="titleModal" style="cursor: pointer; "><img ng-click="close()" src="images/mini-game/allgames/modal_bg_lose_btn.png"></div>
                </div>

            </div>
        </div>

   </div>

</div>

<div class="preload-images" style="display: none;">
    <img src="/images/mini-game/card/1_card_01_spade.png">
    <img src="/images/mini-game/card/2_card_01_Hearts.png">
    <img src="/images/mini-game/card/card_red_front.png">
</div>

<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
