<?php
require_once(dirname(__FILE__) . '/_init_.php');

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

//echo '<pre>';
//print_r($service_matchInfo);
//echo '</pre>';

$footerScript .= '<script id="favoriteScript" src="scripts/favorite.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php');
?>


    <div class="wrapper-content">
        <div class="content">
            <div class="box-all-livemath">
                <div class="tab-situations"></div>

                <div class="box-speech" ng-repeat="competition in competitions">
                    <div class="box-situations image-flag">
                        <span><img ng-src="images/countries/{{ competition.competitionId }}.png"/></span>
                    </div>
                    <div class="speech-left speech-left-white"></div>
                    <div class="tab-favorite" ng-click="toggleSub($event)">
                        <table>
                            <tr>
                                <td class="name-teamcountry" ng-bind="competition.competitionName"></td>
                                <td class="like">
                                    <div class="box-like-total" ng-bind="competition.leagueList.length"></div>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="wrapper-box-league-favorite" style="display: none;">
                        <div class="wrapper-league" ng-repeat="sub in competition.leagueList">
                            <div class="box-speech">
                                <div class="tab-favorite">
                                    <table>
                                        <tr>
                                            <td class="name-teamcountry"><img src="images/icon/list-icon.png"/> <a href="league.php?id={{ sub.leagueId }}">{{sub.leagueName }}</a>
                                                <span ng-click="toggleLeague($event, sub.leagueId)"> +</span>
                                            </td>
                                            <td class="like" ng-click="favorite('league', sub)">
                                            <span ng-switch on="checkFavoriteLeague(sub.leagueId)">
                                                <img ng-switch-when="false" src="images/icon/heart-hide.png"/>
                                                <img ng-switch-when="true" src="images/icon/heart.png"/>
                                            </span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div style="clear: both;"></div>

                                <div class="wrapper-league-team" style="display: none;">
                                    <div class="wrapper-league">

                                        <div class="box-speech" ng-repeat="team in leagues[sub.leagueId].stat_table">
                                            <div class="tab-favorite tab-favorite-team">
                                                <table>
                                                    <tr>
                                                        <td class="name-teamcountry"><img src="images/logo-ba.png"/>
                                                            <a href="/team.php?id={{ team.tid }}">{{ team.tn }}</a>
                                                        </td>
                                                        <td class="like" ng-click="favorite('team', team)">
                                                        <span ng-switch on="checkFavoriteTeam(team.tid)">
                                                            <img ng-switch-when="false"
                                                                 src="images/icon/heart-hide.png"/>
                                                            <img ng-switch-when="true" src="images/icon/heart.png"/>
                                                        </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>

                </div>


            </div>
            <div style="clear: both;"></div>
        </div>


    </div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>