<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ผลบอลสด อัพเดทรวดเร็วที่สุดและแม่นยำที่สุด';
$meta = '<meta name="description" content="เช็คผลบอลสด ผลบอลเมื่อคืนและผลบอลย้อนหลังได้ที่นี่ ข้อมูลแม่นยำอัพเดทผลรวดเร็ว ซึ่งจะทำให้คุณไม่พลาดทุกวินาทีสำคัญ">' . "\n";
$meta .= '<meta name="keyword" content="ผลบอลสด,ผลบอล,ผลบอลเมื่อคืน,ผลบอลย้อนหลัง,ไฮไลท์ฟุตบอล">' . "\n";

$service_w14 = Services::getW14();

//echo '<pre>';
//print_r($service_w14->matches);
//echo '</pre>';
//exit;

$footerScript .= '<script src="scripts/w14topscore.js"></script>';
use Carbon\Carbon;

require_once(__INCLUDE_DIR__ . '/header.php')
?>


    <div id="news-top-slide-box" class="wrapper-slide-comment-top" style="display: none;">

        <div class="box-comment-top" ng-repeat="item in newsTopSlide">
            <a href="/news.php?id={{ news.ontimelines[item.newsId]}}">
                <table>
                    <tr>
                        <td><img ng-src="{{ item.imageLink}}"></td>
                        <td>
                            <b ng-bind="news.titles[item.newsId]"></b>
                            <span class="detail-news" ng-bind="news.desc[item.newsId]"></span>
                        </td>
                    </tr>
                </table>
            </a>
        </div>
    </div>



    <div class="wrapper-content content-profile">

        <div class="banner" style="padding-left: 5px ">
            <a href="/w14index.php"><img src="images/banner.jpg"></a>
        </div>

        <div class="tab-heading-title">Top Scoreres</div>
        <div class="wrapper-box-feed-expand">
            <?php foreach ($service_w14->top_scorer as $item): if ($item->no != 1) break; ?>
                <div class="tab-topScore">
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <div class="numberOrder"><b>1</b></div>
                                <div class="userPhoto"><img
                                        src="http://dowebsite.com:2999/player-logo/<?php echo $item->playerId; ?>.jpg">
                                </div>
                            </td>
                            <td class="infoOrder topScore">
                                <b><?php echo $item->{'playerName' . ucfirst(__LANGUAGE__)}; ?></b><br><img
                                    src="http://ws.1ivescore.com/worldcup/<?php echo $item->tid; ?>_.png"> <span class="label-text"><?php echo $item->{'teamName' . ucfirst(__LANGUAGE__)}; ?></span></td>
                            <td><img src="images/icon/scorers.png"></td>
                            <td><span class="label-text">Goal(PEN)</span><br>

                                <h2><?php echo $item->totalScore; ?></h2></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            <?php endforeach; ?>

            <div class="tab-otherScore">
                <table>
                    <tbody>
                    <?php foreach ($service_w14->top_scorer as $idx => $item): if ($item->no == '1') continue;; ?>
                        <tr>
                            <td><?php echo $item->no; ?></td>
                            <td class="detailUser">
                                <div class="otherPhoto"><img
                                        src="http://dowebsite.com:2999/player-logo/<?php echo $item->playerId; ?>.jpg">
                                </div>
                            </td>
                            <td class="infoOrder">
                                <b><?php echo $item->{'playerName' . ucfirst(__LANGUAGE__)}; ?></b><br><img
                                    src="http://ws.1ivescore.com/worldcup/<?php echo $item->tid; ?>_.png"> <span class="label-text"><?php echo $item->{'teamName' . ucfirst(__LANGUAGE__)}; ?></span></td>
                            <td><span class="label-text">Goal(PEN)</span><br>

                                <h3><?php echo $item->totalScore; ?></h3></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>


        </div>

    </div>




<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>