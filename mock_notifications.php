<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$service_liveMatch = Services::getLiveMatch();
$service_liveWait = Services::getLiveWait();

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

$footerScript .= '<script src="scripts/main.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div ng-controller="mainCtrl">


    <!--Content-->
    <div class="wrapper-content content-profile">
        <div class="tab-heading-title"><img src="images/icon/noti-red.png"> Notifications</div>
        <div class="tab-notifications">
            <table>
                <tr>
                    <td class="activeNoti"><b>แจ้งเตือนทั้งหมด (23)</b></td>
                    <td><b>โพสต์ของคุณ (2)</b></td>
                    <td><b>เกมส์ (30)</b></td>
                    <td><b>ติดตามคุณ (10)</b></td>
                </tr>
            </table>
        </div>


        <div class="wrapper-box-notifications">

            <div class="heading-dateNoti">วันนี้</div>
            <div class="wrapper-boxNoti">

                <div class="notifications games">
                    <span class="closeNoti">x</span>
                    <table>
                        <tr>
                            <td><img src="images/icon/total.png"> ผลการเล่นเกมส์ : <b><span class="font-red-live">อุรุกวัย 2-0</span> <span class="team-favored">อังกฤษ</span> : Win ที่ <span class="font-red-live">+0.5</span> : คะแนน <span class="font-red-live">+8.60</span></b></td>
                            <td><img src="images/icon/win.png"></td>
                        </tr>
                    </table>
                </div>

                <div class="notifications">
                    <span class="closeNoti">x</span>
                    <img src="images/icon/bet2.png"> <span class="nameUser">Watcharaporn Thammasart</span> : เพื่อนของคุณได้แสดงทรรศนะในคู่บอลที่คุณเล่น <b>"2-0 มาแน่ๆ ฮ่าๆๆ สุดยอดแล้วทีมนี้อ่ะ จัดไปครับท่าน"</b> <span class="font-blue">23:30</span>
                </div>

                <div class="notifications">
                    <span class="closeNoti">x</span>
                    <img src="images/icon/follow.png"> <span class="nameUser">หลวงปู่เณรคำ</span> : กำลังติดตามคุณ <span class="font-blue">12:00</span>
                </div>

                <div class="notifications">
                    <span class="closeNoti">x</span>
                    <img src="images/icon/heart.png"> <span class="nameUser">diarylovely Bim</span> : ได้ถูกใจโพสต์ของคุณ <span class="font-blue">16:30</span>
                </div>

                <div class="notifications">
                    <span class="closeNoti">x</span>
                    <img src="images/icon/bet2.png"> <span class="nameUser">หลวงปู่เณรคำ</span> : กำลังทายผลคู่ : <b>อังกฤษ - ฝรั่งเศษ</b> <span class="font-blue">05:30</span>
                </div>

                <div class="notifications">
                    <span class="closeNoti">x</span>
                    <img src="images/icon/commenting.png"> <span class="nameUser">Nana Nana</span> : ได้แสดงความคิดเห็นบนโพสต์ของคุณ <b>"มาแน่ ทีมนี้อ่ะ"</b> <span class="font-blue">23:30</span>
                </div>

            </div>

            <div class="heading-dateNoti">21/07/2014</div>

            <div class="wrapper-boxNoti">

                <div class="notifications games">
                    <span class="closeNoti">x</span>
                    <table>
                        <tr>
                            <td><img src="images/icon/total.png"> ผลการเล่นเกมส์ : <b><span class="font-red-live">อุรุกวัย 2-0</span> <span class="team-favored">อังกฤษ</span> : Win ที่ <span class="font-red-live">+0.5</span> : คะแนน <span class="font-red-live">+8.60</span></b></td>
                            <td><img src="images/icon/win.png"></td>
                        </tr>
                    </table>
                </div>

                <div class="notifications">
                    <span class="closeNoti">x</span>
                    <img src="images/icon/bet2.png"> <span class="nameUser">diarylovely</span> : ได้แสดงความคิดเห็นบนโพสต์ของคุณ <b>"ขอทีเด็ดหน่อยวันนี้ท่าน..."</b> <span class="font-blue">23:30</span>
                </div>

                <div class="notifications">
                    <span class="closeNoti">x</span>
                    <img src="images/icon/follow.png"> <span class="nameUser">Yutthana</span> : กำลังติดตามคุณ <span class="font-blue">12:00</span>
                </div>

                <div class="notifications">
                    <span class="closeNoti">x</span>
                    <img src="images/icon/commenting.png"> <span class="nameUser">Nana Nana</span> : ได้แสดงความคิดเห็นบนโพสต์ของคุณ <b>"ฟันธง 3-0 ชัวร์"</b> <span class="font-blue">23:30</span>
                </div>

                <div class="notifications">
                    <span class="closeNoti">x</span>
                    <img src="images/icon/commenting.png"> <span class="nameUser">Bebe Jung</span> : ได้แสดงความคิดเห็นบนโพสต์ของคุณ <b>"ตามมาเชียร์ นักเตะทีมนี้หล่อ ฮ่าๆๆ"</b> <span class="font-blue">23:30</span>
                </div>

                <div class="notifications">
                    <span class="closeNoti">x</span>
                    <img src="images/icon/heart.png"> <span class="nameUser">Indy</span> : ได้ถูกใจโพสต์ของคุณ <span class="font-blue">00:30</span>
                </div>

            </div>


            <div class="readMore-noti">ดูทั้งหมด</div>
        </div>
    </div>



    <!--    ส่วน modal สรุปผลการเล่นเกมส์-->

    <div class="Modal-resultGame" style="display: none;">
        <div class="fadeGame"></div>
        <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="false" style="display: block; z-index: 5;">

            <div class="header-resultGame">
                <div class="closed" style="background-color: #fff; color: #333; position: absolute; margin-left: 545px; margin-top: -10px;">X</div>

                <div class="resultGames">
                    <h4>สรุปผลการเล่นเกมส์</h4>
                    <table>
                        <tr>
                            <td><span class="result">20</span> Play</td>
                            <td class="scorePta"><span class="result pta">83.54%</span> PTA</td>
                            <td>
                                <div class="wrapScore">
                                    <div class="tscore"><b>+123.30</b>SCORE</div>
                                </div>
                            </td>
                            <td><span class="result twin">12</span> Win</td>
                            <td><span class="result tdraw">3</span> Draw</td>
                            <td><span class="result tlose">5</span> Lose</td>
                        </tr>
                    </table>
                    <div class="tab">
                        <div class="tab1">Fri, Jan 31</div>
                        <div class="tab2">Rating : 69.54%  Ranking :  1790.00</div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
            </div>

            <div class="tabUtility">
                <div class="tab1 text-purple">สรุปคู่ที่เล่นวันนี้</div>
                <div class="tab2"><span class="seeMore">ดูทั้งหมด</span></div>
                <div style="clear: both;"></div>
            </div>
            <div class="tableResult-game">
                <table>
                    <tr>
                        <td>Honduras (2.06)</td>
                        <td>0.75</td>
                        <td><span class="team-favored">Equador (1.88)</span></td>
                        <td class="scored">1 - 2</td>
                        <td>+7.05</td>
                        <td><img src="images/icon/win.png"></td>
                    </tr>
                    <tr>
                        <td>Honduras (2.06)</td>
                        <td>0.75</td>
                        <td><span class="team-favored">Equador (1.88)</span></td>
                        <td class="scored">1 - 2</td>
                        <td>+7.05</td>
                        <td><img src="images/icon/win.png"></td>
                    </tr>
                    <tr>
                        <td>Honduras (2.06)</td>
                        <td>0.75</td>
                        <td><span class="team-favored">Equador (1.88)</span></td>
                        <td class="scored">1 - 2</td>
                        <td>+7.05</td>
                        <td><img src="images/icon/win.png"></td>
                    </tr>
                    <tr>
                        <td>Honduras (2.06)</td>
                        <td>0.75</td>
                        <td><span class="team-favored">Equador (1.88)</span></td>
                        <td class="scored">1 - 2</td>
                        <td>+7.05</td>
                        <td><img src="images/icon/win.png"></td>
                    </tr>
                    <tr>
                        <td>Honduras (2.06)</td>
                        <td>0.75</td>
                        <td><span class="team-favored">Equador (1.88)</span></td>
                        <td class="scored">1 - 2</td>
                        <td>+7.05</td>
                        <td><img src="images/icon/win.png"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <!--End-->
</div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
