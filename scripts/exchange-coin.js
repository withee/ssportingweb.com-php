angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope, $scope, $location, $http) {

        $scope.sgoldAmount = 0;
        $scope.exchangeSgoldToDiamond = 0;
        $scope.exchangeSgoldToScoin = 0;
        $scope.exchangeScoinToSgold = 0;

        $scope.exchangeDiamond = function () {
            if ($scope.exchangeSgoldToDiamond / $scope.systemConfig.sgold_to_diamond < 1) {
                alert('SGold ที่ใช้ต้องไม่ต่ำกว่า ' + $scope.systemConfig.sgold_to_diamond);
                return false;
            }

            if ($scope.exchangeSgoldToDiamond % $scope.systemConfig.sgold_to_diamond !== 0) {
                alert('SGold ที่ใช้ต้องหารลงตัว');
                return false;
            }

            var conf = confirm("แน่ใจที่จะแลก SGold เป็น Diamond");

            if (conf) {
                $http({
                    method: 'post',
                    url: $rootScope.apiUrl+'/statement/exchangeDiamond',
                    data: {
                        uid: $scope.userInfo.uid,
                        diamond: ($scope.exchangeSgoldToDiamond / $scope.systemConfig.sgold_to_diamond)
                    }
                })
                    .success(function (response) {
                        $rootScope.userInfo.sgold = response.balance_sgold;
                        $rootScope.userInfo.diamond = response.balance_diamond;
                        $scope.exchangeSgoldToDiamond = 0;
                        alert(response.description);
                    });
            }

        }

        $scope.exchangeSgold2Scoin = function () {
            if ($scope.exchangeSgoldToScoin <= 0) {
                alert('Sgold ที่ใช้ต้องมากกว่า 0');
                return false;
            }

            var conf = confirm("แน่ใจที่จะแลก SGold เป็น Scoin");

            if (conf) {
                $http({
                    method: 'post',
                    url: $rootScope.apiUrl+'/statement/exchange',
                    data: {
                        uid: $scope.userInfo.uid,
                        type: 0,
                        amount: $scope.exchangeSgoldToScoin
                    }
                })
                    .success(function (response) {
                        $rootScope.userInfo.sgold = response.balance_sgold;
                        $rootScope.userInfo.diamond = response.balance_diamond;
                        $scope.exchangeSgoldToScoin = 0;
                        alert(response.description);
                    });
            }
        }

        $scope.exchangeScoin2Sgold = function () {
            if ($scope.exchangeScoinToSgold / $scope.systemConfig.exchange_rate[1][1] < 1) {
                alert('Scoin ที่ใช้ต้องไม่ต่ำกว่า ' + $scope.systemConfig.exchange_rate[1][1]);
                return false;
            }

            if ($scope.exchangeScoinToSgold % $scope.systemConfig.exchange_rate[1][1] !== 0) {
                alert('Scoin ที่ใช้ต้องหารลงตัว');
                return false;
            }

            var conf = confirm("แน่ใจที่จะแลก Scoin เป็น Sgold");

            if (conf) {
                $http({
                    method: 'post',
                    url: $rootScope.apiUrl+'/statement/exchange',
                    data: {
                        uid: $scope.userInfo.uid,
                        type: 1,
                        amount: $scope.exchangeScoinToSgold / $scope.systemConfig.exchange_rate[1][1]
                    }
                })
                    .success(function (response) {
                        $rootScope.userInfo.sgold = response.balance_sgold;
                        $rootScope.userInfo.diamond = response.balance_diamond;
                        $scope.exchangeScoinToSgold = 0;
                        alert(response.description);
                    });
            }
        }

        $scope.exchangeStepBonus = function () {

            var conf = confirm("แน่ใจที่จะแลก Step เป็น Diamond");

            if (conf) {
                $http({
                    method: 'get',
                    url: $rootScope.apiUrl+'/statement/exchangeStepBonus/' + $scope.userInfo.uid,
                    data: {}
                })
                    .success(function (response) {
                        console.log('@', response);
                        $rootScope.userInfo.diamond = response.balance_diamond
                        alert(response.description);
                    });
            }


        }

    });