angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope, $scope, $location, $http, $FB, $upload) {
        $scope.more=0;
        $scope.moreloading=false;
        $scope.fb_uid = document.getElementById('profileScript').getAttribute('fb_uid');
        $scope.q = document.getElementById('profileScript').getAttribute('q');

        if($scope.q === 'ranking') {
            $scope.currentTab = 'ranking';
        } else {
            $scope.currentTab = 'feed';
        }

        $scope.currentRankingTab = 'all';
        $scope.currentPlayTab = 'all';
        $scope.currentPlayDate = moment().format("YYYY-MM-DD");
        $scope.resultFilter = {};
        $scope.playResultDateSelect = '';//moment().add('days', -1).format('YYYY-MM-DD');
        $scope.playResultDate = [
            { date: moment().format('YYYY-MM-DD'), month: moment().format('MMM.YYYY'), day: moment().format('DD') },
            { date: moment().add('days', -1).format('YYYY-MM-DD'), month: moment().add('days', -1).format('MMM.YYYY'), day: moment().add('days', -1).format('DD') },
            { date: moment().add('days', -2).format('YYYY-MM-DD'), month: moment().add('days', -2).format('MMM.YYYY'), day: moment().add('days', -2).format('DD') },
            { date: moment().add('days', -3).format('YYYY-MM-DD'), month: moment().add('days', -3).format('MMM.YYYY'), day: moment().add('days', -3).format('DD') },
            { date: moment().add('days', -4).format('YYYY-MM-DD'), month: moment().add('days', -4).format('MMM.YYYY'), day: moment().add('days', -4).format('DD') }
        ];


        $scope.getInitialData = function () {
            $http({
                method: 'get',
                url: 'api/ws/initProfile?fb_uid=' + $scope.fb_uid
            })
                .success(function(response){
                    $scope.matchFeeds = response.matchcommentlist.data;
                    $scope.plays = response.betlist;
                    $scope.plays_w = _.size(_.where(response, {betResult: 'win'}));
                    $scope.plays_l = _.size(_.where(response, {betResult: 'lose'}));

                    $scope.follower = response.follower;
                    var o = response.following;
                    var uid = [];
                    _.each(response.following, function (val) {
                        if (val.fb_uid) uid.push(val.fb_uid);
                    });
                    uid = uid.join(',');

                    $FB.api({
                        method: 'fql.query',
                        query: 'SELECT uid, name, username, current_location, sex, status, online_presence, profile_blurb, devices, quotes FROM user WHERE uid IN (' + uid + ')'
                    }, function (response) {
                        _.each(o.user, function (val) {
                            val.original = _.findWhere(response, { uid: val.fb_uid });
                        });

                        $scope.following = o;
                    });

                });
        }

        $scope.getAchieve = function () {
            $http({
                method: 'get',
                url: 'api/ws/getAchieve?fb_uid=' + $scope.fb_uid
            })
                .success(function(response){
                    $scope.achieve = response;
                });
        }

        $scope.refreshNewsSlide = function () {
            $http({
                method: 'get',
                url: 'api/ws/dailyNews?lang=' + $scope.language + '&limit=10'
            })
                .success(function (response) {
                    $scope.news = response;

                    var pp = 4;
                    var mm = [];
                    for (var i = 0; i <= response.data.length; i ++) {
                        if (!_.isUndefined(response.data[i]) && !_.isUndefined(response.ontimelines[response.data[i].newsId])) {

                            if(mm.length === 0) mm.push([]);
                            if(mm[mm.length - 1].length >= pp) mm.push([]);
                            mm[mm.length -1].push(response.data[i]);

                        }
                    }

                    var i = 0;
                    if(mm.length > 0) {
                        clearInterval($scope.newsTopSlideInterval);
                        $scope.newsTopSlide = mm[i];
                        angular.element('#news-top-slide-box').fadeIn();

                        $scope.newsTopSlideInterval = setInterval(function () {
                            $scope.$apply(function () {
                                i++;
                                if(_.isUndefined(mm[i])) i = 0;
                                $scope.newsTopSlide = mm[i];

                            });
                        }, 5000);
                    }
                });
        }


        $scope.refreshMatchFeed = function () {

            angular.element('#fetching-data-feed').show();
            angular.element('#fetched-data-feed').hide();

            $http({
                method: 'GET',
                url: 'api/ws/getMatchCommentList?fb_uid=' + $scope.fb_uid+' &limit=10'
            })
                .success(function (response) {
                    console.log('***************matchFeeds******************')
                    $scope.matchFeeds = response.data
                    angular.element('#temp-data-feed').hide();
                    angular.element('#fetching-data-feed').hide();
                    angular.element('#fetched-data-feed').fadeIn();
                });
        }

        $scope.refreshNewFeed = function () {
            $http({
                method: 'GET',
                url: 'api/ws/commentFacebookFriends/' + $scope.fb_uid
            })
                .success(function (response) {
                    clearInterval($rootScope.feedTopSlideInterval);
                    $rootScope.newfeed = response;

                    var t = 0;
                    var m = [];
                    for (var i = t; i <= response.length; i++) {
                        if (!_.isUndefined(response[i])) {
                            t = i;
                            m.push(response[i]);
                            if (m.length >= 4) break;
                        } else {
                            t = 0;
                            break;
                        }
                    }

                    $rootScope.feedTopSlide = m;

                    $rootScope.feedTopSlideInterval = setInterval(function () {
                        $rootScope.$apply(function () {
                            var m = [];
                            for (var i = t + 1; i <= response.length; i++) {
                                if (!_.isUndefined(response[i])) {
                                    t = i;
                                    m.push(response[i]);
                                    if (m.length >= 4) break;
                                } else {
                                    t = 0;
                                    break;
                                }

                                if (t >= response.length) {
                                    t = 0;
                                    break;
                                }
                            }
                            $rootScope.feedTopSlide = m;
                        });
                    }, 5000);

                    angular.element('#feed-top-slide-box').show();
                    angular.element('#news-feed-box').show();

                });
        }

        $scope.$watch('currentTab', function (tab) {
//            if (!$scope.ready) return false;

            switch (tab) {
                case 'feed':
                    $scope.tabInclude = 'views/profile_tabs/feed.html';
                    break;
                case 'following':
                    $scope.tabInclude = 'views/profile_tabs/following.html';
                    break;
                case 'play':
                    $scope.tabInclude = 'views/profile_tabs/play.html';
                    break;
                case 'ranking':
                    $scope.tabInclude = 'views/profile_tabs/ranking.html';
                    break;
            }
        });

        $scope.changePlayResultDate = function (date) {
            if (date == '') {
                $scope.playResultDateSelect = '';
            } else {
                $scope.playResultDateSelect = date;
            }

        }

        $scope.playResultDateFilter = function (item) {
            if ($scope.playResultDateSelect != '') {
                if (moment(item.betDatetime * 1000).format('YYYY-MM-DD') == $scope.playResultDateSelect) {
                    return item;
                } else {
                    return false;
                }
            } else {
                return item;
            }
        }

        $scope.clearResultFilter = function () {
            $scope.resultFilter = {};
        }

        $scope.changeTab = function (tab) {
            $scope.currentTab = tab;
        }



        $scope.redirectToGamePage = function (mid) {
            window.location = '/game.php?mid=' + mid;
        }


        $scope.getCurrentUserFollowing = function (offset){
            if (_.isUndefined(offset)) offset = 0;

            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/following?fb_uid=' + $scope.facebookInfo.id + '&offset=' + offset
            })
                .success(function (response) {
                    var o = response;
                    var uid = [];
                    _.each(response.user, function (val) {
                        if (val.fb_uid) uid.push(val.fb_uid);
                    });
                    uid = uid.join(',');

                    $FB.api({
                        method: 'fql.query',
                        query: 'SELECT uid, name, username, current_location, sex, status, online_presence, profile_blurb, devices, quotes FROM user WHERE uid IN (' + uid + ')'
                    }, function (response) {
                        _.each(o.user, function (val) {
                            val.original = _.findWhere(response, { uid: val.fb_uid });
                        });
                        $scope.currentUserFollowing = o;
                    });
                });
        }


        $scope.getFollowing = function (offset) {
            if (_.isUndefined(offset)) offset = 0;

            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/following?fb_uid=' + $scope.fb_uid + '&offset=' + offset
            })
                .success(function (response) {
                    var o = response;
                    var uid = [];
                    _.each(response.user, function (val) {
                        if (val.fb_uid) uid.push(val.fb_uid);
                    });
                    uid = uid.join(',');

                    $FB.api({
                        method: 'fql.query',
                        query: 'SELECT uid, name, username, current_location, sex, status, online_presence, profile_blurb, devices, quotes FROM user WHERE uid IN (' + uid + ')'
                    }, function (response) {
                        _.each(o.user, function (val) {
                            val.original = _.findWhere(response, { uid: val.fb_uid });
                        });
                        $scope.following = o;
                    });
                });
        }

        $scope.getFollower = function (offset) {
            if (_.isUndefined(offset)) offset = 0;

            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/followers?fb_uid=' + $scope.fb_uid + '&offset=' + offset
            })
                .success(function (response) {
                    $scope.followerCount = response.length;
                    $scope.follower = _.indexBy(response, 'fb_uid');
                    console.log('follower', $scope.follower);
                });
        }

        $scope.follow = function (follow, fb_uid) {

            if (_.isUndefined($rootScope.facebookInfo)) return false;
            if (_.isUndefined($scope.fb_uid) && fb_uid) return false;
            if (_.isUndefined(fb_uid)) fb_uid =  $scope.fb_uid;

            if (follow) {
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/follow?fb_uid=' + $rootScope.facebookInfo.id + '&fb_follow_uid=' + fb_uid + '&checked=Y'
                })
                    .success(function (response) {
                        if (response) {
                            $scope.currentUserFollowing.user[fb_uid] = true;
                        }
                    });
            } else {
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/follow?fb_uid=' + $rootScope.facebookInfo.id + '&fb_follow_uid=' + fb_uid + '&checked=N'
                })
                    .success(function (response) {
                        if (response) {
                            $scope.currentUserFollowing.user[fb_uid] = false;
                        }
                    });
            }

        }

        $scope.addFollowing = function (facebook_id) {
            $scope.follow(facebook_id, true, function (response) {
                $rootScope.following.user[facebook_id] = {};
            });
        }

        $scope.getPlays = function () {
            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/betLists/' + $scope.fb_uid + '?' + new Date().getTime()
            })
                .success(function (response) {
                    $scope.plays = response;
                    $scope.plays_w = _.size(_.where(response, {betResult: 'win'}));
                    $scope.plays_l = _.size(_.where(response, {betResult: 'lose'}));
                });
        }

        if ($scope.fb_uid) {

            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/facebookInfo/' + $scope.fb_uid
            })
                .success(function (response) {
                    $rootScope.localFacebookInfo = response;
                    $rootScope.localFacebookInfo.play = parseInt($rootScope.localFacebookInfo.w) + parseInt($rootScope.localFacebookInfo.d) + parseInt($rootScope.localFacebookInfo.l);
                    $rootScope.localFacebookInfo.pta = (parseInt($rootScope.localFacebookInfo.w) / (parseInt($rootScope.localFacebookInfo.w) + parseInt($rootScope.localFacebookInfo.l))) * 100;
                });

            $scope.refreshMatchFeed();
            $scope.refreshNewFeed();
            $scope.getFollowing();
            $scope.getFollower();
            $scope.getPlays();
            $scope.getAchieve();

            angular.element('#fetching-data').hide();
            angular.element('#fetched-data').show();



        } else {
            $rootScope.$watch('facebookInfo', function (response) {
                if (!$scope.fb_uid && !_.isUndefined(response) && !_.isUndefined(response.id)) {

                    $scope.fb_uid = response.id;

                    $http({
                        method: 'get',
                        url: 'profile-feed.php?id=' + $scope.fb_uid
                    })
                        .success(function(response){
                            angular.element('#temp-data-feed').html(response);
                        });

//                    $scope.getInitialData();

                    $http({
                        method: 'GET',
                        url: $rootScope.apiUrl+'/facebookInfo/' + $scope.fb_uid
                    })
                        .success(function (response) {
                            $rootScope.localFacebookInfo = response;
                            $rootScope.localFacebookInfo.play = parseInt($rootScope.localFacebookInfo.w) + parseInt($rootScope.localFacebookInfo.d) + parseInt($rootScope.localFacebookInfo.l);
                            $rootScope.localFacebookInfo.pta = (parseInt($rootScope.localFacebookInfo.w) / (parseInt($rootScope.localFacebookInfo.w) + parseInt($rootScope.localFacebookInfo.l))) * 100;

                        });

                    $scope.refreshMatchFeed();
                    $scope.refreshNewFeed();
                    $scope.getFollowing();
                    $scope.getFollower();
                    $scope.getPlays();
                    $scope.getAchieve();

                    angular.element('#fetching-data').hide();
                    angular.element('#fetched-data').show();

                }
            });
        }

        $scope.checkIsFollowing = function (id) {
            if ($scope.currentUserFollowing.user[id]) {
                return true;
            }

            return false;
        }



        //region update user info
        $scope.updateDisplayname = function () {
            if ($scope.edited_displayname && $scope.edited_displayname != $scope.localFacebookInfo.display_name) {
                $scope.localFacebookInfo.display_name = $scope.edited_displayname;
                $http({
                    method: 'GET',
//                    url: $rootScope.wsUrl + '/updateUserStatus?fb_uid=' + $rootScope.facebookInfo.id + '&status=' + $scope.edited_displayname
                    url: $rootScope.apiUrl+'/setDisplayName?fb_uid=' + $rootScope.facebookInfo.id + '&name=' + encodeURI($scope.edited_displayname)
                })
                    .success(function (response) {

                    });
            }

            $scope.edited_displayname = false;
        }

        $scope.updateStatus = function () {
            if ($scope.edited_status && $scope.edited_status != $scope.localFacebookInfo.mind) {
                $scope.localFacebookInfo.mind = $scope.edited_status;
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/setMind?fb_uid=' + $rootScope.facebookInfo.id + '&message=' + encodeURI($scope.edited_status)
                })
                    .success(function (response) {

                    });
            }

            $scope.edited_status = false;
        }

        $scope.updateSite = function () {
            if ($scope.edited_site && $scope.edited_site != $scope.localFacebookInfo.site) {
                $scope.localFacebookInfo.site = $scope.edited_site;
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/setSite?fb_uid=' + $rootScope.facebookInfo.id + '&site=' + encodeURI($scope.edited_site)
                })
                    .success(function (response) {

                    });
            }

            $scope.edited_site = false;
        }
        //endregion

        //region upload
        $scope.uploadCover = function () {
            angular.element('#upload-browse').trigger('click');
        }

        $scope.updateCover = function ($files) {
            var file = $files[0];
            $scope.upload = $upload.upload({
                url: 'api/upload/cover',
                method: 'POST',
                data: {
                    fb_uid: $rootScope.facebookInfo.id
                },
                file: file
            }).progress(function (evt) {
//                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).success(function (data, status, headers, config) {
                $scope.localFacebookInfo.cover = data.path;
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/setCover?fb_uid=' + $rootScope.facebookInfo.id + '&picture=' + data.path
                })
                    .success(function (response) {

                    });

            });
        }
        //endregion

        $scope.$watch('facebookInfo', function(newVal){
            if(!_.isUndefined(newVal) && !_.isUndefined(newVal.id)) {
                $scope.getCurrentUserFollowing();
            }

        });

        $scope.refreshNewsSlide();

        $scope.MoreProfileView = function () {
            console.log('More');
            $scope.more=$scope.more+1;
            var limit=10,scrollTop=0;
            if($scope.more!=0){
                limit=limit+($scope.more*10);
                //scrollTop=$(window).scrollTop();
            }
            $scope.moreloading=true;
            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/getMatchCommentList?fb_uid=' + $scope.fb_uid + '&limit=10 &offset='+(limit-10)
                //url: 'http://api.ssporting.com/getTimelines?fb_uid=' + $scope.fb_uid + '&limit=40' + more
            })
                .success(function (response) {
                    if($scope.more==1){
                        $scope.matchFeeds1 = response;
                    }else if($scope.more==2){
                        $scope.matchFeeds2 = response;
                    }else if($scope.more==3){
                        $scope.matchFeeds3 = response;
                    }else if($scope.more==4){
                        $scope.matchFeeds4 = response;
                    }else if($scope.more==5){
                        $scope.matchFeeds5 = response;
                    }
                    $scope.moreloading=false;
                });
        }

    });