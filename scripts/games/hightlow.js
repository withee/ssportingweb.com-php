angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope, $scope, $http, $interval, $timeout) {
        var gameType = 'hi_low'
            , rMap = {
                'r0': '1',
                'r1': '2',
                'r2': '3',
                'r3': '4',
                'r4': '5',
                'r5': '6'
            };
        $scope.prevent = false;
        $scope.stateTab = 'user';
        $scope.sgold = 200;
        $scope.choose = false;
        $scope.chooseC = '1';
        $scope.chooseL = '1';
        $scope.chooseR = '1';
        $scope.totalPoint = 3;

        var sc = _.random(0, 5);
        var sl = _.random(0, 5);
        var sr = _.random(0, 5);

        $scope.close = function (){
            angular.element('.Modal-resultGame').fadeOut();
        }

        $scope.play = function (choose) {
            if ($scope.prevent) return false;

            if($scope.sgold > $scope.userInfo.scoin) {
                alert('เครดิตไม่พอในการเล่น!');
                return false;
            }

            $scope.prevent = true;
            $scope.result = false;
            $scope.choose = choose;

            var cc = $interval(function () {
                $scope.chooseC = rMap['r' + sc];
                sc = _.random(0, 5);
            }, 300);

            var cl = $interval(function () {
                $scope.chooseL = rMap['r' + sl];
                sl = _.random(0, 5);
            }, 200);

            var cr = $interval(function () {
                $scope.chooseR = rMap['r' + sr];
                sr = _.random(0, 5);
            }, 300);

            $http({
                method: 'post',
                url: 'api/post/mini-games/play',
                data: {
                    uid: $scope.userInfo.uid,
                    game_type: gameType,
                    scoin: $scope.sgold,
                    player_play: choose
                },
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                console.log('res', response);

                $timeout(function () {
                    $interval.cancel(cc);
                    $interval.cancel(cl);
                    $interval.cancel(cr);

                    if (response.result === 'win' || response.result === 'draw' || response.result === 'lose') {
                        var com_play = response.player2["com_play"];
                        $scope.chooseC = com_play[1];
                        $scope.chooseL = com_play[0];
                        $scope.chooseR = com_play[2];
                        $scope.result = response.result;
                        $scope.totalPoint = response.player2["sum"];

                        $scope.resultScoin = parseInt(response.scoin_result_amount);
                        if($scope.resultScoin < 0){
                            $scope.resultScoin*=-1;
                        }

                        if(response.result === 'win'){
                            angular.element('#winModal').fadeIn();
                        }else{
                            angular.element('#loseModal').fadeIn();
                        }

                        $http({
                            method: 'get',
                            url: 'api/ws/updateUserfile/'+$scope.userInfo.fb_uid,
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).success(function (response) {
                            console.log('update fb info res', response);
                        });
                    } else {
                        $scope.chooseC = '1';
                        $scope.chooseL = '1';
                        $scope.chooseR = '1';
                        alert(response.result);
                    }

                    $scope.userInfo.sgold = response.balance_sgold;
                    $scope.userInfo.scoin = response.balance_scoin;
                    $scope.prevent = false;
                }, 2000);

            });

        }

        $scope.getStatement = function () {
            $http({
                method: 'get',
                url: 'api/ws/mini-games/mini_game_statement?uid=' + $scope.userInfo.uid + '&game_type=' + gameType + '&timestamp=' + new Date().getTime()
            })
                .success(function (response) {
                    $scope.statement = response;

                    console.log('statement', response)
                });
        }


        $scope.$watchCollection('userInfo', function (newVal, oldVal) {
            if (!_.isUndefined(newVal)) {
                $scope.getStatement();
            }
        });



    });
