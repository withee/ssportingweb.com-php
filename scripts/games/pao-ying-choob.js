angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope, $scope, $http, $interval, $timeout) {
        var gameType = 'rock_paper_scissors'
            , rMap = {
                'r0': 's',
                'r1': 'r',
                'r2': 'p'
            };
        $scope.prevent = false;
        $scope.stateTab = 'user';
        $scope.scoin = 200;
        $scope.choose = false;
        $scope.chooseL = 'd';
        $scope.chooseR = 'd';


        var sl = _.random(0, 2);
        var sr = _.random(0, 2);

        $scope.close = function (){
            angular.element('.Modal-resultGame').fadeOut();
        }

        $scope.play = function (choose) {
            if ($scope.prevent) return false;

            if($scope.scoin > $scope.userInfo.scoin) {
                alert('เครดิตไม่พอในการเล่น!');
                return false;
            }

            $scope.prevent = true;
            $scope.result = false;
            $scope.choose = choose;

            $scope.chooseL = choose;
//            var cl = $interval(function () {
//                $scope.chooseL = rMap['r' + sl];
//                sl++;
//                if (sl >= 3) sl = 0;
//            }, 200);

            var cr = $interval(function () {
                $scope.chooseR = rMap['r' + sr];
                sr++;
                if (sr >= 3) sr = 0;
            }, 300);

            $http({
                method: 'post',
                url: 'api/post/mini-games/play',
                data: {
                    uid: $scope.userInfo.uid,
                    game_type: gameType,
                    scoin: $scope.scoin,
                    player_play: choose
                },
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
                .success(function (response) {
                    console.log(response);

                    $timeout(function () {
                        //$interval.cancel(cl);
                        $interval.cancel(cr);

                        if (response.result === 'win' || response.result === 'draw' || response.result === 'lose') {
                            $scope.chooseL = response.player1;
                            $scope.chooseR = response.player2;
                            //$scope.result = response.result;

                            if(response.result === 'win'){
                                angular.element('#winModal').fadeIn();
                            }else if(response.result === 'lose'){
                                angular.element('#loseModal').fadeIn();
                            }else{
                                angular.element('#drawModal').fadeIn();
                            }

                            $http({
                                method: 'get',
                                url: 'api/ws/updateUserfile/'+$scope.userInfo.fb_uid,
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).success(function (response) {
                                console.log('update fb info res', response);
                            });
                        } else {
                            $scope.chooseL = 'd';
                            $scope.chooseR = 'd';
                            alert(response.result);
                        }

                        $scope.userInfo.sgold = response.balance_sgold;
                        $scope.userInfo.scoin = response.balance_scoin;
                        $scope.prevent = false;
                    }, 2000);

                });
        }

        $scope.getStatement = function () {
            $http({
                method: 'get',
                url: 'api/ws/mini-games/mini_game_statement?uid=' + $scope.userInfo.uid + '&game_type=' + gameType + '&timestamp=' + new Date().getTime()
            })
                .success(function (response) {
                    $scope.statement = response;

                    console.log('statement', response)
                });
        }


        $scope.$watchCollection('userInfo', function (newVal, oldVal) {
            if (!_.isUndefined(newVal)) {
                $scope.getStatement();
            }
        });
    });
