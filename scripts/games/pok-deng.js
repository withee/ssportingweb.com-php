angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope, $scope, $http, $interval, $timeout) {
        var gameType = 'pok_deng';
        $scope.prevent = false;
        $scope.stateTab = 'user';
        $scope.scoin = 200;
        $scope.player1Cards = [{"face":"b","num":"b"},{"face":"b","num":"b"},{"face":"b","num":"b"}];
        $scope.player2Cards = [{"face":"b","num":"b"},{"face":"b","num":"b"},{"face":"b","num":"b"}];
        $scope.player1Score = "";
        $scope.player2Score = "";

        $scope.close = function (){
            angular.element('.Modal-resultGame').fadeOut();
        }

        $scope.play = function () {
            if ($scope.prevent) return false;

            if($scope.scoin > $scope.userInfo.scoin) {
                alert('เครดิตไม่พอในการเล่น!');
                return false;
            }

            $scope.prevent = true;

            $http({
                method: 'post',
                url: 'api/post/mini-games/play',
                data: {
                    uid: $scope.userInfo.uid,
                    game_type: gameType,
                    scoin: $scope.scoin
                },
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
                .success(function (response) {
                    $timeout(function () {
                        if (response.result === 'win' || response.result === 'draw' || response.result === 'lose') {
                            console.log('res',response);
                            var player1 = angular.fromJson(response.player1);
                            var player2 = angular.fromJson(response.player2);
                            $scope.player1Cards = player1.player1Cards;
                            $scope.player2Cards = player2.player2Cards;
                            $scope.player1Score = parseScore(player1.player1Score.num, player1.player1Score.special, player1.player1Score.mul);
                            $scope.player2Score = parseScore(player2.player2Score.num, player2.player2Score.special, player2.player2Score.mul);

                            $scope.resultScoin = parseInt(response.scoin_result_amount);
                            if($scope.resultScoin < 0){
                                $scope.resultScoin*=-1;
                            }

                            if(response.result === 'win'){
                                angular.element('#winModal').fadeIn();
                            }else if(response.result === 'lose'){
                                angular.element('#loseModal').fadeIn();
                            }else{
                                angular.element('#drawModal').fadeIn();
                            }

                            $http({
                                method: 'get',
                                url: 'api/ws/updateUserfile/'+$scope.userInfo.fb_uid,
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).success(function (response) {
                                console.log('update fb info res', response);
                            });
                        } else {
                            $scope.player1Cards = [{"face":"b","num":"b"},{"face":"b","num":"b"},{"face":"b","num":"b"}];
                            $scope.player2Cards = [{"face":"b","num":"b"},{"face":"b","num":"b"},{"face":"b","num":"b"}];
                            $scope.player1Score = "";
                            $scope.player2Score = "";
                            alert(response.result);
                        }

                        $scope.userInfo.sgold = response.balance_sgold;
                        $scope.userInfo.scoin = response.balance_scoin;
                        $scope.prevent = false;
                    }, 1000);

                });
        }

        $scope.getStatement = function () {
            $http({
                method: 'get',
                url: 'api/ws/mini-games/mini_game_statement?uid=' + $scope.userInfo.uid + '&game_type=' + gameType + '&timestamp=' + new Date().getTime()
            })
                .success(function (response) {
                    $scope.statement = response;

                    console.log('statement', response)
                });
        }


        $scope.$watchCollection('userInfo', function (newVal, oldVal) {
            if (!_.isUndefined(newVal)) {
                $scope.getStatement();
            }
        })

        function parseScore(num, special, mul){
            var scoreStr = "";
            if(special != "none"){//special
                if(special == "deng3"){
                    if(num == 0){
                        scoreStr = "บอด "+rMap['deng3'];
                    }else{
                        scoreStr = num+ " แต้ม "+rMap['deng3'];
                    }
                }else if(special == "deng2"){
                    if(num == 0){
                        scoreStr = "บอด "+rMap['deng2'];
                    }else{
                        scoreStr = num+ " แต้ม "+rMap['deng2'];
                    }
                }else{
                    scoreStr = rMap[special];
                    if(mul == 2){
                        scoreStr += " "+rMap['deng2'];
                    }
                }
            }else{//regular
                if(num == 0){
                    scoreStr = "บอด";
                }else{
                    scoreStr = num+ " แต้ม";
                }

                if(mul == 2){
                    scoreStr += " "+rMap['deng2'];
                }
            }
            return scoreStr;
        }
    });

var rMap = {
    'pok9': 'ป๊อก 9',
    'pok8': 'ป๊อก 8',
    'deng2': 'สองเด้ง',
    'deng3': 'สามเด้ง',
    'yellow3': 'สามเหลือง',
    'tong3': 'ตอง',
    'riang': 'เรียง',
    'straightFlush': 'เรียง+สี'
};
