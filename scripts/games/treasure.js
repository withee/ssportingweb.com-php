angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope, $scope, $http, $interval, $timeout) {
        var gameType = 'treasure_medium'
            , rMap = {
                'treasure_medium': 250,
                'treasure_high': 500,
                'treasure_low': 100
            };
        $scope.prevent = false;
        $scope.choose = false;
        $scope.stateTab = 'user';
        $scope.sgold = rMap[gameType];

        $scope.close = function (){
            angular.element('#rewardModal').fadeOut();
        }

        $scope.play = function (choose) {
            if ($scope.prevent) return false;

            console.log('curGold', $scope.userInfo.sgold);
            console.log('playGold', $scope.sgold);

            $scope.choose = choose;
            gameType = choose;
            $scope.sgold = rMap[gameType];

            if($scope.sgold > $scope.userInfo.scoin) {
                alert('เครดิตไม่พอในการเล่น!');
                return false;
            }

            $scope.prevent = true;

            var data ={};
            data['uid'] = $scope.userInfo.uid;
            data['game_type'] = gameType;
            data['scoin'] = $scope.sgold;
            console.log(data);

            $http({
                method: 'post',
                url: 'api/post/mini-games/play',
                data: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                console.log('res', response);

                $timeout(function () {

                    if (response.result == 'game_limit'){
                        alert("หีบสมบัติในแต่ละระดับสามารถเปิดได้ 10 ครั้งต่อ 1 วัน");
                    }else if (response.result === 'win' || response.result === 'draw' || response.result === 'lose') {

                        if(response.scoin_result_amount != 0){
                            $scope.reward = response.scoin_result_amount+" Scoin";
                        }else{
                            $scope.reward = response.sgold_result_amount+" Sgold";
                        }
                        angular.element('#rewardModal').fadeIn();

                        $http({
                            method: 'get',
                            url: 'api/ws/updateUserfile/' + $scope.userInfo.fb_uid,
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).success(function (response) {
                            console.log('update fb info res', response);
                        });
                    } else {
                        alert(response.result);
                    }

                    $scope.userInfo.sgold = response.balance_sgold;
                    $scope.userInfo.scoin = response.balance_scoin;
                    $scope.prevent = false;
                }, 500);

            });

        }

        $scope.getStatement = function () {
            $http({
                method: 'get',
                url: 'api/ws/mini-games/mini_game_statement?uid=' + $scope.userInfo.uid + '&game_type=treasure&timestamp=' + new Date().getTime()
            })
                .success(function (response) {
                    $scope.statement = response;

                    console.log('statement', response)
                });
        }


        $scope.$watchCollection('userInfo', function (newVal, oldVal) {
            if (!_.isUndefined(newVal)) {
                $scope.getStatement();
            }
        });



    });
