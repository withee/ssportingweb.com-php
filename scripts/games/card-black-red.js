angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope, $scope, $http, $interval, $timeout) {
        var gameType = 'red_black';
        $scope.prevent = false;
        $scope.stateTab = 'user';
        $scope.scoin = 200;
        $scope.choose = false;

        $scope.close = function (){
            angular.element('.Modal-resultGame').fadeOut();
        }

        $scope.play = function (choose) {
            if ($scope.prevent) return false;

            if($scope.scoin > $scope.userInfo.scoin) {
                alert('เครดิตไม่พอในการเล่น!');
                return false;
            }

            $scope.cardResult = false;
            $scope.prevent = true;
            $scope.result = false;
            $scope.choose = choose;

            $http({
                method: 'post',
                url: 'api/post/mini-games/play',
                data: {
                    uid: $scope.userInfo.uid,
                    game_type: gameType,
                    scoin: $scope.scoin,
                    player_play: choose
                },
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
                .success(function (response) {
                    console.log(response);

                    $timeout(function () {

                        if (response.result === 'win' || response.result === 'draw' || response.result === 'lose') {
                            $scope.result = response.result;
                            //$scope.cardResult = response.player2;

                            if(response.result === 'win'){
                                angular.element('#winModal').fadeIn();
                            }else{
                                angular.element('#loseModal').fadeIn();
                            }

                            $http({
                                method: 'get',
                                url: 'api/ws/updateUserfile/'+$scope.userInfo.fb_uid,
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).success(function (response) {
                                console.log('update fb info res', response);
                            });
                        } else {
                            alert(response.result);
                        }

                        $scope.userInfo.sgold = response.balance_sgold;
                        $scope.userInfo.scoin = response.balance_scoin;
                        $scope.prevent = false;
                    }, 1000);

                });
        }

        $scope.getStatement = function () {
            $http({
                method: 'get',
                url: 'api/ws/mini-games/mini_game_statement?uid=' + $scope.userInfo.uid + '&game_type=' + gameType + '&timestamp=' + new Date().getTime()
            })
                .success(function (response) {
                    $scope.statement = response;

                    console.log('statement', response)
                });
        }


        $scope.$watchCollection('userInfo', function (newVal, oldVal) {
            if (!_.isUndefined(newVal)) {
                $scope.getStatement();
            }
        });
    });
