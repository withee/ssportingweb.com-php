angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope, $scope, $http) {
        $scope.stateTab = 'all';

        $scope.getStatement = function () {
            $http({
                method: 'get',
                url: 'api/ws/mini-games/mini_game_statement?uid=' + $scope.userInfo.uid + '&timestamp=' + new Date().getTime()
            })
                .success(function (response) {
                    $scope.statement = response;

                    console.log('statement', response)
                });
        }


        $scope.$watchCollection('userInfo', function (newVal, oldVal) {
            if (!_.isUndefined(newVal)) {
                $scope.getStatement();
            }
        });
    });