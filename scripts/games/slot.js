angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope, $scope, $http, $interval, $timeout) {
        var gameType = 'calabash_crap_fish'
            , rMap = {
                'r0': 'ca',
                'r1': 'cr',
                'r2': 'f',
                'r3': 't',
                'r4': 's',
                'r5': 'ch'
            };
        $scope.prevent = false;
        $scope.stateTab = 'user';
        $scope.sgold = 200;
        $scope.choose = false;
        $scope.slotL = rMap['r' + 0];
        $scope.slotM = rMap['r' + 0];
        $scope.slotR = rMap['r' + 0];

        var sl = _.random(0, 5);
        var sm = _.random(0, 5);
        var sr = _.random(0, 5);

        $scope.close = function (){
            angular.element('.Modal-resultGame').fadeOut();
        }

        $scope.play = function (choose) {

            if ($scope.prevent) return false;

            if($scope.sgold > $scope.userInfo.scoin) {
                alert('เครดิตไม่พอในการเล่น!');
                return false;
            }

            $scope.prevent = true;
            $scope.result = false;
            $scope.choose = choose;

            var cl = $interval(function () {
                $scope.slotL = rMap['r' + sl];
                sl = (sl+1)%6;
            }, 200);

            var cm = $interval(function () {
                $scope.slotM = rMap['r' + sm];
                sm = (sm+1)%6;
            }, 200);

            var cr = $interval(function () {
                $scope.slotR = rMap['r' + sr];
                sr = (sr+1)%6;
            }, 200);

            $http({
                method: 'post',
                url: 'api/post/mini-games/play',
                data: {
                    uid: $scope.userInfo.uid,
                    game_type: gameType,
                    scoin: $scope.sgold,
                    player_play: choose
                },
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                console.log('res', response);

                $timeout(function () {

                    if (response.result === 'win' || response.result === 'draw' || response.result === 'lose') {

                        $interval.cancel(cl);
                        $scope.slotL = response.player2[0];

                        $timeout(function(){
                            $interval.cancel(cm);
                            $scope.slotM = response.player2[1];
                        },1000);

                        $timeout(function(){
                            $interval.cancel(cr);
                            $scope.slotR = response.player2[2];

                            $scope.result = response.result;
                            $scope.reward = response.sgold_result_amount;

                            if(response.result === 'win'){
                                angular.element('#winModal').fadeIn();
                            }else if(response.result === 'lose'){
                                angular.element('#loseModal').fadeIn();
                            }else{
                                angular.element('#drawModal').fadeIn();
                            }

                        },2000);

                        $http({
                            method: 'get',
                            url: 'api/ws/updateUserfile/'+$scope.userInfo.fb_uid,
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).success(function (response) {
                            console.log('update fb info res', response);
                        });
                    } else {
                        $interval.cancel(cl);
                        $interval.cancel(cm);
                        $interval.cancel(cr);
                        $scope.slotL = rMap['r' + 0];
                        $scope.slotM = rMap['r' + 0];
                        $scope.slotR = rMap['r' + 0];
                        alert(response.result);
                    }

                    $scope.userInfo.sgold = response.balance_sgold;
                    $scope.userInfo.scoin = response.balance_scoin;
                    $scope.prevent = false;
                }, 2000);

            });

        }

        $scope.getStatement = function () {
            $http({
                method: 'get',
                url: 'api/ws/mini-games/mini_game_statement?uid=' + $scope.userInfo.uid + '&game_type=' + gameType + '&timestamp=' + new Date().getTime()
            })
                .success(function (response) {
                    $scope.statement = response;

                    console.log('statement', response)
                });
        }


        $scope.$watchCollection('userInfo', function (newVal, oldVal) {
            if (!_.isUndefined(newVal)) {
                $scope.getStatement();
            }
        });



    });
