angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope,$scope, $location, $http) {

        $scope.pmId = document.getElementById('mainScript').getAttribute('data-id');

        $scope.currentBox = 'inbox';

        $scope.getInbox = function () {
            $http({
                method: 'get',
                url: 'api/ws/getInbox?receiver=' + $scope.userInfo.uid
            })
                .success(function (response) {
                    $scope.inbox = response
                });
        }

        $scope.getSendbox = function () {
            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/getSendbox?sender=' + $scope.userInfo.uid
            })
                .success(function (response) {
                    $scope.sendbox = response
                });
        }

        $scope.setCurrentView = function (item) {
            $scope.currentView = item;
        }


        $scope.sendPm = function (pmMessage, receiver) {
            if (pmMessage.length === 0) return false;

            $http({
                method: 'post',
                url: 'api/post/sendMsg',
                data: {
                    sender: $scope.userInfo.uid,
                    receiver: receiver,
                    message: pmMessage
                }
            })
                .success(function (response) {
                    $scope.getSendbox();
                    if (response.success) {
                        alert('ส่งข้อความเรียบร้อยแล้ว');
                    }

                });
        }

        $scope.$watch('userInfo', function (newVal, oldVal) {
            if (!_.isUndefined(newVal)) {
                $scope.getInbox();
                $scope.getSendbox();
            }
        });

        $scope.$watchCollection('inbox', function (newVal, oldVal) {
            if (!_.isUndefined(newVal)) {
                $scope.setCurrentView(_.findWhere(newVal.messagelist, {id: $scope.pmId}));
            }
        });


    });