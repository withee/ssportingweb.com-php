angular.module('scorspot.controller', [])
    .run(function ($rootScope, $http, $cookieStore, $FB, $location, $interval) {

        //region global
        $rootScope.language = document.getElementById('app-config').getAttribute('language');
        $rootScope.gmtOffset = document.getElementById('app-config').getAttribute('gmtOffset');

        $rootScope.highlightCompetitionOnly = true;
        $rootScope.competitionToggleStatus = {};
        $rootScope.matchNotifications = {};
        $rootScope.logoUrl = 'http://beta.ssporting.com/uploads/media/teams_clean';

        $rootScope.uuid = $cookieStore.get('uuid');

        if (!$rootScope.uuid) {
            $rootScope.uuid = createUUID();
            $cookieStore.put('uuid', $rootScope.uuid);
        }

        $rootScope.login = function () {
            $FB.login(function (response) {
                $rootScope.loginStatus = response;
            }, {scope: 'email,user_friends,user_about_me,user_activities,user_birthday,user_groups,user_interests,user_likes,friends_about_me,friends_activities,friends_birthday,friends_likes,user_online_presence,friends_online_presence,publish_actions,publish_stream,publish_actions'});
        }

        $FB.getLoginStatus(function (response) {
            console.log('FB', response);
            $rootScope.loginStatus = response;

            if (response.status === 'connected') {
                angular.element('.wait-for-facebook-session').fadeIn();
                $rootScope.facebookAccessToken = response.authResponse.accessToken;

                $FB.api('/me/permissions', function (response) {
                    console.log('permissions', response.data);
                    if (_.isUndefined(response.data[0].publish_stream)) {

                        $FB.ui({
                            method: 'permissions.request',
                            perms: 'publish_stream',
                            display: 'popup'
                        }, function (response) {
                            console.log('req perms', response);
                        });

                    }
                });
            }
        });

        $rootScope.getLiveMatchWaitOnlySorted = function () {
            $http({
                method: 'get',
                url: 'api/ws/sortedliveMatchWaitOnly/' + $rootScope.language
            })
                .success(function (response) {
                    if (response.length === 0) {
                        console.log('wait empty!');
                        return;
                    }

                    $rootScope.live_match_wait_sorted = response;
                    $rootScope.matchOddsCount = 0;
                    angular.forEach(response.live_league, function (val, key) {
                        $rootScope.matchOddsCount += val.playable;
//                        angular.forEach(val.matches, function (v, k) {
//                            if ($rootScope.matchNotifications[v.mid]) {
//                                val.hasNotification = true;
//                                return;
//                            }
//
//                        });
                    });
                    angular.element('#temp-live-match-wait').remove();
                    angular.element('#live_match_wait_table').show();
                });
        }

        $rootScope.getMyBet = function () {
            $http({
                method: 'GET',
                url: 'api/ws/betLists/' + $rootScope.facebookInfo.id + '?' + new Date().getTime()
            })
                .success(function (response) {
                    $rootScope.myBet = _.indexBy(response, 'mid');
                });
        }

        $rootScope.getLastEvents = function () {
            $http({
                method: 'GET',
                url: 'api/ws/getLastEvent'
            })
                .success(function (response) {
                    if (response.list) {
                        $rootScope.lastEvents = response.list.reverse();
                        angular.element('#latest-event-box').show();
                    }
                });
        }

        $rootScope.toggleCompetition = function (competitionId) {
            if (_.isUndefined($rootScope.competitionToggleStatus[competitionId])) {
                $rootScope.competitionToggleStatus[competitionId] = true;
            } else {
                delete $rootScope.competitionToggleStatus[competitionId];
            }
        }

        //region odds
        $http({
            method: 'GET',
            url: 'api/ws/oddsToday'
        })
            .success(function (response) {
                $rootScope.odds = response;
                angular.element('#bet-list-box').show();
            });

        $rootScope.getOddsHdp = function (key, need) {
            if (!_.isUndefined($rootScope.odds.data[key])) {
                if (!_.isUndefined($rootScope.odds.data[key].SBOBET) && !_.isUndefined($rootScope.odds.data[key].SBOBET[need]) && $rootScope.odds.data[key].SBOBET[need] != '') return $rootScope.odds.data[key].SBOBET[need];
                else if (!_.isUndefined($rootScope.odds.data[key].Bet365) && !_.isUndefined($rootScope.odds.data[key].Bet365[need]) && $rootScope.odds.data[key].Bet365[need] != '') return $rootScope.odds.data[key].Bet365[need];
                else if (!_.isUndefined($rootScope.odds.data[key].Ladbrokes) && !_.isUndefined($rootScope.odds.data[key].Ladbrokes[need]) && $rootScope.odds.data[key].Ladbrokes[need] != '') return $rootScope.odds.data[key].Ladbrokes[need];
            }

            return false;
        }

        //endregion

        //region favorite

        $rootScope.favorite = function (type, obj) {
            if (type == 'league') {
                if (!_.isUndefined($rootScope.favoriteLeague[obj.leagueId])) {
                    delete $rootScope.favoriteLeague[obj.leagueId];
                    var url = 'api/ws/unfavorite?id=' + obj.leagueId + '&device_id=' + $rootScope.uuid + '&platform=web&favorite_type=league&competition_id=' + obj.competitionId + '&league_id=0';
                } else {
                    $rootScope.favoriteLeague[obj.leagueId] = {
                        id: obj.leagueId,
                        competition_id: obj.competitionId,
                        name: obj.leagueName
                    };
                    var url = 'api/ws/favorite?id=' + obj.leagueId + '&device_id=' + $rootScope.uuid + '&platform=web&favorite_type=league&competition_id=' + obj.competitionId + '&league_id=0';
                }
            } else {
                if (_.isUndefined(obj.leagueId)) obj.leagueId = 0;
                if (!_.isUndefined($rootScope.favoriteTeam[obj.tid])) {
                    delete $rootScope.favoriteTeam[obj.tid];
                    var url = 'api/ws/unfavorite?id=' + obj.tid + '&device_id=' + $rootScope.uuid + '&platform=web&favorite_type=team&competition_id=0&league_id=' + obj.leagueId;
                } else {
                    $rootScope.favoriteTeam[obj.tid] = {
                        id: obj.tid,
                        name: obj.tn
                    };
                    var url = 'api/ws/favorite?id=' + obj.tid + '&device_id=' + $rootScope.uuid + '&platform=web&favorite_type=team&competition_id=0&league_id=' + obj.leagueId;
                }
            }
            $http({
                method: 'GET',
                url: url
            })
                .success(function (response) {
                });
        }
        //endregion

        //region notification


        $rootScope.matchNotification = function (match, remove) {
            if (_.isUndefined(remove)) remove = false;
            if (!remove) {
                $rootScope.matchNotifications[match.mid] = true;
                $http({
                    method: 'GET',
                    url: 'api/ws/services/RequestNotification?apple_id=' + $rootScope.uuid + '&match_id=' + match.mid + '&hid=' + match.hid + '&gid=' + match.gid + '&app=ballscore&status=Y'
                })
                    .success(function (response) {
                        console.log('qqq', $rootScope.live_match_sorted);
                        if (!_.isUndefined($rootScope.live_match_sorted.live_league[match._lid]))
                            $rootScope.live_match_sorted.live_league[match._lid].hasNotification = true;
                        if (!_.isUndefined($rootScope.live_match_wait_sorted.live_league[match._lid]))
                            $rootScope.live_match_wait_sorted.live_league[match._lid].hasNotification = true;
                    });
            } else {
                delete $rootScope.matchNotifications[match.mid];
                $http({
                    method: 'GET',
                    url: 'api/ws/services/RequestNotification?apple_id=' + $rootScope.uuid + '&match_id=' + match.mid + '&hid=' + match.hid + '&gid=' + match.gid + '&app=ballscore&status=N'
                })
                    .success(function (response) {
                    });
            }

            $rootScope.matchNotificationsCount = _.size($rootScope.matchNotifications);

        }
        //endregion

        $rootScope.$watch('loginStatus', function (response) {
            if (!_.isUndefined(response) && response.status == 'connected') {
                var uid = response.authResponse.userID;
                $FB.api('/me', function (response) {
                    $rootScope.facebookInfo = response;
                    angular.element('#facebook-info-box-temp').hide();
                    angular.element('#facebook-info-box').show();
                    angular.element('.wait-for-facebook-session').show();
                    $rootScope.uuid = $rootScope.facebookInfo.id;
                    saveFacebookInfo();
                    $rootScope.getMyBet();

                    $http({
                        method: 'GET',
                        url: 'settings.php?item=saveFacebookId&facebookId=' + $rootScope.facebookInfo.id + '-' + $rootScope.facebookInfo.name
                    });

                    $http({
                        method: 'GET',
                        url: 'api/ws/services/NotificationList?apple_id=' + $rootScope.uuid + '&app=ballscore'
                    })
                        .success(function (response) {

                            $rootScope.matchNotifications = _.indexBy(response.result, 'match_id');
                            $rootScope.matchNotificationsCount = _.size($rootScope.matchNotifications);
                        });

                    $http({
                        method: 'GET',
                        url: 'api/ws/favoriteTeam/' + $rootScope.language + '/web/' + $rootScope.uuid
                    })
                        .success(function (response) {
                            $rootScope.favoriteTeam = _.indexBy(response, 'id');
                            angular.element('#favorite-team-list-box').show();
                            angular.element('#favorite-list-box').show();
                        });

                    $http({
                        method: 'GET',
                        url: 'api/ws/favoriteLeague/' + $rootScope.language + '/web/' + $rootScope.uuid
                    })
                        .success(function (response) {
                            $rootScope.favoriteLeague = _.indexBy(response, 'id');
                            angular.element('#favorite-league-list-box').show();
                            angular.element('#favorite-list-box').show();
                        });
                });

                $http({
                    method: 'GET',
                    url: 'api/ws/getRanking?v=4&fb_uid=' + response.authResponse.userID
                })
                    .success(function (response) {
                        $rootScope.topRanking = response.ranks;

                        angular.element('#ranking-wrapper').show();
                    });

                $http({
                    method: 'GET',
                    url: 'api/ws/worldcupsRanking?fb_uid=' + response.authResponse.userID
                })
                    .success(function (response) {
                        $rootScope.w14ranking = response.ranks;
                        $rootScope.w14ownRanking = response.own;

                        angular.element('#w14ranking').show();
                        angular.element('#w14ranking-guest').hide();
                        angular.element('#w14ranking-member').show();
                    });


                $rootScope.notificationsBox = false;
                $rootScope.toggleNotifications = function () {

                    if(angular.element('#notifications-box').is(':not(:visible)')) {
                        angular.element('#notifications-box').fadeIn();
                        $http({
                            url: 'api/ws/notiSeen/' + response.authResponse.userID
                        })
                            .success(function(){
                                $rootScope.notifications.unseen = 0;
                            });
                    } else {
                        angular.element('#notifications-box').fadeOut();
                    }

//                    $rootScope.notificationsBox = $rootScope.notificationsBox ? false : true;
//                    if ($rootScope.notificationsBox) {
//                        angular.element('#notifications-box').fadeIn();
//                        $http({
//                            url: 'api/ws/notiSeen/' + response.authResponse.userID
//                        })
//                            .success(function(){
//                                $rootScope.notifications.unseen = 0;
//                            });
//                    } else {
//                        angular.element('#notifications-box').fadeOut();
//                    }
                }

                $http({
                    url: 'api/ws/getNotifications/' + response.authResponse.userID
                })
                    .success(function (response) {
                        $rootScope.notifications = response.all;
                        $rootScope.notificationsPost = response.post;
                        $rootScope.notificationsGame = response.game;
                        $rootScope.notificationsFollow = response.follow;
                        $rootScope.notificationsM = [];
                        _.each(response.all.list, function (v) {
                            $rootScope.notificationsM = _.union($rootScope.notificationsM, v);
                        });

                        angular.element('#notifications-button').show();


                    });

                $rootScope.markAsSeen = function (rfb_uid, noti_id) {
                    $http({
                        url: 'api/ws/notiSeen/' + rfb_uid + '/' + noti_id
                    })
                        .success(function (response) {
//                            angular.element('[data-noti-id="' + noti_id + '"]').fadeOut();
                            $rootScope.notifications = response.all;
                            $rootScope.notificationsPost = response.post;
                            $rootScope.notificationsGame = response.game;
                            $rootScope.notificationsFollow = response.follow;
                            $rootScope.notificationsM = [];
                            _.each(response.all.list, function (v) {
                                $rootScope.notificationsM = _.union($rootScope.notificationsM, v);
                            });
                        });
                }

                $rootScope.removeNoti = function (fb_uid, noti_id) {
                    $http({
                        url: 'api/ws/notiRemove/' + fb_uid + '/' + noti_id
                    })
                        .success(function (response) {
                            $rootScope.notifications = response.all;
                            $rootScope.notificationsPost = response.post;
                            $rootScope.notificationsGame = response.game;
                            $rootScope.notificationsFollow = response.follow;
                            $rootScope.notificationsM = [];
                            _.each(response.all.list, function (v) {
                                $rootScope.notificationsM = _.union($rootScope.notificationsM, v);
                            });
                        });
                }

            } else {
                $http({
                    method: 'GET',
                    url: 'api/ws/getRanking?v=4'
                })
                    .success(function (response) {
                        $rootScope.topRanking = response.ranks;

                        angular.element('#ranking-wrapper').show();
                    });

                $http({
                    method: 'GET',
                    url: 'api/ws/worldcupsRanking'
                })
                    .success(function (response) {
                        $rootScope.w14ranking = response.ranks;

                        angular.element('#w14ranking').show();
                    });
            }
        });

        $rootScope.setHighlightCompetitionOnly = function (status) {
            if (status) {
                angular.element('.is-not-highlight-competition').fadeOut();
            } else {
                angular.element('.is-not-highlight-competition').fadeIn();
            }
        }

        $rootScope.logout = function () {
            $http({
                url: 'settings.php?item=removeFacebookId'
            })
                .success(function () {
                    $FB.logout(function (response) {
                        window.location.reload();
                    });
                });

        }

        function saveFacebookInfo() {
            $http({
                method: 'GET',
                url: 'api/ws/saveFacebookInfo?fb_uid=' + $rootScope.facebookInfo.id + '&device_id=' + $rootScope.uuid + '&app=ballscore&platform=web&fb_email=' + $rootScope.facebookInfo.email + '&fb_firstname=' + $rootScope.facebookInfo.first_name + '&fb_lastname=' + $rootScope.facebookInfo.last_name + '&fb_access_token=' + $rootScope.facebookAccessToken
            })
                .success(function (response) {
                    console.log('save facebook info');
                    $cookieStore.put('saved', true);
                });

            $FB.api('/me/friends', function (response) {
                var uid = [];
                angular.forEach(response.data, function (val) {
                    uid.push(val.id);
                });

                var data = {
                    facebook_id: $rootScope.facebookInfo.id,
                    facebook_friend_list: uid.join(',')
                };

                $http({
                    method: 'POST',
//                    url: 'api/ws/saveFacebookFriends?facebook_id=' + $rootScope.facebookInfo.id + '&facebook_friend_list=' + uid.join(',')
                    url: 'api/ws/saveFacebookFriends',
                    data: data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
                    .success(function (response) {
                    });
            });
        }

        function createUUID() {
            return Math.random().toString(36).substring(2, 15) +
                Math.random().toString(36).substring(2, 15);
        }

        $rootScope.getLiveMatchWaitOnlySorted();
        $interval(function () {
            $rootScope.getLiveMatchWaitOnlySorted();
        }, 60000);

        $rootScope.getLastEvents();

        angular.element('.pop-wrapper').click(function(e){
            e.stopPropagation();
        });

        angular.element('html').click(function(){
            angular.element('#fx-set, #notifications-box').hide();
        });

        //endregion
    })
    .controller('mainCtrl', function ($scope, $location, $http) {


        $scope.refreshNewsSlide = function () {
            $http({
                method: 'get',
                url: 'api/ws/dailyNews?lang=' + $scope.language + '&limit=8'
            })
                .success(function (response) {
                    $scope.news = response;

                    var pp = 4;
                    var mm = [];
                    for (var i = 0; i <= response.data.length; i++) {
                        if (!_.isUndefined(response.data[i]) && !_.isUndefined(response.ontimelines[response.data[i].newsId])) {

                            if (mm.length === 0) mm.push([]);
                            if (mm[mm.length - 1].length >= pp) mm.push([]);
                            mm[mm.length - 1].push(response.data[i]);

                        }
                    }

                    var i = 0;
                    if (mm.length > 0) {
                        clearInterval($scope.newsTopSlideInterval);
                        $scope.newsTopSlide = mm[i];
                        angular.element('#news-top-slide-box').fadeIn();

                        $scope.newsTopSlideInterval = setInterval(function () {
                            $scope.$apply(function () {
                                i++;
                                if (_.isUndefined(mm[i])) i = 0;
                                $scope.newsTopSlide = mm[i];

                            });
                        }, 5000);
                    }
                });
        }

        $scope.refreshNewsSlide();
    });