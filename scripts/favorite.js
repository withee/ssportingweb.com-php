angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope, $scope, $location, $http) {
        $scope.selectedCompetition = false;
        $scope.leagues = {};

        $scope.toggleSub = function ($event) {
            $event.preventDefault();
            angular.element($event.currentTarget).next('.wrapper-box-league-favorite').toggle();
        }

        $scope.toggleLeague = function ($event, leagueId) {
            $event.preventDefault();

            if (_.isUndefined($scope.leagues[leagueId])) {
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/leagueMainOnly/' + leagueId
                })
                    .success(function (response) {
                        $scope.leagues[leagueId] = response;
                        angular.element($event.currentTarget).parent().parent().parent().parent().parent().next().next('.wrapper-league-team').toggle();
                    });
            } else {
                angular.element($event.currentTarget).parent().parent().parent().parent().parent().next().next('.wrapper-league-team').toggle();
            }
        }


        $scope.checkFavoriteTeam = function (tid) {

            if(_.isUndefined($rootScope.favoriteTeam)) return false;
            return !_.isUndefined($rootScope.favoriteTeam[tid]);
        }

        $scope.checkFavoriteLeague = function (leagueId) {
            if(_.isUndefined($rootScope.favoriteLeague)) return false;
            return !_.isUndefined($rootScope.favoriteLeague[leagueId]);
        }

        $http({
            method: 'get',
            url: $rootScope.apiUrl+'/competitionOnlyWithSubLeague'
        })
            .success(function(response){
                $scope.competitions = response.competitions;
//                $scope.competitions = _.indexBy(response.competitions, 'competitionId');
            });
    });