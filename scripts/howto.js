angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope, $scope, $location, $http) {
        $http({
            method: 'get',
            url: $rootScope.apiUrl+'/getInstruction'
        })
            .success(function (response) {
                console.log('how to', response);
                angular.forEach(response, function (val, key) {
                    angular.element('#howto-' + (key + 1)).html(val.detail);
                })
            });

        $scope.showHowto = function (num) {
            $('.howto-memu').removeClass('active_menu');
            $('.howto-content').hide();
            $('#how-to-menu-' + num).addClass('active_menu');
            $('#howto-' + num).show();
        }
    });