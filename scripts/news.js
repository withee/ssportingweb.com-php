angular.module('scorspot.controller', ['videosharing-embed'])
    .controller('mainCtrl', function ($rootScope, $scope, $location, $http, $upload, $timeout) {

        $scope.newId = document.getElementById('newsScript').getAttribute('data-id');

        $scope.inProgress = {};

        var alreadyLike = {}
            , inProgress = {}
            , inProgressTime = 2000;

        $scope.newComment = {
            message: '',
            picture: '',
            thumbnail: ''
        };

        $scope.newReply = {
            parent_id: false,
            message: {},
            picture: '',
            thumbnail: ''
        };

        $scope.removeImage = function (w) {
            if (w === 'comment') {
                $scope.newComment.picture = '';
                $scope.newComment.thumbnail = '';
            } else if (w === 'reply') {
                $scope.newReply.picture = '';
                $scope.newReply.thumbnail = '';
            }
        }

        $scope.refreshNewsSlide = function () {
            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/dailyNews?lang=' + $scope.language + '&limit=8'
            })
                .success(function (response) {
                    $scope.news = response;

                    var pp = 4;
                    var mm = [];
                    for (var i = 0; i <= response.data.length; i++) {
                        if (!_.isUndefined(response.data[i]) && !_.isUndefined(response.ontimelines[response.data[i].newsId])) {

                            if (mm.length === 0) mm.push([]);
                            if (mm[mm.length - 1].length >= pp) mm.push([]);
                            mm[mm.length - 1].push(response.data[i]);

                        }
                    }

                    var i = 0;
                    if (mm.length > 0) {
                        clearInterval($scope.newsTopSlideInterval);
                        $scope.newsTopSlide = mm[i];
                        angular.element('#news-top-slide-box').fadeIn();

                        $scope.newsTopSlideInterval = setInterval(function () {
                            $scope.$apply(function () {
                                i++;
                                if (_.isUndefined(mm[i])) i = 0;
                                $scope.newsTopSlide = mm[i];

                            });
                        }, 5000);
                    }
                });
        }

        $scope.getNewsContent = function () {
            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/timelineSingleContent?id=' + $scope.newId
            })
                .success(function (response) {
                    $scope.contents = response.timelines[0];
//                    $scope.commentsInfo = response.main_reply[$scope.newId];
                    $scope.comments = response.main_reply[$scope.newId];
                    $scope.replies = response.sub_reply;

                    angular.element('#comments-block').fadeIn();
                });
        }

        $scope.addNewComment = function (comments, $event) {

            if ($scope.inProgress.comment) return false;

            if ($scope.newComment.message.length > 0 || $scope.newComment.picture.length > 0 || $scope.newComment.thumbnail.length > 0) {
                var more = '';
                if ($scope.newComment.thumbnail.length > 0) {
                    more = '&picture=' + $scope.newComment.picture + '|' + $scope.newComment.thumbnail;
                }

                if ($scope.newComment.message.length === 0) $scope.newComment.message = ' ';

                $scope.inProgress.comment = true;

                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/timelineReply?timeline_id=' + $scope.newId
                        + '&fb_uid=' + $rootScope.facebookInfo.id
                        + '&message=' + encodeURI($scope.newComment.message)
                        + more
                })
                    .success(function (response) {
                        if (response) {
                            if (_.isUndefined($scope.comments)) $scope.comments = [];
                            $scope.comments.push(response);
                            $scope.newComment.message = '';
                            $scope.newComment.thumbnail = '';
                            $scope.newComment.picture = '';

                            angular.element('.new-comment-thumbnail').attr('src', '');
                        } else {
                            alert('!');
                        }

                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.inProgress.comment = false;
                            });
                        }, inProgressTime);
                    });
            } else {
                alert('ต้องกรอกความเห็น หรือ รูปภาพ!');
            }
        }

        $scope.addNewReply = function (comment_id, $event) {

            if ($scope.inProgress.reply) return false;

            if ((!_.isUndefined($scope.newReply.message[comment_id]) && $scope.newReply.message[comment_id].length > 0) || $scope.newReply.picture.length > 0 || $scope.newReply.thumbnail.length > 0) {
                var more = '', message = '&message= ';
                if ($scope.newReply.thumbnail.length > 0) {
                    more = '&picture=' + $scope.newReply.picture + '|' + $scope.newReply.thumbnail;
                }
                if (!_.isUndefined($scope.newReply.message[comment_id]) && $scope.newReply.message[comment_id].length > 0) {
                    message = '&message=' + encodeURI($scope.newReply.message[comment_id]);
                }

                $scope.inProgress.reply = true;

                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/timelineReply?timeline_id=' + $scope.newId
                        + '&fb_uid=' + $rootScope.facebookInfo.id
                        + '&parent=' + comment_id
                        + message
                        + more
                })
                    .success(function (response) {
                        console.log('comment on match', response);
                        if (response) {
                            if (_.isUndefined($scope.replies[comment_id])) {
                                $scope.replies[comment_id] = [];
                            }
                            $scope.replies[comment_id].push(response);
//                            comments.push(response.data);
                            $scope.newReply.id = false;
                            $scope.newReply.message = {};
                            $scope.newReply.thumbnail = '';
                            $scope.newReply.picture = '';
                            angular.element('.new-reply-thumbnail').attr('src', '');
                        } else {
                            alert('!');
                        }

                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.inProgress.reply = false;
                            });
                        }, inProgressTime);

                    });
            } else {
                alert('ต้องกรอกความเห็น หรือ รูปภาพ!');
            }
        }

        $scope.removeReply = function (id, parentId) {
            var conf = confirm('Sure ?');
            if (conf) {
                $http({
                    method: 'get',
                    url: $rootScope.apiUrl+'/removeTimelineReply?id=' + id + '&fb_uid=' + $scope.facebookInfo.id
                })
                    .success(function (response) {
                        if (response.success) {
                            if (!parentId) {
                                $scope.comments = _.reject($scope.comments, function (val) {
                                    return val.id == id;
                                });
                            } else {
                                $scope.replies[parentId] = _.reject($scope.replies[parentId], function (val) {
                                    return val.id == id;
                                });
                            }
                        } else {
                            alert(response.desc);
                        }
                    });
            }

        }

        $scope.likeTopic = function (timeline_id, index) {

            if (!_.isUndefined(alreadyLike[timeline_id])) return false;

            if (inProgress.like) return;
            inProgress.like = true;

            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/timelineLike?fb_uid=' + $rootScope.facebookInfo.id
                    + '&timeline_id=' + timeline_id
            })
                .success(function (response) {
                    if ((response.success && response.success != '0') || response.success == '1') {
                        angular.element('#like-count').text(response.timelines[0].content[0].like)
                    } else {
                        console.log(response);
                        alert(response.desc);
                    }

                    alreadyLike[timeline_id] = true;

                    $timeout(function () {
                        inProgress.like = false;
                    }, inProgressTime);

                });
        }

        $scope.likeComment = function (id, match_id, index) {

            if (!_.isUndefined(alreadyLike[id])) return false;

            if (inProgress.like) return;
            inProgress.like = true;

            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/timelineReplyLike?reply_id=' + id + '&fb_uid=' + $rootScope.facebookInfo.id
            })
                .success(function (response) {
                    if ((response.success && response.success != '0') || response.success == '1') {
                        $scope.comments[index].like = response.like;
//                        $scope.comments[index].like++;
                    } else {
                        alert(response.desc);
                    }

                    alreadyLike[id] = true;

                    setTimeout(function () {
                        inProgress.like = false;
                    }, inProgressTime);
                });
        }

        $scope.likeReply = function (id, comment_id, index) {

            if (!_.isUndefined(alreadyLike[id])) return false;

            if (inProgress.like) return;
            inProgress.like = true;

            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/timelineReplyLike?reply_id=' + id + '&fb_uid=' + $rootScope.facebookInfo.id
            })
                .success(function (response) {
                    if ((response.success && response.success != '0') || response.success == '1') {
                        $scope.replies[comment_id][index].like = response.like;
//                        $scope.replies[comment_id][index].like++;
                    } else {
                        alert(response.desc);
                    }

                    alreadyLike[id] = true;

                    setTimeout(function () {
                        inProgress.like = false;
                    }, inProgressTime);
                });
        }

        $scope.browsePicture = function () {
            $timeout(function(){
                angular.element('#upload-browse').trigger('click');
            }, 100);
        }

        $scope.uploadPicture = function ($files) {
            var file = $files[0];
            $scope.upload = $upload.upload({
                url: 'api/upload/cover',
                method: 'POST',
                data: {
                    fb_uid: $rootScope.facebookInfo.id
                },
                file: file
            }).progress(function (evt) {
//                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).success(function (data, status, headers, config) {
//                $scope.profileInfo.cover = data.path;
                console.log('upload', data);
                $scope.newComment.thumbnail = data.thumbnail;
                $scope.newComment.picture = data.path;

            });
        }

        $scope.browsePictureReply = function (comment_id) {
            $scope.newReply.id = comment_id;
            $timeout(function(){
                angular.element('#upload-browse-reply').trigger('click');
            }, 100);
        }

        $scope.uploadPictureReply = function ($files) {
            var file = $files[0];
            $scope.upload = $upload.upload({
                url: 'api/upload/cover',
                method: 'POST',
                data: {
                    fb_uid: $rootScope.facebookInfo.id
                },
                file: file
            }).progress(function (evt) {
//                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).success(function (data, status, headers, config) {
//                $scope.profileInfo.cover = data.path;
                console.log('upload', data);
                $scope.newReply.thumbnail = data.thumbnail;
                $scope.newReply.picture = data.path;
                console.log('newReply', $scope.newReply);

            });
        }

        $scope.openReplyBox = function (id) {
            angular.element('.reply-box').hide();
            angular.element('[data-comment-id="' + id + '"] .reply-box').fadeIn();
        }

        $scope.gotoImg = function (s, $event) {
            $event.preventDefault();


            if (_.isUndefined($scope.currentIdx)) $scope.currentIdx = 0;
            if (_.isUndefined($scope.lengthIdx)) $scope.lengthIdx = angular.element('.idx-img').length;

            var cp = angular.copy($scope.currentIdx);

            if ($scope.lengthIdx > 0) {
                if (s === 'next') {
                    $scope.currentIdx++;
                } else {
                    $scope.currentIdx--;
                }

                if ($scope.currentIdx > ($scope.lengthIdx - 1) || $scope.currentIdx < 0) {
                    $scope.currentIdx = cp;
                    return false; //$scope.currentIdx = 0;
                }

                angular.element('.idx-img').hide();
                angular.element('.idx-img[data-idx="' + $scope.currentIdx + '"]').fadeIn();
            }

        }

        $scope.lengthIdx = angular.element('.idx-img').length;

        $scope.refreshNewsSlide();
        $scope.getNewsContent();

    });