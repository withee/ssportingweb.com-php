angular.module('scorspot.controller', ['videosharing-embed'])
    .controller('mainCtrl', function ($rootScope, $scope, $http, $upload, $timeout, $interval, $q) {
        var alreadyLike = {}
            , inProgress = {}
            , inProgressTime = 2000
            ;

        $scope.forceId = document.getElementById('boardScript').getAttribute('data-id');

        $scope.inProgress = {};
        $scope.more=0;
        $scope.moreloading=false;
//        $scope.defaultValue = {
//            type: 6
//        };
        $scope.activeTopicType = [];
        $scope.newTopic = {
            title: '',
            message: '',
            picture: [],
            thumbnail: [],
            video: ''
        };

        $scope.focusBox = function ($event) {
            angular.element($event.currentTarget).addClass('tabPost-expand');
        };

        $scope.blurBox = function ($event) {
            angular.element($event.currentTarget).removeClass('tabPost-expand');
        };

        $scope.browsePictureNewTopic = function () {
            $timeout(function(){
                angular.element('#upload-browse-new-topic').trigger('click');
            }, 100);

        }

        $scope.uploadPictureNewTopic = function ($files) {
            for (var i = 0; i < $files.length; i++) {
                var file = $files[i];

                $scope.upload = $upload.upload({
                    url: 'api/upload/cover',
                    method: 'POST',
                    data: {
                        fb_uid: $scope.facebookInfo.id
                    },
                    file: file
                }).progress(function (evt) {
                    $scope.inProgress.uploadNewTopic = true;
                    console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                }).success(function (data, status, headers, config) {
                    console.log('upload', data);
                    if (data.success) {
                        $scope.newTopic.thumbnail.push(data.thumbnail);
                        $scope.newTopic.picture.push(data.path);
                    }

                    $scope.inProgress.uploadNewTopic = false;
                });
            }
        }

        $scope.removeNewTopicUploadPicture = function (index) {
            $scope.newTopic.thumbnail.splice(index, 1);
            $scope.newTopic.picture.splice(index, 1);
        }

        $scope.addNewTopic = function () {
            if ($scope.inProgress.newTopic) return false;

            if ($scope.newTopic.title.length > 0 && ($scope.newTopic.message.length > 0 || $scope.isVideo($scope.newTopic.video) || $scope.newTopic.picture.length > 0 || $scope.newTopic.thumbnail.length > 0)) {

                var data = {
                    uid: $rootScope.userInfo.uid,
                    fb_uid: $rootScope.facebookInfo.id,
                    title: $scope.newTopic.title,
                    message: $scope.newTopic.message,
                    category: $scope.selectedTopicType.fid
                };

                console.log(data);

                if ($scope.newTopic.thumbnail.length > 0) {
                    var picture = [];
                    for (var i = 0; i < $scope.newTopic.thumbnail.length; i++) {
                        picture.push($scope.newTopic.picture[i] + '|' + $scope.newTopic.thumbnail[i]);
                    }
                    _.extend(data, { picture: picture.join(',') });
                }

                if ($scope.newTopic.video.length > 0) {
                    _.extend(data, { video: $scope.newTopic.video });
                }

                $scope.inProgress.newTopic = true;

                $http({
                    method: 'post',
                    url: 'api/post/newTopic',
                    data: data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
                    .success(function (response) {
                        console.log('post board', response);
                        $scope.newTopic = {
                            title: '',
                            message: '',
                            picture: [],
                            thumbnail: [],
                            video: ''
                        };

                        if ($scope.activeTopicType.length > 0 && !$scope.isActiveTopicType($scope.selectedTopicType.fid)) {
                            $scope.toggleActiveTopicType($scope.selectedTopicType.fid);
                        }

                        $scope.getTopics();

                        alert('ตั้งกระทู้เรียบร้อยแล้ว');

                        $timeout(function () {
                            $scope.inProgress.newTopic = false;
                        }, inProgressTime)
                    });

            } else {
                alert('ต้องกรอกความเห็น หรือ รูปภาพ!');
            }
        }

        $scope.likeTopic = function (timeline_id, index) {

            if (!_.isUndefined(alreadyLike[timeline_id])) return false;

            if (inProgress.like) return;
            inProgress.like = true;

            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/timelineLike?fb_uid=' + $rootScope.facebookInfo.id
                    + '&timeline_id=' + timeline_id
            })
                .success(function (response) {
                    if ((response.success && response.success != '0') || response.success == '1') {
                        console.log('1');
                        $scope.topics.timelines[index].content[0].like = response.timelines[0].content[0].like;
//                        $scope.timelines.timelines[index].content[0].like++;
                    } else {
                        console.log(response);
                        alert(response.desc);
                    }

                    alreadyLike[timeline_id] = true;
                    $timeout(function () {
                        inProgress.like = false;
                    }, inProgressTime);

                });
        }

//        $scope.toggleActiveTopicType = function (id) {
//
//            if ($scope.isActiveTopicType(id)) {
//                $scope.activeTopicType = _.reject($scope.activeTopicType, function (o) {
//                    return o.fid === id
//                });
//
//            } else {
//                $scope.activeTopicType.push(_.findWhere($scope.topicType, {fid: id}));
//            }
//
//            $http({
//                method: 'get',
//                url: 'api/ws/setCatview/' + $scope.userInfo.uid + '/' + $scope.getActiveTopicType('fid').join(',')
//            })
//                .success(function (response) {
//
//                });
//        }

//        $scope.isActiveTopicType = function (id) {
//            var n = _.findWhere($scope.activeTopicType, {fid: id});
//            if (_.isUndefined(n)) return false;
//            else return n;
//        }

//        $scope.getActiveTopicType = function (attr) {
//            if (_.isUndefined(attr)) attr = 'fid';
//
//            return _.pluck($scope.activeTopicType, attr);
//        }

//        $scope.getActiveTopicTypeSettings = function () {
//            $http({
//                url: 'api/ws/getCatview/' + $rootScope.userInfo.uid
//            })
//                .success(function (response) {
//                    $scope.activeTopicType = response;
//                });
//        }

        $scope.getTopics = function () {
            var filter = '';
//            if ($scope.activeTopicType.length > 0) {
//                filter = '&category=' + $scope.getActiveTopicType('fid').join(',');
//            } else filter = '';
            if ($scope.forceId.length > 0) {
                filter = '&category=' + $scope.forceId;
            } else filter = '';

            console.log('filter', filter);

            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/getTimelines?&view_type=board&fb_uid=' + $rootScope.facebookInfo.id + '&limit=10' + filter
            })
                .success(function (response) {
                    $scope.topics = response;
                    angular.element('#board-right').fadeIn();
                });
        }

        $scope.MoreTimeLine = function () {
            var filter = '';
            if ($scope.forceId.length > 0) {
                filter = '&category=' + $scope.forceId;
            } else filter = '';

            $scope.more=$scope.more+1;
            var limit=10,scrollTop=0;
            if($scope.more!=0){
                limit=limit+($scope.more*10);
                scrollTop=$(window).scrollTop();
            }

            $scope.moreloading=true;
            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/getTimelines?&view_type=board&fb_uid=' + $rootScope.facebookInfo.id + '&limit=10&offset='+$scope.more+ filter
                //url: 'http://api.ssporting.com/getTimelines?fb_uid=' + $scope.fb_uid + '&limit=40' + more
            })
                .success(function (response) {
                    if($scope.more==1){
                        $scope.topics1 = response;
                    }else if($scope.more==2){
                        $scope.topics2 = response;
                    }else if($scope.more==3){
                        $scope.topics3 = response;
                    }else if($scope.more==4){
                        $scope.topics4 = response;
                    }else if($scope.more==5){
                        $scope.topics5 = response;
                    }
                    $scope.moreloading=false;
                });
        }

        $scope.getRecommendTopics = function () {

            if ($scope.forceId == '4' || $scope.forceId == '5' || $scope.forceId == '6' || $scope.forceId == '8') {
                var tmp = $scope.topics.recommend.timelines;
                var pp = 2;
                var mm = [];
                for (var i = 0; i <= tmp.length; i++) {
                    if (!_.isUndefined(tmp[i])) {

                        if (mm.length === 0) mm.push([]);
                        if (mm[mm.length - 1].length >= pp) mm.push([]);
                        mm[mm.length - 1].push(tmp[i]);

                    }
                }

                var i = 0;
                if (mm.length > 0) {
                    clearInterval($scope.recommendTopicsInterval);
                    $scope.recommendTopics = mm[i];
//                angular.element('#news-top-slide-box').fadeIn();

                    $scope.recommendTopicsInterval = $interval(function () {
                        i++;
                        if (_.isUndefined(mm[i])) i = 0;
                        $scope.recommendTopics = mm[i];
                    }, 5000);
                }
            } else if ($scope.forceId == '2' || $scope.forceId == '3' || $scope.forceId == '7') {
                $scope.recommendTopics = $scope.topics.recommend.timelines;
            }

            console.log('recommend', $scope.topics.recommend, $scope.recommendTopics);

        }

        $scope.getTopicCover = function (data) {
            var cover = 'images/test.jpg';
            if (!_.isUndefined(data)) {
                switch (data.owner.content_type) {
                    case 'board':
                        if ($scope.hasImage(data.content[0].medialist)) cover = data.content[0].medialist[0].thumbnail
                        else if ($scope.hasVideo(data.content[0].medialist)) cover = $scope.getVideoScreenShot(data.content[0].medialist[0].path)
                        else cover = 'images/default_webboard.png';
                        break;
                    case 'status':
                        cover = 'images/default_status.png';
                        break;
                    case 'image':
                        cover = data.content[0].path;
                        break;
                    case 'news':
                        cover = data.content[0].imageLink;
                        break;
                    case 'highlight':
                        cover = $scope.getVideoScreenShot(data.content[0].content);
                        break;
                    case 'game':
                        cover = 'http://www.scorspot.com/WebObject/images/s' + data.content[0].mid + '.jpg';
                        break;
                }
            }

            return cover;
        }

        $scope.getTopicTypeFromId = function (id) {
            return  _.findWhere($scope.topicType, {fid: id});
        }

//        $scope.hasVideo = function (medialist) {
//            if (!_.isUndefined(medialist[0]) && !_.isUndefined(medialist[0].path)) {
//                return $scope.isVideo(medialist[0].path);
//            } else return false;
//        }
//
//        $scope.getVideoScreenShot = function (url, size) {
//            size = (size === null) ? "big" : size;
//            var vid;
//            var results;
//            if (url.match(/^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/) !== null) {
//                results = url.match("[\\?&]v=([^&#]*)");
//                vid = ( results === null ) ? url : results[1];
//
//                if (size == "small") {
//                    return "http://img.youtube.com/vi/" + vid + "/2.jpg";
//                } else {
//                    return "http://img.youtube.com/vi/" + vid + "/0.jpg";
//                }
//            } else if (url.match(/^.+dailymotion.com\/((video|hub)\/([^_]+))?[^#]*(#video=([^_&]+))?/) !== null) {
//                results = url.match(".*\/video\/([^&#]*)");
//                vid = ( results === null ) ? url : results[1];
//                return "http://www.dailymotion.com/thumbnail/video/" + vid;
//            } else if (url.match(/^.*(vimeo\.com\/)((channels\/[A-z]+\/)|(groups\/[A-z]+\/videos\/))?([0-9]+)/) !== null) {
//
//            }
//
//
//        }
//
//        $scope.hasImage = function (medialist) {
//            if (!_.isUndefined(medialist[0]) && !_.isUndefined(medialist[0].path)) {
//                if (medialist[0].path.match(/\.(jpeg|jpg|gif|png)$/)) return true;
//                else return false;
//            } else return false;
//        }
//
//        $scope.isVideo = function (url) {
//            return url.match(/^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/) !== null
//                || url.match(/^.+dailymotion.com\/((video|hub)\/([^_]+))?[^#]*(#video=([^_&]+))?/) !== null
//                || url.match(/^.*(vimeo\.com\/)((channels\/[A-z]+\/)|(groups\/[A-z]+\/videos\/))?([0-9]+)/) !== null;
//        }

        $scope.toggleVideoLinkBox = function () {
            $scope.videoLinkBox = $scope.videoLinkBox ? false : true;
            if (!$scope.videoLinkBox) $scope.newTopic.video = '';
        }

        $scope.refreshNewsSlide();

//        $http({
//            url: 'api/ws/getTopictype'
//        })
//            .success(function (response) {
//                $scope.topicType = response;
//                $scope.selectedTopicType = $scope.topicType[$scope.defaultValue.type];
//                angular.element('#basic-left-menu').hide();
//                angular.element('#webboard-filter').show();
//            });

        $scope.$watch('userInfo', function (newVal, oldVal) {
            if (!_.isUndefined(newVal) && !_.isUndefined(newVal.uid)) {
//                $scope.getActiveTopicTypeSettings();
                $scope.getTopics();

            }
        });

        $scope.$watch('topics.timelines', function (newVal, oldVal) {
            console.log('timelines');
        })

        $scope.$watch('topics', function (newVal, oldVal) {
            if (!_.isUndefined(newVal) && $scope.activeTopicType.length === 0 && _.isUndefined($scope.recommendTopics)) {
                $scope.getRecommendTopics();
                angular.element('#box-recommend-topics').fadeIn();
            }
            angular.element('#wrapForums').show();
            angular.element('#title-board').show();
            angular.element('#title-all').show();
            angular.element('#topics-recommend').show();
        });

//        $scope.$watchCollection('activeTopicType', function (newVal, oldVal) {
//            if (!_.isUndefined(newVal)) {
//                $scope.getTopics();
//            }
//        })

        $scope.$watchCollection('topicType', function(newVal, oldVal){
            if(!_.isUndefined(newVal)) {
                $scope.selectedTopicType = $scope.getTopicTypeFromId($scope.forceId);
            }
        });

        $scope.getThumbnail=function(content){
            //var dataThumbnailDailymotion =new Array();
            //$http({
            //    method: 'post',
            //    url: 'https://api.dailymotion.com/video/x3ihuro_20151217w-00479_creation?fields=thumbnail_large_url'
            //})
            //    .success(function (response) {
            //        dataThumbnailDailymotion=response;
            //    });
            console.log('9');
            //return 'http://s1.dmcdn.net/RBGT6/x240-Pff.jpg';
            return false
        }

    });