angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope, $scope, $location, $http, $interval, $cookieStore) {

        $scope.currentNotiTab = 'all';
        $scope.changeNotiTab = function (tab) {
            angular.element('#noti-all, #noti-post, #noti-follow, #noti-game').hide();
            angular.element('#noti-' + tab).fadeIn();
            $scope.currentNotiTab = tab;
        }

    });