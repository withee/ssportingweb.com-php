angular.module('scorspot.controller', ['videosharing-embed'])
    .controller('mainCtrl', function ($rootScope, $scope, $location, $http, $FB, $upload, $timeout) {

        $scope.fb_uid = document.getElementById('timelineScript').getAttribute('fb_uid');
        $scope.q = document.getElementById('timelineScript').getAttribute('q');
        $scope.own = document.getElementById('timelineScript').getAttribute('data-own');

        $scope.defaultConfig = {
            timelineCommentsLimit: 5
        };

        $scope.inProgress = {};
        $scope.more=0;
        $scope.moreloading=false;

        var alreadyLike = {}
            , inProgress = {}
            , inProgressTime = 2000;

        if ($scope.q === 'ranking') {
            $scope.currentTab = 'ranking';
        } else if ($scope.q === 'play') {
            $scope.currentTab = 'play';
        } else if ($scope.q === 'following') {
            $scope.currentTab = 'following';
        } else {
            $scope.currentTab = 'timeline';
        }
        $scope.currentRankingTab = 'all';
        $scope.currentPlayTab = 'all';
        $scope.currentPlayDate = moment().format("YYYY-MM-DD");
        $scope.resultFilter = {};
        $scope.playResultDateSelect = '';//moment().add('days', -1).format('YYYY-MM-DD');
        $scope.playResultDate = [
            { date: moment().format('YYYY-MM-DD'), month: moment().format('MMM.YYYY'), day: moment().format('DD') },
            { date: moment().add('days', -1).format('YYYY-MM-DD'), month: moment().add('days', -1).format('MMM.YYYY'), day: moment().add('days', -1).format('DD') },
            { date: moment().add('days', -2).format('YYYY-MM-DD'), month: moment().add('days', -2).format('MMM.YYYY'), day: moment().add('days', -2).format('DD') },
            { date: moment().add('days', -3).format('YYYY-MM-DD'), month: moment().add('days', -3).format('MMM.YYYY'), day: moment().add('days', -3).format('DD') },
            { date: moment().add('days', -4).format('YYYY-MM-DD'), month: moment().add('days', -4).format('MMM.YYYY'), day: moment().add('days', -4).format('DD') }
        ];
        $scope.newTimeline = {
            message: '',
            picture: [],
            thumbnail: []
        };

        $scope.newComment = {
            message: {},
            picture: '',
            thumbnail: ''
        };

        $scope.newReply = {
            parent_id: false,
            message: {},
            picture: '',
            thumbnail: ''
        };

        $scope.removeImage = function (w, idx) {
            if (w === 'new') {
                $scope.picture.splice(idx, 1);
                $scope.thumbnail.splice(idx, 1);
            } else if (w === 'comment') {
                $scope.newComment.picture = '';
                $scope.newComment.thumbnail = '';
            } else if (w === 'reply') {
                $scope.newReply.picture = '';
                $scope.newReply.thumbnail = '';
            }
        }

        $scope.refreshTimeline = function () {
            var more = '';
            if ($scope.own == 'true') {
                more = '&view_type=own';
            }
            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/getTimelines?fb_uid=' + $scope.fb_uid + '&limit=10' + more
                //url: 'http://api.ssporting.com/getTimelines?fb_uid=' + $scope.fb_uid + '&limit=40' + more
            })
                .success(function (response) {
                    //angular.element('#moreTimeline').show();
                    $scope.timelines = response;
                });
        }

        $scope.refreshMatchFeed = function () {
            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/getMatchCommentList?fb_uid=' + $scope.fb_uid
                //url: 'http://api.ssporting.com/getMatchCommentList?fb_uid=' + $scope.fb_uid
            })
                .success(function (response) {
                    $scope.matchFeeds = response.data
                });
        }

        $scope.refreshNewFeed = function () {
            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/commentFacebookFriends/' + $scope.fb_uid
                //url: 'http://api.ssporting.com/commentFacebookFriends/' + $scope.fb_uid
            })
                .success(function (response) {
                    clearInterval($rootScope.feedTopSlideInterval);
                    $rootScope.newfeed = response;

                    var t = 0;
                    var m = [];
                    for (var i = t; i <= response.length; i++) {
                        if (!_.isUndefined(response[i])) {
                            t = i;
                            m.push(response[i]);
                            if (m.length >= 4) break;
                        } else {
                            t = 0;
                            break;
                        }
                    }

                    $rootScope.feedTopSlide = m;

                    $rootScope.feedTopSlideInterval = setInterval(function () {
                        $rootScope.$apply(function () {
                            var m = [];
                            for (var i = t + 1; i <= response.length; i++) {
                                if (!_.isUndefined(response[i])) {
                                    t = i;
                                    m.push(response[i]);
                                    if (m.length >= 4) break;
                                } else {
                                    t = 0;
                                    break;
                                }

                                if (t >= response.length) {
                                    t = 0;
                                    break;
                                }
                            }
                            $rootScope.feedTopSlide = m;
                        });
                    }, 5000);

                    angular.element('#feed-top-slide-box').show();
                    angular.element('#news-feed-box').show();

                });
        }

        $scope.refreshNewsSlide = function () {
            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/dailyNews?lang=' + $scope.language + '&limit=8'
            })
                .success(function (response) {
                    $scope.news = response;

                    var pp = 4;
                    var mm = [];
                    for (var i = 0; i <= response.data.length; i++) {
                        if (!_.isUndefined(response.data[i]) && !_.isUndefined(response.ontimelines[response.data[i].newsId])) {

                            if (mm.length === 0) mm.push([]);
                            if (mm[mm.length - 1].length >= pp) mm.push([]);
                            mm[mm.length - 1].push(response.data[i]);

                        }
                    }

                    var i = 0;
                    if (mm.length > 0) {
                        clearInterval($scope.newsTopSlideInterval);
                        $scope.newsTopSlide = mm[i];
                        angular.element('#news-top-slide-box').fadeIn();

                        $scope.newsTopSlideInterval = setInterval(function () {
                            $scope.$apply(function () {
                                i++;
                                if (_.isUndefined(mm[i])) i = 0;
                                $scope.newsTopSlide = mm[i];

                            });
                        }, 5000);
                    }
                });
        }

        $scope.$watch('currentTab', function (tab) {
//            if (!$scope.ready) return false;

            switch (tab) {
                case 'feed':
                    $scope.tabInclude = 'views/profile_tabs/feed.html';
                    break;
                case 'following':
                    $scope.tabInclude = 'views/profile_tabs/following.html';
                    break;
                case 'play':
                    $scope.tabInclude = 'views/profile_tabs/play.html';
                    break;
                case 'ranking':
                    $scope.tabInclude = 'views/profile_tabs/ranking.html';
                    break;
                case 'timeline':
                    $scope.tabInclude = 'views/profile_tabs/timeline.html';
                    break;
            }
        });

        $scope.changePlayResultDate = function (date) {
            if (date == '') {
                $scope.playResultDateSelect = '';
            } else {
                $scope.playResultDateSelect = date;
            }

        }

        $scope.playResultDateFilter = function (item) {
            if ($scope.playResultDateSelect != '') {
                if (moment(item.betDatetime * 1000).format('YYYY-MM-DD') == $scope.playResultDateSelect) {
                    return item;
                } else {
                    return false;
                }
            } else {
                return item;
            }
        }

        $scope.clearResultFilter = function () {
            $scope.resultFilter = {};
        }

        $scope.changeTab = function (tab) {
            $scope.currentTab = tab;
        }


        $scope.redirectToGamePage = function (mid) {
            window.location = '/game.php?mid=' + mid;
        }

        $scope.focusOut = function ($event) {
            angular.element($event.currentTarget).parent().focusout();
        }


        $scope.getFollowing = function (offset) {
            if (_.isUndefined(offset)) offset = 0;

            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/following?fb_uid=' + $scope.fb_uid + '&offset=' + offset
            })
                .success(function (response) {
                    var o = response;
                    var uid = [];
                    _.each(response.user, function (val) {
                        if (val.fb_uid) uid.push(val.fb_uid);
                    });
                    uid = uid.join(',');

                    $FB.api({
                        method: 'fql.query',
                        query: 'SELECT uid, name, username, current_location, sex, status, online_presence, profile_blurb, devices, quotes FROM user WHERE uid IN (' + uid + ')'
                    }, function (response) {
                        _.each(o.user, function (val) {
                            val.original = _.findWhere(response, { uid: val.fb_uid });
                        });

                        $scope.following = o;
                    });
                });
        }

        $scope.getCurrentUserFollowing = function (offset) {
            if (_.isUndefined(offset)) offset = 0;

            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/following?fb_uid=' + $scope.facebookInfo.id + '&offset=' + offset
            })
                .success(function (response) {
                    var o = response;
                    var uid = [];
                    _.each(response.user, function (val) {
                        if (val.fb_uid) uid.push(val.fb_uid);
                    });
                    uid = uid.join(',');

                    $FB.api({
                        method: 'fql.query',
                        query: 'SELECT uid, name, username, current_location, sex, status, online_presence, profile_blurb, devices, quotes FROM user WHERE uid IN (' + uid + ')'
                    }, function (response) {
                        _.each(o.user, function (val) {
                            val.original = _.findWhere(response, { uid: val.fb_uid });
                        });
                        $scope.currentUserFollowing = o;
                    });
                });
        }

        $scope.checkIsFollowing = function (id) {
            if (!_.isUndefined($scope.currentUserFollowing) && $scope.currentUserFollowing.user[id]) {
                return true;
            }

            return false;
        }

        $scope.getFollower = function (offset) {
            if (_.isUndefined(offset)) offset = 0;

            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/followers?fb_uid=' + $scope.fb_uid + '&offset=' + offset
            })
                .success(function (response) {
                    $scope.followerCount = response.length;
                    $scope.follower = _.indexBy(response, 'fb_uid');
                });
        }

        $scope.getPlays = function () {
            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/betLists/' + $scope.fb_uid + '?' + new Date().getTime()
            })
                .success(function (response) {
                    $scope.plays = response;
                    $scope.plays_w = _.size(_.where(response, {betResult: 'win'}));
                    $scope.plays_l = _.size(_.where(response, {betResult: 'lose'}));
                });
        }

        $scope.getAchieve = function () {
            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/getAchieve?fb_uid=' + $scope.fb_uid
            })
                .success(function (response) {
                    $scope.achieve = response;
                });
        }

        $scope.follow = function (follow, fb_uid) {

            if (_.isUndefined($rootScope.facebookInfo)) return false;
            if (_.isUndefined($scope.fb_uid) && fb_uid) return false;
            if (_.isUndefined(fb_uid)) fb_uid = $scope.fb_uid;

            if (follow) {
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/follow?fb_uid=' + $rootScope.facebookInfo.id + '&fb_follow_uid=' + fb_uid + '&checked=Y'
                })
                    .success(function (response) {
                        if (response) {
                            $scope.currentUserFollowing.user[fb_uid] = true;
                        }
                    });
            } else {
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/follow?fb_uid=' + $rootScope.facebookInfo.id + '&fb_follow_uid=' + fb_uid + '&checked=N'
                })
                    .success(function (response) {
                        if (response) {
                            $scope.currentUserFollowing.user[fb_uid] = false;
                        }
                    });
            }

        }

        $scope.addNewTimeline = function () {

            if ($scope.inProgress.newTimeline) return false;

            if ($scope.newTimeline.message.length > 0 || $scope.newTimeline.picture.length > 0 || $scope.newTimeline.thumbnail.length > 0) {
                var more = '&picture=';
                if ($scope.newTimeline.thumbnail.length > 0) {
                    for (var i = 0; i < $scope.newTimeline.thumbnail.length; i++) {
                        more += $scope.newTimeline.picture[i] + '|' + $scope.newTimeline.thumbnail[i] + ',';
                    }
                }

                $scope.inProgress.newTimeline = true;

                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/UpdateWallStatus?fb_uid=' + $scope.fb_uid
                        + '&fb_uid=' + $rootScope.facebookInfo.id
                        + '&message=' + encodeURI($scope.newTimeline.message)
                        + more
                })
                    .success(function (response) {
                        console.log('post timeline', response);
                        if (response !== null && !_.isUndefined(response.timelines)) {
                            $scope.timelines.timelines.unshift(response.timelines[0]);
                            $scope.newTimeline.message = '';
                            $scope.newTimeline.thumbnail = '';
                            $scope.newTimeline.picture = '';

//                            return;
                        } else {
                            alert('!!!!');
                        }

                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.inProgress.newTimeline = false;
                            });
                        }, inProgressTime);

                    });
            } else {
                alert('ต้องกรอกความเห็น หรือ รูปภาพ!');
            }
        }

        $scope.addNewComment = function (timeline_id) {

            if ($scope.inProgress['comment' + timeline_id]) return false;

            if (_.isUndefined($scope.newComment.message[timeline_id])) {
                $scope.newComment.message[timeline_id] = '';
            }

            if ($scope.newComment.message[timeline_id].length > 0 || $scope.newComment.picture.length > 0 || $scope.newComment.thumbnail.length > 0) {
                //var more = '';
                //if ($scope.newComment.thumbnail.length > 0) {
                //    more = '&picture=' + $scope.newComment.picture + '|' + $scope.newComment.thumbnail;
                //}

                $scope.inProgress['comment' + timeline_id] = true;

                var data = {
                    timeline_id: timeline_id,
                    fb_uid: $rootScope.facebookInfo.id,
                    message: $scope.newComment.message[timeline_id]
                };

                if ($scope.newComment.thumbnail.length > 0) {
                    data.picture = $scope.newComment.picture + '|' + $scope.newComment.thumbnail;
                }

                $http({
                    method: 'POST',
                    url: $rootScope.apiUrl+'/timelineReply',
                    data: decodeURIComponent($.param(data)),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })

                //$http({
                //    method: 'GET',
                //    url: 'http://api.ssporting.com/timelineReply?timeline_id=' + timeline_id
                //        + '&fb_uid=' + $rootScope.facebookInfo.id
                //        + '&message=' + encodeURI($scope.newComment.message[timeline_id])
                //        + more
                //})
                    .success(function (response) {
                        console.log('comment on timeline', response);
                        if (response !== null) {
                            if (_.isUndefined($scope.timelines.main_reply[timeline_id])) {
                                $scope.timelines.main_reply[timeline_id] = []
                            }
                            $scope.timelines.main_reply[timeline_id].push(response);
                            $scope.newComment.message = {};
                            $scope.newComment.thumbnail = '';
                            $scope.newComment.picture = '';
                        } else {
                            alert('!!!!');
                        }

                        setTimeout(function () {
                            $scope.inProgress['comment' + timeline_id] = false;
                        }, inProgressTime);

                    });
            } else {
                alert('ต้องกรอกความเห็น หรือ รูปภาพ!');
            }
        }

        $scope.addNewReply = function (timeline_id, comment_id, $event) {

            if ($scope.inProgress.reply) return false;

            if (_.isUndefined($scope.newReply.message[comment_id])) {
                $scope.newReply.message[comment_id] = '';
            }
            if ($scope.newReply.message[comment_id].length > 0 || $scope.newReply.picture.length > 0 || $scope.newReply.thumbnail.length > 0) {
                var more = '';
                if ($scope.newReply.thumbnail.length > 0) {
                    more = '&picture=' + $scope.newReply.picture + '|' + $scope.newReply.thumbnail;
                }

                $scope.inProgress.reply = true;

                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/timelineReply?timeline_id=' + timeline_id
                        + '&fb_uid=' + $rootScope.facebookInfo.id
                        + '&parent=' + comment_id
                        + '&message=' + encodeURI($scope.newReply.message[comment_id])
                        + more
                })
                    .success(function (response) {
                        console.log('comment on match', response);
                        if (response !== null) {
                            if (_.isUndefined($scope.timelines.sub_reply[comment_id])) {
                                $scope.timelines.sub_reply[comment_id] = []
                            }
                            $scope.timelines.sub_reply[comment_id].push(response);
//                            comments.push(response.data);
                            $scope.newReply.id = false;
                            $scope.newReply.message = {};
                            $scope.newReply.thumbnail = '';
                            $scope.newReply.picture = '';
                        } else {
                            alert('!!!!!');
                        }

                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.inProgress.reply = false;
                            });
                        }, inProgressTime);

                    });
            } else {
                alert('ต้องกรอกความเห็น หรือ รูปภาพ!');
            }
        }

        $scope.removeTimeline = function (id, $event) {
            if ($event) $event.preventDefault();
            var conf = confirm('Sure ?');
            if (conf) {
                $http({
                    method: 'get',
                    url: $rootScope.apiUrl+'/removeTimeline?id=' + id + '&fb_uid=' + $scope.facebookInfo.id
                })
                    .success(function (response) {
                        if (response.success) {
                            $scope.timelines.timelines = _.reject($scope.timelines.timelines, function (val) {
                                return val.owner.id == id;
                            });
                        } else {
                            alert(response.desc);
                        }
                    });
            }
        }

        $scope.removeReply = function (id, parentId, ownerId) {
            var conf = confirm('Sure ?');
            if (conf) {
                $http({
                    method: 'get',
                    url: $rootScope.apiUrl+'/removeTimelineReply?id=' + id + '&fb_uid=' + $scope.facebookInfo.id
                })
                    .success(function (response) {
                        if (response.success) {
                            if (!parentId) {
                                $scope.timelines.main_reply[ownerId] = _.reject($scope.timelines.main_reply[ownerId], function (val) {
                                    return val.id == id;
                                });
                            } else {
                                $scope.timelines.sub_reply[parentId] = _.reject($scope.timelines.sub_reply[parentId], function (val) {
                                    return val.id == id;
                                });
                            }
                        } else {
                            alert(response.desc);
                        }
                    });
            }

        }

        $scope.likeTimeline = function (timeline_id, index) {

            if (!_.isUndefined(alreadyLike[timeline_id])) return false;

            if (inProgress.like) return;
            inProgress.like = true;

            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/timelineLike?fb_uid=' + $rootScope.facebookInfo.id
                    + '&timeline_id=' + timeline_id
            })
                .success(function (response) {
                    if ((response.success && response.success != '0') || response.success == '1') {
                        $scope.timelines.timelines[index].content[0].like = response.timelines[0].content[0].like;
//                        $scope.timelines.timelines[index].content[0].like++;
                    } else {
                        alert(response.desc);
                    }

                    alreadyLike[timeline_id] = true;

                    setTimeout(function () {
                        inProgress.like = false;
                    }, inProgressTime);

                });
        }

        $scope.likeTimelineComment = function (comment_id, index) {

            if (!_.isUndefined(alreadyLike[comment_id])) return false;

            if (inProgress.like) return;
            inProgress.like = true;

            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/timelineReplyLike?fb_uid=' + $rootScope.facebookInfo.id + '&reply_id=' + comment_id
            })
                .success(function (response) {
                    if ((response.success && response.success != '0') || response.success == '1') {
                        if (response.parent_id == '0') {
                            $scope.timelines.main_reply[response.reply_to][index] = response;
//                            $scope.timelines.main_reply[response.reply_to][index].like++;
                        } else {
                            $scope.timelines.sub_reply[response.parent_id][index] = response
//                            $scope.timelines.sub_reply[response.parent_id][index].like++;
                        }
                    } else {
                        alert(response.desc);
                    }

                    alreadyLike[comment_id] = true;

                    setTimeout(function () {
                        inProgress.like = false;
                    }, inProgressTime);

                });
        }

        $scope.removeTimelineUploadPicture = function (index) {
            $scope.newTimeline.thumbnail.splice(index, 1);
            $scope.newTimeline.picture.splice(index, 1);
        }

        $scope.removeTimelineCommentUploadPicture = function () {
            $scope.newComment.picture = '';
            $scope.newComment.thumbnail = '';
        }

        $scope.removeTimelineReplyUploadPicture = function () {
            $scope.newReply.picture = '';
            $scope.newReply.thumbnail = '';
        }

        $scope.setTimelineActiveTo = function ($event, timeline_id) {
            $event.preventDefault();
            $scope.timelineActive = timeline_id;
            return;
        }

        $scope.parseTeamStat = function (stat, tid, attr) {
            var f = _.findWhere(stat, {tid: tid});
            if(!_.isUndefined(f)) {
                switch (attr) {
                    case 'number':
                        if (!_.isUndefined(f) && !_.isUndefined(f[attr])) return '[' + f[attr] + ']';
                        else return '';
                        break;
                    case 'lastplay':

                        if (!_.isUndefined(f) && !_.isUndefined(f[attr])) {
                            if (f[attr] == null) return [];
                            else return angular.fromJson(f[attr]);
                        } else {
                            return [];
                        }
                        break;
                    default:
                        return '';
                }
            }
        }

        $scope.browsePictureTimeline = function () {
            $timeout(function(){
                angular.element('#timeline-upload-browse-timeline').trigger('click');
            }, 100);

        }

        $scope.uploadPictureTimeline = function ($files) {
            console.log('upload');
            for (var i = 0; i < $files.length; i++) {
                var file = $files[i];

                $scope.upload = $upload.upload({
                    url: 'api/upload/cover',
                    method: 'POST',
                    data: {
                        fb_uid: $rootScope.facebookInfo.id
                    },
                    file: file
                }).progress(function (evt) {
                    $scope.inProgress.uploadTimeline = true;
                    console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                }).success(function (data, status, headers, config) {
//                $scope.profileInfo.cover = data.path;
                    console.log('upload', data);
                    if (data.success) {
                        $scope.newTimeline.thumbnail.push(data.thumbnail);
                        $scope.newTimeline.picture.push(data.path);
                    }

                    $scope.inProgress.uploadTimeline = false;
                });
            }
        }

        $scope.browsePicture = function (timeline_id) {
            $scope.newComment.id = timeline_id;
            $timeout(function(){
                angular.element('#timeline-upload-browse').trigger('click');
                angular.element('#comment-box-' + timeline_id).focus();
            }, 100);

        }

        $scope.uploadPicture = function ($files) {
            console.log('upload');
            var file = $files[0];
            $scope.upload = $upload.upload({
                url: 'api/upload/cover',
                method: 'POST',
                data: {
                    fb_uid: $rootScope.facebookInfo.id
                },
                file: file
            }).progress(function (evt) {
//                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).success(function (data, status, headers, config) {
//                $scope.profileInfo.cover = data.path;
                console.log('upload', data);
                $scope.newComment.thumbnail = data.thumbnail;
                $scope.newComment.picture = data.path;

            });
        }

        $scope.browsePictureReply = function (comment_id) {
            $scope.newReply.id = comment_id;
            $timeout(function(){
                angular.element('#timeline-upload-browse-reply').trigger('click');
                angular.element('#comment-box-' + comment_id).focus();
            }, 100);

        }

        $scope.uploadPictureReply = function ($files) {
            var file = $files[0];
            $scope.upload = $upload.upload({
                url: 'api/upload/cover',
                method: 'POST',
                data: {
                    fb_uid: $rootScope.facebookInfo.id
                },
                file: file
            }).progress(function (evt) {
//                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).success(function (data, status, headers, config) {
//                $scope.profileInfo.cover = data.path;
                console.log('upload', data);
                $scope.newReply.thumbnail = data.thumbnail;
                $scope.newReply.picture = data.path;
                console.log('newReply', $scope.newReply);

            });
        }

        if ($scope.fb_uid) {

            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/facebookInfo/' + $scope.fb_uid
            })
                .success(function (response) {
                    $rootScope.localFacebookInfo = response;
                    $rootScope.localFacebookInfo.play = parseInt($rootScope.localFacebookInfo.w) + parseInt($rootScope.localFacebookInfo.d) + parseInt($rootScope.localFacebookInfo.l);
                    $rootScope.localFacebookInfo.pta = (parseInt($rootScope.localFacebookInfo.w) / (parseInt($rootScope.localFacebookInfo.pts))) * 100;

                });

            $scope.refreshTimeline();
            $scope.refreshMatchFeed();
            $scope.refreshNewFeed();
            $scope.getFollowing();
            $scope.getFollower();
            $scope.getPlays();
            $scope.getAchieve();

            angular.element('#fetching-data').hide();
            angular.element('#fetched-data').show();

        } else {
            $rootScope.$watch('facebookInfo', function (response) {
                if (!$scope.fb_uid && !_.isUndefined(response) && !_.isUndefined(response.id)) {
                    $scope.fb_uid = response.id;

                    $http({
                        method: 'GET',
                        url: $rootScope.apiUrl+'/facebookInfo/' + $scope.fb_uid
                    })
                        .success(function (response) {
                            $rootScope.localFacebookInfo = response;
                            $rootScope.localFacebookInfo.play = parseInt($rootScope.localFacebookInfo.w) + parseInt($rootScope.localFacebookInfo.d) + parseInt($rootScope.localFacebookInfo.l);
                            $rootScope.localFacebookInfo.pta = (parseInt($rootScope.localFacebookInfo.w) / (parseInt($rootScope.localFacebookInfo.pts))) * 100;

                        });

                    $scope.refreshTimeline();
                    $scope.refreshMatchFeed();
                    $scope.refreshNewFeed();
                    $scope.getFollowing();
                    $scope.getFollower();
                    $scope.getPlays();
                    $scope.getAchieve();

                    angular.element('#fetching-data').hide();
                    angular.element('#fetched-data').show();

                }
            });
        }


        //region update user info

        $scope.editDisplayname = function () {
            $scope.editing_displayname = true;
            $scope.edited_displayname = $scope.localFacebookInfo.display_name;
            console.log($scope.editing_displayname);

        }

        $scope.updateDisplayname = function () {
            if ($scope.edited_displayname && $scope.edited_displayname != $scope.localFacebookInfo.display_name) {
                $scope.localFacebookInfo.display_name = $scope.edited_displayname;
                $http({
                    method: 'GET',
//                    url: $rootScope.wsUrl + '/updateUserStatus?fb_uid=' + $rootScope.facebookInfo.id + '&status=' + $scope.edited_displayname
                    url: $rootScope.apiUrl+'/setDisplayName?fb_uid=' + $rootScope.facebookInfo.id + '&name=' + $scope.edited_displayname
                })
                    .success(function (response) {

                    });
            }

            $scope.editing_displayname = false;
        }

        $scope.editStatus = function () {
            $scope.editing_status = true;
            $scope.edited_status = $scope.localFacebookInfo.mind;
        }

        $scope.updateStatus = function () {
            if ($scope.edited_status && $scope.edited_status != $scope.localFacebookInfo.mind) {
                $scope.localFacebookInfo.mind = $scope.edited_status;
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/setMind?fb_uid=' + $rootScope.facebookInfo.id + '&message=' + encodeURI($scope.edited_status)
                })
                    .success(function (response) {

                    });
            }

            $scope.editing_status = false;
        }

        $scope.editSite = function () {
            $scope.editing_site = true;
            $scope.edited_site = $scope.localFacebookInfo.site;
        }

        $scope.updateSite = function () {
            if ($scope.edited_site && $scope.edited_site != $scope.localFacebookInfo.site) {
                $scope.localFacebookInfo.site = $scope.edited_site;
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/setSite?fb_uid=' + $rootScope.facebookInfo.id + '&site=' + $scope.edited_site
                })
                    .success(function (response) {

                    });
            }

            $scope.editing_site = false;
        }
        //endregion

        //region upload
        $scope.uploadCover = function () {
            $timeout(function(){
                angular.element('#upload-browse').trigger('click');
            }, 100);

        }

        $scope.updateCover = function ($files) {
            var file = $files[0];
            $scope.upload = $upload.upload({
                url: 'api/upload/cover',
                method: 'POST',
                data: {
                    fb_uid: $rootScope.facebookInfo.id
                },
                file: file
            }).progress(function (evt) {
//                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).success(function (data, status, headers, config) {
                $scope.localFacebookInfo.cover = data.path;
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/setCover?fb_uid=' + $rootScope.facebookInfo.id + '&picture=' + data.path
                })
                    .success(function (response) {

                    });

            });
        }
        //endregion

        $scope.openReplyBox = function (id) {
            angular.element('.reply-box').hide();
            angular.element('[data-comment-id="' + id + '"] .reply-box').fadeIn();
        }

        $scope.canPlay = function (mid, sid, hdp) {
            if (sid == '1' && hdp != '' && (_.isUndefined($scope.myBet) || _.isUndefined($scope.myBet[mid]))) {
                return true;
            } else {
                return false;
            }
        }

        $scope.seeMore = {};

        $scope.gotoCommentBox = function (id, $event) {
            $event.preventDefault();
            console.log(angular.element($event.currentTarget).next('.s-comment-box'));
            angular.element('#comment-box-' + id).focus()
        }

        $scope.getHeight = function (id) {
            return parseInt(angular.element('[data-message-id="' + id + '"]').height());
        }

        $scope.freeHeight = function (id) {
            if (_.isUndefined($scope.seeMore[id]) || !$scope.seeMore[id]) {
                $scope.seeMore[id] = true;
            } else {
                $scope.seeMore[id] = false;
            }
//            angular.element('[data-message-id="' + id + '"]').css({ 'max-height': 'auto' });
        }

        $scope.getTrophy = function (action, limit) {
            $scope.trophyList = [];
            if (_.isUndefined($scope.trophyCurrentPage)) $scope.trophyCurrentPage = 1;
            if (_.isUndefined(limit)) limit = 8;

            $scope.trophyPages = Math.ceil($scope.localFacebookInfo.achieve.length / limit);

            if (action === 'next') {
                $scope.trophyCurrentPage++;
            } else if (action === 'prev') {
                $scope.trophyCurrentPage--;
                console.log($scope.trophyPages);
            }

            if ($scope.trophyCurrentPage < 1) $scope.trophyCurrentPage = $scope.trophyPages;
            else if ($scope.trophyCurrentPage > $scope.trophyPages) $scope.trophyCurrentPage = 1;

            angular.forEach($scope.localFacebookInfo.achieve, function (val, idx) {
                if ((idx + 1) >= (($scope.trophyCurrentPage * limit - limit)) && (idx + 1) <= ($scope.trophyCurrentPage * limit)) {
                    $scope.trophyList.push(val);
                }
            });

        }

        $scope.viewTrophy = function (item) {
            $scope.selectedTrophy = item;
            console.log(item);
            if(item.type === 'wc') {
                angular.element('#selected-trophy-wc').fadeIn();
            } else {
                angular.element('#selected-trophy').fadeIn();
            }

        };

        $scope.closeTrophy = function () {
            angular.element('#selected-trophy').fadeOut();
            angular.element('#selected-trophy-wc').fadeOut();
        };

        $scope.$watch('facebookInfo', function (newVal) {
            if (!_.isUndefined(newVal) && !_.isUndefined(newVal.id)) {
                $scope.getCurrentUserFollowing();

            }
        });

        $scope.$watch('localFacebookInfo', function (newVal) {
            console.log('!!!', newVal);
            if (!_.isUndefined(newVal) && !_.isUndefined(newVal.achieve)) {
                $scope.getTrophy();
            }
        });

        $scope.refreshNewsSlide();

        $scope.focusBox = function ($event) {
            angular.element($event.currentTarget).addClass('tabPost-expand');
        };

        $scope.blurBox = function ($event) {
            angular.element($event.currentTarget).removeClass('tabPost-expand');
        };

        $scope.$watchCollection('activeTopicType', function (newVal, oldVal) {
            if (!_.isUndefined(newVal) && !_.isUndefined(oldVal) && newVal !== oldVal) {
                $scope.refreshTimeline();
            }
        });

        $scope.MoreTimeLine = function () {
            $scope.more=$scope.more+1;
            var limit=10,scrollTop=0;
            if($scope.more!=0){
                limit=limit+($scope.more*10);
                scrollTop=$(window).scrollTop();
            }

            $scope.moreloading=true;
            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/getTimelines?fb_uid=' + $scope.fb_uid + '&limit=10 &offset='+(limit-10)
                //url: 'http://api.ssporting.com/getTimelines?fb_uid=' + $scope.fb_uid + '&limit=40' + more
            })
                .success(function (response) {
                    if($scope.more==1){
                        $scope.timelines1 = response;
                    }else if($scope.more==2){
                        $scope.timelines2 = response;
                    }else if($scope.more==3){
                        $scope.timelines3 = response;
                    }else if($scope.more==4){
                        $scope.timelines4 = response;
                    }else if($scope.more==5){
                        $scope.timelines5 = response;
                    }
                    $scope.moreloading=false;
            });
        }

    });