angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope, $scope, $location, $http, $interval, $cookieStore) {
        $scope.ready = true;
        $scope.matchFilter = false;
        $scope.matchOdds = {};
        $scope.matchOddsCount = 0;

        $scope.getLiveMatchOnly = function () {
            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/liveMatchOnly'
                //url: 'api/ws/liveMatchOnly'
            })
                .success(function (response) {
                    $scope.live_match = response;

                    angular.element('#temp-live-match').remove();
                    angular.element('#live_match_table').show();
                });
        }

        $scope.getLiveMatchWaitOnly = function () {
            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/liveMatchWaitOnly'
                //url: 'api/ws/liveMatchWaitOnly'
            })
                .success(function (response) {
                    $scope.live_match_wait = response;

                    angular.element('#temp-live-match-wait').remove();
                    angular.element('#live_match_wait_table').show();
                });
        }

        $scope.hasMatchNotification = function (id, wait) {

            if (_.isUndefined(wait)) wait = false;

            var response = false;

            if (wait) {
                angular.forEach($scope.live_match_wait_sorted.live_league[id].matches, function (val, key) {
                    if ($scope.matchNotifications[val.mid]) {
                        response = true;
                        return;
                    }
                });
            } else {
                angular.forEach($scope.live_match_sorted.live_league[id].matches, function (val, key) {
                    if ($scope.matchNotifications[val.mid]) {
                        response = true;
                        return;
                    }
                });
            }


            return response;
        }


        $scope.y = function (cx) {
            if (parseInt(cx) > 0) {
                return parseInt(cx);
            } else {
                return 45;
            }
        }

        $scope.currentMin = function (c0, c1, c2, cx, sid) {

            var str = '-';
            var ap;
            var d = new Date();
            var offset = $scope.gmtOffset * 60000;
            var ao;
            var label = {
                '1': 'schedule',
                '2': 'RUN',
                '3': 'HT',
                '4': 'RUN',
                '5': 'FT',
                '6': 'ET',
                '7': 'Pen.',
                '8': 'AET',
                '9': 'AP',
                '10': 'ABD',
                '11': 'Postp.',
                '12': 'FT only',
                '13': "Susp.",
                '14': "Pen.",
                '15': "AP",
                '16': "W.O.",
                '17': "ANL"
            }

            if (typeof fix == 'undefined') {
                var fix = 0;
            }
            if (sid == 2) {
                ap = $scope.y(cx);
                ao = (((d.getTime()/*+offset*/) / 1000) - parseInt(c0) - parseInt(c1)) / 60;
                if (ao < 1) {
                    str = 1 + "'";
                } else if (ao > ap) {
                    str = Math.floor(ap) + fix + "+'";
                } else {
                    str = Math.floor(ao) + fix + "'";
                }
            } else if (sid == 4) {
                ap = $scope.y(cx);
                ao = Math.floor((((d.getTime()/*+offset*/) / 1000) - parseInt(c0) - parseInt(c2)) / 60 + ap);
                if (ao <= ap) {
                    str = Math.floor(ap + 1) + fix + "'";
                } else if (ao > ap) {
                    ap = Math.floor(ap * 2);
                    if (ao > ap) {
                        str = ap + "+'";
                    } else {
                        str = ao + fix + "'";
                    }
                }
            } else {
                if (!_.isUndefined(label[sid])) str = label[sid];
            }

            return str;

        }

        $scope.hasOdds = function (mid, leagueId, tid1, tid2) {

            if (_.isUndefined($scope.odds) || _.isUndefined($scope.odds.data)) return false;
            else {
                console.log('hasOdds', leagueId + '_' + tid1 + '_' + tid2, !_.isUndefined($scope.odds.data[leagueId + '_' + tid1 + '_' + tid2]));
                if (!_.isUndefined($scope.odds.data[leagueId + '_' + tid1 + '_' + tid2])) {
                    $scope.matchOdds[mid] = true;
                    $scope.matchOddsCount = _.size($scope.matchOdds);
                    return true;
                }

            }
        }


        $scope.$watch('matchFilter', function (newVal, oldVal) {
            if (newVal === 'bet') {

            }
        });

        $rootScope.$watch('matchOddsCount', function () {
            $scope.matchOddsCount = $rootScope.matchOddsCount;
        })


        $scope.adsViewAt = $cookieStore.get('ads-view-at');


        if($scope.adsViewAt !== moment().format('dddd')) {
            angular.element('#display-Ads').fadeIn(600);
            $cookieStore.put('ads-view-at', moment().format('dddd'));
        }


        $rootScope.closeAds = function () {
            angular.element('#display-Ads').fadeOut(300);
        }

        $scope.$watch('userInfo', function () {
            if (!_.isUndefined($scope.userInfo)) {

                $rootScope.getDailyBonus = function () {
                    $http({
                        method: 'get',
                        url: $rootScope.apiUrl+'/dailyBonus/' + $scope.userInfo.uid
                        //url: 'api/ws/dailyBonus/' + $scope.userInfo.uid
                    })
                        .success(function (response) {
                            var dailyBonusViewAt = $cookieStore.get('daily-bonus-view-at-' + $scope.facebookInfo.id);
                            if (dailyBonusViewAt !== moment().format('dddd')) {
                                $rootScope.dailyBonus = response;
                                angular.element('#daily-bonus').fadeIn();
                                $cookieStore.put('daily-bonus-view-at-' + $scope.facebookInfo.id, moment().format('dddd'));
                            }
                        });
                }

                $rootScope.getDailyReport = function () {
                    $http({
                        url: $rootScope.apiUrl+'/dailyAlert/' + $scope.facebookInfo.id
                        //url: 'api/ws/dailyAlert/' + $scope.facebookInfo.id
                    })
                        .success(function (response) {
                            $rootScope.dailyReport = response;

                            var dailyReportViewAt = $cookieStore.get('daily-report-view-at-' + $scope.facebookInfo.id);

                            if (dailyReportViewAt !== moment().format('dddd')) {
                                angular.element('#daily-report').fadeIn();
                                $cookieStore.put('daily-report-view-at-' + $scope.facebookInfo.id, moment().format('dddd'));
                            } else {
                                $rootScope.getDailyBonus();
                            }
                        });
                }

                $rootScope.closeDailyReport = function () {
                    angular.element('#daily-report').fadeOut();
                    $rootScope.getDailyBonus();
                }

                $rootScope.closeDailyBonus = function () {
                    angular.element('#daily-bonus').fadeOut();
                }

                $rootScope.getDailyReport();
            }
        });

    });