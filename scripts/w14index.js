angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($scope, $location, $http) {


        $scope.refreshNewsSlide = function () {
            $http({
                method: 'get',
                url: 'api/ws/dailyNews?lang=' + $scope.language + '&limit=8'
            })
                .success(function (response) {
                    $scope.news = response;

                    var pp = 4;
                    var mm = [];
                    for (var i = 0; i <= response.data.length; i++) {
                        if (!_.isUndefined(response.data[i]) && !_.isUndefined(response.ontimelines[response.data[i].newsId])) {

                            if (mm.length === 0) mm.push([]);
                            if (mm[mm.length - 1].length >= pp) mm.push([]);
                            mm[mm.length - 1].push(response.data[i]);

                        }
                    }

                    var i = 0;
                    if (mm.length > 0) {
                        clearInterval($scope.newsTopSlideInterval);
                        $scope.newsTopSlide = mm[i];
                        angular.element('#news-top-slide-box').fadeIn();

                        $scope.newsTopSlideInterval = setInterval(function () {
                            $scope.$apply(function () {
                                i++;
                                if (_.isUndefined(mm[i])) i = 0;
                                $scope.newsTopSlide = mm[i];

                            });
                        }, 5000);
                    }
                });
        }

        $scope.upcomingMatch = function () {
            var el = angular.element('.upcoming-match');
            var l = el.length;
            var start = 0;

            if(l > 2) {
                setInterval(function(){
                    $scope.$apply(function(){
                        angular.element('.upcoming-match').hide();
                        for (i=start; i<(start+2); i++) {
                            angular.element('.upcoming-match[data-idx="' + i + '"]').fadeIn();
                        }
                        start = i;
                        if(start >= l) start = 0;
                    });
                }, 10000);
            } else {
                angular.element('.upcoming-match').fadeIn();
            }
        }

        $scope.refreshNewsSlide();

        $scope.upcomingMatch();


        angular.element('.dm-select').bind('click', function(e){
            e.preventDefault();
            var d = angular.element(e.currentTarget).attr('data-d');

            angular.element('.dm-select').removeClass('activeDate');
            angular.element('.dm-data').hide();
            angular.element('.dm-select[data-d="' + d + '"]').addClass('activeDate');
            if(d === 'all') {
                angular.element('.dm-data').fadeIn();
            } else {
                angular.element('.dm-data[data-ds="' + d + '"]').fadeIn();
            }

        });

        $scope.rankRating = function(w,d,l){
            w = parseInt(w);
            d = parseInt(d);
            l = parseInt(l);

            return parseFloat(((w/(w+d+l)) * 100).toFixed(2) + 0);
        }

    });