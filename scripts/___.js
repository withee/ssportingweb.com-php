angular.module('scorspot.comApp', ['scorspot.controller', 'ngRoute', 'ngCookies', 'ezfb', 'ngAudio', 'angularFileUpload', 'naturalSort', 'ui.bootstrap', 'LocalStorageModule'])
    .config(function ($FBProvider, $httpProvider) {

        var url = purl();

        if (url.attr('host') === 'ssporting.com' || url.attr('host') === 'www.ssporting.com') {
            $FBProvider.setInitParams({
                appId: '400809029976308'
            });
        } else {
            $FBProvider.setInitParams({
                appId: '400809029976308'
            });
        }

        //if (!$httpProvider.defaults.headers.get) {
        //    $httpProvider.defaults.headers.get = {};
        //}
        //
        //$httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    })
    .filter('kickTime', function ($rootScope) {
        return function (time) {
            return moment(time).add('hours', $rootScope.gmtOffset).format("HH:mm");
        };
    })
    .filter('toFixed', function () {
        return function (num, pos) {
            if (_.isUndefined(pos)) {
                pos = 2;
            }
            num = parseFloat(num);
            return num.toFixed(pos);
        }
    })
    .filter('range', function () {
        return function (input, total) {
            total = parseInt(total);
            for (var i = 0; i < total; i++)
                input.push(i);
            return input;
        };
    })
    .filter('formatScore', function () {
        return function (score) {
            var pattern = new RegExp(/\d-\d/);
            if (score != undefined) {
                if (pattern.test(score)) {
                    var s = score.split('-');
                    if (s[0].length == 0 || s[1].length == 0) {
                        return '0 - 0';
                    }

                    return s[0] + ' - ' + s[1];
                } else {
                    return score;
                }

            }
        };
    })
    .filter('parseOdds', function () {
        return function (odds, s) {
            if (odds) {
                var p = JSON.parse(odds);
                if (s == 'home' || s == 'away') {
                    return p['hdp_' + s];
                } else {
                    return p[s];
                }
            }
        };
    })
    .filter('parseScore', function () {
        return function (player, playerOrder) {
            var rMap = {
                'pok9': 'ป๊อก 9',
                'pok8': 'ป๊อก 8',
                'deng2': 'สองเด้ง',
                'deng3': 'สามเด้ง',
                'yellow3': 'สามเหลือง',
                'tong3': 'ตอง',
                'riang': 'เรียง',
                'straightFlush': 'เรียง+สี'
            };

            var p = JSON.parse(player);
            var playerScore;
            if (playerOrder == 1) {
                playerScore = p.player1Score;
            } else {
                playerScore = p.player2Score;
            }
            var num = playerScore.num;
            var special = playerScore.special;
            var mul = playerScore.mul;

            var scoreStr = "";
            if (special != "none") {//special
                if (special == "deng3") {
                    if (num == 0) {
                        scoreStr = "บอด " + rMap['deng3'];
                    } else {
                        scoreStr = num + " แต้ม " + rMap['deng3'];
                    }
                } else if (special == "deng2") {
                    if (num == 0) {
                        scoreStr = "บอด " + rMap['deng2'];
                    } else {
                        scoreStr = num + " แต้ม " + rMap['deng2'];
                    }
                } else {
                    scoreStr = rMap[special];
                    if (mul == 2) {
                        scoreStr += " " + rMap['deng2'];
                    }
                }
            } else {//regular
                if (num == 0) {
                    scoreStr = "บอด";
                } else {
                    scoreStr = num + " แต้ม";
                }

                if (mul == 2) {
                    scoreStr += " " + rMap['deng2'];
                }
            }
            return scoreStr;
        };
    })
    .filter('NaNto0', function () {
        return function (num) {
            console.log('xxx', num);
            if (num === 'NaN') {
                return 0;
            }
        }
    })
    .filter('formatDate', function ($rootScope) {
        return function (str, format) {
            return moment(str).add('hours', $rootScope.gmtOffset).lang('th').format(format);
        }
    })
    .filter('timeago', function ($rootScope) {
        return function (time) {
            return moment(time * 1000).fromNow();
        };
    })
    .filter('timeagoByDateTime', function () {
        return function (time) {
            return moment(time, 'YYYY-MM-DD HH:mm:ss z').add('hours', this.gmtOffset).fromNow();
        }
    })
    .filter('scoreSelect', function () {
        return function (score, side) {
            if (score != undefined && score != '') {
                var s = score.split('-');
                if (side === 'home')
                    return s[0];
                else
                    return s[1];
            }
            return '';
        }
    })
    .filter('fromInitial', function () {
        return function (s) {
            switch (s) {
                case 'w':
                    return 'win';
                case 'l':
                    return 'lose';
                case 'd':
                    return 'draw';
                case 'w_h':
                    return 'win_half';
                case 'l_h':
                    return 'lose_half';
            }
        }
    })
    .filter('sid2string', function () {
        return function (sid) {
            switch (sid) {
                case '1':
                    return 'Schedule';
                case '2':
                    return 'RUN';
                case '3':
                    return 'HT';
                case '4':
                    return 'RUN';
                case '5':
                    return 'FT';
                case '6':
                    return 'ET';
                case '7':
                    return 'PK';
                case '8':
                    return 'AET';
                case '9':
                    return 'AP';
                case '10':
                    return 'ABD';
                case '11':
                    return 'Postp.';
                case '12':
                    return 'FT only.';
                case '13':
                    return 'Susp.';
                case '14':
                    return 'PK';
                case '15':
                    return 'AP';
                case '16':
                    return 'W.O.';
                case '17':
                    return 'ANL';
            }
        }
    })
    .filter('countz', function () {
        return function (obj) {
            return _.size(obj);
        }
    })
    .filter('nl2br', function ($sce) {
        return function (text) {
            return text ? $sce.trustAsHtml(text.replace(/\n/g, '<br/>')) : '';
        };
    })
    .filter('toTrusted', ['$sce', function ($sce) {
        return function (text) {
            return $sce.trustAsHtml(text);
        };
    }])
    .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .directive([ 'focus', 'blur', 'keyup', 'keydown', 'keypress' ].reduce(function (container, name) {
        var directiveName = 'ng' + name[ 0 ].toUpperCase() + name.substr(1);

        container[ directiveName ] = [ '$parse', function ($parse) {
            return function (scope, element, attr) {
                var fn = $parse(attr[ directiveName ]);
                element.bind(name, function (event) {
                    scope.$apply(function () {
                        fn(scope, {
                            $event: event
                        });
                    });
                });
            };
        } ];

        return container;
    }, { }))
    .directive('tooltip', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                $(element)
                    .attr('title', scope.$eval(attrs.tooltip))
                    .tooltip({placement: "top"});
            }
        }
    })
    .run(function ($rootScope, $http, $cookieStore, $FB, $location, $interval, ngAudio, $timeout, $upload, localStorageService) {

        var url = purl();
        var protocol = url.attr('protocol');
        $rootScope.protocol = protocol;
        if (protocol === 'https') {
            //$rootScope.apiUrl = 'api/ws'
            $rootScope.apiUrl='https://api.ssporting.com'
        } else {
            //$rootScope.apiUrl = 'http://api.ssporting.com'
            $rootScope.apiUrl='http://api.ssporting.com'
        }

        $rootScope.status=true;
        //region global
        $rootScope.highlightLeague = ['34885', '35909', '37120', '35980', '35998'];
        $rootScope.topRankingLeagueSelect = '34885';
        $rootScope.sup = 100007730416796;
        $rootScope.language = document.getElementById('app-config').getAttribute('language');
        $rootScope.gmtOffset = document.getElementById('app-config').getAttribute('gmtOffset');

        $rootScope.highlightCompetitionOnly = true;
        $rootScope.competitionToggleStatus = {};
        $rootScope.matchNotifications = {};
        //$rootScope.logoUrl = 'http://beta.ssporting.com/uploads/media/teams_clean';
        $rootScope.logoUrl = $rootScope.apiUrl+'/teams_clean/team_default';

        $rootScope.uuid = $cookieStore.get('uuid');
        //$rootScope.serverStatus={"id":"3","news":"Y","server_online":"N","title":"ปิดปรับปรุงชั่วคราว","message":"ประกาศ ทีมงาน ssporting ขอเรียนแจ้งผู้เล่นทุกท่านให้ทราบว่า เนื่องจากวันอังคารที่ 1 ธันวาคม 2558 ทีมงานจะทำการปิดปรับปรุงเซิร์ฟเวอร์ชั่วคราว ตั้งแต่เวลา 05.00 - 12.00 น. เพื่อดำเนินการปรับปรุงประสิทธิภาพเซิร์ฟเวอร์ให้ดียิ่งขึ้น พร้อมทำการย้ายเซิร์ฟเวอร์ในส่วนฐานข้อมูลใหม่ ซึ่งจะส่งผลให้ผู้เล่นไม่สามารถใช้งานในส่วนต่างๆ เป็นการชั่วคราวดังนี้ •ไม่สามารถเข้าเล่นเกมทายผลบอลได้ในช่วงเวลาดังกล่าว •ไม่สามารถเข้าใช้งานเว็บไซต์และเว็บบอร์ด www.ssporting.com ","created_at":"2015-11-13"}
        $rootScope.statusMessage = $cookieStore.get('statusMessage');
        angular.element('#boxMessage').show();
        angular.element('#BgBoxMessage').show();

        $rootScope.changeStatusMessage=function(){
            $cookieStore.put('statusMessage', true);
            $rootScope.statusMessage=true;
        }

        /** serverstatus **/
        $http({
            url: $rootScope.apiUrl + '/serverstatus'
        })
            .success(function (response) {
                $rootScope.serverStatus = response;
            });

        if (!$rootScope.uuid) {
            $rootScope.uuid = createUUID();
            $cookieStore.put('uuid', $rootScope.uuid);
        }

        $rootScope.login = function () {
            $FB.login(function (response) {
                $rootScope.loginStatus = response;
            }, {scope: 'email,user_friends,user_about_me,user_activities,user_birthday,user_groups,user_interests,user_likes,friends_about_me,friends_activities,friends_birthday,friends_likes,user_online_presence,friends_online_presence,publish_actions,publish_stream,publish_actions'});
        }

        $FB.getLoginStatus(function (response) {
            console.log('FB', response);
            $rootScope.loginStatus = response;

            if (response.status === 'connected') {
                angular.element('.wait-for-facebook-session').fadeIn();
                $rootScope.facebookAccessToken = response.authResponse.accessToken;

                //$FB.api('/me/permissions', function (response) {
                //    console.log('permissions', response.data);
                //    if (_.isUndefined(response.data[0].publish_stream)) {
                //
                //        $FB.ui({
                //            method: 'permissions.request',
                //            perms: 'publish_stream',
                //            display: 'popup'
                //        }, function (response) {
                //            console.log('req perms', response);
                //        });
                //
                //    }
                //});
            }
        });

        $rootScope.getLiveMatchWaitOnlySorted = function () {
            $http({
                method: 'get',
                url: $rootScope.apiUrl + '/sortedliveMatchWaitOnly/' + $rootScope.language
            })
                .success(function (response) {
                    if (response.length === 0) {
                        //console.log('wait empty!');
                        alert('wait empty')
                        $rootScope.status=false;
                        return;
                    }

                    $rootScope.live_match_wait_sorted = response;
                    $rootScope.matchOddsCount = 0;
                    angular.forEach(response.live_league, function (val, key) {
                        $rootScope.matchOddsCount += val.playable;
                        //console.log(val)
                        //console.log(val.data.leagueId+':'+val.playable);
//                        angular.forEach(val.matches, function (v, k) {
//                            if ($rootScope.matchNotifications[v.mid]) {
//                                val.hasNotification = true;
//                                return;
//                            }
//
//                        });
                    });
                    angular.element('#temp-live-match-wait').remove();
                    angular.element('#live_match_wait_table').show();
                }).error(function(error){
                    $rootScope.status=false;
                });;
        }

        /** board **/

        $rootScope.defaultValue = {
            type: 6
        };

        $http({
            url: $rootScope.apiUrl + '/getTopictype'
        })
            .success(function (response) {
                $rootScope.topicType = response;
                $rootScope.selectedTopicType = $rootScope.topicType[$rootScope.defaultValue.type];
                angular.element('#basic-left-menu').hide();
                angular.element('#webboard-filter').show();
            });

        $rootScope.isActiveTopicType = function (id) {
            var n = _.findWhere($rootScope.activeTopicType, {fid: id});
            if (_.isUndefined(n)) return false;
            else return n;
        }

        $rootScope.getActiveTopicTypeSettings = function () {
            $http({
                url: $rootScope.apiUrl + '/getCatview/' + $rootScope.userInfo.uid
            })
                .success(function (response) {
                    $rootScope.activeTopicType = response;
                    console.log('!!!', response);
                });
        }

        $rootScope.toggleActiveTopicType = function (id) {
            angular.element('#board-filter-loading').show();
            if ($rootScope.isActiveTopicType(id)) {
                $rootScope.activeTopicType = _.reject($rootScope.activeTopicType, function (o) {
                    return o.fid === id
                });

            } else {
                $rootScope.activeTopicType.push(_.findWhere($rootScope.topicType, {fid: id}));
            }

            $http({
                method: 'get',
                url: $rootScope.apiUrl + '/setCatview/' + $rootScope.userInfo.uid + '/' + $rootScope.getActiveTopicType('fid').join(',')
            })
                .success(function (response) {
                    $timeout(function () {
                        angular.element('#board-filter-loading').hide();
                    }, 2000);

                });
        }

        $rootScope.getActiveTopicType = function (attr) {
            if (_.isUndefined(attr)) attr = 'fid';

            return _.pluck($rootScope.activeTopicType, attr);
        }


        $rootScope.hasVideo = function (medialist) {
            if (!_.isUndefined(medialist[0]) && !_.isUndefined(medialist[0].path)) {
                return $rootScope.isVideo(medialist[0].path);
            } else return false;
        }

        $rootScope.getVideoScreenShot = function (url, size) {
            size = (size === null) ? "big" : size;
            var vid;
            var results;
            if (url.match(/^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/) !== null) {
                results = url.match("[\\?&]v=([^&#]*)");
                vid = ( results === null ) ? url : results[1];

                if (size == "small") {
                    return "http://img.youtube.com/vi/" + vid + "/2.jpg";
                } else {
                    return "http://img.youtube.com/vi/" + vid + "/0.jpg";
                }
            } else if (url.match(/^.+dailymotion.com\/((video|hub)\/([^_]+))?[^#]*(#video=([^_&]+))?/) !== null) {
                results = url.match(".*\/video\/([^&#]*)");
                vid = ( results === null ) ? url : results[1];
                return "http://www.dailymotion.com/thumbnail/video/" + vid;
            } else if (url.match(/^.*(vimeo\.com\/)((channels\/[A-z]+\/)|(groups\/[A-z]+\/videos\/))?([0-9]+)/) !== null) {

            }


        }

        $rootScope.hasImage = function (medialist) {
            if (!_.isUndefined(medialist[0]) && !_.isUndefined(medialist[0].path)) {
                if (medialist[0].path.match(/\.(jpeg|jpg|gif|png)$/)) return true;
                else return false;
            } else return false;
        }

        $rootScope.isVideo = function (url) {
            return url.match(/^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/) !== null
                || url.match(/^.+dailymotion.com\/((video|hub)\/([^_]+))?[^#]*(#video=([^_&]+))?/) !== null
                || url.match(/^.*(vimeo\.com\/)((channels\/[A-z]+\/)|(groups\/[A-z]+\/videos\/))?([0-9]+)/) !== null;
        }
        /** board end **/

        $rootScope.getMyBet = function () {
            $http({
                method: 'GET',
                url: $rootScope.apiUrl + '/betLists/' + $rootScope.facebookInfo.id + '?' + new Date().getTime()
            })
                .success(function (response) {
                    $rootScope.myBet = _.indexBy(response, 'mid');
                });
        }

        $rootScope.getLastEvents = function () {
            $http({
                method: 'GET',
                url: $rootScope.apiUrl + '/getLastEvent'
            })
                .success(function (response) {
                    if (response.length === 0) {
                        //console.log('live empty!');
                        alert('LastEvent empty!')
                        $rootScope.status=false;
                        return;
                    }
                    if (response.list) {
                        $rootScope.lastEvents = response.list.reverse();
                        angular.element('#latest-event-box').show();
                    }
                }).error(function(error){
                    $rootScope.status=false;
                });
        }

        $rootScope.toggleCompetition = function (competitionId) {
            if (_.isUndefined($rootScope.competitionToggleStatus[competitionId])) {
                $rootScope.competitionToggleStatus[competitionId] = true;
            } else {
                delete $rootScope.competitionToggleStatus[competitionId];
            }
        }

        //region odds
        $http({
            method: 'GET',
            url: $rootScope.apiUrl + '/oddsToday'
        })
            .success(function (response) {
                $rootScope.odds = response;
                angular.element('#bet-list-box').show();
            });

        $rootScope.getOddsHdp = function (key, need) {
            if (!_.isUndefined($rootScope.odds.data[key])) {
                if (!_.isUndefined($rootScope.odds.data[key].SBOBET) && !_.isUndefined($rootScope.odds.data[key].SBOBET[need]) && $rootScope.odds.data[key].SBOBET[need] != '') return $rootScope.odds.data[key].SBOBET[need];
                else if (!_.isUndefined($rootScope.odds.data[key].Bet365) && !_.isUndefined($rootScope.odds.data[key].Bet365[need]) && $rootScope.odds.data[key].Bet365[need] != '') return $rootScope.odds.data[key].Bet365[need];
                else if (!_.isUndefined($rootScope.odds.data[key].Ladbrokes) && !_.isUndefined($rootScope.odds.data[key].Ladbrokes[need]) && $rootScope.odds.data[key].Ladbrokes[need] != '') return $rootScope.odds.data[key].Ladbrokes[need];
            }

            return false;
        }

        //endregion

        //region favorite

        $rootScope.favorite = function (type, obj) {
            if (type == 'league') {
                if (!_.isUndefined($rootScope.favoriteLeague[obj.leagueId])) {
                    delete $rootScope.favoriteLeague[obj.leagueId];
                    var url = $rootScope.apiUrl + '/unfavorite?id=' + obj.leagueId + '&device_id=' + $rootScope.uuid + '&platform=web&favorite_type=league&competition_id=' + obj.competitionId + '&league_id=0';
                    //var url = 'api/ws/unfavorite?id=' + obj.leagueId + '&device_id=' + $rootScope.uuid + '&platform=web&favorite_type=league&competition_id=' + obj.competitionId + '&league_id=0';
                } else {
                    $rootScope.favoriteLeague[obj.leagueId] = {
                        id: obj.leagueId,
                        competition_id: obj.competitionId,
                        name: obj.leagueName
                    };
                    var url = $rootScope.apiUrl + '/favorite?id=' + obj.leagueId + '&device_id=' + $rootScope.uuid + '&platform=web&favorite_type=league&competition_id=' + obj.competitionId + '&league_id=0';
                    //var url = 'api/ws/favorite?id=' + obj.leagueId + '&device_id=' + $rootScope.uuid + '&platform=web&favorite_type=league&competition_id=' + obj.competitionId + '&league_id=0';
                }
            } else {
                if (_.isUndefined(obj.leagueId)) obj.leagueId = 0;
                if (!_.isUndefined($rootScope.favoriteTeam[obj.tid])) {
                    delete $rootScope.favoriteTeam[obj.tid];
                    var url = $rootScope.apiUrl + '/unfavorite?id=' + obj.tid + '&device_id=' + $rootScope.uuid + '&platform=web&favorite_type=team&competition_id=0&league_id=' + obj.leagueId;
                    //var url = 'api/ws/unfavorite?id=' + obj.tid + '&device_id=' + $rootScope.uuid + '&platform=web&favorite_type=team&competition_id=0&league_id=' + obj.leagueId;
                } else {
                    $rootScope.favoriteTeam[obj.tid] = {
                        id: obj.tid,
                        name: obj.tn
                    };
                    var url = $rootScope.apiUrl + '/favorite?id=' + obj.tid + '&device_id=' + $rootScope.uuid + '&platform=web&favorite_type=team&competition_id=0&league_id=' + obj.leagueId;
                    //var url = 'api/ws/favorite?id=' + obj.tid + '&device_id=' + $rootScope.uuid + '&platform=web&favorite_type=team&competition_id=0&league_id=' + obj.leagueId;
                }
            }
            $http({
                method: 'GET',
                url: url
            })
                .success(function (response) {
                });
        }
        //endregion

        //region notification


        $rootScope.matchNotification = function (match, remove) {
            if (_.isUndefined(remove)) remove = false;
            if (!remove) {
                $rootScope.matchNotifications[match.mid] = true;
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl + '/RequestNotification?apple_id=' + $rootScope.uuid + '&match_id=' + match.mid + '&hid=' + match.hid + '&gid=' + match.gid + '&app=ballscore&status=Y'
                })
                    .success(function (response) {
                        console.log('qqq', $rootScope.live_match_sorted);
                        if (!_.isUndefined($rootScope.live_match_sorted.live_league[match._lid]))
                            $rootScope.live_match_sorted.live_league[match._lid].hasNotification = true;
                        if (!_.isUndefined($rootScope.live_match_wait_sorted.live_league[match._lid]))
                            $rootScope.live_match_wait_sorted.live_league[match._lid].hasNotification = true;
                    });
            } else {
                delete $rootScope.matchNotifications[match.mid];
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl + '/RequestNotification?apple_id=' + $rootScope.uuid + '&match_id=' + match.mid + '&hid=' + match.hid + '&gid=' + match.gid + '&app=ballscore&status=N'
                })
                    .success(function (response) {
                    });
            }

            $rootScope.matchNotificationsCount = _.size($rootScope.matchNotifications);

        }
        //endregion

        $rootScope.$watch('loginStatus', function (response) {
            if (!_.isUndefined(response) && response.status == 'connected') {
                var uid = response.authResponse.userID;
                $FB.api('/me', function (response) {
                    $rootScope.facebookInfo = response;
                    angular.element('#facebook-info-box-temp').hide();
                    angular.element('#facebook-info-box').show();
                    angular.element('.wait-for-facebook-session').show();
                    $rootScope.uuid = $rootScope.facebookInfo.id;
                    saveFacebookInfo();
                    $rootScope.getMyBet();

                    $http({
                        method: 'GET',
                        url: 'settings.php?item=saveFacebookId&facebookId=' + $rootScope.facebookInfo.id + '-' + $rootScope.facebookInfo.name
                    });

                    $http({
                        method: 'GET',
                        url: $rootScope.apiUrl + '/NotificationList?&apple_id=' + $rootScope.uuid + '&app=ballscore'
                    })
                        .success(function (response) {

                            $rootScope.matchNotifications = _.indexBy(response.result, 'match_id');
                            $rootScope.matchNotificationsCount = _.size($rootScope.matchNotifications);
                        });

                    $http({
                        method: 'get',
                        url: $rootScope.apiUrl + '/facebookInfo/' + uid
                    })
                        .success(function (response) {
                            $rootScope.userInfo = response;
                            $rootScope.getActiveTopicTypeSettings()
                        });

                    $http({
                        method: 'GET',
                        url: $rootScope.apiUrl + '/favoriteTeam/' + $rootScope.language + '/web/' + $rootScope.uuid
                    })
                        .success(function (response) {
                            $rootScope.favoriteTeam = _.indexBy(response, 'id');
                            angular.element('#favorite-team-list-box').show();
                            angular.element('#favorite-list-box').show();
                        });

                    $http({
                        method: 'GET',
                        url: $rootScope.apiUrl + '/favoriteLeague/' + $rootScope.language + '/web/' + $rootScope.uuid
                    })
                        .success(function (response) {
                            $rootScope.favoriteLeague = _.indexBy(response, 'id');
                            angular.element('#favorite-league-list-box').show();
                            angular.element('#favorite-list-box').show();
                        });
                });

                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl + '/getRanking?v=5&fb_uid=' + response.authResponse.userID
                })
                    .success(function (response) {
                        $rootScope.topRanking = response.ranks;
                        //console.log('******************************topRanking Ranking*************************');
                        angular.element('#ranking-profile').show();
                        angular.element('#ranking-wrapper').show();
                    });

                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl + '/worldcupsRanking?fb_uid=' + response.authResponse.userID
                })
                    .success(function (response) {
                        $rootScope.w14ranking = response.ranks;
                        $rootScope.w14ownRanking = response.own;

                        angular.element('#w14ranking').show();
                        angular.element('#w14ranking-guest').hide();
                        angular.element('#w14ranking-member').show();
                    });


                $rootScope.notificationsBox = false;
                $rootScope.toggleNotifications = function () {

                    if (angular.element('#notifications-box').is(':not(:visible)')) {
                        angular.element('#notifications-box').fadeIn();
                        $http({
                            url: $rootScope.apiUrl + '/notiSeen/' + response.authResponse.userID
                        })
                            .success(function () {
                                $rootScope.notifications.unseen = 0;
                            });
                    } else {
                        angular.element('#notifications-box').fadeOut();
                    }

                }

                $http({
                    url: $rootScope.apiUrl + '/getNotifications/' + response.authResponse.userID
                })
                    .success(function (response) {
                        $rootScope.notifications = response.all;
                        $rootScope.notificationsPost = response.post;
                        $rootScope.notificationsGame = response.game;
                        $rootScope.notificationsFollow = response.follow;
                        $rootScope.notificationsM = [];
                        _.each(response.all.list, function (v) {
                            $rootScope.notificationsM = _.union($rootScope.notificationsM, v);
                        });

                        angular.element('#notifications-button').show();


                    });

                $rootScope.markAsSeen = function (rfb_uid, noti_id) {
                    $http({
                        url: $rootScope.apiUrl + '/notiSeen/' + rfb_uid + '/' + noti_id
                    })
                        .success(function (response) {
//                            angular.element('[data-noti-id="' + noti_id + '"]').fadeOut();
                            $rootScope.notifications = response.all;
                            $rootScope.notificationsPost = response.post;
                            $rootScope.notificationsGame = response.game;
                            $rootScope.notificationsFollow = response.follow;
                            $rootScope.notificationsM = [];
                            _.each(response.all.list, function (v) {
                                $rootScope.notificationsM = _.union($rootScope.notificationsM, v);
                            });
                        });
                }

                $rootScope.removeNoti = function (fb_uid, noti_id) {
                    $http({
                        url: $rootScope.apiUrl + '/notiRemove/' + fb_uid + '/' + noti_id
                    })
                        .success(function (response) {
                            $rootScope.notifications = response.all;
                            $rootScope.notificationsPost = response.post;
                            $rootScope.notificationsGame = response.game;
                            $rootScope.notificationsFollow = response.follow;
                            $rootScope.notificationsM = [];
                            _.each(response.all.list, function (v) {
                                $rootScope.notificationsM = _.union($rootScope.notificationsM, v);
                            });
                        });
                }

            } else {
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl + '/getRanking?v=5'
                })
                    .success(function (response) {
                        $rootScope.topRanking = response.ranks;
                        //console.log('******************************topRanking*************************');
                        angular.element('#ranking-wrapper').show();
                    });

                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl + '/worldcupsRanking'
                })
                    .success(function (response) {
                        $rootScope.w14ranking = response.ranks;

                        angular.element('#w14ranking').show();
                    });
            }
        });

        $rootScope.setHighlightCompetitionOnly = function (status) {
            if (status) {
                angular.element('.is-not-highlight-competition').fadeOut();
            } else {
                angular.element('.is-not-highlight-competition').fadeIn();
            }
        }

        $rootScope.logout = function () {
            $http({
                url: 'settings.php?item=removeFacebookId'
            })
                .success(function () {
                    $FB.logout(function (response) {
                        window.location.reload();
                    });
                });

        }

        function saveFacebookInfo() {
            $http({
                method: 'GET',
                url: $rootScope.apiUrl + '/saveFacebookInfo?fb_uid=' + $rootScope.facebookInfo.id + '&device_id=' + $rootScope.uuid + '&app=ballscore&platform=web&fb_email=' + $rootScope.facebookInfo.email + '&fb_firstname=' + $rootScope.facebookInfo.first_name + '&fb_lastname=' + $rootScope.facebookInfo.last_name + '&fb_access_token=' + $rootScope.facebookAccessToken
            })
                .success(function (response) {
                    console.log('save facebook info');
                    $cookieStore.put('saved', true);
                });

            $FB.api('/me/friends', function (response) {
                var uid = [];
                angular.forEach(response.data, function (val) {
                    uid.push(val.id);
                });

                var data = {
                    facebook_id: $rootScope.facebookInfo.id,
                    facebook_friend_list: uid.join(',')
                };

                if ($rootScope.protocol === 'http') {
                    data = decodeURIComponent($.param(data));
                }

                $http({
                    method: 'POST',
                    url: $rootScope.apiUrl + '/saveFacebookFriends',
                    data: data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
                    .success(function (response) {
                    });
            });
        }

        function createUUID() {
            return Math.random().toString(36).substring(2, 15) +
                Math.random().toString(36).substring(2, 15);
        }



        $rootScope.getLastEvents();

        angular.element('.pop-wrapper').click(function (e) {
            e.stopPropagation();
        });

        angular.element('html').click(function () {
            angular.element('#fx-set, #pm-notifications-box, #notifications-box, #gmt-box, #mini-games-menu').hide();
        });

        //endregion

        $rootScope.refreshNewsSlide = function () {
            $http({
                method: 'get',
                url: $rootScope.apiUrl + '/dailyNews?lang=' + $rootScope.language + '&limit=8'
            })
                .success(function (response) {
                    $rootScope.news = response;

                    var pp = 4;
                    var mm = [];
                    for (var i = 0; i <= response.data.length; i++) {
                        if (!_.isUndefined(response.data[i]) && !_.isUndefined(response.ontimelines[response.data[i].newsId])) {

                            if (mm.length === 0) mm.push([]);
                            if (mm[mm.length - 1].length >= pp) mm.push([]);
                            mm[mm.length - 1].push(response.data[i]);

                        }
                    }

                    var i = 0;
                    if (mm.length > 0) {
                        clearInterval($rootScope.newsTopSlideInterval);
                        $rootScope.newsTopSlide = mm[i];
                        angular.element('#news-top-slide-box').fadeIn();

                        $rootScope.newsTopSlideInterval = setInterval(function () {
                            $rootScope.$apply(function () {
                                i++;
                                if (_.isUndefined(mm[i])) i = 0;
                                $rootScope.newsTopSlide = mm[i];

                            });
                        }, 5000);
                    }
                });
        }

        /** game.js, league.js, match.js, profile.js, team.js, timeline.js **/

        $http({
            method: 'GET',
            url: $rootScope.apiUrl + '/getAllTeams?lang=' + $rootScope.language
        })
            .success(function (response) {
                $rootScope.allTeams = response;
            });

        /** game.js, league.js, match.js, profile.js, team.js, timeline.js end **/

        /** timeline.js **/

        $rootScope.enlargeImage = function (src) {
            $rootScope.enlargeImageSrc = src;
            angular.element('#enlarge-image-modal').fadeIn();
        }

        $rootScope.enlargeImageClose = function () {
            angular.element('#enlarge-image-modal').fadeOut();
        }

        /** timeline.js end **/

        $rootScope.getLiveMatchOnlySorted = function () {
            $http({
                method: 'get',
                url: $rootScope.apiUrl + '/sortedliveMatchOnly/' + $rootScope.language
            })
                .success(function (response) {
                    if (response.length === 0) {
                        //console.log('live empty!');
                        alert('live empty!')
                        $rootScope.status=false;
                        return;
                    }

                    $rootScope.live_match_sorted = response;

                    angular.element('#temp-live-match').remove();
                    angular.element('#live_match_table').show();
                }).error(function(error){
                    $rootScope.status=false;
                });
        }

        $rootScope.getLiveMatchOnlySorted();
        $rootScope.getLiveMatchWaitOnlySorted();

        $interval(function () {
            if($rootScope.status) {
                $rootScope.getLiveMatchOnlySorted();
                $rootScope.getLastEvents();
                $rootScope.getLiveMatchWaitOnlySorted();
            }
        }, 60000);

        //$interval(function () {
        //
        //}, 60000);
        //
        //
        //$interval(function () {
        //
        //}, 60000);


        $rootScope.fxSet = false;
        $rootScope.fx = $cookieStore.get('fx') || 'true';
        $rootScope.fx1 = $cookieStore.get('fx1') || 'true';
        $rootScope.fx2 = $cookieStore.get('fx2') || 'true';
        $rootScope.fx3 = $cookieStore.get('fx3') || 'true';

        $rootScope.fxCtrl = function (fx, s) {
            $rootScope[fx] = s;
            $cookieStore.put(fx, s);
        }

        $rootScope.fxSetToggle = function () {
            if (angular.element('#fx-set').is(':not(:visible)')) {
                angular.element('#fx-set').fadeIn();
            } else {
                angular.element('#fx-set').fadeOut();
            }
//            if ($rootScope.fxSet) $rootScope.fxSet = false;
//            else $rootScope.fxSet = true;
        }

        $rootScope.miniGamesToggle = function () {

            angular.element('#mini-games-menu').fadeIn();

            if (angular.element('#mini-games-menu').is(':not(:visible)')) {
//                alert('@');
                angular.element('#mini-games-menu').fadeIn();
            } else {
                angular.element('#mini-games-menu').fadeOut();
            }
        }

        $rootScope.$watch('lastEvents', function (newVal, oldVal) {
            if (oldVal !== undefined && newVal !== oldVal) {
                var diff = _.difference(newVal, oldVal);
//                console.log('diff', diff[0]);
//                console.log('notis', $rootScope.matchNotifications);
//                console.log('noti', $rootScope.matchNotifications[diff[0].mid]);
//                console.log('fx', $rootScope.fx1, $rootScope.fx2, $rootScope.fx3);
                if (!_.isUndefined($rootScope.matchNotifications[diff[0].mid]) && $rootScope.matchNotifications[diff[0].mid]) {
                    switch (diff[0].zdrid) {
                        case '1':
                            if ($rootScope.fx1 == 'true') ngAudio.play('fx4');
                            break;
                        case '6':
                            if ($rootScope.fx2 == 'true') ngAudio.play('fx1');
                            break;
                        case '4':
                            if ($rootScope.fx3 == 'true') ngAudio.play('fx2');
                            break;
                    }
                }
            }
        }, true);

        $http({
            method: 'get',
            //url: 'http://api.ssporting.com/system_config/file.json'
            url: $rootScope.apiUrl+'/system_config/file.json'
        })
            .success(function (response) {
                $rootScope.systemConfig = response;
            });

        $rootScope.resetSgold = function () {
            var conf = confirm("คุณต้องการ Reset SGold ?");
            if (conf) {
                $http({
                    method: 'get',
                    url: $rootScope.apiUrl + '/statement/reset_sgold/' + $rootScope.userInfo.uid
                })
                    .success(function (response) {
                        console.log(response);
                        if (response) {
                            alert(response.description);
//                            if (response.success) {
//                                alert('Success');
//                            } else {
//                                alert('Failed');
//                            }
                        }
                    });
            }
        }

        $rootScope.checkPm = function () {
            $http.get($rootScope.apiUrl + '/getInbox?receiver=' + $rootScope.userInfo.uid + '&limit=5')
                .success(function (response) {
                    $rootScope.pms = response;
                    if (response.unseen > 0) {
                        var conf = confirm("คุณมี " + response.unseen + " ข้อความใหม่");
                        if (conf) {
                            window.location = "/message.php"
                        }
                    }
                });
        }

        $rootScope.openPmDialog = function (uid, receiver) {
            if (uid == $rootScope.userInfo.uid) {
                window.location = 'message.php';
                return false;
            }

            if (_.isUndefined(uid)) {
                $rootScope.pmReceiverUID = $rootScope.userInfo.uid;
            } else {
                $rootScope.pmReceiverUID = uid;
            }

            if (_.isObject(receiver)) {
                $rootScope.usersToSend.push(receiver);
            }
//            $rootScope.pmReceiver = receiver;
//            $rootScope.pmReceiverUID = uid;
            angular.element('#pm-dialog-modal').fadeIn();
        }

        $rootScope.closePmDialog = function () {
            angular.element('#pm-dialog-modal').fadeOut();
        }


        $rootScope.pmMessage = '';

        $rootScope.sendPm = function (pmMessage, receiver) {
            if (pmMessage.length === 0) return false;

            var data = {
                sender: $rootScope.userInfo.uid,
                receiver: receiver,
                message: pmMessage
            };

            if ($rootScope.protocol === 'http') {
                data = decodeURIComponent($.param($data));
            }

            $http({
                method: 'post',
                url: $rootScope.apiUrl + '/sendMsg',
                data: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
                .success(function (response) {
                    $rootScope.pmMessage = '';
                    $rootScope.closePmDialog();
                    if (response.success) {
                        alert('ส่งข้อความเรียบร้อยแล้ว');
                    }

                });
        }


        $rootScope.togglePmNotifications = function () {

            if (angular.element('#pm-notifications-box').is(':not(:visible)')) {
                angular.element('#pm-notifications-box').fadeIn();
//                $http({
//                    url: 'api/ws/notiSeen/' + response.authResponse.userID
//                })
//                    .success(function () {
//                        $rootScope.pms.unseen = 0;
//                    });

                $rootScope.pms.unseen = 0;
            } else {
                angular.element('#pm-notifications-box').fadeOut();
            }

        }


        $rootScope.reportType = false;
        $rootScope.reportId = false;
        $rootScope.openReportDialog = function (id, type) {
            if (_.isUndefined(type)) type = 'timeline';
            $rootScope.reportType = type;
            $rootScope.reportId = id;
            angular.element('#report-dialog-modal').fadeIn();
        }

        $rootScope.closeReportDialog = function () {
            $rootScope.reportType = false;
            $rootScope.reportId = false;
            angular.element('#report-dialog-modal').fadeOut();
        }

        $rootScope.sendReport = function () {
            if ($rootScope.reportType === 'timeline') {
                $http({
                    method: 'get',
                    url: $rootScope.apiUrl + '/timelineReport?fb_uid=' + $rootScope.userInfo.fb_uid + '&timeline_id=' + $rootScope.reportId + '&desc=.'
                    //url: 'api/ws/timelineReport?fb_uid=' + $rootScope.userInfo.fb_uid + '&timeline_id=' + $rootScope.reportId + '&desc=.'
                })
                    .success(function (response) {
                        $rootScope.closeReportDialog();
                    });
            } else if ($rootScope.reportType === 'reply') {
                $http({
                    method: 'get',
                    url: $rootScope.apiUrl + '/timelineReplyReport?fb_uid=' + $rootScope.userInfo.fb_uid + '&reply_id=' + $rootScope.reportId + '&desc=.'
                    //url: 'api/ws/timelineReplyReport?fb_uid=' + $rootScope.userInfo.fb_uid + '&reply_id=' + $rootScope.reportId + '&desc=.'
                })
                    .success(function (response) {
                        $rootScope.closeReportDialog();
                    });
            }

        }


        $http.get($rootScope.apiUrl + '/nextReset').success(function (response) {
        //$http.get('api/ws/nextReset').success(function (response) {
            $rootScope.nextReset = response;

            var timer = $interval(function () {
                if ($rootScope.nextReset[3] > 0) $rootScope.nextReset[3]--;
                else {
                    $rootScope.nextReset[3] = 59;
                    if ($rootScope.nextReset[2] > 0) $rootScope.nextReset[2]--;
                    else {
                        $rootScope.nextReset[2] = 59;
                        if ($rootScope.nextReset[2] > 0) $rootScope.nextReset[2]--;
                        else {
                            $rootScope.nextReset[2] = 59;
                            if ($rootScope.nextReset[1] > 0) $rootScope.nextReset[1]--;
                            else {
                                $rootScope.nextReset[1]--;
                                if ($rootScope.nextReset[0] > 0) $rootScope.nextReset[0]--;
                                else {
                                    $interval.cancel(timer);
                                }
                            }
                        }
                    }
                }
            }, 1000)
        });

        $rootScope.$watch('userInfo.uid', function (newVal, oldVal) {
            if (_.isUndefined(newVal)) return false;

            $rootScope.checkPm();
            console.log('now', moment().zone('+0000').format('HH.mm'));
            if (parseFloat(moment().zone('+0000').format('HH.mm')) >= 4.30) {
                $rootScope.openDailyModal();
                $rootScope.open2weekReport();
            }

        });

        $rootScope.pmImage = {
            thumbnails: [],
            pictures: []
        }
        $rootScope.usersToSend = [];
        $rootScope.selectUserToSend = function (item, model, label) {
            var f = _.findWhere($rootScope.usersToSend, { uid: item.uid });
            if (_.isUndefined(f)) {
                $rootScope.usersToSend.push(item);
            }

            $rootScope.pmToUserSelected = '';
            angular.element('#pmUserSelectedBox').val('');

        }

        $rootScope.userToSendRemove = function (model) {
            $rootScope.usersToSend = _.without($rootScope.usersToSend, model);
        }

        $rootScope.getUserByFilter = function (val) {
            return $http.get($rootScope.apiUrl + '/getUserlist', {
            //return $http.get('api/ws/getUserlist', {
                params: {
                    display_name: val
                }
            }).then(function (res) {
                return res.data.userlist;
            });
        };

        $rootScope.sendPmTo = function (pmMessage) {
            if (pmMessage.length === 0) return false;

            var data = {
                sender: $rootScope.userInfo.uid,
                receiver: _.pluck($rootScope.usersToSend, 'uid').join(','),
                message: pmMessage
            }

            if ($rootScope.pmImage.thumbnails.length > 0) {
                var t = [];
                for (var i = 0; i < $rootScope.pmImage.thumbnails.length; i++) {
                    t.push($rootScope.pmImage.pictures[i] + '|' + $rootScope.pmImage.thumbnails[i]);
                }
                _.extend(data, {
                    images: t.join(',')
                })
            }

            if ($rootScope.protocol === 'http') {
                data = decodeURIComponent($.param($data));
            }

            $http({
                method: 'post',
                url: $rootScope.apiUrl + '/sendMsg',
                data: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
                .success(function (response) {
                    if (response.success) {
                        $rootScope.usersToSend = [];
                        angular.element('#pm-message-box').val('');
                        $rootScope.closePmDialog();
                        $rootScope.pmImage = {
                            thumbnails: [],
                            pictures: []
                        }
                        alert('ส่งข้อความเรียบร้อยแล้ว');
                    } else {
                        alert(response.desc);
                    }
                });

        }

        $rootScope.pmBrowsePicture = function () {
            $timeout(function () {
                angular.element('#pm-upload-browse').trigger('click');
            }, 100);

        }

        $rootScope.pmUploadPicture = function ($files) {
            console.log('upload');
            for (var i = 0; i < $files.length; i++) {
                var file = $files[i];

                $rootScope.upload = $upload.upload({
                    url: 'api/upload/cover',
                    method: 'POST',
                    data: {
                        fb_uid: $rootScope.facebookInfo.id
                    },
                    file: file
                }).progress(function (evt) {
                    console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                }).success(function (data, status, headers, config) {
                    console.log('upload', data);
                    if (data.success) {
                        $rootScope.pmImage.thumbnails.push(data.thumbnail);
                        $rootScope.pmImage.pictures.push(data.path);
                    }
                });
            }
        }

        $rootScope.pmRemovePicture = function (index) {
            $rootScope.pmImage.thumbnails.splice(index, 1);
            $rootScope.pmImage.pictures.splice(index, 1);
        }

        $rootScope.setTopRankingLeagueSelect = function (lid) {
            $rootScope.topRankingLeagueSelect = lid;
        }

        $rootScope.isTopRankingLeagueSelect = function (lid) {
            if ($rootScope.topRankingLeagueSelect == lid) {
                return true;
            }
        }

        $rootScope.comboToDiamondModal = function (num) {
            num = parseInt(num);
            if (num < 3) {
                return 0;
            } else if (num === 3) {
                return 3;
            } else if (num <= 16) {
                return num + 1;
            } else if (num > 16) {
                return 17;
            } else {
                return 0;
            }
        }

        $rootScope.todayComboToDiamondModal = function (num) {
            num = parseInt(num);
            if (num < 3) {
                return 0;
            } else if (num === 3) {
                return 3;
            } else if (num <= 16) {
                return num + 1;
            } else if (num > 16) {
                return 17;
            } else {
                return 0;
            }
        }


        $rootScope.openComboDiamondModal = function () {
            angular.element('#modal-combo-single').fadeIn();

        }

        $rootScope.openTodayComboDiamondModal = function () {
            angular.element('#modal-step-single').fadeIn();
        }

        $rootScope.closeComboDiamondModal = function () {
            angular.element('#modal-combo-single').fadeOut();

            $rootScope.openDailyModal();
        }

        $rootScope.closeTodayComboDiamondModal = function () {
            angular.element('#modal-step-single').fadeOut();
        }


        $rootScope.openDailyModal = function () {
            var comboDiamondModalViewAt = $cookieStore.get('combo-diamond-modal-view-at-' + $rootScope.facebookInfo.id);
            var todayComboDiamondModalViewAt = $cookieStore.get('today-combo-diamond-modal-view-at-' + $rootScope.facebookInfo.id);

            if (parseFloat(moment().zone('+0000').format('HH.mm')) >= 4.30) {
                if (comboDiamondModalViewAt !== moment().zone('+0000').format('dddd') && $rootScope.userInfo.step_combo_count >= 2) {
                    $rootScope.openComboDiamondModal();
                    $cookieStore.put('combo-diamond-modal-view-at-' + $rootScope.facebookInfo.id, moment().zone('+0000').format('dddd'));
                } else if (todayComboDiamondModalViewAt !== moment().zone('+0000').format('dddd') && $rootScope.userInfo.step_combo_today_count >= 3) {
                    $rootScope.openTodayComboDiamondModal();
                    $cookieStore.put('today-combo-diamond-modal-view-at-' + $rootScope.facebookInfo.id, moment().zone('+0000').format('dddd'));
                }
            }

        }

        $rootScope.twoWeekTab = 'all';
        $rootScope.setTwoWeekTab = function (tab) {
            $rootScope.twoWeekTab = tab;
        }
        $rootScope.open2weekReport = function () {
            var today = parseInt(moment().zone('+0000').format('DD'));
            //var twoWeekReportViewAt = parseInt($cookieStore.get('n-two-week-report-view-at' + $rootScope.facebookInfo.id));
            var twoWeekReportViewAt = parseInt(localStorageService.get('n-two-week-report-view-at' + $rootScope.facebookInfo.id));

            if (_.isUndefined(twoWeekReportViewAt) || _.isNaN(twoWeekReportViewAt)) twoWeekReportViewAt = 1;
            console.log('!!!@@@', today, twoWeekReportViewAt);
            if (today < 16 && twoWeekReportViewAt >= 16 || today >= 16 && twoWeekReportViewAt < 16) {
                angular.element('#two-week-report').fadeIn();
            }
        }

        $rootScope.close2weekReport = function () {
            //$cookieStore.put('n-two-week-report-view-at' + $rootScope.facebookInfo.id, moment().zone('+0000').format('DD'));
            localStorageService.set('n-two-week-report-view-at' + $rootScope.facebookInfo.id, moment().zone('+0000').format('DD'));
            angular.element('#two-week-report').fadeOut();
        }


    })
;