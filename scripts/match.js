angular.module('scorspot.controller', ['videosharing-embed'])
    .controller('mainCtrl', function ($rootScope, $scope, $location, $http, $interval) {

        $scope.mid = document.getElementById('matchScript').getAttribute('mid');

        $scope.vote = {
            home: 0,
            draw: 0,
            away: 0,
            percentHome: 0,
            percentDraw: 0,
            percentAway: 0
        };

        $scope.getCurrentMatch = function () {
            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/getGameData?mid=' + $scope.mid
                //url: 'api/ws/getGameData?mid=' + $scope.mid
            })
                .success(function (response) {
                    $scope.currentMatch = response.game;
                    $scope.now = $scope.currentMin($scope.currentMatch.c0, $scope.currentMatch.c1, $scope.currentMatch.c2, $scope.currentMatch.cx, $scope.currentMatch.sid);
                });
        }

        $scope.getEvent = function () {
            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/liveMatchEvent/' + $scope.mid
                //url: 'api/ws/liveMatchEvent/' + $scope.mid
            })
                .success(function (response) {
                    if (!_.isUndefined(response.live_match_event) && response.live_match_event.length > 0) {
                        $scope.events = response.live_match_event.reverse();

                        angular.element('#match-events-none').hide();
                        angular.element('#match-events').show();
                    } else {
                        angular.element('#match-events').hide();
                        angular.element('#match-events-none').show();
                    }

                });
        }

        $scope.getCurrentMatchComments = function () {
            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/getCommentOnmatch?mid=' + $scope.mid
                //url: 'api/ws/getCommentOnmatch?mid=' + $scope.mid
            })
                .success(function (response) {
                    console.log('current comments', response);
                    $scope.currentMatchComments = response.list;
                    $scope.getComments();
                    setInterval(function () {
                        $scope.$apply(function () {
                            $scope.getComments('next');
                        });
                    }, 5000);


//                    var t = 0;
//                    var m = [];
//                    for (var i = t; i <= response.comment_list.length; i++) {
//                        if (!_.isUndefined(response.comment_list[i])) {
//                            t = i;
//                            m.push(response.comment_list[i]);
//                            if (m.length >= 4) break;
//                        } else {
//                            t = 0;
//                            break;
//                        }
//                    }
//
//                    $rootScope.currentMatchComments = m;
//
//                    angular.element('#feed-comment-match-box').show();
//
//                    $scope.topSlide = setInterval(function () {
//                        $scope.$apply(function () {
//                            var m = [];
//                            for (var i = t + 1; i <= response.comment_list.length; i++) {
//                                if (!_.isUndefined(response.comment_list[i])) {
//                                    t = i;
//                                    m.push(response.comment_list[i]);
//                                    if (m.length >= 4) break;
//                                } else {
//                                    t = 0;
//                                    break;
//                                }
//
//                                if (t >= response.comment_list.length) {
//                                    t = 0;
//                                    break;
//                                }
//                            }
//                            $rootScope.currentMatchComments = m;
//                        });
//                    }, 5000);
                });
        }

        $scope.getComments = function (action, limit) {
            $scope.commentList = [];
            if (_.isUndefined($scope.cCurrentPage)) $scope.cCurrentPage = 1;
            if (_.isUndefined(limit)) var limit = 2;
            $scope.cPages = Math.ceil($scope.currentMatchComments.length / limit);

            if (action === 'next') {
                $scope.cCurrentPage++;
            } else if (action === 'prev') {
                $scope.cCurrentPage--;
            }

            if ($scope.cCurrentPage < 1) $scope.cCurrentPage = $scope.cPages;
            else if ($scope.cCurrentPage > $scope.cPages) $scope.cCurrentPage = 1;

            angular.forEach($scope.currentMatchComments, function (val, idx) {
                if ((idx) >= (($scope.cCurrentPage * limit - limit)) && (idx + 1) <= ($scope.cCurrentPage * limit)) {
                    $scope.commentList.push(val);
                }

            });
        }

        $scope.getVote = function () {
            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/voteBet/' + $scope.mid
                //url: 'api/ws/voteBet/' + $scope.mid
            })
                .success(function (response) {
                    var home = parseInt(response.home)
                        , draw = parseInt(response.draw)
                        , away = parseInt(response.away)
                        , sum = home + draw + away
                        ;
                    $scope.vote = {
                        home: home,
                        draw: draw,
                        away: away,
                        percentHome: (home / sum) * 100 || 0,
                        percentDraw: (draw / sum) * 100 || 0,
                        percentAway: (away / sum) * 100 || 0
                    };

                    angular.element('#poll-to-win').show();
                });
        }

        $scope.voteTo = function (choose, $event) {
            $event.preventDefault();
            if ($scope.facebookInfo == undefined) {
                return false;
            }

            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/bet?fb_uid=' + $scope.facebookInfo.id + '&platorm=web&mid=' + $scope.mid + '&vote_choose=' + choose
                //url: 'api/ws/bet?fb_uid=' + $scope.facebookInfo.id + '&platorm=web&mid=' + $scope.mid + '&vote_choose=' + choose
            })
                .success(function (response) {
                    $scope.getVote();
                });
        }

        $scope.refreshNewFeed = function () {
            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/commentFacebookFriends/' + $rootScope.facebookInfo.id
                //url: 'api/ws/commentFacebookFriends/' + $rootScope.facebookInfo.id
            })
                .success(function (response) {
                    clearInterval($rootScope.feedTopSlideInterval);
                    $rootScope.newfeed = response;

                    var t = 0;
                    var m = [];
                    for (var i = t; i <= response.length; i++) {
                        if (!_.isUndefined(response[i])) {
                            t = i;
                            m.push(response[i]);
                            if (m.length >= 4) break;
                        } else {
                            t = 0;
                            break;
                        }
                    }

                    $rootScope.feedTopSlide = m;

                    $rootScope.feedTopSlideInterval = setInterval(function () {
                        $rootScope.$apply(function () {
                            var m = [];
                            for (var i = t + 1; i <= response.length; i++) {
                                if (!_.isUndefined(response[i])) {
                                    t = i;
                                    m.push(response[i]);
                                    if (m.length >= 4) break;
                                } else {
                                    t = 0;
                                    break;
                                }

                                if (t >= response.length) {
                                    t = 0;
                                    break;
                                }
                            }
                            $rootScope.feedTopSlide = m;
                        });
                    }, 5000);

                    angular.element('#feed-top-slide-box').show();
                    angular.element('#news-feed-box').show();

                });
        }

        $scope.refreshNewsSlide = function () {
            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/dailyNews?lang=' + $scope.language + '&limit=10'
                //url: 'api/ws/dailyNews?lang=' + $scope.language + '&limit=10'
            })
                .success(function (response) {
                    $scope.news = response;

                    var pp = 4;
                    var mm = [];
                    for (var i = 0; i <= response.data.length; i++) {
                        if (!_.isUndefined(response.data[i]) && !_.isUndefined(response.ontimelines[response.data[i].newsId])) {

                            if (mm.length === 0) mm.push([]);
                            if (mm[mm.length - 1].length >= pp) mm.push([]);
                            mm[mm.length - 1].push(response.data[i]);

                        }
                    }

                    var i = 0;
                    if (mm.length > 0) {
                        clearInterval($scope.newsTopSlideInterval);
                        $scope.newsTopSlide = mm[i];
                        angular.element('#news-top-slide-box').fadeIn();

                        $scope.newsTopSlideInterval = setInterval(function () {
                            $scope.$apply(function () {
                                i++;
                                if (_.isUndefined(mm[i])) i = 0;
                                $scope.newsTopSlide = mm[i];

                            });
                        }, 5000);
                    }
                });
        }

        $scope.y = function (cx) {
            if (parseInt(cx) > 0) {
                return parseInt(cx);
            } else {
                return 45;
            }
        }

        $scope.currentMin = function (c0, c1, c2, cx, sid) {

            var str = '-';
            var ap;
            var d = new Date();
            var offset = $scope.gmtOffset * 60000;
            var ao;
            var label = {
                '1': 'schedule',
                '2': 'RUN',
                '3': 'HT',
                '4': 'RUN',
                '5': 'FT',
                '6': 'ET',
                '7': 'Pen.',
                '8': 'AET',
                '9': 'AP',
                '10': 'ABD',
                '11': 'Postp.',
                '12': 'FT only',
                '13': "Susp.",
                '14': "Pen.",
                '15': "AP",
                '16': "W.O.",
                '17': "ANL"
            }

            if (typeof fix == 'undefined') {
                var fix = 0;
            }
            if (sid == 2) {
                ap = $scope.y(cx);
                ao = (((d.getTime()/*+offset*/) / 1000) - parseInt(c0) - parseInt(c1)) / 60;
                if (ao < 1) {
                    str = 1 + "'";
                } else if (ao > ap) {
                    str = Math.floor(ap) + fix + "+'";
                } else {
                    str = Math.floor(ao) + fix + "'";
                }
            } else if (sid == 4) {
                ap = $scope.y(cx);
                ao = Math.floor((((d.getTime()/*+offset*/) / 1000) - parseInt(c0) - parseInt(c2)) / 60 + ap);
                if (ao <= ap) {
                    str = Math.floor(ap + 1) + fix + "'";
                } else if (ao > ap) {
                    ap = Math.floor(ap * 2);
                    if (ao > ap) {
                        str = ap + "+'";
                    } else {
                        str = ao + fix + "'";
                    }
                }
            } else {
                if (!_.isUndefined(label[sid])) str = label[sid];
            }

            return str;

        }

        $scope.getCurrentMatch();
        $scope.getEvent();
        $scope.getVote();
        $scope.getCurrentMatchComments();
        $scope.refreshNewsSlide();

//        $rootScope.$watch('facebookInfo', function (response) {
//            if (!_.isUndefined(response) && !_.isUndefined(response.id)) {
//                $scope.refreshNewFeed();
//            }
//        });

        $scope.upcomingMatch = function () {
            var el = angular.element('.upcoming-match');
            var l = el.length;
            var start = 0;

            if (l > 2) {
                setInterval(function () {
                    $scope.$apply(function () {
                        angular.element('.upcoming-match').hide();
                        for (i = start; i < (start + 2); i++) {
                            angular.element('.upcoming-match[data-idx="' + i + '"]').fadeIn();
                        }
                        start = i;
                        if (start >= l) start = 0;
                    });
                }, 10000);
            } else {
                angular.element('.upcoming-match').fadeIn();
            }
        }

        $scope.upcomingMatch();



        $scope.mobileFeedCurrentIndex = -1;
        $scope.mobileFeedItems = 0;
        $scope.mobileFeedActive = [];
        $scope.getMobileFeed = function () {
            $http.get($rootScope.apiUrl+'/getMobileFeed?fb_uid=(null)&limit=10&view_type=board&mid=' + $scope.mid + '&category=6')
            //$http.get('api/ws/getMobileFeed?fb_uid=(null)&limit=10&view_type=board&mid=' + $scope.mid + '&category=6')
                .then(function (response) {
                    $scope.mobileFeed = response.data.timelines;
                    $scope.mobileFeedItems = response.data.timelines.length;
                    $scope.getActiveMobileFeed();
                });
        }

        $scope.getActiveMobileFeed = function () {
            $interval(function(){
                if ($scope.mobileFeedCurrentIndex + 2 >= $scope.mobileFeedItems) {
                    $scope.mobileFeedCurrentIndex = 0;
                } else {
                    $scope.mobileFeedCurrentIndex += 2;
                }
                $scope.mobileFeedActive[0] = $scope.mobileFeedCurrentIndex;
                $scope.mobileFeedActive[1] = $scope.mobileFeedCurrentIndex + 1;
            }, 10000)
        }

        $scope.getMobileFeed();

    });