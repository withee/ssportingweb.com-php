angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope, $scope, $location, $http, $upload, $timeout) {
        $scope.scoin = 200;
        $scope.mid = document.getElementById('gameScript').getAttribute('mid');
        $scope.match_id = document.getElementById('gameScript').getAttribute('match_id');
        $scope.hid = document.getElementById('gameScript').getAttribute('hid');
        $scope.gid = document.getElementById('gameScript').getAttribute('gid');
        $scope.leagueId = document.getElementById('gameScript').getAttribute('leagueId');
        $scope.betType = document.getElementById('gameScript').getAttribute('betType');
        $scope.hdp = document.getElementById('gameScript').getAttribute('betHdp');
        $scope.hdp_home = document.getElementById('gameScript').getAttribute('Hdp_home');
        $scope.hdp_away = document.getElementById('gameScript').getAttribute('Hdp_away');

        var alreadyLike = {}
            , inProgress = {}
            , inProgressTime = 2000;



        $scope.inProgress = {};

        $scope.newComment = {
            message: '',
            picture: '',
            thumbnail: ''
        };

        $scope.newReply = {
            parent_id: false,
            message: {},
            picture: '',
            thumbnail: ''
        };

        $scope.setScoin = function (scoin) {
            $scope.scoin = scoin;
        }

        $scope.removeImage = function (w) {
            if (w === 'comment') {
                $scope.newComment.picture = '';
                $scope.newComment.thumbnail = '';
            } else if (w === 'reply') {
                $scope.newReply.picture = '';
                $scope.newReply.thumbnail = '';
            }
        }

        $scope.getCurrentMatch = function () {
            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/getGameData?mid=' + $scope.mid
            })
                .success(function (response) {
                    $scope.currentMatch = response.game;
                })
                .error(function(data, status, headers, config) {

                });
        }

        $scope.getMatchOnFile = function () {
            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/matchonfile/' + $scope.mid
            })
                .success(function (response) {
                    $scope.commentsInfo = response.beton;
                    $scope.comments = response.betlist.slice().reverse();
                    $scope.replies = response.reply;

                    angular.element('#game-comment-list-box').fadeIn();
                    angular.element('#game-comment-list-box-home').show();
                    angular.element('#game-comment-list-box-away').show();
                    angular.element('#box-show-comment-all').show();
                })
                .error(function(data, status, headers, config) {

                });
        }

        $scope.getVotes = function () {
            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/getCommentOnmatch?mid=' + $scope.mid
            })
                .success(function(response){
                    $scope.votes = response.vote;
                })
                .error(function(data, status, headers, config) {

                });
        }

        $scope.chooseTeam = function (team, confirm, $event) {
            $event.preventDefault();
            console.log($scope.canPlay);
            if(!($scope.canPlay || $scope.canVote)) return false;

            if ($rootScope.loginStatus.status !== 'connected') {
                $rootScope.login();
                return false;
            }


//            if (!_.isUndefined($scope.myBet[$scope.mid])) {
//                return false;
//            }


            if (!team) {
                $scope.teamSelect = false;
                angular.element('#team-select-dialog').fadeOut();

                return false;
            }

            if (!confirm) {
                $scope.teamSelect = team;
                angular.element('#team-select-dialog').fadeIn();

                return false;
            }

//            var choose = false;
//            if (team == 'home') {
//                choose = 'hdp_home=' + $scope.hdp_home;
//            } else if (team == 'away') {
//                choose = 'hdp_away=' + $scope.hdp_away;
//            }

//            var more = '';
//            if ($scope.newComment.thumbnail.length > 0) {
//                more = '&photo=' + $scope.newComment.picture + '&thumbnail=' + $scope.newComment.thumbnail;
//            }

            if($scope.canPlay) {
                var data = {
                    scoin: $scope.scoin,
                    fb_uid: $scope.facebookInfo.id,
                    platform: 'web',
                    match_id: $scope.match_id,
                    mid: $scope.mid,
                    hid: $scope.hid,
                    gid: $scope.gid,
                    leagueId: $scope.leagueId,
                    betType: $scope.betType,
                    betHdp: $scope.hdp,
                    amount: 1,
                    message: angular.element('#bet-comment-box').val()
                };

                if (team === 'home') {
                    _.extend(data, { hdp_home: $scope.hdp_home });
                } else if (team === 'away') {
                    _.extend(data, { hdp_away: $scope.hdp_away });
                }

                if ($scope.newComment.thumbnail.length > 0) {
                    _.extend(data, {
                        photo: $scope.newComment.picture,
                        thumbnail: $scope.newComment.thumbnail
                    });
                }
            } else if ($scope.canVote) {
                var data = {
                    fb_uid: $scope.facebookInfo.id,
                    platform: 'web',
                    mid: $scope.mid,
                    vote_choose: team,
                    message: angular.element('#bet-comment-box').val()
                };
            }

            if($scope.inProgress.play) return false;
            $scope.inProgress.play = true;

            $http({
                method: 'POST',
//                url: 'http://api.ssporting.com/bet?fb_uid=' + $rootScope.facebookInfo.id + '&platform=web&mid=' + $scope.mid + '&match_id=' + $scope.match_id + '&hid=' + $scope.hid + '&gid=' + $scope.gid + '&leagueId=' + $scope.leagueId + '&betType=' + $scope.betType + '&betHdp=' + $scope.hdp + '&amount=' + 1 + '&' + choose + '&message=' + encodeURI(angular.element('#bet-comment-box').val())
                url: $rootScope.apiUrl+'/bet',
                data: decodeURIComponent($.param(data)),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
                .success(function (response) {
                    if (response.status == 2) {
                        _.each(_.keys(response.value_change), function (val) {
                            $scope[val] = response.value_change[val];
                        });
                        alert("ราคาเปลี่ยน!\nกรุณารอสักครู่แล้วลองใหม่อีกครั้ง\n\n" + response.value_change.hdp_home + ' (' + response.value_change.hdp + ') ' + response.value_change.hdp_away);
                    } else if (response.status == 3) {
                        if (response.message === 24) {
                            alert('Timeout!');
                        } else {
                            alert(response.message);
                        }
                    } else if (response.status == 1) {
//                        $scope.getMatchOnFile();
//                        angular.element('#bet-comment-box').val('');
                        $scope.newComment.message = '';
//                        angular.element('.bet-select-button-set').fadeOut();
                        angular.element('#team-select-dialog').fadeOut();
                        $scope.teamSelect = false;
                        $scope.getMatchOnFile();

                        if($scope.canPlay) {
                            alert('เล่นเรียบร้อย!');
                        } else {
                            alert('โหวตเรียบร้อย!');
                        }


                    }

                    setTimeout(function(){
                        $scope.$apply(function(){
                            $scope.inProgress.play = false;
                        });
                    }, inProgressTime);

                    if($scope.canPlay) {
                        $rootScope.getMyBet();
                    } else if ($scope.canVote) {
                        $scope.getVotes();
                    }

                })
                .error(function(data, status, headers, config) {
                    alert('เกิดข้อผิดพลาด กรุณาลอง Reload/Refresh');
                });


        }

        $scope.addNewComment = function (comments, $event) {
            if ($scope.newComment.message.length > 0 || $scope.newComment.picture.length > 0 || $scope.newComment.thumbnail.length > 0) {
                if ($scope.newComment.message.length === 0) $scope.newComment.message = ' ';
                if ($scope.newComment.thumbnail.length > 0) {
                    var data = {
                        mid: $scope.mid,
                        fb_uid: $scope.facebookInfo.id,
                        message: $scope.newComment.message,
                        picture: $scope.newComment.picture,
                        thumbnail: $scope.newComment.thumbnail
                    };
                } else {
                    var data = {
                        mid: $scope.mid,
                        fb_uid: $scope.facebookInfo.id,
                        message: $scope.newComment.message
                    };
                }

                if($scope.inProgress.comment) return false;
                $scope.inProgress.comment = true;

                $http({
                    method: 'POST',
                    url: $rootScope.apiUrl+'/commentOnmatch',
                    data: decodeURIComponent($.param(data)),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
                    .success(function (response) {
                        console.log('comment on match', response);
                        if (response.success) {
//                            comments.push(response.data);
//                            comments.unshift(response.data);
                            $scope.getMatchOnFile();
                            $scope.newComment.message = '';
                            $scope.newComment.thumbnail = '';
                            $scope.newComment.picture = '';

                            angular.element('.new-comment-thumbnail').attr('src', '');
                        } else {
                            alert(response.desc);
                        }

                        setTimeout(function(){
                            $scope.$apply(function(){
                                $scope.inProgress.comment = false;
                            });
                        }, inProgressTime);

                    })
                    .error(function(data, status, headers, config) {
                        alert('เกิดข้อผิดพลาด กรุณาลอง Reload/Refresh');
                    });
            } else {
                alert('ต้องกรอกความเห็น หรือ รูปภาพ!');
            }
        }

        $scope.addNewReply = function (comment_id, $event) {

            if ((!_.isUndefined($scope.newReply.message[comment_id]) && $scope.newReply.message[comment_id].length > 0) || $scope.newReply.picture.length > 0 || $scope.newReply.thumbnail.length > 0) {
                var more = '', message = '&message= ';
                if ($scope.newReply.thumbnail.length > 0) {
                    more = '&picture=' + $scope.newReply.picture + '&thumbnail=' + $scope.newReply.thumbnail;
                }
                if (!_.isUndefined($scope.newReply.message[comment_id]) && $scope.newReply.message[comment_id].length > 0) {
                    message = '&message=' + encodeURI($scope.newReply.message[comment_id]);
                }

                if($scope.inProgress.reply) return false;
                $scope.inProgress.reply = true;

                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/commentOnmatch?mid=' + $scope.mid
                        + '&fb_uid=' + $rootScope.facebookInfo.id
                        + '&parent_id=' + comment_id
                        + message
                        + more
                })
                    .success(function (response) {
                        console.log('comment on match', response);
                        if (response.success) {
//                            if (_.isUndefined($scope.replies[comment_id])) {
//                                $scope.replies[comment_id] = [];
//                            }
//                            $scope.replies[comment_id].push(response.data);

                            $scope.getMatchOnFile();

//                            comments.push(response.data);
                            $scope.newReply.id = false;
                            $scope.newReply.message = {};
                            $scope.newReply.thumbnail = '';
                            $scope.newReply.picture = '';
                            angular.element('.new-reply-thumbnail').attr('src', '');
                        } else {
                            alert(response.desc);
                        }

                        setTimeout(function(){
                            $scope.$apply(function(){
                                $scope.inProgress.reply = false;
                            });
                        }, inProgressTime);

                    })
                    .error(function(data, status, headers, config) {
                        alert('เกิดข้อผิดพลาด กรุณาลอง Reload/Refresh');
                    });
            } else {
                alert('ต้องกรอกความเห็น หรือ รูปภาพ!');
            }
        }


        $scope.removeReply = function (id, parentId, ownerId) {
            var conf = confirm('Sure ?');
            if (conf) {
                $http({
                    method: 'get',
                    url: $rootScope.apiUrl+'/removeCommentOnmatch?id=' + id + '&fb_uid=' + $scope.facebookInfo.id
                })
                    .success(function (response) {
                        if (response.success) {
                            if (!parentId) {
                                $scope.comments = _.reject($scope.comments, function (val) {
                                    return val.id == id;
                                });
                            } else {
                                $scope.replies[parentId] = _.reject($scope.replies[parentId], function (val) {
                                    return val.id == id;
                                });
                            }
                        } else {
                            alert(response.desc);
                        }
                    })
                    .error(function(data, status, headers, config) {
                        alert('เกิดข้อผิดพลาด กรุณาลอง Reload/Refresh');
                    });
            }

        }

        $scope.likeComment = function (id, match_id, index) {

            if(!_.isUndefined(alreadyLike[id])) return false;

            if (inProgress.like) return;
            inProgress.like = true;

            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/mCommentLike?commentid=' + id + '&fb_uid=' + $rootScope.facebookInfo.id + '&mid=' + $scope.mid
            })
                .success(function (response) {

                    if ((response.success && response.success != '0') || response.success == '1') {
                        var t = _.findWhere($scope.comments, {id: id});
                        console.log('t', t);
//                        t.like++;
                        t.like = response.likelist.length;

//                        $scope.comments[$scope.comments.length - index].like = response.likelist.length;
                        console.log('like',  _.pluck($scope.comments, 'like'));

                    } else {
                        alert(response.desc);
                    }

                    alreadyLike[id] = true;


                    setTimeout(function () {
                        inProgress.like = false;
                    }, inProgressTime);

                    if (!_.isUndefined(response.likelist)) {
                        $scope.comments[index].like = response.likelist.length;
                    }
                })
                .error(function(data, status, headers, config) {
                    alert('เกิดข้อผิดพลาด กรุณาลอง Reload/Refresh');
                });
        }

        $scope.likeReply = function (id, comment_id, index) {

            if(!_.isUndefined(alreadyLike[id])) return false;

            if (inProgress.like) return;
            inProgress.like = true;

            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/mCommentLike?commentid=' + id + '&fb_uid=' + $rootScope.facebookInfo.id + '&mid=' + $scope.mid
            })
                .success(function (response) {

                    if ((response.success && response.success != '0') || response.success == '1') {
                        var t = _.findWhere($scope.replies[comment_id], {id: id});
//                        t.like++;
                        t.like = response.likelist.length;
                    } else {
                        alert(response.desc);
                    }

                    alreadyLike[id] = true;

                    setTimeout(function () {
                        inProgress.like = false;
                    }, inProgressTime);

//                    if (!_.isUndefined(response.likelist)) {
//                        $scope.replies[comment_id][index].like = response.likelist.length;
//                    }
                })
                .error(function(data, status, headers, config) {
                    alert('เกิดข้อผิดพลาด กรุณาลอง Reload/Refresh');
                });
        }

        $scope.openReplyBox = function (id) {
            angular.element('.reply-box').hide();
            angular.element('[data-comment-id="' + id + '"] .reply-box').fadeIn();
        }

        $scope.browsePicture = function () {
            $timeout(function(){
                angular.element('#upload-browse').trigger('click');
            }, 300)

        }

        $scope.uploadPicture = function ($files) {
            var file = $files[0];
            $scope.upload = $upload.upload({
                url: 'api/upload/cover',
                method: 'POST',
                data: {
                    fb_uid: $rootScope.facebookInfo.id
                },
                file: file
            }).progress(function (evt) {
//                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).success(function (data, status, headers, config) {
//                $scope.profileInfo.cover = data.path;
                console.log('upload', data);
                $scope.newComment.thumbnail = data.thumbnail;
                $scope.newComment.picture = data.path;

            });
        }

        $scope.browsePictureReply = function (comment_id) {
            $scope.newReply.id = comment_id;
            angular.element('#upload-browse-reply').trigger('click');
        }

        $scope.uploadPictureReply = function ($files) {
            var file = $files[0];
            $scope.upload = $upload.upload({
                url: 'api/upload/cover',
                method: 'POST',
                data: {
                    fb_uid: $rootScope.facebookInfo.id
                },
                file: file
            }).progress(function (evt) {
//                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).success(function (data, status, headers, config) {
//                $scope.profileInfo.cover = data.path;
                console.log('upload', data);
                $scope.newReply.thumbnail = data.thumbnail;
                $scope.newReply.picture = data.path;
                console.log('newReply', $scope.newReply);

            });
        }

        $scope.getCurrentMatch();
        $scope.getMatchOnFile();
        $scope.getVotes();

//        $scope.$watch('myBet', function (newVal, oldVal) {
//            if ($scope.hdp != '' && !_.isUndefined(newVal) && _.isUndefined($scope.myBet[$scope.mid])) {
//                angular.element('.bet-select-button-set').fadeIn();
//            }
//        });

        $scope.$watchCollection('[currentMatch,myBet]', function (newVal, oldVal){
            if(!_.isUndefined($scope.currentMatch)) {
                if ($scope.currentMatch.sid == '1' && $scope.hdp != '' && (_.isUndefined($scope.myBet) || _.isUndefined($scope.myBet[$scope.mid]))) {
                    $scope.canPlay = true;
                } else {
                    $scope.canPlay = false;
                }

                $scope.playHomePercent = Math.floor(($scope.commentsInfo.home.count / ($scope.commentsInfo.home.count + $scope.commentsInfo.draw.count + $scope.commentsInfo.away.count)) * 100);
                $scope.playDrawPercent = Math.floor(($scope.commentsInfo.draw.count / ($scope.commentsInfo.home.count + $scope.commentsInfo.draw.count + $scope.commentsInfo.away.count)) * 100);
//                $scope.playAwayPercent = Math.floor(($scope.commentsInfo.away.count / ($scope.commentsInfo.home.count + $scope.commentsInfo.draw.count + $scope.commentsInfo.away.count)) * 100);
                $scope.playAwayPercent = 100 - ($scope.playHomePercent + $scope.playDrawPercent);

                if(_.isNaN($scope.playHomePercent)) $scope.playHomePercent = 0;
                if(_.isNaN($scope.playDrawPercent)) $scope.playDrawPercent = 0;
                if(_.isNaN($scope.playAwayPercent)) $scope.playAwayPercent = 0;
            }

        }, true);

        $scope.$watch('[votes, facebookInfo]', function(newVal, oldVal){

            if(!_.isUndefined($scope.votes) && !_.isUndefined($scope.facebookInfo) && $scope.hdp === '') {
                if($scope.currentMatch.sid == '1' && _.indexOf($scope.votes.home.people, $scope.facebookInfo.id) === -1 && _.indexOf($scope.votes.draw.people, $scope.facebookInfo.id) === -1 && _.indexOf($scope.votes.away.people, $scope.facebookInfo.id) === -1) {
                    $scope.canVote = true;
                } else {
                    $scope.canVote = false;
                }

                $scope.voteHomePercent = Math.floor(($scope.votes.home.count / ($scope.votes.home.count + $scope.votes.draw.count + $scope.votes.away.count)) * 100).toFixed(0);
                $scope.voteDrawPercent = Math.floor(($scope.votes.draw.count / ($scope.votes.home.count + $scope.votes.draw.count + $scope.votes.away.count)) * 100).toFixed(0);
//                $scope.voteAwayPercent = Math.floor(($scope.votes.away.count / ($scope.votes.home.count + $scope.votes.draw.count + $scope.votes.away.count)) * 100).toFixed(0);
                $scope.voteAwayPercent = 100 - ($scope.voteHomePercent + $scope.voteDrawPercent);

                if(_.isNaN($scope.voteHomePercent) || $scope.voteHomePercent === 'NaN') $scope.voteHomePercent = 0;
                if(_.isNaN($scope.voteDrawPercent) || $scope.voteDrawPercent === 'NaN') $scope.voteDrawPercent = 0;
                if(_.isNaN($scope.voteAwayPercent) || $scope.voteAwayPercent === 'NaN') $scope.voteAwayPercent = 0;

            }
        }, true);

        $scope.upcomingMatch = function () {
            var el = angular.element('.upcoming-match');
            var l = el.length;
            var start = 0;

            if(l > 2) {
                setInterval(function(){
                    $scope.$apply(function(){
                        angular.element('.upcoming-match').hide();
                        for (i=start; i<(start+2); i++) {
                            angular.element('.upcoming-match[data-idx="' + i + '"]').fadeIn();
                        }
                        start = i;
                        if(start >= l) start = 0;
                    });
                }, 10000);
            } else {
                angular.element('.upcoming-match').fadeIn();
            }
        }

        $scope.upcomingMatch();


        $scope.refreshNewsSlide = function () {
            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/dailyNews?lang=' + $scope.language + '&limit=8'
            })
                .success(function (response) {
                    $scope.news = response;

                    var pp = 4;
                    var mm = [];
                    for (var i = 0; i <= response.data.length; i++) {
                        if (!_.isUndefined(response.data[i]) && !_.isUndefined(response.ontimelines[response.data[i].newsId])) {

                            if (mm.length === 0) mm.push([]);
                            if (mm[mm.length - 1].length >= pp) mm.push([]);
                            mm[mm.length - 1].push(response.data[i]);

                        }
                    }

                    var i = 0;
                    if (mm.length > 0) {
                        clearInterval($scope.newsTopSlideInterval);
                        $scope.newsTopSlide = mm[i];
                        angular.element('#news-top-slide-box').fadeIn();

                        $scope.newsTopSlideInterval = setInterval(function () {
                            $scope.$apply(function () {
                                i++;
                                if (_.isUndefined(mm[i])) i = 0;
                                $scope.newsTopSlide = mm[i];

                            });
                        }, 5000);
                    }
                })
                .error(function(data, status, headers, config) {

                });
        }

        $scope.refreshNewsSlide();

    });