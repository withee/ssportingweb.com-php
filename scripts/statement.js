angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope,$scope, $http) {
        $scope.selected = 'all';

        $scope.getStatement = function () {
            $http({
                method: 'get',
                url: $rootScope.apiUrl+'/statement/total_statement?uid=' + $scope.userInfo.uid
            })
                .success(function (response) {
                    $scope.statements = response;
                });
        }

        $scope.$watchCollection('userInfo', function (newVal, oldVal) {
            if (!_.isUndefined(newVal)) {
                $scope.getStatement();
            }
        });
    })
;