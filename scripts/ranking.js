angular.module('scorspot.controller', [])
    .controller('mainCtrl', function ($rootScope,$scope, $http, $FB) {

        $scope.fb_uid = document.getElementById('rankingScript').getAttribute('fb_uid');

        $scope.getCurrentUserFollowing = function (offset) {
            if (_.isUndefined(offset)) offset = 0;

            $http({
                method: 'GET',
                url: $rootScope.apiUrl+'/following?fb_uid=' + $scope.facebookInfo.id + '&offset=' + offset
            })
                .success(function (response) {
                    var o = response;
                    var uid = [];
                    _.each(response.user, function (val) {
                        if (val.fb_uid) uid.push(val.fb_uid);
                    });
                    uid = uid.join(',');

                    $FB.api({
                        method: 'fql.query',
                        query: 'SELECT uid, name, username, current_location, sex, status, online_presence, profile_blurb, devices, quotes FROM user WHERE uid IN (' + uid + ')'
                    }, function (response) {
                        _.each(o.user, function (val) {
                            val.original = _.findWhere(response, { uid: val.fb_uid });
                        });
                        $scope.currentUserFollowing = o;
                    });
                });
        }

        $scope.checkIsFollowing = function (id) {
            if (!_.isUndefined($scope.currentUserFollowing) && $scope.currentUserFollowing.user[id]) {
                return true;
            }

            return false;
        }

        $scope.follow = function (follow, fb_uid) {

            if (_.isUndefined($scope.facebookInfo)) return false;
            if (_.isUndefined($scope.fb_uid) && fb_uid) return false;
            if (_.isUndefined(fb_uid)) fb_uid = $scope.fb_uid;

            if (follow) {
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/follow?fb_uid=' + $scope.facebookInfo.id + '&fb_follow_uid=' + fb_uid + '&checked=Y'
                })
                    .success(function (response) {
                        if (response) {
                            $scope.currentUserFollowing.user[fb_uid] = true;
                        }
                    });
            } else {
                $http({
                    method: 'GET',
                    url: $rootScope.apiUrl+'/follow?fb_uid=' + $scope.facebookInfo.id + '&fb_follow_uid=' + fb_uid + '&checked=N'
                })
                    .success(function (response) {
                        if (response) {
                            $scope.currentUserFollowing.user[fb_uid] = false;
                        }
                    });
            }

        }

        $scope.$watch('facebookInfo', function (newVal) {
            if (!_.isUndefined(newVal) && !_.isUndefined(newVal.id)) {
                $scope.getCurrentUserFollowing();

            }
        });
    });
