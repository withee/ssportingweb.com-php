<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$service_liveMatch = Services::getLiveMatch();
$service_liveWait = Services::getLiveWait();

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

$footerScript .= '<script src="scripts/statement.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div>


    <!--Content-->
    <div class="wrapper-content content-profile">

        <div class="bgHead">
            <div class="tab-heading-title" style="float: left; margin: 10px;"><img src="images/icon/statement.png" style="width: 22px;"> Statement</div>
            <div class="selectStatementType" style="float: right; margin: 10px;">
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                        <span ng-init="search = ''" ng-switch="search">
                            <span ng-switch-when="">ทั้งหมด</span>
                            <span ng-switch-when="ball_game">Game</span>
                            <span ng-switch-when="mini_game">Mini-Game</span>
                        </span>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <li role="presentation"><a ng-click="search = ''" role="menuitem" tabindex="-1">ทั้งหมด</a></li>
                        <li role="presentation"><a ng-click="search = 'ball_game'" role="menuitem" tabindex="-1">Game</a></li>
                        <li role="presentation"><a ng-click="search = 'mini_game'" role="menuitem" tabindex="-1">Mini-Game</a></li>
                    </ul>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div class="tableStatement table-striped" style="margin: 0px 10px 0px 10px;">

            <table>
                <thead>
                    <tr>
                        <th>วันที่/เวลา</th>
                        <th>รายการ</th>
                        <th colspan="2">
                            เข้า

                        </th>
                        <th colspan="2">ออก

                        </th>
                        <th colspan="2">คงเหลือ

                        </th>
                    </tr>
                    <tr style="background-color: #fff;">
                        <td colspan="2" style="border: 0;">&nbsp;</td>
                        <td class="text-Sgold"><b>Sgold</b></td>
                        <td class="text-Scoin"><b>Scoin</b></td>
                        <td class="text-Sgold"><b>Sgold</b></td>
                        <td class="text-Scoin"><b>Scoin</b></td>
                        <td class="text-Sgold"><b>Sgold</b></td>
                        <td class="text-Scoin"><b>Scoin</b></td>
<!--                        <td>

                            <table class="statementCoins">
                                <tr>
                                    <td class="text-Sgold"><b>Sgold</b></td>
                                    <td class="text-Scoin"><b>Scoin</b></td>
                                </tr>

                            </table>
                        </td>
                        <td>
                            <table class="statementCoins">
                                <tr>
                                    <td class="text-Sgold"><b>Sgold</b></td>
                                    <td class="text-Scoin"><b>Scoin</b></td>
                                </tr>

                            </table>
                        </td>
                        <td>
                            <table class="statementCoins">
                                <tr>
                                    <td class="text-Sgold"><b>Sgold</b></td>
                                    <td class="text-Scoin"><b>Scoin</b></td>
                                </tr>

                            </table>
                        </td>-->
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in statements| filter:search">
                        <td class="dateTimestamp">{{ item.statement_timestamp | formatDate:'D/M/YYYY HH:mm' }}</td>
                        <td class="listType">
                            <span ng-switch="item.statement_type">
                                <span ng-switch-when="ball_game">Game</span>
                                <span ng-switch-when="mini_game">Mini-Game</span>
                                <span ng-switch-when="login">Login</span>
                                <span ng-switch-when="today_bonus">Today Bonus</span>
                                <span ng-switch-when="exchange">Exchange</span>
                                <span ng-switch-when="reset_sgold">Reset SGold</span>
                                <span ng-switch-when="deposit">Deposit</span>
                                <span ng-switch-when="withdraw">withdraw</span>
                                <span ng-switch-default>-</span>
                            </span>
                        </td>
                        <td class="text-Sgold">{{ item.sgold_income | number }}</td>
                        <td class="text-Scoin">{{ item.scoin_income | number }}</td>
                        <td class="text-Sgold">{{ item.sgold_outcome | number }}</td>
                        <td class="text-Scoin">{{ item.scoin_outcome | number }}</td>
                        <td class="text-Sgold">{{ item.balance_sgold | number }}</td>
                        <td class="text-Scoin totalScoin">{{ item.balance_scoin | number }}</td>

<!--                        <td>
    <table class="displayCoin">
        <tr>
            <td class="text-Sgold">{{ item.sgold_income | number }}</td>
            <td class="text-Scoin">{{ item.scoin_income | number }}</td>
        </tr>

    </table>

</td>
<td>
    <table class="displayCoin">
        <tr>
            <td class="text-Sgold">{{ item.sgold_outcome | number }}</td>
            <td class="text-Scoin">{{ item.scoin_outcome | number }}</td>
        </tr>

    </table>
</td>
<td>
    <table class="displayCoin">
        <tr>
            <td class="text-Sgold">{{ item.balance_sgold | number }}</td>
            <td class="text-Scoin totalScoin">{{ item.balance_scoin | number }}</td>
        </tr>

    </table>
</td>-->
                    </tr>
                </tbody>

            </table>

        </div>


    </div>


    <?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
