<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$service_liveMatch = Services::getLiveMatch();
$service_liveWait = Services::getLiveWait();

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

$footerScript .= '<script src="scripts/main.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div ng-controller="mainCtrl">


    <!--Content-->
    <div class="wrapper-content content-profile">

        <div class="wrapper-miniGames">
            <div class="tab-heading-title"><img src="images/mini-game/paoyingchoob/title_icon_g1.png"> เกมส์เป่ายิงฉุบ</div>
            <div class="totalPoint paoyingchoob">
                <b>คุณแพ้</b> <br>
                <div class="resultAlert wrong" style="display: none;">คุณทายผิด</div>    
            </div>
            <div class="bodyGames">
                <table>
                    <tr>
                        <td>
                            <span class="player"><img src="https://graph.facebook.com/100001037283424/picture" style="width: 40px;"></span>
                            <img src="images/mini-game/paoyingchoob/L_cut_g1.png">
                        </td>
                        <td class="vsGames"><img src="images/mini-game/paoyingchoob/V_S_Image.png"></td>
                        <td>
                            <span class="player"><img src="images/mini-game/paoyingchoob/avata_GM_g1.png" style="width: 40px;"></span>
                            <img src="images/mini-game/paoyingchoob/R_paper_g1.png">
                        </td>
                    </tr>
                </table>
            </div>
            <div class="selectCoin">
                <table>
                    <tr>
                        <td class="selectCoin-100"></td>
                        <td class="selectCoin-200  activeCoin-200"></td>
                        <td class="selectCoin-500"></td>
                    </tr>
                </table>
            </div>

            <div class="selectWeapon">
                <table>
                    <tr>
                        <td class="weaponScissors"></td>                      
                        <td class="weaponHammer"></td>
                        <td class="weaponPaper activePaper"></td>
                    </tr>
                </table>
            </div>

            <div class="weaponName">
                <table>
                    <tr>
                        <td>กรรไกร</td>                      
                        <td>ค้อน</td>
                        <td>กระดาษ</td>
                    </tr>
                </table>
            </div>


            <!--            ตารางสรุปผลการเล่นเกมส์-->
            <div class="titleGames">ตารางสรุปผลการเล่นเกมส์</div>

            <div class="table-ResultMiniGames">
                <div class="tabs-tableResult">
                    <ul>
                        <li class="active">เฉพาะฉัน</li>
                        <li>ทุกคน</li>
                    </ul>
                    <div style="clear: both;"></div>
                </div>

                <table>
                    <thead>
                        <tr>
                            <th>ชื่อผู้เล่น</th>
                            <th>ชื่อเกมส์</th>
                            <th>ผู้เล่น</th>
                            <th>คอมพ์</th>
                            <th>ผลการเล่น</th>
                            <th>จำนวน</th>
                            <th>ได้/เสีย</th>
                            <th>วันที่/เวลา</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><img src="images/imguser.png"> หลวงปู่เณรคำ</td>
                            <td>เปายิงฉุบ</td>
                            <td><img src="images/mini-game/paoyingchoob/slt_defualt_paper.png"></td>
                            <td><img src="images/mini-game/paoyingchoob/slt_defualt_hammer.png"></td>
                            <td>ชนะ</td>
                            <td><img src="images/icon/scoin30.png"> 100</td>
                            <td><img src="images/icon/scoin30.png"> 200</td>
                            <td>22/6/57 10:00</td>
                        </tr>
                        <tr class="bgGray">
                            <td><img src="images/imguser.png"> หลวงปู่เณรคำ</td>
                            <td>เปายิงฉุบ</td>
                            <td><img src="images/mini-game/paoyingchoob/slt_defualt_paper.png"></td>
                            <td><img src="images/mini-game/paoyingchoob/slt_defualt_hammer.png"></td>
                            <td>ชนะ</td>
                            <td><img src="images/icon/scoin30.png"> 100</td>
                            <td><img src="images/icon/scoin30.png"> 200</td>
                            <td>22/6/57 10:00</td>
                        </tr>
                        <tr>
                            <td><img src="images/imguser.png"> หลวงปู่เณรคำ</td>
                            <td>เปายิงฉุบ</td>
                            <td><img src="images/mini-game/paoyingchoob/slt_defualt_paper.png"></td>
                            <td><img src="images/mini-game/paoyingchoob/slt_defualt_hammer.png"></td>
                            <td>ชนะ</td>
                            <td><img src="images/icon/scoin30.png"> 100</td>
                            <td><img src="images/icon/scoin30.png"> 200</td>
                            <td>22/6/57 10:00</td>
                        </tr>
                        <tr class="bgGray">
                            <td><img src="images/imguser.png"> หลวงปู่เณรคำ</td>
                            <td>เปายิงฉุบ</td>
                            <td><img src="images/mini-game/paoyingchoob/slt_defualt_paper.png"></td>
                            <td><img src="images/mini-game/paoyingchoob/slt_defualt_hammer.png"></td>
                            <td>ชนะ</td>
                            <td><img src="images/icon/scoin30.png"> 100</td>
                            <td><img src="images/icon/scoin30.png"> 200</td>
                            <td>22/6/57 10:00</td>
                        </tr>
                        <tr>
                            <td><img src="images/imguser.png"> หลวงปู่เณรคำ</td>
                            <td>เปายิงฉุบ</td>
                            <td><img src="images/mini-game/paoyingchoob/slt_defualt_paper.png"></td>
                            <td><img src="images/mini-game/paoyingchoob/slt_defualt_hammer.png"></td>
                            <td>ชนะ</td>
                            <td><img src="images/icon/scoin30.png"> 100</td>
                            <td><img src="images/icon/scoin30.png"> 200</td>
                            <td>22/6/57 10:00</td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>






    </div>
</div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
