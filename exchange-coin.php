<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$footerScript .= '<script src="scripts/exchange-coin.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div>
    <!--Content-->
    <div class="wrapper-content content-profile">

        <div class="boxLeft">
            <div class="tabHead">แลก Sgold เป็น Diamond</div>
            <div class="boxCoinExchange">
                <table>
                    <tr>
                        <td><img src="images/icon/sgold.png"></td>
                        <td><b ng-bind="userInfo.sgold || 0"></b><br>Sgold ของคุณ</td>
                    </tr>
                </table>
            </div>

            <div class="boxSgoldText">
                <table>
                    <tr>
                        <td>จำนวนSgoldที่ต้องการแลก : <input ng-model="sgoldAmount" type="text" placeholder="0"></td>
                    </tr>
                    <tr>
                        <td style="text-align: right; padding-right: 10px;"><span ng-bind="systemConfig.sgold_to_diamond || '?'"></span> Sgold = 1 Diamond</td>
                    </tr>
                </table>
            </div>
            <div class="buttonExchange">
                <button class="btn btn-success btn-large" ng-click="exchangeDiamond()">แลกเปลี่ยน</button>
            </div>

            <div class="boxCoinExchange">
                <table>
                    <tr>
                        <td><img src="images/icon/diamond.png"></td>
                        <td><b ng-bind="userInfo.diamond || 0"></b><br>Diamond ของคุณ</td>
                    </tr>
                </table>
            </div>


        </div>


        <div class="boxRightside">
            <div class="bgResetSgold" ng-click="resetSgold()">
                <table>
                    <tr>
                        <td><img src="images/icon/reset.png"></td>
                        <td><b>รีเซ็ต SGOLD</b> <br>กดเพื่อ reset Sgold ให้เป็น 0</td>
                    </tr>
                </table>
            </div>
            <div class="boxRight">
                <div class="tabHead">แลก Combo เป็น Diamond</div>

                <div class="comboBox">
                    <b ng-bind="userInfo.step_combo_count || 0"></b><br><span>Combo ต่อเนื่องของคุณ</span>
                </div>
                
                <div class="boxExchange">
                    <button class="btn btn-success btn-large" ng-click="exchangeStepBonus()"><img src="images/icon/downup.png" style="width: 22px;"> แลก</button>
                </div>

                <div class="boxCoinExchange">
                    <table>
                        <tr>
                            <td><img src="images/icon/diamond.png"></td>
                            <td><b ng-bind="userInfo.diamond || 0"></b><br>Diamond ของคุณ</td>
                        </tr>
                    </table>
                </div>

            </div>
        </div>
        
        <div style="clear: both;"></div>
        
        <div class="wrapper-box-feed-expand">
            <div class="headingBox">กฏ กติกาการแลกรางวัล</div>
            
        </div>
        



    </div>

</div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
