<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$service_liveMatch = Services::getLiveMatch();
$service_liveWait = Services::getLiveWait();

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

$footerScript .= '<script src="scripts/games/treasure.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div ng-controller="mainCtrl">


    <!--Content-->
    <div class="wrapper-content content-profile">

        <div class="wrapper-miniGames">
            <div class="tab-heading-title tabTypeGames"><img src="images/mini-game/chest/title_icon_g5.png" width="40px;" style="float: left;"> เกมส์หีบสมบัติ</div>
            <div class="bodyGames TreasureChest">
                <table>
                    <tr>
                        <td ng-click="play('treasure_low')" ng-class="{'activeNormalLevel':choose === 'treasure_low'}" class="chestNormalLevel">
                            <div class="coinChest normal"><img src="images/icon/scoin.png" style="width: 18px;"> 100</div>
                        </td>
                        <td ng-click="play('treasure_high')" ng-class="{'activeMaximumLevel':choose === 'treasure_high'}" class="chestMaximumLevel">
                            <div class="coinChest maximum"><img src="images/icon/scoin.png" style="width: 20px;"> 500</div>
                        </td>
                        <td ng-click="play('treasure_medium')" ng-class="{'activeHightLevel':choose === 'treasure_medium'}" class="chestHightLevel">
                            <div class="coinChest hight"><img src="images/icon/scoin.png" style="width: 18px;"> 250</div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="PlayGames-1">
                <div class="NoteButton"><img src="/images/mini-game/chest/note_game.png"></div>
            </div>


            <div class="titleGames">วิธีการเล่นเกมส์</div>
            <div class="boxHowToPlay-Games">
                <ul>
                    <li><b>เกมส์หีบสมบัติเป็นเกมส์ที่ให้ผู้เล่นใช้เหรียญ Scoin ในการเปิดหีบสมบัติ โดยจะได้โบนัสจากเกมส์ สามารถเล่นได้ 10 ครั้ง ต่อ 1 วัน โดยมีวิธีการเล่นดังนี้</b></li>
                    <li>- เลือกหีบสมบัติที่ต้องการเปิด โดยมีหีบสมบัติอยู่ 3 หีบ ดังนี้</li>
                    <li>1. หีบสมบัติระดับธรรมดา ใช้เหรียญเปิด 100 เหรียญ Scoin / ถ้าชนะจะได้ 100 เหรียญ Scoin คืน</li>
                    <li>2. หีบสมบัติระดับสูง ใช้เหรียญเปิด 250 เหรียญ Scoin / ถ้าชนะจะได้ 250 เหรียญ Scoinคืน</li>
                    <li>3. หีบสมบัติระดับสูงสุด ใช้เหรียญเปิด 500 เหรียญ Scoin / ถ้าชนะจะได้ 500 เหรียญ Scoin คืน</li>
                    <li>ผลการเปิดหีบสมบัติแสดงให้คุณได้โบนัสเท่าไหร่  โดยสามารถดูสถิติการเปิดหีบสมบัติได้ที่ตารางสรุปผลการเล่นเกมส์</li>
                </ul>
            </div>

            <!--            ตารางสรุปผลการเล่นเกมส์-->
            <div class="titleGames">ตารางสรุปผลการเล่นเกมส์</div>
            <div class="table-ResultMiniGames">
                <div class="tabs-tableResult">
                    <ul>
                        <li ng-click="stateTab = 'user'" ng-class="{'active':stateTab === 'user'}">เฉพาะฉัน</li>
                        <li ng-click="stateTab = 'all'" ng-class="{'active':stateTab === 'all'}">ทุกคน</li>
                    </ul>
                    <div style="clear: both;"></div>
                </div>

                <table>
                    <thead>
                        <tr>
                            <th>ชื่อผู้เล่น</th>
                            <th>ชื่อเกมส์</th>
                            <th>ผู้เล่น</th>
                            <th>ผลการเล่น</th>
                            <th>จำนวน</th>
                            <th>ได้</th>
                            <th>วันที่/เวลา</th>
                        </tr>
                    </thead>
                    <tbody ng-show="stateTab === 'user'">
                        <tr ng-repeat="item in statement.user">
                            <td><a href="profile.php?id={{ item.fb_uid}}"><img
                                        ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"> {{ item.display_name ||
                                item.fb_firstname }}</a></td>
                            <td>หีบสมบัติ</td>
                            <td>
                                <span ng-switch="item.game_type">
                                    <span ng-switch-when="treasure_low">ธรรมดา</span>
                                    <span ng-switch-when="treasure_high">สูงสุด</span>
                                    <span ng-switch-when="treasure_medium">สูง</span>
                                </span>
                            </td>
                            <td>
                                {{ item.player2}}
                            </td>
                            <td>
                                <img src="images/icon/scoin30.png"> {{ item.scoin_amount}}
                            </td>
                            <td>
                                <img src="images/icon/sgold30.png"> {{ item.scoin_result_amount}}
                            </td>
                            <td>{{ item.play_timestamp | timeagoByDateTime }}</td>
                        </tr>
                    </tbody>
                    <tbody ng-show="stateTab === 'all'">
                        <tr ng-repeat="item in statement.all">
                            <td><a href="profile.php?id={{ item.fb_uid}}"><img
                                        ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"> {{ item.display_name ||
                                item.fb_firstname }}</a></td>
                            <td>หีบสมบัติ</td>
                            <td>
                                <span ng-switch="item.game_type">
                                    <span ng-switch-when="treasure_low">ธรรมดา</span>
                                    <span ng-switch-when="treasure_high">สูงสุด</span>
                                    <span ng-switch-when="treasure_medium">สูง</span>
                                </span>
                            </td>
                            <td>
                                {{ item.player2}}
                            </td>
                            <td>
                                <img src="images/icon/scoin30.png"> {{ item.scoin_amount}}
                            </td>
                            <td>
                                <img src="images/icon/sgold30.png"> {{ item.scoin_result_amount}}
                            </td>
                            <td>{{ item.play_timestamp | timeagoByDateTime }}</td>
                        </tr>
                    </tbody>
                </table>

            </div>

        </div>


        <!--alert จำนวนเงินที่ได้รับจากการเปิดหีบ-->
        <div id="rewardModal" class="Modal-resultGame" style="display: none;">
            <div class="fadeGame"></div>
            <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="false" style="display: block; z-index: 5; top: 200px; background: none; box-shadow: none; border: 0;">

                <div class="alertGift">
                    <div class="titleModal"><img src="images/mini-game/chest/title_modal.png"></div>
                    <img src="images/mini-game/chest/modal_sgold.png">
                    <h3>{{ reward }}</h3>
                    <div class="titleModal" style="cursor: pointer;"><img ng-click="close()" src="images/mini-game/chest/btn_modal_defualt.png"></div>
                </div>

            </div>
        </div>

    </div>
</div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
