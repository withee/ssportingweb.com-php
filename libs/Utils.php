<?php

class Utils
{
    protected static $languages = false;
    public static $availableGmtOffset = array(
        array('offset' => -11, 'label' => 'GMT -11:00'),
        array('offset' => -10, 'label' => 'GMT -10:00'),
        array('offset' => -9, 'label' => 'GMT -9:00'),
        array('offset' => -8, 'label' => 'GMT -8:00'),
        array('offset' => -7, 'label' => 'GMT -7:00'),
        array('offset' => -6, 'label' => 'GMT -6:00'),
        array('offset' => -5, 'label' => 'GMT -5:00'),
        array('offset' => -4, 'label' => 'GMT -4:00'),
        array('offset' => -3.5, 'label' => 'GMT -3:30'),
        array('offset' => -3, 'label' => 'GMT -3:00'),
        array('offset' => -2, 'label' => 'GMT -2:00'),
        array('offset' => -1, 'label' => 'GMT -1:00'),
        array('offset' => 0, 'label' => 'GMT +0:00'),
        array('offset' => 1, 'label' => 'GMT +1:00'),
        array('offset' => 2, 'label' => 'GMT +2:00'),
        array('offset' => 3, 'label' => 'GMT +3:00'),
        array('offset' => 3.5, 'label' => 'GMT +3:30'),
        array('offset' => 4, 'label' => 'GMT +4:00'),
        array('offset' => 5, 'label' => 'GMT +5:00'),
        array('offset' => 6, 'label' => 'GMT +6:00'),
        array('offset' => 7, 'label' => 'GMT +7:00'),
        array('offset' => 8, 'label' => 'GMT +8:00'),
        array('offset' => 9, 'label' => 'GMT +9:00'),
        array('offset' => 9.5, 'label' => 'GMT +9:30'),
        array('offset' => 10, 'label' => 'GMT +10:00'),
        array('offset' => 11, 'label' => 'GMT +11:00'),
        array('offset' => 12, 'label' => 'GMT +12:00'),
        array('offset' => 13, 'label' => 'GMT +13:00'),
    );

    public static $sidLabel = array(
        '1' => 'schedule',
        '2' => 'RUN',
        '3' => 'HT',
        '4' => 'RUN',
        '5' => 'FT',
        '6' => 'ET',
        '7' => 'Pen.',
        '8' => 'AET',
        '9' => 'AP',
        '10' => 'ABD',
        '11' => 'Postp.',
        '12' => 'FT only',
        '13' => "Susp.",
        '14' => "Pen.",
        '15' => "AP",
        '16' => "W.O.",
        '17' => "ANL"
    );

    public static function highlightCompetition()
    {
        return array(
//            '122', // China
//            '144', // Japan
            '152', // Thailand
            '43', // France
            '59', // England
//            '66', // Holland
            '70', // Spain
            '78', // Germany
            '80', // Italy
            '80', // Argentina
        );
    }

    public static function isHighlightCompetition($id)
    {
        if (in_array($id, self::highlightCompetition())) return true;

        return false;
    }

    protected static function y($cx)
    {
        if ((int)$cx > 0) {
            return (int)$cx;
        } else {
            return 45;
        }
    }

    public static function getCurrentGmtOffset()
    {
        $offset = __GMT_OFFSET__;
        !$offset and $offset = 0;

        $item = __::find(self::$availableGmtOffset, function ($val) use ($offset) {
            return $val['offset'] == $offset;
        });

        return $item['label'];
    }

    public static function getCurrentLanguage()
    {
        return __LANGUAGE__;
    }


    protected static function loadLanguage()
    {
        if (file_exists(__DOCS_ROOT__ . '/languages/' . __LANGUAGE__ . '.php'))
            self::$languages = include(__DOCS_ROOT__ . '/languages/' . __LANGUAGE__ . '.php');
        else
            self::$languages = include(__DOCS_ROOT__ . '/languages/en.php');
    }

    public static function trans($key)
    {
        !self::$languages and self::loadLanguage();

        if (isset(self::$languages[$key]))
            return self::$languages[$key];
        else
            return $key;
    }

    public static function kickTime($datetime)
    {
        $datetime = preg_replace("/\.0$/", '', $datetime);
        return date('H:i', strtotime($datetime . self::getCurrentGmtOffset()));
    }

    public static function wldStr($str)
    {
        if ($str == 'w') {
            return 'win';
        } else if ($str == 'l') {
            return 'lose';
        } else if ($str == 'd') {
            return 'draw';
        } else {
            return false;
        }
    }

    public static function getCurrentMin($c0, $c1, $c2, $cx, $sid)
    {
        $str = '-';
        $fix = 0;

        if ($sid == 2) {
            $ap = self::y($cx);
            $ao = ((date('U', strtotime(date("Y-m-d H:i:s") . self::getCurrentGmtOffset())) / 10) - (int)$c0 - (int)$c1) / 60;

            if ($ao < 1) {
                $str = 1 . "'";
            } else if ($ao > $ap) {
                return $ao;
                $str = floor($ap) . $fix . "+'";
            } else {
                $str = floor($ao) . $fix . "'";
            }
        } else if ($sid == 4) {
            $ap = self::y($cx);

            $ao = floor((((date('U', strtotime(self::getCurrentGmtOffset()))) / 1000) - (int)$c0 - (int)$c2) / 60 + $ap);
            if ($ao <= $ap) {
                $str = floor($ap + 1) . $fix . "'";
            } else if ($ao > $ap) {
                $ap = floor($ap * 2);
                if ($ao > $ap) {
                    $str = $ap . "+'";
                } else {
                    $str = $ao . $fix . "'";
                }
            }
        }

        return $str;
    }

    public static function getScore($score, $side = false)
    {

        $r = '';

        switch ($side) {
            case 'h':
                $r = reset(explode('-', $score));
                break;
            case 'a':
                $r = end(explode('-', $score));
                break;
            case 'space-colon':
                $r = reset(explode('-', $score)) . ' : ' . end(explode('-', $score));
            default:
        }

        return $r;

    }

    public static function dayOfWeek($d, $t = 'normal')
    {
        $normal = array(
            '0' => self::trans('Sunday'),
            '1' => self::trans('Monday'),
            '2' => self::trans('Tuesday'),
            '3' => self::trans('Wednesday'),
            '4' => self::trans('Thursday'),
            '5' => self::trans('Friday'),
            '6' => self::trans('Saturday')
        );

        return ${$t}[$d];
    }

    public static function monthOfYear($m, $t = 'normal')
    {
        $normal = array(
            '1' => self::trans('January'),
            '2' => self::trans('February'),
            '3' => self::trans('March'),
            '4' => self::trans('April'),
            '5' => self::trans('May'),
            '6' => self::trans('June'),
            '7' => self::trans('July'),
            '8' => self::trans('August'),
            '9' => self::trans('September'),
            '10' => self::trans('October'),
            '11' => self::trans('November'),
            '12' => self::trans('December')
        );

        $short = array(
            '1' => self::trans('Jan'),
            '2' => self::trans('Feb'),
            '3' => self::trans('Mar'),
            '4' => self::trans('Apr'),
            '5' => self::trans('May'),
            '6' => self::trans('Jun'),
            '7' => self::trans('Jul'),
            '8' => self::trans('Aug'),
            '9' => self::trans('Sep'),
            '10' => self::trans('Oct'),
            '11' => self::trans('Nov'),
            '12' => self::trans('Dec')
        );

        return ${$t}[$m];
    }

    public static function parseSignedRequest($signed_request)
    {
        list($encoded_sig, $payload) = explode('.', $signed_request, 2);

        $secret = "93e21cf1551a4536d20843fcdf236402"; // Use your app secret here

        // decode the data
        $sig = self::base64_url_decode($encoded_sig);
        $data = json_decode(self::base64_url_decode($payload), true);

        // confirm the signature
        $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
        if ($sig !== $expected_sig) {
            error_log('Bad Signed JSON signature!');
            return null;
        }

        return $data;
    }

    public static function base64_url_decode($input)
    {
        return base64_decode(strtr($input, '-_', '+/'));
    }

    public static function isVideo($url)
    {
        return preg_match('/(http|https):\/\/(www\.)*youtube\.com\/.*/', $url)
        || preg_match('/(http|https):\/\/(www\.)*vimeo\.com\/.*/', $url)
        || preg_match('/(http|https):\/\/(www\.)*dailymotion\.com\/.*/', $url);
    }

    public static function hasImage($list)
    {
        $e = array('png', 'jpg', 'gif');

        $ext = explode('.', $list[0]->path);
        $ext = end($ext);
        if (in_array($ext, $e)) {
            return true;
        }

        return false;
    }

}