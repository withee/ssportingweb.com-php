<?php

class Services
{
//    public static $host = 'http://api.ssporting.com';
//    public static $host = 'http://128.199.109.220:8080';
      public static $host = 'http://newapi.ssporting.com';

    public static function getLiveMatch()
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage();
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/liveMatch.json';

        if (!file_exists($file) || date('U', strtotime('100 minute ago')) > date('U', filemtime($file))) {
            $data = file_get_contents(self::$host . '/liveMatchOnly', false, stream_context_create(array(
                'http' => array(
                    'header' => 'Connection: close'
                )
            )));

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }

    public static function getLiveWait()
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage();
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/liveWait.json';

        if (!file_exists($file) || date('U', strtotime('100 minute ago')) > date('U', filemtime($file))) {
            $data = file_get_contents(self::$host . '/liveMatchWaitOnly', false, stream_context_create(array(
                'http' => array(
                    'header' => 'Connection: close'
                )
            )));

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }

    public static function getLiveWaitSorted()
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage();
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/liveWaitSorted.json';

        if (!file_exists($file) || date('U', strtotime('10 minute ago')) > date('U', filemtime($file))) {
            $data = file_get_contents(self::$host . 'sortedliveMatchWaitOnly', false, stream_context_create(array(
                'http' => array(
                    'header' => 'Connection: close'
                )
            )));

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }

    public static function getTodayResult()
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage();
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/todayResult.json';

        if (!file_exists($file) || date('U', strtotime('10 minute ago')) > date('U', filemtime($file))) {
            $data = file_get_contents(self::$host . '/liveMatchResultOnly', false, stream_context_create(array(
                'http' => array(
                    'header' => 'Connection: close'
                )
            )));

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }

    public static function getYesterdayResult()
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage();
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/yesterdayResult.json';
        if (!file_exists($file) || date('U', strtotime('100 minute ago')) > date('U', filemtime($file))) {
            $data = file_get_contents(self::$host . '/yesterdayResultOnly', false, stream_context_create(array(
                'http' => array(
                    'header' => 'Connection: close'
                )
            )));

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }

    public static function getCompetition()
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage();
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/competition.json';

        if (!file_exists($file) || date('U', strtotime('1 hour ago')) > date('U', filemtime($file))) {
            $data = file_get_contents(self::$host . '/competitionOnlyWithSubLeague', false, stream_context_create(array(
                'http' => array(
                    'header' => 'Connection: close'
                )
            )));

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }

    public static function getAllLeague()
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage();
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/all_leagues.json';

        if (!file_exists($file) || date('U', strtotime('1 hour ago')) > date('U', filemtime($file))) {
            $data = file_get_contents(self::$host . '/getAllLeagues?lang=' . Utils::getCurrentLanguage(), false, stream_context_create(array(
                'http' => array(
                    'header' => 'Connection: close'
                )
            )));

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }

    public static function getAllTeam()
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage();
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/all_teams.json';

        if (!file_exists($file) || date('U', strtotime('1 hour ago')) > date('U', filemtime($file))) {
            $data = file_get_contents(self::$host . '/getAllTeams?lang=' . Utils::getCurrentLanguage(), false, stream_context_create(array(
                'http' => array(
                    'header' => 'Connection: close'
                )
            )));

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }

    public static function getMatchInfo($mid)
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage() . '/match';
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/' . $mid . '.json';

        if (!file_exists($file) || date('U', strtotime('5 minute ago')) > date('U', filemtime($file))) {
            $data = file_get_contents(self::$host . '/getGameData?mid=' . $mid, false, stream_context_create(array(
                'http' => array(
                    'header' => 'Connection: close'
                )
            )));

//            echo $data; exit;

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }

    public static function getLeagueInfo($id)
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage() . '/league';
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/' . $id . '.json';

        if (!file_exists($file) || date('U', strtotime('1 hour ago')) > date('U', filemtime($file))) {
            $data = file_get_contents(self::$host . '/leagueMainOnlys/en/' . $id, false, stream_context_create(array(
                'http' => array(
                    'header' => 'Connection: close'
                )
            )));

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }

    public static function getTeamInfo($id)
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage() . '/team';
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/' . $id . '.json';

        if (!file_exists($file) || date('U', strtotime('1 hour ago')) > date('U', filemtime($file))) {
            $data = file_get_contents(self::$host . '/teamOnlys/' . Utils::getCurrentLanguage() . '/' . $id, false, stream_context_create(array(
                'http' => array(
                    'header' => 'Connection: close'
                )
            )));

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }

    public static function getProfileFeed($id)
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage() . '/profile';
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/' . $id . '.json';

        if (!file_exists($file) || date('U', strtotime('1 hour ago')) > date('U', filemtime($file))) {
            $data = file_get_contents(self::$host . '/initProfile?fb_uid=' . $id, false, stream_context_create(array(
                'http' => array(
                    'header' => 'Connection: close'
                )
            )));

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }

    public static function getTimelineContent($id)
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage() . '/timeline_content';
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/' . $id . '.json';

        if (!file_exists($file) || date('U', strtotime('5 minutes ago')) > date('U', filemtime($file))) {
            $data = file_get_contents(self::$host . '/timelineSingleContent?id=' . $id, false, stream_context_create(array(
                'http' => array(
                    'header' => 'Connection: close'
                )
            )));

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }

    public static function getW14()
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage();
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/w14.json';

        if (!file_exists($file) || date('U', strtotime('5 minutes ago')) > date('U', filemtime($file))) {
            $data = file_get_contents('http://dowebsite.com:2999/wc2014', false, stream_context_create(array(
                'http' => array(
                    'header' => 'Connection: close'
                )
            )));

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }

    public static function getNews()
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage();
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/news.json';

        if (!file_exists($file) || date('U', strtotime('5 minutes ago')) > date('U', filemtime($file))) {
            $data = file_get_contents(self::$host . '/dailyNews?lang=' . Utils::getCurrentLanguage() . '&limit=8', false, stream_context_create(array(
                'http' => array(
                    'header' => 'Connection: close'
                )
            )));

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }

    public static function getW14ranking()
    {
        $dir = __CACHE_DIR__ . '/' . Utils::getCurrentLanguage();
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $file = $dir . '/w14ranking.json';

        if (!file_exists($file) || date('U', strtotime('1 hour ago')) > date('U', filemtime($file))) {
            $data = file_get_contents(self::$host . '/worldcupsRanking', false, stream_context_create(array(
                'http' => array(
                    'header' => "Connection: close"
                )
            )));

            if (!empty($data)) {
                file_put_contents($file, $data);
            } else if (!file_exists($file)) {
                $data = file_get_contents($file);
            }

            return json_decode($data);
        } else {
            return json_decode(file_get_contents($file));
        }
    }
}