<?php
define('__DOCS_ROOT__', dirname(__FILE__));
define('__CACHE_DIR__', __DOCS_ROOT__ . '/cache');
define('__INCLUDE_DIR__', __DOCS_ROOT__ . '/includes');
if (!empty($_REQUEST['gmtOffset'])) {
    define('__GMT_OFFSET__', $_REQUEST['gmtOffset']);
} else {
    define('__GMT_OFFSET__', 7);
}

if (!empty($_REQUEST['defaultLanguage'])) {
    define('__LANGUAGE__', $_REQUEST['defaultLanguage']);
} else {
    define('__LANGUAGE__', 'en');
}

require_once(__DOCS_ROOT__ . '/libs/vendors/underscore-php/underscore.php');

function __autoload($class_name) {
    include_once(__DOCS_ROOT__ . '/libs/' . $class_name . '.php');
}

$service_matchInfo = Services::getMatchInfo($_REQUEST['id']);
$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.png">

        <title></title>
        <!-- Custom CSS -->
        <link href="/styles/reset.css" rel="stylesheet">
        <link href="/styles/shareFB.css" rel="stylesheet">


    </head>

    <body>

<?php if ($_REQUEST['type'] === 'square') { ?>
            <div class="container-main-shareFB">
                <div class="b1-main-shareFB">
                    <div class="img-b1-main-shareFB">
                        <div class="img-b1-main-shareFB-home"><img src="<?php echo $service_matchInfo->game->h256x256; ?>"/></div>
                        <div class="img-b1-main-shareFB-away"><img src="<?php echo $service_matchInfo->game->a256x256; ?>"/></div>
                        <div class="boxVS">VS</div>
                    </div>
                </div>
            </div>
<?php } else if ($_REQUEST['type'] === 'rectangle') { ?>
            <!--//////Page Streming////////////// -->
            <div class="container-main-page-stm">
                <div class="topic-main-page-stm">
                    <span><?php echo isset($service_allleague->{$service_matchInfo->game->_lid}->name) ? $service_allleague->{$service_matchInfo->game->_lid}->name : $service_matchInfo->game->leagueName; ?> <?php echo Utils::kickTime($service_matchInfo->game->show_date); ?></span>
                </div>
                <div class="main-page-stm-row02">
                    <div class="col-01-page-stm">
                        <span class="col-01-logo-01"><img src="<?php echo $service_matchInfo->game->h64x64; ?>"/></span>
                        <label class="title-logo-01 <?php if ($service_matchInfo->game->hdp < 0): ?>title-home-b1<?php endif; ?>"><?php echo (isset($service_allteam->{$service_matchInfo->game->hid}) && isset($service_allteam->{$service_matchInfo->game->hid}->name)) ? $service_allteam->{$service_matchInfo->game->hid}->name : $service_matchInfo->game->homeName; ?></label>

                        <div class="col-01-logo-01-01"><?php echo isset($service_matchInfo->game->hdp_home) ? $service_matchInfo->game->hdp_home : '-' ?></div>
                        <div class="col-01-page-stm-btn-home"></div>
                    </div>
                    <div class="col-02-page-stm"><?php echo isset($service_matchInfo->game->hdp) ? $service_matchInfo->game->hdp : '-' ?></div>
                    <div class="col-02-page-stm-02">PLAY</div>
                    <div class="col-02-page-stm-text-01">Select Team</div>
                    <div class="col-01-page-stm">
                        <span class="col-01-logo-01"><img src="<?php echo $service_matchInfo->game->a64x64; ?>"/></span>
                        <label class="title-logo-01 <?php if ($service_matchInfo->game->hdp > 0): ?>title-home-b1<?php endif; ?>"><?php echo (isset($service_allteam->{@$service_matchInfo->game->gid}) && isset($service_allteam->{@$service_matchInfo->game->gid}->name)) ? $service_allteam->{$service_matchInfo->game->gid}->name : $service_matchInfo->game->awayName; ?></label>

                        <div class="col-01-logo-01-01 col-01-logo-01-02"><?php echo isset($service_matchInfo->game->hdp_away) ? $service_matchInfo->game->hdp_away : '-' ?></div>
                        <div class="col-01-page-stm-btn-away"></div>
                    </div>
                </div>
            </div>
<?php } ?>
    </body>
</html>
