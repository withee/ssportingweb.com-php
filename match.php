<?php
require_once(dirname(__FILE__) . '/_init_.php');

define('__MID__', $_REQUEST['mid']);

$service_matchInfo = Services::getMatchInfo(__MID__);
$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();
//$service_w14 = Services::getW14();

//echo '<pre>';
//print_r($service_matchInfo);
//echo '</pre>';

$worldcupId = '34189';

$footerScript .= '<script id="matchScript" src="scripts/match.js" mid="' . __MID__ . '"></script>';

use Carbon\Carbon;

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div>

    <!--<div id="feed-top-slide-box" class="wrapper-slide-comment-top" style="display: none;">-->
    <!---->
    <!--    <div class="box-comment-top" ng-repeat="item in feedTopSlide">-->
    <!--        <table>-->
    <!--            <tr>-->
    <!--                <td><a href="/profile.php?id={{ item.fb_uid }}"><img-->
    <!--                            ng-src="http://graph.facebook.com/{{ item.fb_uid }}/picture"/></a></td>-->
    <!--                <td ng-click="openBetDialogFromId(item.mid, $event)">-->
    <!--                    <b>{{ item.fb_firstname }} {{ item.fb_lastname }}</b>-->
    <!--                    <span class="text-blue">Play</span> : <span ng-switch="item.choose">-->
    <!--                        <a href="#!/team/{{ item.hid }}" ng-switch-when="home">{{-->
    <!--                            allTeams[item.hid].name }}</a>-->
    <!--                        <a href="#!/team/{{ item.gid }}" ng-switch-when="away">{{-->
    <!--                            allTeams[item.gid].name }}</a>-->
    <!--                    </span> {{ item.betValue }}-->
    <!--                </td>-->
    <!--            </tr>-->
    <!--        </table>-->
    <!--    </div>-->
    <!---->
    <!--</div>-->

    <div id="news-top-slide-box" class="wrapper-slide-comment-top" style="display: none;">

        <div class="box-comment-top" ng-repeat="item in newsTopSlide">
            <a href="/news.php?id={{ news.ontimelines[item.newsId]}}">
                <table>
                    <tr>
                        <td><img ng-src="{{ item.imageLink}}"></td>
                        <td>
                            <b ng-bind="news.titles[item.newsId]"></b>
                            <span class="detail-news" ng-bind="news.desc[item.newsId]"></span>
                        </td>
                    </tr>
                </table>
            </a>
        </div>
    </div>

    <div class="wrapper-content new-match-bg">
        <div class="banner">
            <img src="images/banner.png" style="width: 560px;">
        </div>

        <!-- <div class="navigator">Home / Match</div>--->
        <div class="tab-situations-match"></div>


        <div class="wrapper-match-stat">
            <div class="section-match-vs">
                <div class="icon-status-match">
                    <?php if (isset($service_matchInfo->game) && isset($service_matchInfo->game->competitionId) && $service_matchInfo->game->competitionId != 0): ?>
                        <img src="images/countries/<?php echo $service_matchInfo->game->competitionId; ?>.png"/>
                    <?php endif; ?>
                </div>
                <table>
                    <tbody>
                        <tr class="box-mathes-stat">
                            <td class="logoteam1"><?php if (isset($service_matchInfo->game) && isset($service_matchInfo->game->h64x64)): ?><img src="http://api.ssporting.com/teams_clean/team_default_256x256.png"/><?php endif; ?> <span class="team">
                                    <?php if ($service_matchInfo->game): ?>
                                        <a href="/team.php?id=<?php echo $service_matchInfo->game->hid; ?>"><?php echo (isset($service_allteam->{$service_matchInfo->game->hid}) && isset($service_allteam->{$service_matchInfo->game->hid}->name)) ? $service_allteam->{$service_matchInfo->game->hid}->name : $service_matchInfo->game->homeName; ?></a>
                                    <?php endif; ?>
                                </span>
                            </td>
                            <td class="time-stat">
                                <div class="tab-leagued">
                                    <span><?php echo (isset($service_allleague->{$service_matchInfo->game->_lid}) && isset($service_allleague->{$service_matchInfo->game->_lid}->name)) ? $service_allleague->{$service_matchInfo->game->_lid}->name : $service_matchInfo->game->leagueName; ?></span> 
                                    <!--                    <?php echo Utils::trans('Match Start'); ?>
                                                        :
                                                        <span ng-bind="currentMatch.show_date | kickTime">--:--</span>-->
                                </div>
                                <div class="bg-score">
                                    <span class="score-left-team" ng-bind="currentMatch.score | scoreSelect:'home'">?</span>
                                    <span class="score-right-team" ng-bind="currentMatch.score | scoreSelect:'away'">?</span>
                                </div>
                                <div class="box-live-time">
                                    <span class="live-time" ng-bind="currentMatch.currentMin">-:-</span>
                                    <div class="bg-time">
                                        <span ng-if="currentMatch.sid === '1'" class="time-match" ng-bind="currentMatch.show_date | kickTime">--:--</span>
                                        <span ng-if="currentMatch.sid !== '1'" class="time-match now-race" ng-bind="now">--:--</span>
                                    </div>
                                    <div class="button-playbet"><a href="/game.php?mid=<?php echo __MID__; ?>"><?php echo ($service_matchInfo->game->sid === '1') ? Utils::trans('Play') : Utils::trans('View'); ?></a></div>
                                </div>
                            </td>
                            <td class="logoteam2"><img src="http://api.ssporting.com/teams_clean/team_default_256x256.png"/> <span class="team"><a
                                        href="/team.php?id=<?php echo $service_matchInfo->game->gid; ?>"><?php echo (isset($service_allteam->{$service_matchInfo->game->gid}) && isset($service_allteam->{$service_matchInfo->game->gid}->name)) ? $service_allteam->{$service_matchInfo->game->gid}->name : $service_matchInfo->game->awayName; ?></a></span>
                            </td>
                        </tr>
                    </tbody>

                </table>
            </div>


            <div id="match-events" class="wrapper-box-event" style="display: none;">
                <!-- <div class="title-event">Event</div>-->

                <div class="box-comment-user-match">

                    <div class="tab-situations-comment"></div>

                    <div class="box-event-live">

                        <div class="speech-event-live" ng-repeat="item in events">
                            <div class="box-event" ng-class="{'left-event': item.pz == 'H'}">
                                <span class="box-time-live">{{ item.m}}</span>

                                <div ng-switch on="item.pz" ng-show="item.m != 'HT' && item.m != 'FT'">
                                    <div ng-switch-when="H" class="bubble-left">
                                        {{ item.ph || item.pg }}
                                        <span ng-switch on="item.mr">
                                            <span ng-switch-when="1"><img src="images/icon/football-icon.png"/></span>
                                            <span ng-switch-when="4"><img src="images/icon/football-icon.png"/></span>
                                            <span ng-switch-when="5"><img ng-if="item..m !== 'FT'" src="images/icon/ball-p.png"/></span>
                                            <span ng-switch-when="6"><img src="images/icon/ball-no-p.png"/></span>
                                            <span ng-switch-when="2"><img src="images/icon/red-card.png"/></span>
                                            <span ng-switch-when="3"><img ng-if="item..m !== 'HT'" src="images/icon/yellow-card.png"/></span>
                                        </span>
                                    </div>
                                    <div ng-switch-default class="bubble">
                                        <span ng-switch on="item.mr">
                                            <span ng-switch-when="1"><img src="images/icon/football-icon.png"/></span>
                                            <span ng-switch-when="4"><img src="images/icon/football-icon.png"/></span>
                                            <span ng-switch-when="5"><img ng-if="item..m !== 'FT'" src="images/icon/ball-p.png"/></span>
                                            <span ng-switch-when="6"><img src="images/icon/ball-no-p.png"/></span>
                                            <span ng-switch-when="2"><img src="images/icon/red-card.png"/></span>
                                            <span ng-switch-when="3"><img ng-if="item..m !== 'HT'" src="images/icon/yellow-card.png"/></span>
                                        </span>
                                        {{ item.ph || item.pg }}
                                    </div>
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                        </div>

                        <div style="clear: both;"></div>
                    </div>


                    <div style="clear: both;"></div>

                </div>

            </div>
            <div id="match-events-none" style="display: none;">
                <?php if ($service_matchInfo->game->sid == '1'): ?>
                    <span>ยังไม่เริ่มการแข่งขัน</span>
                <?php else: ?>
                    <span>ยังไม่มีการทำประตูเกิดขึ้น</span>
                <?php endif; ?>
            </div>


            <!-- poll who will win-->
            <div id="poll-to-win" class="section-poll" style="display: none;">
                <div class="icon-status-match"><img src="images/icon/poll.png"/></div>

                <div class="tab-header-tag">
                    <div><?php echo Utils::trans('Poll who will win?'); ?></div>
                </div>
                <div class="box-table">
                    <div class="tab-percent">
                        <table>
                            <tr>
                                <td>
                                    <span class="progress-title">1</span>
                                    <div class="progress vertical">
                                        <div class="wrap-percentVote"><span class="percent-vote" ng-bind="vote.percentHome.toFixed(0)">?</span>%</div>
                                        <div class="bar" ng-style="{ width: '65%', height: vote.percentHome.toFixed(0) + '%' }">
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <span class="progress-title">X</span>
                                    <div class="progress vertical">
                                        <div class="wrap-percentVote"><span class="percent-vote" ng-bind="vote.percentDraw.toFixed(0)">?</span>%</div>
                                        <div class="bar bar-warning" ng-style="{ width: '65%', height: vote.percentDraw.toFixed(0) + '%' }">
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <span class="progress-title">2</span>
                                    <div class="progress vertical">
                                        <div class="wrap-percentVote"><span class="percent-vote" ng-bind="vote.percentAway.toFixed(0)">?</span>%</div>
                                        <div class="bar bar-success" ng-style="{ width: '65%', height: vote.percentAway.toFixed(0) + '%' }">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="votes" ng-click="voteTo('home', $event)"> (<span ng-bind="vote.home">?</span>) Vote</td>
                                <td class="votes" ng-click="voteTo('draw', $event)"> (<span ng-bind="vote.draw">?</span>) Vote</td>
                                <td class="votes" ng-click="voteTo('away', $event)"> (<span ng-bind="vote.away">?</span>) Vote</td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>


            <div class="section-poll" style="display: none;">
                <div class="progress">
                    <div class="bar bar-success" style="width: 50%;"></div>
                    <div class="bar bar-warning" style="width: 20%;"></div>
                    <div class="bar bar-danger" style="width: 10%;"></div>
                </div>
            </div>


            <!-- end -->
            <a href="/game.php?mid=<?php echo __MID__; ?>">
                <div class="box-comment-match" xng-show="currentMatchComments" style="display: snone;">
                    <div class="icon-status-match"><img src="images/icon/commentt.png"/></div>
                    <div class="box-comment-match-slide slider1" ng-show="commentList[0]">
                        <table>
                            <tr>
                                <td><a href="/profile.php?id={{ commentList[0].fb_uid}}"><img
                                            ng-src="https://graph.facebook.com/{{ commentList[0].fb_uid}}/picture"/></a><br/>

                                    <div class="icon-team-selected">
                                        <img ng-if="commentList[0].choose === 'home'" src="images/icon/H.png"/>
                                        <img ng-if="commentList[0].choose === 'away'" src="images/icon/a.png"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="commentUser">
                                        <div class="pre-dots"></div>
                                        <div class="dots">&hellip;</div>
                                        <b ng-bind="commentList[0].display_name || commentList[0].fb_firstname""></b><br/>
                                        <span ng-if="commentList[0].choose === 'home'" class="text-blue">@{{ allTeams[currentMatch.hid].name || currentMatch.hn }}</span>
                                        <span ng-if="commentList[0].choose === 'away'" class="text-blue">@{{ allTeams[currentMatch.gid].name || currentMatch.gn }}</span>
                                        <span style="color:#333;" ng-bind="commentList[0].message"></span>
                                        <span class="hidedots1"></span>
                                        <div class="hidedots2"></div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="box-comment-match-slide" ng-show="commentList[1]">
                        <table>
                            <tr>
                                <td><a href="/profile.php?id={{ commentList[1].fb_uid}}"><img
                                            ng-src="https://graph.facebook.com/{{ commentList[1].fb_uid}}/picture""/></a><br/>

                                    <div class="icon-team-selected">
                                        <img ng-if="commentList[1].choose === 'home'" src="images/icon/H.png"/>
                                        <img ng-if="commentList[1].choose === 'away'" src="images/icon/a.png"/>
                                    </div>
                                </td>
                                <td> <div class="commentUser">
                                        <div class="pre-dots"></div>
                                        <div class="dots">&hellip;</div>
                                        <b ng-bind="commentList[1].display_name || commentList[1].fb_firstname"></b><br/>
                                        <span ng-if="commentList[1].choose === 'home'" class="text-blue">@{{ allTeams[currentMatch.hid].name || currentMatch.hn }}</span>
                                        <span ng-if="commentList[1].choose === 'away'" class="text-blue">@{{ allTeams[currentMatch.gid].name || currentMatch.gn }}</span>
                                        <span style="color:#333;" ng-bind="commentList[1].message"></span>
                                        <span class="hidedots1"></span>
                                        <div class="hidedots2"></div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </a>


            <!-- comment-->
            <div class="tab-comment" style="display: none;">
                <div class="box-comment">
                    <div class="upimages-box-upload"><img src="images/icon/cam15.png" title="แนบรูปภาพ"/></div>
                    <input type="text" placeholder="comment..."/>
                </div>

                <div class="tab-select-team"
                     ng-show="currentMatch.odds.SBOBET.hdp || currentMatch.odds.Bet365.hdp || currentMatch.odds.Ladbrokes.hdp">
                    <table>
                        <td>HOME : {{ currentMatch.odds.SBOBET.hdp_home || currentMatch.odds.Bet365.hdp_home ||
                currentMatch.odds.Ladbrokes.hdp_home }}
                        </td>
                        <td>HDP : {{ currentMatch.odds.SBOBET.hdp || currentMatch.odds.Bet365.hdp || currentMatch.odds.Ladbrokes.hdp
                            }}
                        </td>
                        <td>AWAY : {{ currentMatch.odds.SBOBET.hdp_away || currentMatch.odds.Bet365.hdp_away ||
                currentMatch.odds.Ladbrokes.hdp_away }}
                        </td>
                    </table>
                </div>

                <div class="tab-user-comment-home">
                    <table>
                        <tr ng-repeat="item in matchComment.list| filter:notEmptyMessage | filter:{ choose: 'home' } | limitTo:2">
                            <td class="img-commented">
                                <div class="img-user-commented"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                            ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></div>
                            </td>
                            <td>
                                <div class="bubble-comment-right">
                                    <div class="text-commented">{{ item.message}}</div>
                                    <div class="tab-ui"><img src="images/icon/heart.png"/> {{ item.like_count}} Like <a
                                            href=""><img src="images/icon/dott.png"/></a></div>
                                </div>
                                <div class="time-post-comment">{{ item.betDatetime * 1000 | formatDate:'DD MMM YYYY HH:mm' }}</div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="tab-user-comment-away">
                    <table>
                        <tr ng-repeat="item in matchComment.list| filter:notEmptyMessage | filter:{ choose: 'away' } | limitTo:2">
                            <td>
                                <div class="bubble-comment-left">
                                    <div class="left-ui"><img src="images/icon/heart.png"/> {{ item.like_count}} Like <a
                                            href=""><img src="images/icon/dott.png"/></a></div>
                                    <div class="text-commented">{{ item.message}}</div>
                                </div>
                                <div class="time-post-comment left-time">{{ item.betDatetime * 1000 | formatDate:'DD MMM YYYY HH:mm'
                                    }}
                                </div>
                            </td>
                            <td class="img-commented">
                                <div class="img-user-commented right-img"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                            ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></div>
                            </td>
                        </tr>
                    </table>
                </div>


            </div>

            <!-- end -->

            <!-- ส่วนแสดงข่าวและวีดีโอ -->
            <div class="box-comment-match" ng-if="mobileFeedItems > 0" style="display: snone;">
                <div class="icon-status-match"><img src="images/icon/new-vedio.png"/></div>
                <div class="tab-header-tag">
                    <div>News/Video</div>
                </div>
                <div class="wrapperShow">
                    <div class="sectionShow" ng-class="{'News': mobileFeed[item].owner.content_type === 'news', 'Video': mobileFeed[item].owner.content_type === 'highlight'}" ng-repeat="item in mobileFeedActive" ng-if="mobileFeed[item].owner.content_type">
                        <a href="news.php?id={{mobileFeed[item].owner.id}}">
                            <div ng-if="mobileFeed[item].owner.content_type === 'news'">
                                <div class="tabInfoNews">
                                    <span ng-bind="mobileFeed[item].content[0].titleTh"></span>
                                </div>
                                <img ng-src="{{mobileFeed[item].content[0].imageLink}}">
                            </div>
                            <div ng-if="mobileFeed[item].owner.content_type === 'highlight'">
                                <div class="CoverVideo"></div>
                                <div class="tabInfoNews">
                                    <span ng-bind="mobileFeed[item].content[0].title"></span>
                                </div>
                                <a href="{{mobileFeed[item].content[0].content}}" embed-video></a>
                            </div>
                        </a>
                    </div>
                    <!--                    <div class="sectionShow Video">-->
                    <!--                        <div class="tabInfoNews">-->
                    <!--                            <span>แมนยู 2-0 ลิเวอร์พูล</span>-->
                    <!--                        </div>-->
                    <!--                        <iframe width="260" height="150" src="//www.youtube.com/embed/XadxEQWkDL8" frameborder="0" allowfullscreen></iframe>-->
                    <!--                        -->
                    <!--                    </div>-->
                    <div style="clear: both;"></div>
                </div>
            </div>

            <!-- end -->

            <?php if (isset($service_matchInfo->compare->result1)): ?>
                <div class="box-stat-play">
                    <div class="icon-status-match"><img src="images/icon/lastmatch.png"/></div>
                    <div class="section-math-stat-tournament">
                        <table>
                            <tr>
                                <td class="head-stat"><?php echo Utils::trans('All'); ?></td>
                                <td>
                                    <?php foreach ($service_matchInfo->last_result->home->all as $key => $val): ?>
                                        <img ng-src="images/icon/<?php echo Utils::wldStr($val) ?>.png"/>
                                        <?php if ($key >= 4) break; ?>
                                    <?php endforeach; ?>
                                </td>
                                <td>
                                    <?php foreach ($service_matchInfo->last_result->away->all as $key => $val): ?>
                                        <img ng-src="images/icon/<?php echo Utils::wldStr($val) ?>.png"/>
                                        <?php if ($key >= 4) break; ?>
                                    <?php endforeach; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="head-stat"><?php echo Utils::trans('League'); ?></td>
                                <td>
                                    <?php foreach ($service_matchInfo->last_result->home->league as $key => $val): ?>
                                        <img ng-src="images/icon/<?php echo Utils::wldStr($val) ?>.png"/>
                                        <?php if ($key >= 4) break; ?>
                                    <?php endforeach; ?>
                                </td>
                                <td>
                                    <?php foreach ($service_matchInfo->last_result->away->league as $key => $val): ?>
                                        <img ng-src="images/icon/<?php echo Utils::wldStr($val) ?>.png"/>
                                        <?php if ($key >= 4) break; ?>
                                    <?php endforeach; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (isset($service_matchInfo->compare) && isset($service_matchInfo->compare->result_vs)): ?>
                <div>
                    <div class="icon-status-match"><img src="images/icon/today.png"/></div>
                    <div class="header-match"><?php echo Utils::trans('Latest Match Stat'); ?></div>
                    <div class="box-table">
                        <div class="section-lastest-match">
                            <table>
                                <thead>
                                    <tr>
                                        <th><?php echo Utils::trans('League'); ?></th>
                                        <th><?php echo Utils::trans('Date'); ?></th>
                                        <th><?php echo Utils::trans('Home'); ?></th>
                                        <th><?php echo Utils::trans('Goal'); ?></th>
                                        <th><?php echo Utils::trans('Away'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($service_matchInfo->compare->result_vs as $val): ?>
                                        <tr>
                                            <td><?php echo $val->lnr; ?></td>
                                            <td><?php echo $val->date; ?></td>
                                            <td class="bg-team"><a href="/team.php?id=<?php echo $val->tid1; ?>">
                                                    <span class="<?php
                                                    if ($val->tid1 == $service_matchInfo->game->hid)
                                                        echo 'text-red';
                                                    else if ($val->tid1 == $service_matchInfo->game->gid)
                                                        echo 'font-blue';
                                                    ?>">
                                                        <?php echo (isset($service_allteam->{$val->tid1}) && isset($service_allteam->{$val->tid1}->name)) ? $service_allteam->{$val->tid1}->name : $val->hmn; ?></span>
                                                    <img src="{{ logoUrl}}{{ team.tid1}}_32x32.png"></a>
                                            </td>
                                            <td class="bg-goal"><span class="font-gray"><?php echo $val->score; ?></span></td>
                                            <td class="bg-team"><a href="/team.php?id=<?php echo $val->tid2; ?>"><img
                                                        src="{{ logoUrl}}{{ team.tid2}}_32x32.png"> <span class="
                                                        <?php
                                                        if ($val->tid1 == $service_matchInfo->game->gid)
                                                            echo 'text-red';
                                                        else if ($val->tid1 == $service_matchInfo->game->hid)
                                                            echo 'font-blue';
                                                        ?>">
                                                        <?php echo (isset($service_allteam->{$val->tid2}) && isset($service_allteam->{$val->tid2}->name)) ? $service_allteam->{$val->tid2}->name : $val->gmn; ?></span></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            <?php endif; ?>


            <?php if (isset($service_matchInfo->compare) && isset($service_matchInfo->compare->result1)): ?>
                <div>
                    <div class="icon-status-match"><img src="images/icon/H.png"/></div>
                    <div
                        class="header-match team-home"><?php echo isset($service_allteam->{$service_matchInfo->game->hid}->name) ? $service_allteam->{$service_matchInfo->game->hid}->name : $service_matchInfo->game->homeName ?></div>
                    <div class="box-table">
                        <div class="section-lastest-match">
                            <table>
                                <thead>
                                    <tr>
                                        <th><?php echo Utils::trans('League'); ?></th>
                                        <th><?php echo Utils::trans('Date'); ?></th>
                                        <th><?php echo Utils::trans('Home'); ?></th>
                                        <th><?php echo Utils::trans('Goal'); ?></th>
                                        <th><?php echo Utils::trans('Away'); ?></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($service_matchInfo->compare->result1 as $val): ?>
                                        <tr>
                                            <td><?php echo $val->lnk; ?></td>
                                            <td><?php echo $val->date; ?></td>
                                            <td class="bg-team"><a href="/team.php?id=<?php echo $val->tid1; ?>"><span
                                                        class="<?php
                                                        if ($val->tid1 == $service_matchInfo->game->hid)
                                                            echo 'text-red';
                                                        else if ($val->tid1 == $service_matchInfo->game->gid)
                                                            echo 'font-blue';
                                                        ?>"><?php echo (isset($service_allteam->{$val->tid1}) && isset($service_allteam->{$val->tid1}->name)) ? $service_allteam->{$val->tid1}->name : $val->hn; ?></span>
                                                    <img src="{{ logoUrl}}{{ result.tid1}}_32x32.png"></a>
                                            </td>
                                            <td class="bg-goal"><span class="font-gray"><?php echo $val->score; ?></span></a></td>
                                            <td class="bg-team"><a href="/team.php?id=<?php echo $val->tid2; ?>"><img
                                                        src="{{ logoUrl}}{{ result.tid2}}_32x32.png"> <span
                                                        class="<?php
                                                        if ($val->tid2 == $service_matchInfo->game->hid)
                                                            echo 'text-red';
                                                        else if ($val->tid2 == $service_matchInfo->game->gid)
                                                            echo 'font-blue';
                                                        ?>"><?php echo (isset($service_allteam->{$val->tid2}) && isset($service_allteam->{$val->tid2}->name)) ? $service_allteam->{$val->tid2}->name : $val->an; ?></span></a>
                                            </td>
                                            <td class="stat-icon">
                                                <div>
                                                    <img src="images/icon/<?php echo Utils::wldStr($val->r); ?>.png"/>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (isset($service_matchInfo->compare) && isset($service_matchInfo->compare->result2)): ?>
                <div>
                    <div class="icon-status-match"><img src="images/icon/a.png"/></div>
                    <div
                        class="header-match team-away"><?php echo (isset($service_allteam->{$service_matchInfo->game->gid}) && isset($service_allteam->{$service_matchInfo->game->gid}->name)) ? $service_allteam->{$service_matchInfo->game->gid}->name : $service_matchInfo->game->awayName ?></div>
                    <div class="box-table">
                        <div class="section-lastest-match">
                            <table>
                                <thead>
                                    <tr>
                                        <th><?php echo Utils::trans('League'); ?></th>
                                        <th><?php echo Utils::trans('Date'); ?></th>
                                        <th><?php echo Utils::trans('Home'); ?></th>
                                        <th><?php echo Utils::trans('Goal'); ?></th>
                                        <th><?php echo Utils::trans('Away'); ?></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($service_matchInfo->compare->result2 as $val): ?>
                                        <tr>
                                            <td><?php echo $val->lnk; ?></td>
                                            <td><?php echo $val->date; ?></td>
                                            <td class="bg-team"><a href="/team.php?id=<?php echo $val->tid1; ?>"><span
                                                        class="<?php
                                                        if ($val->tid1 == $service_matchInfo->game->hid)
                                                            echo 'text-red';
                                                        else if ($val->tid1 == $service_matchInfo->game->gid)
                                                            echo 'font-blue';
                                                        ?>"><?php echo isset($service_allteam->{$val->tid1}) && isset($service_allteam->{$val->tid1}->name) ? $service_allteam->{$val->tid1}->name : $val->hn; ?></span>
                                                    <img src="{{ logoUrl}}{{ result.tid1}}_32x32.png"></a>
                                            </td>
                                            <td class="bg-goal"><span class="font-gray"><?php echo $val->score; ?></span></a></td>
                                            <td class="bg-team"><a href="/team.php?id=<?php echo $val->tid2; ?>"><img
                                                        src="{{ logoUrl}}{{ result.tid2}}_32x32.png"> <span
                                                        class="<?php
                                                        if ($val->tid2 == $service_matchInfo->game->hid)
                                                            echo 'text-red';
                                                        else if ($val->tid2 == $service_matchInfo->game->gid)
                                                            echo 'font-blue';
                                                        ?>"><?php echo (isset($service_allteam->{$val->tid2}) && isset($service_allteam->{$val->tid2}->name)) ? $service_allteam->{$val->tid2}->name : $val->an; ?></span></a>
                                            </td>
                                            <td class="stat-icon">
                                                <div>
                                                    <img src="images/icon/<?php echo Utils::wldStr($val->r); ?>.png"/>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($service_matchInfo->game->_lid != '34189' && isset($service_matchInfo->stat) && isset($service_matchInfo->stat->stat_table)): ?>
                <div>
                    <div class="icon-status-match"><img src="images/icon/table.png"/></div>
                    <div
                        class="header-match table-bg"><?php echo (isset($service_allleague->{$service_matchInfo->game->_lid}) && isset($service_allleague->{$service_matchInfo->game->_lid}->name)) ? $service_allleague->{$service_matchInfo->game->_lid}->name : $service_matchInfo->game->leagueName; ?></div>
                    <div class="box-table">
                        <div class="section-table">
                            <table>
                                <thead>
                                    <tr>
                                        <th colspan="2"></th>
                                        <th>P</th>
                                        <th>W</th>
                                        <th>D</th>
                                        <th>L</th>
                                        <th>F</th>
                                        <th>A</th>
                                        <th>GD</th>
                                        <th>PTS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($service_matchInfo->stat->stat_table as $val): ?>
                                        <tr class="<?php if ($val->tid == $service_matchInfo->game->hid || $val->tid == $service_matchInfo->game->gid) echo 'bg-current-team'; ?>">
                                            <td class="order-table"><?php echo $val->no; ?></td>
                                            <td class="name-team"><a href="/team.php?id=<?php echo $val->tid; ?>"><img
                                                        src="{{ logoUrl}}{{ team.tid}}_32x32.png"> <?php echo (isset($service_allteam->{$val->tid}) && isset($service_allteam->{$val->tid}->name)) ? $service_allteam->{$val->tid}->name : $val->tn; ?>
                                                </a></td>
                                            <td><?php echo $val->gp; ?></td>
                                            <td><?php echo $val->w; ?></td>
                                            <td><?php echo $val->d; ?></td>
                                            <td><?php echo $val->l; ?></td>
                                            <td><?php echo $val->gf; ?></td>
                                            <td><?php echo $val->ga; ?></td>
                                            <td><?php echo $val->plusminus; ?></td>
                                            <td><?php echo $val->pts; ?></td>
                                        </tr>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            <?php endif; ?>

        </div>
    </div>

    <div class="box-loading" style="display: none;">
        <img src="images/blank_loading.gif"/>
    </div>
</div>

<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>