<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$footerScript .= '<script src="scripts/games/pao-ying-choob.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<!--Content-->
<div class="wrapper-content content-profile">

    <div class="wrapper-miniGames">
        <div class="tab-heading-title"><img src="images/mini-game/paoyingchoob/title_icon_g1.png" style="width: 20px;"> เกมส์เป่ายิงฉุบ</div>
        <div ng-show="result" class="totalPoint paoyingchoob">
            <b ng-switch="result">
                <span class="alert alert-info" ng-switch-when="draw">เสมอ</span>
                <span class="alert alert-success" ng-switch-when="win">คุณ ชนะ</span>
                <span class="alert alert-danger" ng-switch-when="lose">คุณ แพ้</span>
            </b> <br>

            <div class="resultAlert wrong" style="display: none;">คุณทายผิด</div>
        </div>
        <div class="bodyGames">
            <table>
                <tr>
                    <td>
                        <span ng-show="facebookInfo.id" class="player"><img
                                ng-src="https://graph.facebook.com/{{ facebookInfo.id}}/picture" style="width: 40px;"></span>
                        <span ng-switch="chooseL">
                            <img ng-switch-when="d" src="images/mini-game/paoyingchoob/L_defualt_g1.png">
                            <img ng-switch-when="s" src="images/mini-game/paoyingchoob/L_cut_g1.png">
                            <img ng-switch-when="p" src="images/mini-game/paoyingchoob/L_paper_g1.png">
                            <img ng-switch-when="r" src="images/mini-game/paoyingchoob/L_hammer_g1.png">
                        </span>

                    </td>
                    <td class="vsGames"><img src="images/mini-game/paoyingchoob/V_S_Image.png"></td>
                    <td>
                        <span class="player"><img src="images/mini-game/paoyingchoob/avata_GM_g1.png"
                                                  style="width: 40px;"></span>
                        <span ng-switch="chooseR">
                            <img ng-switch-when="d" src="images/mini-game/paoyingchoob/R_defualt_g1.png">
                            <img ng-switch-when="s" src="images/mini-game/paoyingchoob/R_cut_g1.png">
                            <img ng-switch-when="p" src="images/mini-game/paoyingchoob/R_paper_g1.png">
                            <img ng-switch-when="r" src="images/mini-game/paoyingchoob/R_hammer_g1.png">
                        </span>
                    </td>
                </tr>
            </table>
        </div>
        <div class="selectCoin">
            <table>
                <tr>
                    <td ng-class="{'activeCoin-100':scoin === 100}" ng-click="scoin = 100" class="selectCoin-100"></td>
                    <td ng-class="{'activeCoin-200':scoin === 200}" ng-click="scoin = 200" class="selectCoin-200"></td>
                    <td ng-class="{'activeCoin-500':scoin === 500}" ng-click="scoin = 500" class="selectCoin-500"></td>
                </tr>
            </table>
        </div>

        <div class="selectWeapon">
            <table>
                <tr>
                    <td ng-click="play('s')" ng-class="{'activeScissors':choose === 's'}" class="weaponScissors"></td>
                    <td ng-click="play('r')" ng-class="{'activeHammer':choose === 'r'}" class="weaponHammer"></td>
                    <td ng-click="play('p')" ng-class="{'activePaper':choose === 'p'}" class="weaponPaper"></td>
                </tr>
            </table>
        </div>

        <div class="weaponName">
            <table>
                <tr>
                    <td>กรรไกร</td>
                    <td>ค้อน</td>
                    <td>กระดาษ</td>
                </tr>
            </table>
        </div>

        <div class="titleGames" style="display: none;">สติถิการเล่นล่าสุด</div>
        <div class="boxStatPlay-Games" style="display: none;">
            <table>
                <tr>
                    <td><img src="images/mini-game/paoyingchoob/L_cut_g1.png"> <br><img src="images/mini-game/paoyingchoob/R_paper_g1.png"></td>
                    <td><img src="images/mini-game/paoyingchoob/L_paper_g1.png"> <br><img src="images/mini-game/paoyingchoob/R_paper_g1.png"></td>
                    <td><img src="images/mini-game/paoyingchoob/L_cut_g1.png"> <br><img src="images/mini-game/paoyingchoob/R_cut_g1.png"></td>
                    <td><img src="images/mini-game/paoyingchoob/L_hammer_g1.png"> <br><img src="images/mini-game/paoyingchoob/R_cut_g1.png"></td>
                    <td><img src="images/mini-game/paoyingchoob/L_cut_g1.png"> <br><img src="images/mini-game/paoyingchoob/R_paper_g1.png"></td>
                    <td><img src="images/mini-game/paoyingchoob/L_cut_g1.png"> <br><img src="images/mini-game/paoyingchoob/R_hammer_g1.png"></td>
                    <td><img src="images/mini-game/paoyingchoob/L_paper_g1.png"> <br><img src="images/mini-game/paoyingchoob/R_paper_g1.png"></td>
                    <td><img src="images/mini-game/paoyingchoob/L_cut_g1.png"> <br><img src="images/mini-game/paoyingchoob/R_paper_g1.png"></td>
                    <td><img src="images/mini-game/paoyingchoob/L_hammer_g1.png"> <br><img src="images/mini-game/paoyingchoob/R_hammer_g1.png"></td>
                    <td><img src="images/mini-game/paoyingchoob/L_cut_g1.png"> <br><img src="images/mini-game/paoyingchoob/R_hammer_g1.png"></td>
                    <td><img src="images/mini-game/paoyingchoob/L_hammer_g1.png"> <br><img src="images/mini-game/paoyingchoob/R_hammer_g1.png"></td>
                    
            </table>
        </div>


        <div class="titleGames">วิธีการเล่นเกมส์</div>
        <div class="boxHowToPlay-Games">
            <ul>
                <li><b>เกมส์เป่ายิงฉุบ จะเป็นเกมส์ที่ผู้เล่นเลือกอาวุธที่จะใช้สู้กับคอมพิวเตอร์ โดยมีอาวุธที่สามารถใช้ได้คือ กรรไกร ค้อน และกระดาษ โดยมีวิธีการเล่น ดังนี้</b></li>
                <li>- เลือกจำนวนเหรียญที่ต้องการเดิมพัน โดยมีให้เลือก 100, 200, 500 เหรียญ Scoin</li>
                <li>- จากนั้นเลือกอาวุธ ซึ่งมี กรรไกร ค้อน และกระดาษ เพื่อใช้สู้กับคอมพิวเตอร์</li>
                <li>- จะมีการแสดงผลการเล่นให้เห็นว่าคุณแพ้หรือชนะ ซึ่งจะได้เหรียญเพิ่มหรือลดจะขึ้นอยู่กับผลการเล่น โดยสามารถดูสถิติการเล่นได้ที่ตารางสรุปผลการเล่นเกมส์</li>
            </ul>
        </div>


        <!--            ตารางสรุปผลการเล่นเกมส์-->
        <div class="titleGames">ตารางสรุปผลการเล่นเกมส์</div>

        <div class="table-ResultMiniGames">
            <div class="tabs-tableResult">
                <ul>
                    <li ng-click="stateTab = 'user'" ng-class="{'active':stateTab === 'user'}">เฉพาะฉัน</li>
                    <li ng-click="stateTab = 'all'" ng-class="{'active':stateTab === 'all'}">ทุกคน</li>
                </ul>
                <div style="clear: both;"></div>
            </div>

            <table>
                <thead>
                    <tr>
                        <th>ชื่อผู้เล่น</th>
                        <th>ชื่อเกมส์</th>
                        <th>ผู้เล่น</th>
                        <th>คอมพ์</th>
                        <th>ผลการเล่น</th>
                        <th>จำนวน</th>
                        <th>ได้/เสีย</th>
                        <th>วันที่/เวลา</th>
                    </tr>
                </thead>
                <tbody ng-show="stateTab === 'user'">
                    <tr ng-repeat="item in statement.user">
                        <td><a href="profile.php?id={{ item.fb_uid}}"><img
                                    ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"> {{ item.display_name ||
                            item.fb_firstname }}</a></td>
                        <td>เป่ายิ่งฉุบ</td>
                        <td>
                            <span ng-switch="item.player1">
                                <img ng-switch-when="s" src="images/mini-game/paoyingchoob/L_cut_g130.png">
                                <img ng-switch-when="r" src="images/mini-game/paoyingchoob/L_hammer_g130.png">
                                <img ng-switch-when="p" src="images/mini-game/paoyingchoob/L_paper_g130.png">
                            </span>
                        </td>
                        <td>
                            <span ng-switch="item.player2">
                                <img ng-switch-when="s" src="images/mini-game/paoyingchoob/R_cut_g130.png">
                                <img ng-switch-when="r" src="images/mini-game/paoyingchoob/R_hammer_g130.png">
                                <img ng-switch-when="p" src="images/mini-game/paoyingchoob/R_paper_g130.png">
                            </span>
                        </td>
                        <td>
                            <span ng-switch="item.result">
                                <span class="correct" ng-switch-when="win">ชนะ</span>
                                <span class="wrong" ng-switch-when="lose">แพ้</span>
                                <span ng-switch-when="draw">เสมอ</span>
                            </span>
                        </td>
                        <td><img src="images/icon/scoin30.png"> {{ item.scoin_amount}}</td>
                        <td><img src="images/icon/sgold30.png"> <span ng-class="{'correct':item.scoin_result_amount > 0, 'wrong':item.scoin_result_amount < 0 }">{{ item.scoin_result_amount}}</span></td>
                        <td>{{ item.play_timestamp | timeagoByDateTime }}</td>
                    </tr>
                </tbody>
                <tbody ng-show="stateTab === 'all'">
                    <tr ng-repeat="item in statement.all">
                        <td><a href="profile.php?id={{ item.fb_uid}}"><img
                                    ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"> {{ item.display_name ||
                            item.fb_firstname }}</a></td>
                        <td>เป่ายิ่งฉุบ</td>
                        <td>
                            <span ng-switch="item.player1">
                                <img ng-switch-when="s" src="images/mini-game/paoyingchoob/L_cut_g1.png">
                                <img ng-switch-when="r" src="images/mini-game/paoyingchoob/L_hammer_g1.png">
                                <img ng-switch-when="p" src="images/mini-game/paoyingchoob/L_paper_g1.png">
                            </span>
                        </td>
                        <td>
                            <span ng-switch="item.player2">
                                <img ng-switch-when="s" src="images/mini-game/paoyingchoob/R_cut_g130.png">
                                <img ng-switch-when="r" src="images/mini-game/paoyingchoob/R_hammer_g130.png">
                                <img ng-switch-when="p" src="images/mini-game/paoyingchoob/R_paper_g130.png">
                            </span>
                        </td>
                        <td>
                            <span ng-switch="item.result">
                                <span class="correct" ng-switch-when="win">ชนะ</span>
                                <span class="wrong" ng-switch-when="lose">แพ้</span>
                                <span ng-switch-when="draw">เสมอ</span>
                            </span>
                        </td>
                        <td><img src="images/icon/scoin30.png"> {{ item.scoin_amount}}</td>
                        <td><img src="images/icon/sgold30.png"> <span ng-class="{'correct':item.scoin_result_amount > 0, 'wrong':item.scoin_result_amount < 0 }">{{ item.scoin_result_amount}}</span></td>
                        <td>{{ item.play_timestamp | timeagoByDateTime }}</td>
                    </tr>
                </tbody>
            </table>

        </div>

    </div>

    <!--alert กรณีชนะ -->
    <div id="winModal" class="Modal-resultGame" style="display: none;">
        <div class="fadeGame"></div>
        <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="false" style="display: block; z-index: 5; top: 200px; background: none; box-shadow: none; border: 0;">

            <div class="alertWin">      
                <div class="titleModal"></div>
                <img src="images/mini-game/chest/modal_sgold.png">
                <div class="resultPlay">คุณได้รับ</div>
                <h3>{{ scoin}} <span class="goldText">S</span> GOLD</h3>
                <div class="titleModal" style="cursor: pointer;"><img ng-click="close()" src="images/mini-game/allgames/modal_bg_win_btn.png"></div>
            </div>

        </div>
    </div>


    <!--alert กรณีแพ้ -->
    <div id="loseModal" class="Modal-resultGame" style="display: none;">
        <div class="fadeGame"></div>
        <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="false" style="display: block; z-index: 5; top: 200px; background: none; box-shadow: none; border: 0;">

            <div class="alertLose">      
                <div class="titleModal"></div>
                <img src="images/mini-game/paoyingchoob/modal_bg_lose_sgold.png" style="padding: 25px;">
                <div class="resultPlay">คุณเสีย</div>
                <h3>- <span class="text-red">{{ scoin}}</span> <span class="goldText">S</span> GOLD</h3>
                <div class="titleModal" style="cursor: pointer;"><img ng-click="close()" src="images/mini-game/allgames/modal_bg_lose_btn.png"></div>
            </div>

        </div>
    </div>

    <!--alert กรณีเสมอ -->
    <div id="drawModal" class="Modal-resultGame" style="display: none;">
        <div class="fadeGame"></div>
        <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="false" style="display: block; z-index: 5; top: 200px; background: none; box-shadow: none; border: 0;">

            <div class="alertDraw">      
                <div class="titleModal"></div>
                <div class="resultPlay"></div>
                <h3></h3>
                <div class="titleModal" style="cursor: pointer; margin-top: 250px;"><img ng-click="close()" src="images/mini-game/allgames/modal_bg_draw_btn.png"></div>
            </div>

        </div>
    </div>


</div>

<div class="preload-images" style="display: none;">
    <img src="images/mini-game/paoyingchoob/L_defualt_g1.png">
    <img src="images/mini-game/paoyingchoob/L_cut_g1.png">
    <img src="images/mini-game/paoyingchoob/L_paper_g1.png">
    <img src="images/mini-game/paoyingchoob/L_hammer_g1.png">
    <img src="images/mini-game/paoyingchoob/R_defualt_g1.png">
    <img src="images/mini-game/paoyingchoob/R_cut_g1.png">
    <img src="images/mini-game/paoyingchoob/R_paper_g1.png">
    <img src="images/mini-game/paoyingchoob/R_hammer_g1.png">
</div>

<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
