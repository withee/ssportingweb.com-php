<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ผลบอลสด อัพเดทรวดเร็วที่สุดและแม่นยำที่สุด';
$meta = '<meta name="description" content="เช็คผลบอลสด ผลบอลเมื่อคืนและผลบอลย้อนหลังได้ที่นี่ ข้อมูลแม่นยำอัพเดทผลรวดเร็ว ซึ่งจะทำให้คุณไม่พลาดทุกวินาทีสำคัญ">' . "\n";
$meta .= '<meta name="keyword" content="ผลบอลสด,ผลบอล,ผลบอลเมื่อคืน,ผลบอลย้อนหลัง,ไฮไลท์ฟุตบอล">' . "\n";

$footerScript .= '<script id="rankingScript" src="scripts/ranking.js" fb_uid="' . __FB_UID__ . '"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>


    <div class="wrapper-content content-profile" id="ranking-profile" style="display: none;">
    <div class="section-new-feed-user">

    <div class="tab-follower" ng-init="currentRankingTab = 'score'">
    <div class="menutop-type-ranking">
        <table>
            <tr>
                <td ng-class="{'active-type-ranking': currentRankingTab == 'score'}"
                    ng-click="currentRankingTab = 'score'"><img src="images/icon-profile/300150.png"/> <h5>TOP</h5>Ranking
                </td>
                <!--<td ng-class="{'active-type-ranking': currentRankingTab == 'rating'}" ng-click="currentRankingTab = 'rating'"><h4>Rating</h4>Top</td>-->
                <td ng-class="{'active-type-ranking': currentRankingTab == 'sgold'}"
                    ng-click="currentRankingTab = 'sgold'"><img src="images/icon-profile/000150.png"/><h5>TOP</h5>Sgold
                </td>
                <td ng-class="{'active-type-ranking': currentRankingTab == 'league'}"
                    ng-click="currentRankingTab = 'league'"><img src="images/icon/ranking-ufa.png"/><h5>TOP</h5>5 League</td>
                <td ng-class="{'active-type-ranking': currentRankingTab == 'friends'}"
                    ng-click="currentRankingTab = 'friends'"><img src="images/icon-profile/200150.png"/><h5>TOP</h5>
                    Friends
                </td>
            </tr>
        </table>
    </div>

    <div ng-init="currentRankingScoreSelect = 'all'" ng-show="currentRankingTab == 'score'">
    <div class="tab-type-rank">
        <table>
            <tr>
                <td ng-class="{'active-rank': currentRankingScoreSelect == 'all'}"
                    ng-click="currentRankingScoreSelect = 'all'">All
                </td>
                <td ng-class="{'active-rank': currentRankingScoreSelect == 'now'}"
                    ng-click="currentRankingScoreSelect = 'now'">Now
                </td>
                <td ng-class="{'active-rank': currentRankingScoreSelect == '2week'}"
                    ng-click="currentRankingScoreSelect = '2week'">Last Champ
                </td>
            </tr>
        </table>
    </div>


    <div class="tab-menu-ranking" style="display: none;">
        <table>
            <tr>
                <td ng-click="currentRankingTab = 'all'" ng-class="{'active-menu-ranking':currentRankingTab == 'all'}">
                    Top Ranking
                </td>
                <td ng-click="currentRankingTab = 'friend'"
                    ng-class="{'active-menu-ranking':currentRankingTab == 'friend'}">Top Friend Ranking
                </td>
            </tr>
        </table>
    </div>


    <!-- top score begin -->
    <div class="wrapper-rank">
    <div ng-show="currentRankingScoreSelect == 'now'" class="box-follower"
         ng-repeat="item in topRanking.top_ranking.now | limitTo:10">
        <table>
            <tr>
                <td class="icon-follower">
                    <div class="icon-ranking"><img ng-src="/images/icon/{{ $index + 1}}.png"/></div>
                    <div class="tab-situations-icon" style="display: none;"></div>
                </td>

                <td class="img-user-follow">
                    <div class="user-follow"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></div>
                </td>

                <td class="info-ranking">
                    <table>
                        <tr>
                            <td><span
                                    class="name-rank">{{ item.display_name || item.displayname || item.fb_firstname}}</span>

                            </td>
                        </tr>
                        <tr>
                            <td><span class="font-score">{{ item.overall_gp | toFixed }}</span>

                                <div class="icon-stat"><img ng-repeat="r in item.lastResult.split(',') track by $index"
                                                            ng-src="images/icon/{{ r | fromInitial }}.png" ng-show="r">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="tab-trophy">
                    <img ng-repeat="prize in item.bestprize" ng-src="{{ prize.picture }}">
                </td>
                <td class="box-following">
                                <span ng-if="item.fb_uid != facebookInfo.id" class="tab-follow" ng-switch
                                      on="checkIsFollowing(item.fb_uid)">
                                    <span ng-switch-when="true"><img ng-src="images/icon/follow1.png"/> Follow</span>
                                    <span class="button-following" ng-click="follow(true, item.fb_uid)"
                                          ng-switch-when="false" class="btn btn-mini"><img
                                            src="images/icon/follow-1.png"/> Follow</span>
                                </span>

                </td>

            </tr>
        </table>
    </div>
    <div ng-if="topRanking.own.top_ranking.now.rank > 10" ng-show="currentRankingScoreSelect == 'now'"
         class="own-rank box-follower">
        <table>
            <tr>
                <td class="icon-follower">
                    <div class="icon-ranking">{{ topRanking.own.last14day.rank }}</div>
                    <div class="tab-situations-icon" style="display: none;"></div>
                </td>

                <td class="img-user-follow">
                    <div class="user-follow"><a href="/profile.php?id={{ topRanking.own.top_ranking.now.fb_uid}}"><img
                                ng-src="https://graph.facebook.com/{{ topRanking.own.top_ranking.now.fb_uid}}/picture"/></a>
                    </div>
                </td>

                <td class="info-ranking">
                    <table>
                        <tr>
                            <td><span class="name-rank">{{ topRanking.own.top_ranking.now.display_name || topRanking.own.top_ranking.now.displayname || topRanking.own.top_ranking.now.fb_firstname}}</span>

                            </td>
                        </tr>
                        <tr>
                            <td><span class="font-score">{{ topRanking.own.top_ranking.now.overall_gp | toFixed }}</span>

                                <div class="icon-stat"><img
                                        ng-repeat="r in topRanking.own.top_ranking.now.lastResult.split(',') track by $index"
                                        ng-src="images/icon/{{ r | fromInitial }}.png" ng-show="r"></div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="tab-trophy">
                    <img ng-repeat="prize in topRanking.own.top_ranking.now.bestprize" ng-src="{{ prize.picture }}">
                </td>
                <td class="box-following">
                                <span ng-if="topRanking.own.top_ranking.now.fb_uid != facebookInfo.id" class="tab-follow"
                                      ng-switch on="checkIsFollowing(item.fb_uid)">
                                    <span ng-switch-when="true"><img ng-src="images/icon/follow1.png"/> Follow</span>
                                    <span class="button-following" ng-click="follow(true, item.fb_uid)"
                                          ng-switch-when="false" class="btn btn-mini"><img
                                            src="images/icon/follow-1.png"/> Follow</span>
                                </span>

                </td>

            </tr>
        </table>
    </div>
    <!-- end -->
    <div ng-show="currentRankingScoreSelect == '2week'" class="box-follower"
         ng-repeat="item in topRanking.top_ranking.2week | limitTo:10">
        <table>
            <tr>
                <td class="icon-follower">
                    <div class="icon-ranking"><img ng-src="/images/icon/{{ $index + 1}}.png"/></div>
                    <div class="tab-situations-icon" style="display: none;"></div>
                </td>
                <td class="img-user-follow">
                    <div class="user-follow"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></div>
                </td>
                <td class="info-ranking">
                    <table>
                        <tr>
                            <td><span
                                    class="name-rank">{{ item.display_name || item.displayname || item.fb_firstname}}</span>

                            </td>
                        </tr>
                        <tr>
                            <td><span class="font-score">{{ item.overall_gp | toFixed }}</span>

                                <div class="icon-stat"><img ng-repeat="r in item.lastResult.split(',') track by $index"
                                                            ng-src="images/icon/{{ r | fromInitial }}.png" ng-show="r">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="tab-trophy">
                    <img ng-repeat="prize in item.bestprize" ng-src="{{ prize.picture }}">
                </td>
                <td class="box-following">
                                <span ng-if="item.fb_uid != facebookInfo.id" class="tab-follow" ng-switch
                                      on="checkIsFollowing(item.fb_uid)">
                                    <span ng-switch-when="true"><img ng-src="images/icon/follow1.png"/> Follow</span>
                                    <span class="button-following" ng-click="follow(true, item.fb_uid)"
                                          ng-switch-when="false" class="btn btn-mini"><img
                                            src="images/icon/follow-1.png"/> Follow</span>
                                </span>

                </td>

            </tr>
        </table>
    </div>
    <div ng-if="topRanking.own.top_ranking.2week.rank > 10" ng-show="currentRankingScoreSelect == '2week'"
         class="own-rank box-follower">
        <table>
            <tr>
                <td class="icon-follower">
                    <div class="icon-ranking">{{ topRanking.own.top_ranking.2week.rank }}</div>
                    <div class="tab-situations-icon" style="display: none;"></div>
                </td>

                <td class="img-user-follow">
                    <div class="user-follow"><a href="/profile.php?id={{ topRanking.own.top_ranking.2week.fb_uid}}"><img
                                ng-src="https://graph.facebook.com/{{ topRanking.own.top_ranking.2week.fb_uid}}/picture"/></a>
                    </div>
                </td>

                <td class="info-ranking">
                    <table>
                        <tr>
                            <td><span class="name-rank">{{ topRanking.own.top_ranking.2week.display_name || topRanking.own.top_ranking.2week.displayname || topRanking.own.top_ranking.2week.fb_firstname}}</span>

                            </td>
                        </tr>
                        <tr>
                            <td><span class="font-score">{{ topRanking.own.top_ranking.2week.overall_gp | toFixed }}</span>

                                <div class="icon-stat"><img
                                        ng-repeat="r in topRanking.own.top_ranking.2week.lastResult.split(',') track by $index"
                                        ng-src="images/icon/{{ r | fromInitial }}.png" ng-show="r"></div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="tab-trophy">
                    <img ng-repeat="prize in topRanking.own.last2week.bestprize" ng-src="{{ prize.picture }}">
                </td>
                <td class="box-following">
                                <span ng-if="topRanking.own.last2week.fb_uid != facebookInfo.id" class="tab-follow"
                                      ng-switch on="checkIsFollowing(item.fb_uid)">
                                    <span ng-switch-when="true"><img ng-src="images/icon/follow1.png"/> Follow</span>
                                    <span class="button-following" ng-click="follow(true, item.fb_uid)"
                                          ng-switch-when="false" class="btn btn-mini"><img
                                            src="images/icon/follow-1.png"/> Follow</span>
                                </span>

                </td>

            </tr>
        </table>
    </div>
    <!-- end -->

    <div ng-show="currentRankingScoreSelect == 'all'" class="box-follower"
         ng-repeat="item in topRanking.top_ranking.alltime | limitTo:10">
        <table>
            <tr>
                <td class="icon-follower">
                    <div class="icon-ranking"><img ng-src="/images/icon/{{ $index + 1}}.png"/></div>
                    <div class="tab-situations-icon" style="display: none;"></div>
                </td>
                <td class="img-user-follow">
                    <div class="user-follow"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></div>
                </td>
                <td class="info-ranking">
                    <table>
                        <tr>
                            <td><span
                                    class="name-rank">{{ item.display_name || item.displayname || item.fb_firstname}}</span>

                            </td>
                        </tr>
                        <tr>
                            <td><span class="font-score">{{ item.real_gp | toFixed }}</span>

                                <div class="icon-stat"><img ng-repeat="r in item.lastResult.split(',') track by $index"
                                                            ng-src="images/icon/{{ r | fromInitial }}.png" ng-show="r">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="tab-trophy">
                    <img ng-repeat="prize in item.bestprize" ng-src="{{ prize.picture }}">
                </td>
                <td class="box-following">
                                <span ng-if="item.fb_uid != facebookInfo.id" class="tab-follow" ng-switch
                                      on="checkIsFollowing(item.fb_uid)">
                                    <span ng-switch-when="true"><img ng-src="images/icon/follow1.png"/> Follow</span>
                                    <span class="button-following" ng-click="follow(true, item.fb_uid)"
                                          ng-switch-when="false" class="btn btn-mini"><img
                                            src="images/icon/follow-1.png"/> Follow</span>
                                </span>

                </td>

            </tr>
        </table>
    </div>
    <div ng-if="topRanking.own.top_ranking.alltime.rank > 10" ng-show="currentRankingScoreSelect == 'alltime'"
         class="own-rank box-follower">
        <table>
            <tr>
                <td class="icon-follower">
                    <div class="icon-ranking">{{ topRanking.own.top_ranking.alltime.rank }}</div>
                    <div class="tab-situations-icon" style="display: none;"></div>
                </td>

                <td class="img-user-follow">
                    <div class="user-follow"><a href="/profile.php?id={{ topRanking.own.all.fb_uid}}"><img
                                ng-src="https://graph.facebook.com/{{ topRanking.own.top_ranking.alltime.fb_uid}}/picture"/></a></div>
                </td>

                <td class="info-ranking">
                    <table>
                        <tr>
                            <td><span class="name-rank">{{ topRanking.own.top_ranking.alltime.display_name || topRanking.own.top_ranking.alltime.displayname || topRanking.own.top_ranking.alltime.fb_firstname}}</span>

                            </td>
                        </tr>
                        <tr>
                            <td><span class="font-score">{{ topRanking.own.top_ranking.alltime.overall_gp | toFixed }}</span>

                                <div class="icon-stat"><img
                                        ng-repeat="r in topRanking.own.top_ranking.alltime.lastResult.split(',') track by $index"
                                        ng-src="images/icon/{{ r | fromInitial }}.png" ng-show="r"></div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="tab-trophy">
                    <img ng-repeat="prize in topRanking.own.top_ranking.alltime.bestprize" ng-src="{{ prize.picture }}">
                </td>
                <td class="box-following">
                                <span ng-if="topRanking.own.top_ranking.alltime.fb_uid != facebookInfo.id" class="tab-follow" ng-switch
                                      on="checkIsFollowing(item.fb_uid)">
                                    <span ng-switch-when="true"><img ng-src="images/icon/follow1.png"/> Follow</span>
                                    <span class="button-following" ng-click="follow(true, item.fb_uid)"
                                          ng-switch-when="false" class="btn btn-mini"><img
                                            src="images/icon/follow-1.png"/> Follow</span>
                                </span>

                </td>

            </tr>
        </table>
    </div>
    <!-- end -->

    <!-- top score end -->
    </div>
    </div>

    <!-- top league begin -->
    <div ng-init="currentRankingLeagueSelect = '34885'" ng-show="currentRankingTab == 'league'"">
        <div class="tab-type-rank">
            <table>
                <tr>
                    <td ng-class="{'active-rank': currentRankingLeagueSelect == '34885'}" ng-click="currentRankingLeagueSelect = '34885'"><img src="images/five-leagues/tpl.png" style="width: 22px;"><br> {{ topRanking.league['34885'][0].league_name }}</td>
                    <td ng-class="{'active-rank': currentRankingLeagueSelect == '35909'}" ng-click="currentRankingLeagueSelect = '35909'"><img src="images/five-leagues/Englis-Premier-League.png" style="width: 22px;"><br> {{ topRanking.league['35909'][0].league_name }}</td>
                    <td ng-class="{'active-rank': currentRankingLeagueSelect == '37120'}" ng-click="currentRankingLeagueSelect = '37120'"><img src="images/five-leagues/La-Liga-Logo-1.png" style="width: 22px;"><br> {{ topRanking.league['37120'][0].league_name }}</td>
                    <td ng-class="{'active-rank': currentRankingLeagueSelect == '35980'}" ng-click="currentRankingLeagueSelect = '35980'"><img src="images/five-leagues/UEFA-Champions-League-1.png" style="width: 22px;"><br> {{ topRanking.league['35980'][0].league_name }}</td>
                    <td ng-class="{'active-rank': currentRankingLeagueSelect == '35998'}" ng-click="currentRankingLeagueSelect = '35998'"><img src="images/five-leagues/UEFA-Europa-League-1.png" style="width: 22px;"><br> {{ topRanking.league['35998'][0].league_name }}</td>
                </tr>
            </table>
        </div>
        <div ng-repeat="lg in highlightLeague">
            <div ng-show="currentRankingLeagueSelect == lg" class="box-follower"
                 ng-repeat="item in topRanking.league[lg]| limitTo:10">
                <table>
                    <tr>
                        <td class="icon-follower">
                            <div class="icon-ranking"><img ng-src="/images/icon/{{ $index + 1}}.png"/></div>
                            <div class="tab-situations-icon" style="display: none;"></div>
                        </td>
                        <td class="img-user-follow">
                            <div class="user-follow"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                        ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></div>
                        </td>
                        <td class="info-ranking">
                            <table>
                                <tr>
                                    <td><span class="name-rank">{{ item.display_name || item.displayname || item.fb_firstname}}</span>

                                    </td>
                                </tr>
                                <tr>
                                    <td><span class="font-score">{{ item.overall_gp | toFixed }}</span>

                                        <div class="icon-stat"><img
                                                ng-repeat="r in item.lastResult.split(',') track by $index"
                                                ng-src="images/icon/{{ r | fromInitial }}.png" ng-show="r"></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="tab-trophy">
                            <img ng-repeat="prize in item.bestprize" ng-src="{{ prize.picture }}">
                        </td>
                        <td class="box-following">
                                    <span ng-if="item.fb_uid != facebookInfo.id" class="tab-follow" ng-switch
                                          on="checkIsFollowing(item.fb_uid)">
                                        <span ng-switch-when="true"><img ng-src="images/icon/follow1.png"/> Follow</span>
                                        <span class="button-following" ng-click="follow(true, item.fb_uid)"
                                              ng-switch-when="false"
                                              class="btn btn-mini"><img src="images/icon/follow-1.png"/> Follow</span>
                                    </span>

                        </td>

                    </tr>
                </table>
            </div>
        </div>
    </div>

    <!-- top league end -->


    <!-- top sgold -->
    <div ng-init="currentRankingSgoldSelect = 'now'" ng-show="currentRankingTab == 'sgold'">
        <div class="tab-type-rank">
            <table>
                <tr>
                    <td ng-class="{'active-rank': currentRankingSgoldSelect == 'now'}"
                        ng-click="currentRankingSgoldSelect = 'now'">Now
                    </td>
                    <td ng-class="{'active-rank': currentRankingSgoldSelect == '2week'}"
                        ng-click="currentRankingSgoldSelect = '2week'">Last Champ
                    </td>
                </tr>
            </table>
        </div>

        <div ng-show="currentRankingSgoldSelect == 'now'" class="box-follower"
             ng-repeat="item in topRanking.top_sgold.now| limitTo:10">
            <table>
                <tr>
                    <td class="icon-follower">
                        <div class="icon-ranking"><img ng-src="/images/icon/{{ $index + 1}}.png"/></div>
                        <div class="tab-situations-icon" style="display: none;"></div>
                    </td>
                    <td class="img-user-follow">
                        <div class="user-follow"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                    ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></div>
                    </td>
                    <td class="info-ranking">
                        <table>
                            <tr>
                                <td><span class="name-rank">{{ item.display_name || item.displayname || item.fb_firstname}}</span>

                                </td>
                            </tr>
                            <tr>
                                <td><span class="font-score">{{ item.overall_sgold | toFixed }}</span>

                                    <div class="icon-stat"><img
                                            ng-repeat="r in item.lastResult.split(',') track by $index"
                                            ng-src="images/icon/{{ r | fromInitial }}.png" ng-show="r"></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="tab-trophy">
                        <img ng-repeat="prize in item.bestprize" ng-src="{{ prize.picture }}">
                    </td>
                    <td class="box-following">
                            <span ng-if="item.fb_uid != facebookInfo.id" class="tab-follow" ng-switch
                                  on="checkIsFollowing(item.fb_uid)">
                                <span ng-switch-when="true"><img ng-src="images/icon/follow1.png"/> Follow</span>
                                <span class="button-following" ng-click="follow(true, item.fb_uid)"
                                      ng-switch-when="false"
                                      class="btn btn-mini"><img src="images/icon/follow-1.png"/> Follow</span>
                            </span>

                    </td>

                </tr>
            </table>
        </div>

        <div ng-show="currentRankingSgoldSelect == '2week'" class="box-follower"
             ng-repeat="item in topRanking.top_sgold.2week| limitTo:10">
            <table>
                <tr>
                    <td class="icon-follower">
                        <div class="icon-ranking"><img ng-src="/images/icon/{{ $index + 1}}.png"/></div>
                        <div class="tab-situations-icon" style="display: none;"></div>
                    </td>
                    <td class="img-user-follow">
                        <div class="user-follow"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                    ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></div>
                    </td>
                    <td class="info-ranking">
                        <table>
                            <tr>
                                <td><span class="name-rank">{{ item.display_name || item.displayname || item.fb_firstname}}</span>

                                </td>
                            </tr>
                            <tr>
                                <td><span class="font-score">{{ item.overall_sgold}}</span>

                                    <div class="icon-stat"><img
                                            ng-repeat="r in item.lastResult.split(',') track by $index"
                                            ng-src="images/icon/{{ r | fromInitial }}.png" ng-show="r"></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="tab-trophy">
                        <img ng-repeat="prize in item.bestprize" ng-src="{{ prize.picture }}">
                    </td>
                    <td class="box-following">
                            <span ng-if="item.fb_uid != facebookInfo.id" class="tab-follow" ng-switch
                                  on="checkIsFollowing(item.fb_uid)">
                                <span ng-switch-when="true"><img ng-src="images/icon/follow1.png"/> Follow</span>
                                <span class="button-following" ng-click="follow(true, item.fb_uid)"
                                      ng-switch-when="false"
                                      class="btn btn-mini"><img src="images/icon/follow-1.png"/> Follow</span>
                            </span>

                    </td>

                </tr>
            </table>
        </div>
    </div>
    <!-- top sgold -->



    <!-- top friend -->
    <div ng-init="currentRankingFriendSelect = 'now'" ng-show="currentRankingTab == 'friends'">
        <div class="tab-type-rank">
            <table>
                <tr>
                    <td ng-class="{'active-rank': currentRankingFriendSelect == 'now'}"
                        ng-click="currentRankingFriendSelect = 'now'">Now
                    </td>
                    <td ng-class="{'active-rank': currentRankingFriendSelect == '2week'}"
                        ng-click="currentRankingFriendSelect = '2week'">Last Champ
                    </td>
                </tr>
            </table>
        </div>

        <div ng-show="currentRankingFriendSelect == 'now'" class="box-follower"
             ng-repeat="item in topRanking.friend.now| limitTo:10">
            <table>
                <tr>
                    <td class="icon-follower">
                        <div class="icon-ranking"><img ng-src="/images/icon/{{ $index + 1}}.png"/></div>
                        <div class="tab-situations-icon" style="display: none;"></div>
                    </td>
                    <td class="img-user-follow">
                        <div class="user-follow"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                    ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></div>
                    </td>
                    <td class="info-ranking">
                        <table>
                            <tr>
                                <td><span class="name-rank">{{ item.display_name || item.displayname || item.fb_firstname}}</span>

                                </td>
                            </tr>
                            <tr>
                                <td><span class="font-score">{{ item.overall_sgold | toFixed }}</span>

                                    <div class="icon-stat"><img
                                            ng-repeat="r in item.lastResult.split(',') track by $index"
                                            ng-src="images/icon/{{ r | fromInitial }}.png" ng-show="r"></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="tab-trophy">
                        <img ng-repeat="prize in item.bestprize" ng-src="{{ prize.picture }}">
                    </td>
                    <td class="box-following">
                            <span ng-if="item.fb_uid != facebookInfo.id" class="tab-follow" ng-switch
                                  on="checkIsFollowing(item.fb_uid)">
                                <span ng-switch-when="true"><img ng-src="images/icon/follow1.png"/> Follow</span>
                                <span class="button-following" ng-click="follow(true, item.fb_uid)"
                                      ng-switch-when="false"
                                      class="btn btn-mini"><img src="images/icon/follow-1.png"/> Follow</span>
                            </span>

                    </td>

                </tr>
            </table>
        </div>

        <div ng-show="currentRankingFriendSelect == '2week'" class="box-follower"
             ng-repeat="item in topRanking.friend.2week| limitTo:10">
            <table>
                <tr>
                    <td class="icon-follower">
                        <div class="icon-ranking"><img ng-src="/images/icon/{{ $index + 1}}.png"/></div>
                        <div class="tab-situations-icon" style="display: none;"></div>
                    </td>
                    <td class="img-user-follow">
                        <div class="user-follow"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                    ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></div>
                    </td>
                    <td class="info-ranking">
                        <table>
                            <tr>
                                <td><span class="name-rank">{{ item.display_name || item.displayname || item.fb_firstname}}</span>

                                </td>
                            </tr>
                            <tr>
                                <td><span class="font-score">{{ item.overall_sgold}}</span>

                                    <div class="icon-stat"><img
                                            ng-repeat="r in item.lastResult.split(',') track by $index"
                                            ng-src="images/icon/{{ r | fromInitial }}.png" ng-show="r"></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="tab-trophy">
                        <img ng-repeat="prize in item.bestprize" ng-src="{{ prize.picture }}">
                    </td>
                    <td class="box-following">
                            <span ng-if="item.fb_uid != facebookInfo.id" class="tab-follow" ng-switch
                                  on="checkIsFollowing(item.fb_uid)">
                                <span ng-switch-when="true"><img ng-src="images/icon/follow1.png"/> Follow</span>
                                <span class="button-following" ng-click="follow(true, item.fb_uid)"
                                      ng-switch-when="false"
                                      class="btn btn-mini"><img src="images/icon/follow-1.png"/> Follow</span>
                            </span>

                    </td>

                </tr>
            </table>
        </div>
    </div>
    <!-- top friend -->
    </div>
    </div>
    </div>
<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>