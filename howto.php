<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$service_liveMatch = Services::getLiveMatch();
$service_liveWait = Services::getLiveWait();

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

$footerScript .= '<script src="scripts/howto.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div>

    <div class="wrapper-content content-profile">
        <div class="boxHeading"><img src="images/icon/icon_how2play.png"> How To Play</div>

        <div class="boxMenuHowto">
            <table>
                <tr>
                    <td id="how-to-menu-1" ng-click="showHowto(1)" class="active_menu howto-memu">วิธีเล่นเกมส์ทายผลบอล</td>
                    <td id="how-to-menu-2" ng-click="showHowto(2)" class="howto-memu">เกณฑ์การคิดคะแนน</td>
                    <td id="how-to-menu-3" ng-click="showHowto(3)" class="howto-memu"><span>เหรียญและถ้วยรางวัล</span> <img style="display: none;" src="images/icon/downicon.png"></td>
                    <td id="how-to-menu-4" ng-click="showHowto(4)" class="howto-memu">คำถามที่พบบ่อย</td>
                    <td id="how-to-menu-5" ng-click="showHowto(5)" class="howto-memu">Mini Games</td>
                </tr>
            </table>
<!--            <div class="dropdownMenuHowto">-->
<!--                <ul>-->
<!--                    <li>ประเภทของเหรียญ</li>-->
<!--                    <li>ประเภทถ้วยรางวัล</li>-->
<!--                    <li>การแลกเปลี่ยนเหรียญ</li>-->
<!--                </ul>-->
<!--            </div>-->
        </div>


        <div class="boxHowto">
            <div id="howto-1" class="howto-content">

            </div>
            <div id="howto-2" class="howto-content" style="display: none;">

            </div>
            <div id="howto-3" class="howto-content" style="display: none;">

            </div>
            <div id="howto-4" class="howto-content" style="display: none;">

            </div>
        </div>
    </div>
</div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
