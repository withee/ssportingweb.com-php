<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$service_liveMatch = Services::getLiveMatch();
$service_liveWait = Services::getLiveWait();

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

$footerScript .= '<script src="scripts/games/slot.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div ng-controller="mainCtrl">


    <!--Content-->
    <div class="wrapper-content content-profile">

        <div class="wrapper-miniGames">
            <div class="tab-heading-title tabTypeGames"><img src="images/mini-game/allgames/all-game_lotto_logo.png" width="30px;" style="float: left; margin-right: 10px;"> เกมส์น้ำเต้าปูปลา</div>
<!--            <div ng-switch="result" class="totalPoint slotgameAlert">-->
<!--                <b><span ng-switch-when="win" class="correct"> สุดยอดเลย!! คุณได้รับ {{ reward}} Scoin</span></b>-->
<!--                <b><span ng-switch-when="lose" class="wrong">ว๊า !! แย่จังคุณทายไม่ถูก</span></b>-->
<!--            </div>-->
            <div class="bodygame-slot">
                <div class="slot">
                    <table>
                        <tr>
                            <td ng-switch="slotL">
                                <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                            </td>
                            <td class="slot2" ng-switch="slotM">
                                <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                            </td>
                            <td ng-switch="slotR">
                                <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="selectCoin">
                <table>
                    <tr>
                        <td ng-class="{'activeCoin-100':sgold === 100}" ng-click="sgold = 100" class="selectCoin-100"></td>
                        <td ng-class="{'activeCoin-200':sgold === 200}" ng-click="sgold = 200" class="selectCoin-200"></td>
                        <td ng-class="{'activeCoin-500':sgold === 500}" ng-click="sgold = 500" class="selectCoin-500"></td>
                    </tr>
                </table>
            </div>

            <div class="selectSlot">
                <table>
                    <tr>
                        <td ng-click="play('ca')" ng-class="{'active-slot-1':choose === 'ca'}" class="slot-1"></td>
                        <td ng-click="play('cr')" ng-class="{'active-slot-2':choose === 'cr'}" class="slot-2"></td>
                        <td ng-click="play('f')" ng-class="{'active-slot-3':choose === 'f'}" class="slot-3"></td>
                        <td ng-click="play('t')" ng-class="{'active-slot-4':choose === 't'}" class="slot-4"></td>
                        <td ng-click="play('s')" ng-class="{'active-slot-5':choose === 's'}" class="slot-5"></td>
                        <td ng-click="play('ch')" ng-class="{'active-slot-6':choose === 'ch'}" class="slot-6"></td>
                    </tr>
                </table>
            </div>

            <div class="titleGames" style="display: none;">ผลการเล่นล่าสุด</div>
            <div class="boxStatPlay-Games slotGames" style="display: none;">
                <table>
                    <tr>
                        <td><img src="/images/mini-game/slot/slot_001.png"> <img src="/images/mini-game/slot/slot_001.png"> <img src="/images/mini-game/slot/slot_001.png"></td>
                        <td><img src="/images/mini-game/slot/slot_001.png"> <img src="/images/mini-game/slot/slot_002.png"> <img src="/images/mini-game/slot/slot_003.png"></td>
                        <td><img src="/images/mini-game/slot/slot_002.png"> <img src="/images/mini-game/slot/slot_002.png"> <img src="/images/mini-game/slot/slot_003.png"></td>
                        <td><img src="/images/mini-game/slot/slot_002.png"> <img src="/images/mini-game/slot/slot_002.png"> <img src="/images/mini-game/slot/slot_002.png"></td>
                        <td><img src="/images/mini-game/slot/slot_001.png"> <img src="/images/mini-game/slot/slot_001.png"> <img src="/images/mini-game/slot/slot_001.png"></td>
                        <td><img src="/images/mini-game/slot/slot_001.png"> <img src="/images/mini-game/slot/slot_002.png"> <img src="/images/mini-game/slot/slot_003.png"></td>
                        <td><img src="/images/mini-game/slot/slot_002.png"> <img src="/images/mini-game/slot/slot_002.png"> <img src="/images/mini-game/slot/slot_003.png"></td>
                        <td><img src="/images/mini-game/slot/slot_002.png"> <img src="/images/mini-game/slot/slot_003.png"> <img src="/images/mini-game/slot/slot_002.png"></td>
                        <td><img src="/images/mini-game/slot/slot_002.png"> <img src="/images/mini-game/slot/slot_002.png"> <img src="/images/mini-game/slot/slot_003.png"></td>
                        <td><img src="/images/mini-game/slot/slot_002.png"> <img src="/images/mini-game/slot/slot_002.png"> <img src="/images/mini-game/slot/slot_002.png"></td>
                        <td><img src="/images/mini-game/slot/slot_001.png"> <img src="/images/mini-game/slot/slot_001.png"> <img src="/images/mini-game/slot/slot_001.png"></td>

                </table>
            </div>

            <div class="titleGames">วิธีการเล่นเกมส์</div>
            <div class="boxHowToPlay-Games">
                <ul>
                    <li><b>เกมส์น้ำเต้าปูปลา เป็นเกมส์ทายผลว่าจะออกอะไร โดยมีตัวเลือกอยู่ 6 ตัวคือ น้ำเต้า ปู ปลา กุ้ง ไก่ และเสือ ซึ่งมีวิธีการเล่นดังนี้</b></li>
                    <li>- เลือกจำนวนเหรียญที่ต้องการเดิมพัน โดยมีให้เลือก 100, 200, 500 เหรียญ Scoin</li>
                    <li>- จากนั้นเลือกตัวเลือกที่จะทายผล โดยมีตัวเลือก 6 ตัว คือ น้ำเต้า ปู ปลา กุ้ง ไก่ และเสือ โดยมีวิธีคิดคะแนนดังนี้
                        ถ้าตัวที่เลือกถูก  ออก 1 รูป จะได้รับเหรียญ 1 เท่า
                        หรือถ้าตัวที่เลือก  ออก 2 รูป จะได้รับเหรียญ 2 เท่า
                        หรือถ้าตัวที่เลือก  ออก 3 รูป จะได้รับเหรียญ 3 เท่า
                    </li>
                    <li>- ผลการทายจะแสดงให้คุณว่าเราทายถูกหรือทายผิด โดยสามารถดูสถิติการเล่นได้ที่ตารางสรุปผลการเล่นเกมส์</li>
                </ul>
            </div>


            <!--            ตารางสรุปผลการเล่นเกมส์-->
            <div class="titleGames">ตารางสรุปผลการเล่นเกมส์</div>

            <div class="table-ResultMiniGames">
                <div class="tabs-tableResult">
                    <ul>
                        <li ng-click="stateTab = 'user'" ng-class="{'active':stateTab === 'user'}">เฉพาะฉัน</li>
                        <li ng-click="stateTab = 'all'" ng-class="{'active':stateTab === 'all'}">ทุกคน</li>
                    </ul>
                    <div style="clear: both;"></div>
                </div>

                <table>
                    <thead>
                        <tr>
                            <th>ชื่อผู้เล่น</th>
                            <th>ชื่อเกมส์</th>
                            <th>ผู้เล่น</th>
                            <th>คอมพ์</th>
                            <th>ผลการเล่น</th>
                            <th>จำนวน</th>
                            <th>ได้/เสีย</th>
                            <th>วันที่/เวลา</th>
                        </tr>
                    </thead>
                    <tbody ng-show="stateTab === 'user'">
                        <tr ng-repeat="item in statement.user">
                            <td><a href="profile.php?id={{ item.fb_uid}}"><img
                                        ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"> {{ item.display_name ||
                                item.fb_firstname }}</a></td>
                            <td>น้ำเต้าปูปลา</td>
                            <td>
                                <span ng-switch="item.player1">
                                    <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                    <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                    <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                    <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                    <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                    <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                                </span>
                            </td>
                            <td>
                                <span ng-switch="item.player2 | parseOdds:0">
                                    <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                    <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                    <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                    <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                    <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                    <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                                </span>
                                <span class="slot2" ng-switch="item.player2 | parseOdds:1">
                                    <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                    <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                    <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                    <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                    <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                    <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                                </span>
                                <span ng-switch="item.player2 | parseOdds:2">
                                    <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                    <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                    <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                    <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                    <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                    <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                                </span>
                            </td>
                            <td>
                                <span ng-switch="item.result">
                                    <span class="correct" ng-switch-when="win">ชนะ</span>
                                    <span class="wrong" ng-switch-when="lose">แพ้</span>
                                    <span class="draw" ng-switch-when="draw">เสมอ</span>
                                </span>
                            </td>
                            <td><img src="images/icon/scoin30.png"> {{ item.scoin_amount}}</td>
                            <td><img src="images/icon/sgold30.png"> <span ng-class="{'correct':item.sgold_result_amount > 0, 'wrong':item.sgold_result_amount < 0 }">{{ item.sgold_result_amount}}</span></td>
                            <td>{{ item.play_timestamp | timeagoByDateTime }}</td>
                        </tr>
                    </tbody>
                    <tbody ng-show="stateTab === 'all'">
                        <tr ng-repeat="item in statement.all">
                            <td><a href="profile.php?id={{ item.fb_uid}}"><img
                                        ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"> {{ item.display_name ||
                                item.fb_firstname }}</a></td>
                            <td>น้ำเต้าปูปลา</td>
                            <td>
                                <span ng-switch="item.player1">
                                    <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                    <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                    <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                    <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                    <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                    <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                                </span>
                            </td>
                            <td>
                                <span ng-switch="item.player2 | parseOdds:0">
                                    <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                    <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                    <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                    <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                    <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                    <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                                </span>
                                <span class="slot2" ng-switch="item.player2 | parseOdds:1">
                                    <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                    <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                    <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                    <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                    <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                    <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                                </span>
                                <span ng-switch="item.player2 | parseOdds:2">
                                    <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                    <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                    <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                    <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                    <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                    <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                                </span>
                            </td>
                            <td>
                                <span ng-switch="item.result">
                                    <span class="correct" ng-switch-when="win">ชนะ</span>
                                    <span class="wrong" ng-switch-when="lose">แพ้</span>
                                    <span class="draw" ng-switch-when="draw">เสมอ</span>
                                </span>
                            </td>
                            <td><img src="images/icon/scoin30.png"> {{ item.scoin_amount}}</td>
                            <td><img src="images/icon/sgold30.png"> <span ng-class="{'correct':item.sgold_result_amount > 0, 'wrong':item.sgold_result_amount < 0 }">{{ item.sgold_result_amount}}</span></td>
                            <td>{{ item.play_timestamp | timeagoByDateTime }}</td>
                        </tr>
                    </tbody>
                </table>

            </div>

        </div>





        <!--alert กรณีชนะ -->
        <div id="winModal" class="Modal-resultGame" style="display: none;">
            <div class="fadeGame"></div>
            <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="false" style="display: block; z-index: 5; top: 200px; background: none; box-shadow: none; border: 0;">

                <div class="alertCorrect">      
                    <div class="titleModal"></div>

                    <div ng-if="choose == slotL" class="starIcon-1">
                        <img src="images/mini-game/slot/star_icon.png">
                    </div>
                    <div ng-if="choose == slotM" class="starIcon-2">
                        <img src="images/mini-game/slot/star_icon.png">
                    </div>
                    <div ng-if="choose == slotR" class="starIcon-3">
                        <img src="images/mini-game/slot/star_icon.png">
                    </div>

                    <div class="bodygameSlot">
                        <div class="slotSelected">
                            <table>
                                <tr>
                                    <td>
                                        <span ng-switch="slotL">
                                            <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                            <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                            <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                            <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                            <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                            <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                                        </span>
                                        <span ng-switch="slotM">
                                            <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                            <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                            <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                            <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                            <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                            <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                                        </span>
                                        <span ng-switch="slotR">
                                            <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                            <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                            <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                            <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                            <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                            <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                                        </span>
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="resultPlay">คุณได้รับ</div>
                    <h3>{{ reward }} <span class="goldText">S</span> COIN</h3>
                    <div class="titleModal" style="cursor: pointer;"><img ng-click="close()" src="images/mini-game/allgames/modal_bg_win_btn.png"></div>
                </div>

            </div>
        </div>


        <!--alert กรณีแพ้ -->
        <div id="loseModal" class="Modal-resultGame" style="display: none;">
            <div class="fadeGame"></div>
            <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="false" style="display: block; z-index: 5; top: 200px; background: none; box-shadow: none; border: 0;">

                <div class="alertWrong">      
                    <div class="titleModal"></div>
                    <div class="bodygameSlot">
                        <div class="slotSelected">
                            <table>
                                <tr>
                                    <td>
                                        <span ng-switch="slotL">
                                            <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                            <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                            <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                            <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                            <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                            <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                                        </span>
                                        <span ng-switch="slotM">
                                            <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                            <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                            <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                            <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                            <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                            <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                                        </span>
                                        <span ng-switch="slotR">
                                            <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                            <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                            <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                            <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                            <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                            <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                                        </span>
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="resultPlay">คุณเสีย</div>
                    <h3><span class="text-red">{{ reward }}</span> <span class="goldText">S</span> COIN</h3>
                    <div class="titleModal" style="cursor: pointer; "><img ng-click="close()" src="images/mini-game/allgames/modal_bg_lose_btn.png"></div>
                </div>

            </div>
        </div>



























    </div>
</div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
