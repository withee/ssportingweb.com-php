<?php

session_start();

define('__DOCS_ROOT__', dirname(__FILE__));
define('__CACHE_DIR__', __DOCS_ROOT__ . '/cache');
define('__INCLUDE_DIR__', __DOCS_ROOT__ . '/includes');
//define('__LANGUAGE__', !empty($_COOKIE['scorspot-language']) ? $_COOKIE['scorspot-language'] : $defaultLanguage);
//define('__GMT_OFFSET__', !empty($_COOKIE['scorspot-gmt_offset']) ? $_COOKIE['scorspot-gmt_offset'] : false);

if (!empty($_REQUEST['gmtOffset'])) {
    define('__GMT_OFFSET__', $_REQUEST['gmtOffset']);
} else {
    if (empty($_COOKIE['scorspot-gmt_offset'])) define('__GMT_OFFSET__', 7);
    else define('__GMT_OFFSET__', $_COOKIE['scorspot-gmt_offset']);
}

if (!empty($_REQUEST['defaultLanguage'])) {
    define('__LANGUAGE__', $_REQUEST['defaultLanguage']);
} else {
    if (empty($_COOKIE['scorspot-language'])) define('__LANGUAGE__', 'th');
    else define('__LANGUAGE__', $_COOKIE['scorspot-language']);
}

if (empty($_COOKIE['scorspot-facebook-id'])) define('__FACEBOOK_ID__', false);
else define('__FACEBOOK_ID__', $_COOKIE['scorspot-facebook-id']);

//if(empty(__GMT_OFFSET__) && !empty($_REQUEST['gmtOffset'])) define('__GMT_OFFSET__', $_REQUEST['gmtOffset']);

$title = '';
$meta = '';
$headerScript = '';
$footerScript = '';

require_once(__DOCS_ROOT__ . '/libs/vendors/underscore-php/underscore.php');
require_once(__DOCS_ROOT__ . '/libs/vendors/Carbon.php');

function __autoload($class_name)
{
    include_once(__DOCS_ROOT__ . '/libs/' . $class_name . '.php');
}

//mysqldump -u mix_21step -p'x3@x1234' mix_21step | ssh x3dev@203.146.127.215 mysql -u x3dev -p'x3@x1234' x3dev_mix
/*
 *

{"video_id":"8","mid":"1099480","gid":"1162","hid":"1163","leagueId":"32227","competitionId":"70","content":"http://www.dailymotion.com/video/x1t1w5r_le-but-incroyable-de-cristiano-ronaldo-face-a-valence_sport","title":" REAL MADRID VS. VALENCIA","create_datetime":"2014-05-05 05:56:54","like":"1","report":"0"}
 *
 */

//fb400809029976308://authorize#target_url=http://scorspot.com

if (isset($_REQUEST['signed_request'])) {
//    echo $_REQUEST['signed_request'];
    $p = Utils::parseSignedRequest($_REQUEST['signed_request']);
    if (empty($p['user_id'])) {
        $headerScript .= '<script>window.top.location = "https://www.facebook.com/dialog/oauth?client_id=400809029976308&redirect_uri=" + encodeURI("https://apps.facebook.com/scorspot") + "&scope=email,user_friends,user_about_me,user_activities,user_birthday,user_groups,user_interests,user_likes,friends_about_me,friends_activities,friends_birthday,friends_likes,user_online_presence,friends_online_presence,publish_actions,publish_stream,publish_actions";</script>';
//        header('location: https://www.facebook.com/dialog/oauth?client_id=400809029976308&redirect_uri=http%3A%2F%2Fwww.scorspot.com&scope=email,read_stream');
    }
}