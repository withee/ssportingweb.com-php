<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ผลบอลสด อัพเดทรวดเร็วที่สุดและแม่นยำที่สุด';
$meta = '<meta name="description" content="เช็คผลบอลสด ผลบอลเมื่อคืนและผลบอลย้อนหลังได้ที่นี่ ข้อมูลแม่นยำอัพเดทผลรวดเร็ว ซึ่งจะทำให้คุณไม่พลาดทุกวินาทีสำคัญ">' . "\n";
$meta .= '<meta name="keyword" content="ผลบอลสด,ผลบอล,ผลบอลเมื่อคืน,ผลบอลย้อนหลัง,ไฮไลท์ฟุตบอล">' . "\n";

$service_w14 = Services::getW14();

//echo '<pre>';
//print_r($service_w14->matches);
//echo '</pre>';
//exit;

$footerScript .= '<script src="scripts/w14match.js"></script>';
use Carbon\Carbon;
require_once(__INCLUDE_DIR__ . '/header.php')
?>


    <div id="news-top-slide-box" class="wrapper-slide-comment-top" style="display: none;">

        <div class="box-comment-top" ng-repeat="item in newsTopSlide">
            <a href="/news.php?id={{ news.ontimelines[item.newsId]}}">
                <table>
                    <tr>
                        <td><img ng-src="{{ item.imageLink}}"></td>
                        <td>
                            <b ng-bind="news.titles[item.newsId]"></b>
                            <span class="detail-news" ng-bind="news.desc[item.newsId]"></span>
                        </td>
                    </tr>
                </table>
            </a>
        </div>
    </div>

    <div class="wrapper-content content-profile">

        <div class="banner" style="padding-left: 5px ">
            <a href="/w14index.php"><img src="images/banner.jpg"></a>
        </div>

        <div class="tab-heading-title">Matches</div>
        <div class="wrapper-box-feed-expand">
            <?php foreach (get_object_vars($service_w14->matches) as $key => $item): ?>
                <?php
                $d = Carbon::createFromFormat('Y-m-d', $key)->addHours(__GMT_OFFSET__);
                $dw = $d->day . ' ' . Utils::monthOfYear($d->month) . ' ' . $d->year;
                ?>
            <div class="wrapper-box-matches">
                <div class="tab-datetime">
                    <b><?php echo Utils::dayOfWeek($d->dayOfWeek); ?></b> <?php echo $dw; ?>
                </div>
                <?php foreach ($item as $match): ?>
                <div class="tab-matches">
                    <table>
                        <tbody>
                        <tr>
                            <td class="dateTime"><?php echo $dw; ?><br><b><?php echo $match->round_name; ?></b></td>
                            <td class="teamHome"><span class="nameTeam home1"><?php echo (isset($match->hid) && isset($service_w14->team->{$match->hid})) ? $service_w14->team->{$match->hid}->{__LANGUAGE__} : $match->teamA; ?></span></td>
                            <td class="logoCountry1">
                                <?php if($match->hid): ?>
                                    <img src="http://ws.1ivescore.com/worldcup/<?php echo $match->hid; ?>_.png">
                                <?php else: ?>
                                     <img src="images/default-bal70l.png">
                                <?php endif; ?>
                            </td>
                            <td class="teamScores playScore">
                                <?php if ($match->sid <= 1): ?>
                                    <?php echo !empty($match->datetime) ? Carbon::createFromTimestamp($match->datetime)->addHours(__GMT_OFFSET__)->format('H:i') : '?'; ?>
                                <?php else: ?>
                                    <?php echo Utils::getScore($match->s1, 'space-colon'); ?>
                                <?php endif; ?>
                            </td>
                            <td class="logoCountry2">
                                <?php if($match->gid): ?>
                                    <img src="http://ws.1ivescore.com/worldcup/<?php echo $match->gid; ?>_.png">
                                <?php else: ?>
                                     <img src="images/default-bal70l.png">
                                <?php endif; ?>
                            </td>
                            <td class="teamAway"><span class="nameTeam away2"><?php echo (isset($match->gid) && isset($service_w14->team->{$match->gid})) ? $service_w14->team->{$match->gid}->{__LANGUAGE__} : $match->teamB; ?></span></td>
                            <td>
                                <?php if(!empty($match->mid)): ?>
                                <a href="/match.php?mid=<?php echo $match->mid ?>"><img src="images/icon/stat.png"></a> <a href="/game.php?mid=<?php echo $match->mid ?>"><img src="images/icon/bet2.png"></a></td>
                                <?php endif; ?>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <?php endforeach; ?>
            </div>
            <?php endforeach; ?>
        </div>
    </div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>