<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$service_liveMatch = Services::getLiveMatch();
$service_liveWait = Services::getLiveWait();

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

$footerScript .= '<script src="scripts/notifications.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div ng-controller="mainCtrl">


    <!--Content-->
    <div class="wrapper-content content-profile">
        <div class="tab-heading-title"><img src="images/icon/noti-red.png"> Notifications</div>
        <div class="tab-notifications">
            <table>
                <tr>
                    <td style="cursor: pointer;" ng-click="changeNotiTab('all')" ng-class="{'activeNoti':currentNotiTab === 'all'}"><b>แจ้งเตือนทั้งหมด (<span ng-bind="notifications.quantity || 0"></span>)</b></td>
                    <td style="cursor: pointer;" ng-click="changeNotiTab('post')" ng-class="{'activeNoti':currentNotiTab === 'post'}"><b>โพสต์ของคุณ (<span ng-bind="notificationsPost.quantity || 0"></span>)</b></td>
                    <td style="cursor: pointer;" ng-click="changeNotiTab('game')" ng-class="{'activeNoti':currentNotiTab === 'game'}"><b>เกมส์ (<span ng-bind="notificationsGame.quantity || 0"></span>)</b></td>
                    <td style="cursor: pointer;" ng-click="changeNotiTab('follow')" ng-class="{'activeNoti':currentNotiTab === 'follow'}"><b>ติดตามคุณ (<span ng-bind="notificationsFollow.quantity || 0"></span>)</b></td>
                </tr>
            </table>
        </div>


        <div class="wrapper-box-notifications">

            <div id="noti-all">
                <div ng-repeat="v in notifications.list">
                    <div class="heading-dateNoti">{{ v[0].event_at | formatDate:'DD/MM/YYYY' }}</div>
                    <div class="wrapper-boxNoti">



                        <div ng-repeat="item in v">

                            <div ng-if="item.link_type === 'game' && item.type === 'report'" class="notifications games">
                                <span ng-click="removeNoti(item.rfb_uid, item.noti_id)" class="closeNoti">x</span>
                                <a href="game.php?mid={{ item.link_key }}"><table>
                                    <tr>
                                        <td><img src="images/icon/total.png"> {{ item.sys_msg }} : <b>
                                                <span ng-class="{'team-favored':item.bet_hdp < 0, 'font-red-live':item.choose === 'home'}" class="">{{ item.home }}</span> <span>{{ item.score }}</span> <span ng-class="{'team-favored':item.bet_hdp > 0, 'font-red-live':item.choose === 'away'}">{{ item.away }}</span> : <span>{{ item.bet_result }}</span> ที่ <span class="font-red-live"><span ng-if="item.bet_hdp >= 0">+</span>{{ item.bet_hdp }}</span> : คะแนน <span class="font-red-live"><span ng-if="item.gp >= 0">+</span>{{ item.gp }}</span></b></td>
                                        <td>
                                            <img ng-src="images/icon/{{ item.bet_result }}.png">
                                        </td>
                                    </tr>
                                </table></a>
                            </div>

                            <div ng-if="item.link_type === 'game' && item.type === 'play'" class="notifications">
                                <span ng-click="removeNoti(item.rfb_uid, item.noti_id)" class="closeNoti">x</span>
                                <a href="game.php?mid={{ item.link_key }}"><img src="images/icon/bet2.png"> <span class="nameUser">{{ item.display_name }}</span> : <span>{{ item.sys_msg }}</span> <b>{{ item.message }}</b></a> <span class="font-blue">{{ item.event_at | formatDate:'HH:mm' }}</span>
                            </div>
                            <div ng-if="item.link_type === 'follow'" class="notifications">
                                <span ng-click="removeNoti(item.rfb_uid, item.noti_id)" class="closeNoti">x</span>
                                <img src="images/icon/follow.png"> <span class="nameUser">{{ item.display_name }}</span> : <span><a href="profile.php?following">{{ item.sys_msg }}</a></span> <span class="font-blue">{{ item.event_at | formatDate:'HH:mm' }}</span>
                            </div>
                            <div ng-if="item.link_type === 'timeline' && item.type === 'like'" class="notifications">
                                <span ng-click="removeNoti(item.rfb_uid, item.noti_id)" class="closeNoti">x</span>
                                <a href="news.php?id={{ item.link_key }}"><img src="images/icon/heart.png"> <span class="nameUser">{{ item.display_name }}</span> : <span>{{ item.sys_msg }}</span> <b>{{ item.message }}</b></a> <span class="font-blue">{{ item.event_at | formatDate:'HH:mm' }}</span>
                            </div>
                            <div ng-if="item.link_type === 'timeline' && item.type === 'post'" class="notifications">
                                <span ng-click="removeNoti(item.rfb_uid, item.noti_id)" class="closeNoti">x</span>
                                <a href="news.php?id={{ item.link_key }}"><img src="images/icon/commenting.png"> <span class="nameUser">{{ item.display_name }}</span> : <span>{{ item.sys_msg }}</span> <b>{{ item.message }}</b></a> <span class="font-blue">{{ item.event_at | formatDate:'HH:mm' }}</span>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div id="noti-post" style="display: none;">
                <div ng-repeat="(k,v) in notificationsPost.list">
                    <div class="heading-dateNoti">{{ v[0].event_at | formatDate:'DD/MM/YYYY' }}</div>
                    <div class="wrapper-boxNoti">
                        <div ng-repeat="item in v" class="notifications">
                            <div>
                                <span ng-click="removeNoti(item.rfb_uid, item.noti_id)" class="closeNoti">x</span>
                                <a href="news.php?id={{ item.link_key }}"><img src="images/icon/commenting.png"> <span class="nameUser">{{ item.display_name }}</span> : <span>{{ item.sys_msg }}</span> <b>{{ item.message }}</b></a> <span class="font-blue">{{ item.event_at | formatDate:'HH:mm' }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="noti-game" style="display: none;">
                <div ng-repeat="(k,v) in notificationsGame.list">
                    <div class="heading-dateNoti">{{ v[0].event_at | formatDate:'DD/MM/YYYY' }}</div>
                    <div class="wrapper-boxNoti">
                        <div ng-repeat="item in v" class="notifications" ng-class="{'games': item.type === 'report'}">
                            <div ng-if="item.link_type === 'game' && item.type === 'report'">
                                <span ng-click="removeNoti(item.rfb_uid, item.noti_id)" class="closeNoti">x</span>
                                <a href="game.php?mid={{ item.link_key }}"><table>
                                        <tr>
                                            <td><img src="images/icon/total.png"> {{ item.sys_msg }} : <b>
                                                    <span ng-class="{'team-favored':item.bet_hdp < 0, 'font-red-live':item.choose === 'home'}" class="">{{ item.home }}</span> <span>{{ item.score }}</span> <span ng-class="{'team-favored':item.bet_hdp > 0, 'font-red-live':item.choose === 'away'}">{{ item.away }}</span> : <span>{{ item.bet_result }}</span> ที่ <span class="font-red-live"><span ng-if="item.bet_hdp >= 0">+</span>{{ item.bet_hdp }}</span> : คะแนน <span class="font-red-live"><span ng-if="item.gp >= 0">+</span>{{ item.gp }}</span></b></td>
                                            <td>
                                                <img ng-src="images/icon/{{ item.bet_result }}.png">
                                            </td>
                                        </tr>
                                    </table></a>
                            </div>
                            <div ng-if="item.link_type === 'game' && item.type === 'play'">
                                <span ng-click="removeNoti(item.rfb_uid, item.noti_id)" class="closeNoti">x</span>
                                <a href="game.php?mid={{ item.link_key }}"><img src="images/icon/bet2.png"> <span class="nameUser">{{ item.display_name }}</span> : <span>{{ item.sys_msg }}</span> <b>{{ item.message }}</b></a> <span class="font-blue">{{ item.event_at | formatDate:'HH:mm' }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="noti-follow" style="display: none;">
                <div ng-repeat="(k,v) in notificationsFollow.list">
                    <div class="heading-dateNoti">{{ v[0].event_at | formatDate:'DD/MM/YYYY' }}</div>
                    <div class="wrapper-boxNoti">
                        <div ng-repeat="item in v" class="notifications">
                            <div>
                                <span ng-click="removeNoti(item.rfb_uid, item.noti_id)" class="closeNoti">x</span>
                                <img src="images/icon/follow.png"> <span class="nameUser">{{ item.display_name }}</span> : <span><a href="profile.php?following">{{ item.sys_msg }}</a></span> <span class="font-blue">{{ item.event_at | formatDate:'HH:mm' }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<!--            <div class="readMore-noti">ดูทั้งหมด</div>-->
        </div>
    </div>



    <!--    ส่วน modal สรุปผลการเล่นเกมส์-->

    <div class="Modal-resultGame" style="display: none;">
        <div class="fadeGame"></div>
        <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="false" style="display: block; z-index: 5;">

            <div class="header-resultGame">
                <div class="closed" style="background-color: #fff; color: #333; position: absolute; margin-left: 545px; margin-top: -10px;">X</div>

                <div class="resultGames">
                    <h4>สรุปผลการเล่นเกมส์</h4>
                    <table>
                        <tr>
                            <td><span class="result">20</span> Play</td>
                            <td class="scorePta"><span class="result pta">83.54%</span> PTA</td>
                            <td>
                                <div class="wrapScore">
                                    <div class="tscore"><b>+123.30</b>SCORE</div>
                                </div>
                            </td>
                            <td><span class="result twin">12</span> Win</td>
                            <td><span class="result tdraw">3</span> Draw</td>
                            <td><span class="result tlose">5</span> Lose</td>
                        </tr>
                    </table>
                    <div class="tab">
                        <div class="tab1">Fri, Jan 31</div>
                        <div class="tab2">Rating : 69.54%  Ranking :  1790.00</div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
            </div>

            <div class="tabUtility">
                <div class="tab1 text-purple">สรุปคู่ที่เล่นวันนี้</div>
                <div class="tab2"><span class="seeMore">ดูทั้งหมด</span></div>
                <div style="clear: both;"></div>
            </div>
            <div class="tableResult-game">
                <table>
                    <tr>
                        <td>Honduras (2.06)</td>
                        <td>0.75</td>
                        <td><span class="team-favored">Equador (1.88)</span></td>
                        <td class="scored">1 - 2</td>
                        <td>+7.05</td>
                        <td><img src="images/icon/win.png"></td>
                    </tr>
                    <tr>
                        <td>Honduras (2.06)</td>
                        <td>0.75</td>
                        <td><span class="team-favored">Equador (1.88)</span></td>
                        <td class="scored">1 - 2</td>
                        <td>+7.05</td>
                        <td><img src="images/icon/win.png"></td>
                    </tr>
                    <tr>
                        <td>Honduras (2.06)</td>
                        <td>0.75</td>
                        <td><span class="team-favored">Equador (1.88)</span></td>
                        <td class="scored">1 - 2</td>
                        <td>+7.05</td>
                        <td><img src="images/icon/win.png"></td>
                    </tr>
                    <tr>
                        <td>Honduras (2.06)</td>
                        <td>0.75</td>
                        <td><span class="team-favored">Equador (1.88)</span></td>
                        <td class="scored">1 - 2</td>
                        <td>+7.05</td>
                        <td><img src="images/icon/win.png"></td>
                    </tr>
                    <tr>
                        <td>Honduras (2.06)</td>
                        <td>0.75</td>
                        <td><span class="team-favored">Equador (1.88)</span></td>
                        <td class="scored">1 - 2</td>
                        <td>+7.05</td>
                        <td><img src="images/icon/win.png"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <!--End-->
</div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
