<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$service_liveMatch = Services::getLiveMatch();
$service_liveWait = Services::getLiveWait();

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

$footerScript .= '<script src="scripts/games/hightlow.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div ng-controller="mainCtrl">


    <!--Content-->
    <div class="wrapper-content content-profile">

        <div class="wrapper-miniGames">
            <div class="tab-heading-title"><img src="images/mini-game/hightlow/title_icon_g2.png" width="25px;"> เกมส์ไฮโล</div>
            <div ng-switch="result" class="totalPoint">
                <b class="alert alert-info" style="padding-right: 15px;">{{ totalPoint}} แต้ม</b> <br>
            </div>
<!--            <div ng-switch="result" class="totalPoint">-->
<!--                <b class="alert alert-info" style="padding-right: 15px;">{{ totalPoint}} แต้ม</b> <br>-->
<!--                <span ng-switch-when="win" class="alert alert-success alertResult"><img src="images/icon/right-sign-icon.png"> คุณทายถูก</span>-->
<!--                <span ng-switch-when="lose" class="alert alert-danger alertResult"><img src="images/icon/wrong-sign-icon.png"> คุณทายผิด</span>-->
<!--            </div>-->
            <div class="gameHightLow">
                <table>
                    <tr>
                        <td ng-switch="chooseL">
                            <span class="point">{{ chooseL}}</span>
                            <img ng-switch-when="1" src="images/mini-game/hightlow/dice_No_01.png">
                            <img ng-switch-when="2" src="images/mini-game/hightlow/dice_No_02.png">
                            <img ng-switch-when="3" src="images/mini-game/hightlow/dice_No_03.png">
                            <img ng-switch-when="4" src="images/mini-game/hightlow/dice_No_04.png">
                            <img ng-switch-when="5" src="images/mini-game/hightlow/dice_No_05.png">
                            <img ng-switch-when="6" src="images/mini-game/hightlow/dice_No_06.png">
                        </td>
                        <td ng-switch="chooseC">
                            <span class="point">{{ chooseC}}</span>
                            <img ng-switch-when="1" src="images/mini-game/hightlow/dice_No_01.png">
                            <img ng-switch-when="2" src="images/mini-game/hightlow/dice_No_02.png">
                            <img ng-switch-when="3" src="images/mini-game/hightlow/dice_No_03.png">
                            <img ng-switch-when="4" src="images/mini-game/hightlow/dice_No_04.png">
                            <img ng-switch-when="5" src="images/mini-game/hightlow/dice_No_05.png">
                            <img ng-switch-when="6" src="images/mini-game/hightlow/dice_No_06.png">
                        </td>
                        <td ng-switch="chooseR">
                            <span class="point">{{ chooseR}}</span>
                            <img ng-switch-when="1" src="images/mini-game/hightlow/dice_No_01.png">
                            <img ng-switch-when="2" src="images/mini-game/hightlow/dice_No_02.png">
                            <img ng-switch-when="3" src="images/mini-game/hightlow/dice_No_03.png">
                            <img ng-switch-when="4" src="images/mini-game/hightlow/dice_No_04.png">
                            <img ng-switch-when="5" src="images/mini-game/hightlow/dice_No_05.png">
                            <img ng-switch-when="6" src="images/mini-game/hightlow/dice_No_06.png">
                        </td>
                    </tr>
                </table>
            </div>
            <div class="selectCoin">
                <table>
                    <tr>
                        <td ng-class="{'activeCoin-100':sgold === 100}" ng-click="sgold = 100" class="selectCoin-100"></td>
                        <td ng-class="{'activeCoin-200':sgold === 200}" ng-click="sgold = 200" class="selectCoin-200"></td>
                        <td ng-class="{'activeCoin-500':sgold === 500}" ng-click="sgold = 500" class="selectCoin-500"></td>
                    </tr>
                </table>
            </div>

            <div class="selectWeapon-HightLow">
                <div class="hi-lo">
                    <table>
                        <tr>
                            <td ng-click="play('l')" ng-class="{'activeLow':choose === 'l'}" class="weaponLow"></td>
                            <td ng-click="play('11')" ng-class="{'activeEleven':choose === '11'}" class="weaponEleven"></td>
                            <td ng-click="play('h')" ng-class="{'activeHight':choose === 'h'}" class="weaponHight"></td>
                        </tr>
                    </table>
                </div>
            </div>


            <div class="titleGames" style="display: none;">สติถิการเล่นล่าสุด</div>
            <div class="boxStatPlay-Games" style="display: none;">
                <table>
                    <tr>
                        <td><img src="/images/mini-game/hightlow/btn_low_active.png"></td>
                        <td><img src="/images/mini-game/hightlow/btn_hight_active.png"></td>
                        <td><img src="/images/mini-game/hightlow/btn_11_active.png"></td>
                        <td><img src="/images/mini-game/hightlow/btn_low_active.png"></td>
                        <td><img src="/images/mini-game/hightlow/btn_hight_active.png"></td>
                        <td><img src="/images/mini-game/hightlow/btn_11_active.png"></td>
                        <td><img src="/images/mini-game/hightlow/btn_low_active.png"></td>
                        <td><img src="/images/mini-game/hightlow/btn_hight_active.png"></td>
                        <td><img src="/images/mini-game/hightlow/btn_11_active.png"></td>
                        <td><img src="/images/mini-game/hightlow/btn_low_active.png"></td>
                        <td><img src="/images/mini-game/hightlow/btn_low_active.png"></td>
                </table>
            </div>


            <div class="titleGames">วิธีการเล่นเกมส์</div>
            <div class="boxHowToPlay-Games">
                <ul>
                    <li><b>เกมส์ไฮโลจะมีลูกเต๋าอยู่ 3 ลูกทายให้ถูกว่าลูกเต่าทั้ง 3 ลูก เมื่อนำแต้มมาบวกกันแล้วจะมีค่าน้อยกว่า11 มากกว่า 11   หรือ เท่ากับ 11 โดยมีวิธีการเล่นดังนี้</b></li>
                    <li>- เลือกจำนวนเหรียญที่ต้องการเดิมพัน โดยมีให้เลือก 100, 200, 500 เหรียญ Scoin</li>
                    <li>- จากนั้นเลือกทายว่าแต้มที่ลูกเต๋าทั้ง 3 ลูกเมื่อนำมาบวกกันเท่ากับกี่แต้ม โดยมีวิธีคิดคะแนนดังนี้
                        แต้ม <  11  จะเป็น ต่ำ และเมื่อทายถูกจะได้เหรียญ 1 เท่า
                        แต้ม =  11  จะเป็น 11 แต้ม และเมื่อทายถูกจะได้เหรียญ 5 เท่า
                        แต้ม >  11  จะเป็น สูง และเมื่อทายถูกจะได้เหรียญ 1 เท่า
                    </li>
                    <li>- ผลการทายจะแสดงให้คุณว่าเราทายถูกหรือทายผิด โดยสามารถดูสถิติการเล่นได้ที่ตารางสรุปผลการเล่นเกมส์</li>
                </ul>
            </div>

            <!--            ตารางสรุปผลการเล่นเกมส์-->
            <div class="titleGames">ตารางสรุปผลการเล่นเกมส์</div>

            <div class="table-ResultMiniGames">
                <div class="tabs-tableResult">
                    <ul>
                        <li ng-click="stateTab = 'user'" ng-class="{'active':stateTab === 'user'}">เฉพาะฉัน</li>
                        <li ng-click="stateTab = 'all'" ng-class="{'active':stateTab === 'all'}">ทุกคน</li>
                    </ul>
                    <div style="clear: both;"></div>
                </div>

                <table>
                    <thead>
                        <tr>
                            <th>ชื่อผู้เล่น</th>
                            <th>ชื่อเกมส์</th>
                            <th>ผู้เล่น</th>
                            <th>คอมพ์</th>
                            <th>ผลการเล่น</th>
                            <th>จำนวน</th>
                            <th>ได้/เสีย</th>
                            <th>วันที่/เวลา</th>
                        </tr>
                    </thead>
                    <tbody ng-show="stateTab === 'user'">
                        <tr ng-repeat="item in statement.user">
                            <td><a href="profile.php?id={{ item.fb_uid}}"><img
                                        ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"> {{ item.display_name ||
                                item.fb_firstname }}</a></td>
                            <td>ไฮโล</td>
                            <td>
                                <span ng-switch="item.player1">
                                    <img ng-switch-when="l" src="images/mini-game/hightlow/btn_low_defualt.png">
                                    <img ng-switch-when="11" src="images/mini-game/hightlow/btn_11_defualt.png">
                                    <img ng-switch-when="h" src="images/mini-game/hightlow/btn_hight_defualt.png">
                                </span>
                            </td>
                            <td>
                                {{ item.player2 | parseOdds:'sum'}}
                            </td>
                            <td>
                                <span ng-switch="item.result">
                                    <span class="correct" ng-switch-when="win">ชนะ</span>
                                    <span class="wrong" ng-switch-when="lose">แพ้</span>
                                    <span class="draw" ng-switch-when="draw">เสมอ</span>
                                </span>
                            </td>
                            <td><img src="images/icon/scoin30.png"> {{ item.scoin_amount}}</td>
                            <td><img src="images/icon/sgold30.png"> <span ng-class="{'correct':item.sgold_result_amount > 0, 'wrong':item.sgold_result_amount < 0 }">{{ item.sgold_result_amount}}</span></td>
                            <td>{{ item.play_timestamp | timeagoByDateTime }}</td>
                        </tr>
                    </tbody>
                    <tbody ng-show="stateTab === 'all'">
                        <tr ng-repeat="item in statement.all">
                            <td><a href="profile.php?id={{ item.fb_uid}}"><img
                                        ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"> {{ item.display_name ||
                                item.fb_firstname }}</a></td>
                            <td>ไฮโล</td>
                            <td>
                                <span ng-switch="item.player1">
                                    <img ng-switch-when="l" src="images/mini-game/hightlow/btn_low_defualt.png">
                                    <img ng-switch-when="11" src="images/mini-game/hightlow/btn_11_defualt.png">
                                    <img ng-switch-when="h" src="images/mini-game/hightlow/btn_hight_defualt.png">
                                </span>
                            </td>
                            <td>
                                {{ item.player2 | parseOdds:'sum'}}
                            </td>
                            <td>
                                <span ng-switch="item.result">
                                    <span class="correct" ng-switch-when="win">ชนะ</span>
                                    <span class="wrong" ng-switch-when="lose">แพ้</span>
                                    <span class="draw" ng-switch-when="draw">เสมอ</span>
                                </span>
                            </td>
                            <td><img src="images/icon/scoin30.png"> {{ item.scoin_amount}}</td>
                            <td><img src="images/icon/sgold30.png"> <span ng-class="{'correct':item.sgold_result_amount > 0, 'wrong':item.sgold_result_amount < 0 }">{{ item.sgold_result_amount}}</span></td>
                            <td>{{ item.play_timestamp | timeagoByDateTime }}</td>
                        </tr>
                    </tbody>
                </table>

            </div>

        </div>



        <!--alert กรณีทายถูก -->
        <div id="winModal" class="Modal-resultGame" style="display: none;">
            <div class="fadeGame"></div>
            <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="false" style="display: block; z-index: 5; top: 200px; background: none; box-shadow: none; border: 0;">

                <div class="alertCorrect">      
                    <div class="titleModal"></div>
                    <div class="bodygameCard" style="padding: 30px;">
                        <img src="images/mini-game/paoyingchoob/modal_bg_lose_sgold.png">
                    </div>
                    <div class="resultPlay">คุณได้รับ</div>
                    <h3>{{ resultScoin }} <span class="goldText">S</span> COIN</h3>
                    <div class="titleModal" style="cursor: pointer;"><img ng-click="close()" src="images/mini-game/allgames/modal_bg_win_btn.png"></div>

                </div>

            </div>
        </div>



        <!--alert กรณีทายผิด -->
        <div id="loseModal" class="Modal-resultGame" style="display: none;">
            <div class="fadeGame"></div>
            <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="false" style="display: block; z-index: 5; top: 200px; background: none; box-shadow: none; border: 0;">

                <div class="alertWrong">      
                    <div class="titleModal"></div>
                    <div class="bodygameCard" style="padding: 30px;">
                        <img src="images/mini-game/paoyingchoob/modal_bg_lose_sgold.png">
                    </div>
                    <div class="resultPlay">คุณเสีย</div>
                    <h3> <span class="text-red">{{ resultScoin }}</span> <span class="goldText">S</span> COIN</h3>
                    <div class="titleModal" style="cursor: pointer; "><img ng-click="close()" src="images/mini-game/allgames/modal_bg_lose_btn.png"></div>
                </div>

            </div>
        </div>


    </div>
</div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
