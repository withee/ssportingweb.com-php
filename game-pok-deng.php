<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$service_liveMatch = Services::getLiveMatch();
$service_liveWait = Services::getLiveWait();

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

$footerScript .= '<script src="scripts/games/pok-deng.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div ng-controller="mainCtrl">


    <!--Content-->
    <div class="wrapper-content content-profile">

        <div class="wrapper-miniGames">
            <div class="tab-heading-title tabTypeGames"><img src="images/mini-game/card/title_icon_g3.png" width="40px;" style="float: left;"> เกมส์ไพ่ป๊อกเด้ง</div>
            <div class="totalPoint correct poker" style="display: none;">
                <b>คุณชนะ</b>
            </div>
            <div class="bodyGames pokdeng">
                <table>
                    <tr>
                        <td>
                            <span ng-show="facebookInfo.id" class="player"><img
                                    ng-src="https://graph.facebook.com/{{ facebookInfo.id}}/picture" style="width: 40px;"></span>
                            <div class="cards">
                                <img ng-repeat="item in player1Cards" img src="images/mini-game/deck/{{item.face}}{{item.num}}.png">
                            </div>
                            <span class="scoreCard">{{ player1Score}}</span>
                        </td>
                        <td>
                            <span class="player"><img src="images/mini-game/paoyingchoob/avata_GM_g1.png" style="width: 40px;"></span>
                            <div class="cards">
                                <img ng-repeat="item in player2Cards" img src="images/mini-game/deck/{{item.face}}{{item.num}}.png">
                            </div>
                            <span class="scoreCard">{{ player2Score}}</span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="selectCoin">
                <table>
                    <tr>
                        <td ng-class="{'activeCoin-100':scoin === 100}" ng-click="scoin = 100" class="selectCoin-100"></td>
                        <td ng-class="{'activeCoin-200':scoin === 200}" ng-click="scoin = 200" class="selectCoin-200"></td>
                        <td ng-class="{'activeCoin-500':scoin === 500}" ng-click="scoin = 500" class="selectCoin-500"></td>
                    </tr>
                </table>
            </div>

            <div class="PlayGames">
                <div ng-click="play()" class="Play"></div>
            </div>


            <div class="titleGames">วิธีการเล่นเกมส์</div>
            <div class="boxHowToPlay-Games">
                <ul>
                    <li><b>เกมส์ไพ่ป๊อกเด้ง เป็นเกมส์ที่ผู้เล่นแข่งกับคอมพิวเตอร์ว่าฝ่ายไหนจะมีแต้มสูงกว่ากัน โดยมีวิธีการเล่นดังนี้</b></li>
                    <li>- เลือกจำนวนเหรียญที่ต้องการเดิมพัน โดยมีให้เลือก 100, 200, 500 เหรียญ Scoin</li>
                    <li>- จากนั้นผู้เล่นจะได้รับไพ่  2  ใบ โดยจะเมื่อนำตัวเลขไพ่ทั้งสองมาบวกกันแล้วจะเป็นแต้มของผู้เล่น โดยนำแต้มของผู้เล่นมาเปรียบเทียบกับแต้มของคอมพิวเตอร์ แต้มฝ่ายสูงกว่าก็จะเป็นผู้ชนะ โดยมีวิธีคิดคะแนนดังนี้</li>
                </ul>

                <div class="mores">
                    <input type="checkbox" id="toggle">
                    <label for="toggle">ดูเพิ่มเติม</label>
                    <div class="to-be-changed">
                        - แต้มที่นำมาเปรียบเทียบกัน คือ  1-9  แต้ม โดยเมื่อตัวเลขบวกกันได้ 10 แต้ม จะนับเป็น 0 แต้ม หรือ ถ้าตัวเลขบวกกัน เกิน 10 แต้ม ให้นับแต้มจากตัวเลขหลักหน่วย เช่น 9+8 = 17  จะได้เท่ากับ 7 แต้ม <br>
                        - เมื่อนำตัวเลขไพ่ทั้งสองใบบวกกันแล้วได้น้อยกว่าหรือเท่ากับ 4 แต้ม ระบบจะให้ไพ่ใบที่ 3 อัตโนมัติ แล้วนำตัวเลขไพ่ทั้ง 3 มาบวกกัน แล้วนำแต้มที่ได้มาเปรียบเทียบกับแต้มของคอมพิวเตอร์ แต้มฝ่ายไหนสูงกว่าก็จะเป็นผู้ชนะ เมื่อผู้เล่นชนะจะได้รับเหรียญ 1 เท่า<br>
                        - หรือถ้าไพ่ของผู้เล่นทั้ง 2 ใบมีดอกที่เหมือนกัน และแต้มสูงกว่าคอมพิวเตอร์ ผู้เล่นชนะจะได้รับเหรียญ 2 เท่า เช่น ไพ่โพดำ 5  ไพ่โพดำ 2  เมื่อนำมาบวกกันจะได้ 7 แต้ม 2 เด้ง <br>
                        - หรือถ้าไพ่ของผู้เล่นทั้ง 3 ใบมีดอกที่เหมือนกัน และแต้มสูงกว่าคอมพิวเตอร์ ผู้เล่นชนะจะได้รับเหรียญ 3 เท่า<br>
                        เช่น ไพ่โพดำ 7  ไพ่โพดำ 5  ไพ่โพดำ 3  เมื่อนำมาบวกกันจะได้ 5 แต้ม 3 เด้ง<br><br>
                        <b>เงื่อนไขที่ได้คะแนนพิเศษ</b><br>
                        -	ถ้าไพ่ 3 ใบ เป็น J,Q,K เช่น J,J,K  จะได้รับเหรียญ 3 เท่า<br>
                        -	ถ้าไพ่ 3 ใบ เรียงกันทั้ง 3 ใบ เช่น 5,6,7 หรือ J,Q,K  จะได้รับเหรียญ 4 เท่า<br>
                        -	ถ้าไพ่ 3 ใบ เรียงกันทั้ง 3 ใบ และดอกเหมือนกัน จะได้รับเหรียญ 5 เท่า<br>
                        -	ถ้าไพ่ 3 ใบ เหมือนกันทุกใบ เช่น 3,3,3 / J,J,J  เรียกว่า ไพ่ตอง จะได้รับเหรียญ 5 เท่า
                    </div>
                </div>

            </div>


            <!--            ตารางสรุปผลการเล่นเกมส์-->
            <div class="titleGames">ตารางสรุปผลการเล่นเกมส์</div>

            <div class="table-ResultMiniGames">
                <div class="tabs-tableResult">
                    <ul>
                        <li ng-click="stateTab = 'user'" ng-class="{'active':stateTab === 'user'}">เฉพาะฉัน</li>
                        <li ng-click="stateTab = 'all'" ng-class="{'active':stateTab === 'all'}">ทุกคน</li>
                    </ul>
                    <div style="clear: both;"></div>
                </div>

                <table>
                    <thead>
                        <tr>
                            <th>ชื่อผู้เล่น</th>
                            <th>ชื่อเกมส์</th>
                            <th>ผู้เล่น</th>
                            <th>คอมพ์</th>
                            <th>ผลการเล่น</th>
                            <th>จำนวน</th>
                            <th>ได้/เสีย</th>
                            <th>วันที่/เวลา</th>
                        </tr>
                    </thead>
                    <tbody ng-show="stateTab === 'user'">
                        <tr ng-repeat="item in statement.user">
                            <td><a href="profile.php?id={{ item.fb_uid}}"><img
                                        ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"> {{ item.display_name ||
                                item.fb_firstname }}</a></td>
                            <td>ป๊อกเด้ง</td>
                            <td>
                                {{ item.player1 | parseScore:'1' }}
                            </td>
                            <td>
                                {{ item.player2 | parseScore:'2' }}
                            </td>
                            <td>
                                <span ng-switch="item.result">
                                    <span class="correct" ng-switch-when="win">ชนะ</span>
                                    <span class="wrong" ng-switch-when="lose">แพ้</span>
                                    <span ng-switch-when="draw">เสมอ</span>
                                </span>
                            </td>
                            <td><img src="images/icon/scoin30.png"> {{ item.scoin_amount}}</td>
                            <td><img src="images/icon/sgold30.png"> <span ng-class="{'correct':item.scoin_result_amount > 0, 'wrong':item.scoin_result_amount < 0 }">{{ item.scoin_result_amount}}</span></td>
                            <td>{{ item.play_timestamp | timeagoByDateTime }}</td>
                        </tr>
                    </tbody>
                    <tbody ng-show="stateTab === 'all'">
                        <tr ng-repeat="item in statement.all">
                            <td><a href="profile.php?id={{ item.fb_uid}}"><img
                                        ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"> {{ item.display_name ||
                                item.fb_firstname }}</a></td>
                            <td>ป๊อกเด้ง</td>
                            <td>
                                {{ item.player1 | parseScore:'1' }}
                            </td>
                            <td>
                                {{ item.player2 | parseScore:'2' }}
                            </td>
                            <td>
                                <span ng-switch="item.result">
                                    <span class="correct" ng-switch-when="win">ชนะ</span>
                                    <span class="wrong" ng-switch-when="lose">แพ้</span>
                                    <span ng-switch-when="draw">เสมอ</span>
                                </span>
                            </td>
                            <td><img src="images/icon/scoin30.png"> {{ item.scoin_amount}}</td>
                            <td><img src="images/icon/sgold30.png"> <span ng-class="{'correct':item.scoin_result_amount > 0, 'wrong':item.scoin_result_amount < 0 }">{{ item.scoin_result_amount}}</span></td>
                            <td>{{ item.play_timestamp | timeagoByDateTime }}</td>
                        </tr>
                    </tbody>
                </table>

            </div>

        </div>






        <!--alert กรณีชนะ -->
        <div id="winModal" class="Modal-resultGame" style="display: none;">
            <div class="fadeGame"></div>
            <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="false" style="display: block; z-index: 5; top: 200px; background: none; box-shadow: none; border: 0;">

                <div class="alertWin">      
                    <div class="titleModal"></div>
                    <div class="boxShowPoint">
                        <table>
                            <tr>
                                <td>
                                    <img ng-repeat="item in player1Cards" img src="images/mini-game/deck/{{item.face}}{{item.num}}.png">
                                    <h4>คุณได้ : {{ player1Score }}</h4>
                                </td>
                                <td>
                                    <img ng-repeat="item in player2Cards" img src="images/mini-game/deck/{{item.face}}{{item.num}}.png">
                                    <h4>คอมฯ ได้ : {{ player2Score }}</h4>
                                </td>

                            </tr>
                        </table>
                    </div>
                    <div class="resultPlay">คุณได้รับ</div>
                    <h3> {{ resultScoin }} <span class="goldText">S</span> COIN</h3>
                    <div class="titleModal" style="cursor: pointer;"><img ng-click="close()" src="images/mini-game/allgames/modal_bg_win_btn.png"></div>
                </div>

            </div>
        </div>



        <!--alert กรณีแพ้ -->
        <div id="loseModal" class="Modal-resultGame" style="display: none;">
            <div class="fadeGame"></div>
            <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="false" style="display: block; z-index: 5; top: 200px; background: none; box-shadow: none; border: 0;">

                <div class="alertLose">      
                    <div class="titleModal"></div>
                    <div class="boxShowPoint">
                        <table>
                            <tr>
                                <td>
                                    <img ng-repeat="item in player1Cards" img src="images/mini-game/deck/{{item.face}}{{item.num}}.png">
                                    <h4>คุณได้ : {{ player1Score }}</h4>
                                </td>
                                <td>
                                    <img ng-repeat="item in player2Cards" img src="images/mini-game/deck/{{item.face}}{{item.num}}.png">
                                    <h4>คอมฯ ได้ : {{ player2Score }}</h4>
                                </td>

                            </tr>
                        </table>
                    </div>
                    <div class="resultPlay">คุณเสีย</div>
                    <h3> <span class="text-red">{{ resultScoin }}</span> <span class="goldText">S</span> COIN</h3>
                    <div class="titleModal" style="cursor: pointer;"><img ng-click="close()" src="images/mini-game/allgames/modal_bg_lose_btn.png"></div>
                </div>

            </div>
        </div>



        <!--alert กรณีเสมอ -->
        <div id="drawModal" class="Modal-resultGame" style="display: none;">
            <div class="fadeGame"></div>
            <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="false" style="display: block; z-index: 5; top: 200px; background: none; box-shadow: none; border: 0;">

                <div class="alertDraw1">      
                    <div class="titleModal"></div>
                    <div class="boxShowPoint">
                        <table>
                            <tr>
                                <td>
                                    <img ng-repeat="item in player1Cards" img src="images/mini-game/deck/{{item.face}}{{item.num}}.png">
                                    <h4>คุณได้ : {{ player1Score }}</h4>
                                </td>
                                <td>
                                    <img ng-repeat="item in player2Cards" img src="images/mini-game/deck/{{item.face}}{{item.num}}.png">
                                    <h4>คุณได้ : {{ player2Score }}</h4>
                                </td>

                            </tr>
                        </table>
                    </div>

                    <div class="titleModal" style="cursor: pointer; margin-top: 80px;"><img ng-click="close()" src="images/mini-game/allgames/modal_bg_draw_btn.png"></div>
                </div>

            </div>
        </div>

    </div>


</div>



<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
