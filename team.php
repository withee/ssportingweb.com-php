<?php
require_once(dirname(__FILE__) . '/_init_.php');

define('__TID__', $_REQUEST['id']);

$title = 'ข้อมูลและสถิติของทีมฟุตบอลจากลีกทั่วโลก';
$meta = '<meta name="description" content="รวบรวมข้อมูลรายละเอียดของทีมฟุตบอลจากลีกทั่วโลก ไม่ว่าจะเป็นผลการแข่งล่าสุด สถิติH2H และโปรแกรมการแข่งขันล่วงหน้า">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอลสด,ผลบอล,livescore,โปรแกรมแข่งขันล่วงหน้า,ตารางคะแนน">' . "\n";


$service_teamInfo = Services::getTeamInfo(__TID__);
$service_allteam = Services::getAllTeam();

$footerScript .= '<script id="teamScript" src="scripts/team.js" tid="'.__TID__.'"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>


    <div class="wrapper-content">

    <div class="content">
    <div class="section-header-profile league-cover">
        <table>
            <tr>
                <td class="tab-logo-header">
                    <div class="logo-team-stat"><img src="{{ logoUrl}}<?php //echo $service_teamInfo->team->tid; ?>_256x256.png"/></div>
                </td>
                <td class="tab-nameteam-header">
                    <span class="name-user league-name-title"><?php echo isset($service_allteam->{$service_teamInfo->team->tid}->name) ? $service_allteam->{$service_teamInfo->team->tid}->name : $service_teamInfo->team->tn; ?></span>

                    <div class="country-stat"><img src="images/countries/<?php echo $service_teamInfo->team->cid; ?>.png"/> <?php echo $service_teamInfo->team->comName; ?></div>
                </td>
                <td class="tab-like-this-team">
                        <span ng-click='favorite("team", <?php echo json_encode($service_teamInfo->team); ?>)'>
                            <img ng-if="favoriteTeam[<?php echo $service_teamInfo->team->tid; ?>]" src="images/icon/heart.png"/>
                            <img ng-if="!favoriteTeam[<?php echo $service_teamInfo->team->tid; ?>]" src="images/icon/heart-hide.png"/>
                        </span>
                </td>

            </tr>
        </table>

    </div>

    <div class="wrapper-lastest-match-result">
        <?php if (!empty($service_teamInfo->result[0])): ?>
        <div>
            <div class="header-match-title"><?php echo Utils::trans('Latest Match Result'); ?></div>
            <div class="box-table">
                <div class="section-lastest-match">
                    <table>
                        <thead>
                        <tr>
                            <th><?php echo Utils::trans('Date'); ?></th>
                            <th><?php echo Utils::trans('League'); ?></th>
                            <th><?php echo Utils::trans('Home'); ?></th>
                            <th><?php echo Utils::trans('Goal'); ?></th>
                            <th><?php echo Utils::trans('Away'); ?></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($service_teamInfo->result as $index => $item): ?>
                            <tr>
                                <td class="date-match"><?php echo $item->date; ?></td>
                                <td class="league-match"><?php echo $item->lnk; ?></td>
                                <td class="teamhome-match"><a href="/team.php?id=<?php echo $item->tid1; ?>">
                                        <span class="teamhome-m"><?php echo (isset($service_allteam->{$item->tid1}) && isset($service_allteam->{$item->tid1}->name)) ? $service_allteam->{$item->tid1}->name : $item->hn; ?></span>
                                        <img src="{{ logoUrl}}<?php //echo $item->tid1; ?>_32x32.png"/>
                                    </a>
                                </td>
                                <td class="bg-goal score-match <?php if($item->r === 'w'): ?> win-bg<?php elseif($item->r === 'd'): ?> draw-bg<?php elseif($item->r === 'l'): ?> lose-bg<?php endif; ?>">
                                    <?php echo $item->score; ?>
                                </td>
                                <td class="teamaway-match"><a href="/team.php?id=<?php echo $item->tid2; ?>">
                                        <img src="{{ logoUrl}}<?php //echo $item->tid2; ?>_32x32.png"/>
                                        <span class="teamaway-m"><?php echo (isset($service_allteam->{$item->tid2}) && isset($service_allteam->{$item->tid2}->name)) ? $service_allteam->{$item->tid2}->name : $item->an; ?></a></span>
                                </td>
                                <td class="stat-icon">
                                    <div ng-switch="item.r">
                                        <img ng-switch-when="d" src="images/icon/draw.png"/>
                                        <img ng-switch-when="w" src="images/icon/win.png"/>
                                        <img ng-switch-when="l" src="images/icon/lose.png"/>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <?php if (!empty($service_teamInfo->fixture[0])): ?>
        <div>
            <div class="header-match-title"><?php echo Utils::trans('Fixture'); ?></div>
            <div class="box-table">
                <div class="section-fixure">
                    <table>
                        <thead>
                        <tr>
                            <th><?php echo Utils::trans('Date'); ?></th>
                            <th><?php echo Utils::trans('League'); ?></th>
                            <th><?php echo Utils::trans('Home'); ?></th>
                            <th><?php echo Utils::trans('Goal'); ?></th>
                            <th><?php echo Utils::trans('Away'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($service_teamInfo->fixture as $index => $item): ?>
                            <tr>
                                <td class="date-match"><?php echo $item->date; ?></td>
                                <td class="league-match"><?php echo $item->lnk; ?></td>
                                <td class="team-h"><a href="/team.php?id=<?php echo $item->tid1; ?>"><?php echo (isset($service_allteam->{$item->tid1}) && isset($service_allteam->{$item->tid1}->name)) ? $service_allteam->{$item->tid1}->name : $item->hn; ?> <img src="{{ logoUrl}}<?php //echo $item->tid1; ?>_32x32.png"/></a></td>
                                <td class="score-match"> - </td>
                                <td class="team-a"><a href="/team.php?id=<?php echo $item->tid2; ?>"><img src="{{ logoUrl}}<?php //echo $item->tid2; ?>_32x32.png"/> <?php echo (isset($service_allteam->{$item->tid2}) && isset($service_allteam->{$item->tid2}->name)) ? $service_allteam->{$item->tid2}->name : $item->an; ?></a></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <?php if (!empty($service_teamInfo->fixture[0])): ?>
        <div>
            <div class="header-match-title table-bg"><?php echo Utils::trans('Table'); ?></div>
            <div class="box-table">
                <div class="section-table">
                    <div class="tab-menu-table">
                        <table>
                            <tr class="heading-table">
                                <th ng-class="{'tab-total': tableTab == 'total'}" ng-click="tableTab = 'total'">
                                    Total
                                </th>
                                <th ng-class="{'tab-total': tableTab == 'home'}" ng-click="tableTab = 'home'">Home</th>
                                <th ng-class="{'tab-total': tableTab == 'away'}" ng-click="tableTab = 'away'">Away</th>
                            </tr>
                        </table>
                    </div>


                    <table>

                        <tbody ng-show="tableTab == 'total'">
                        <tr>
                            <th colspan="2"></th>
                            <th></th>
                            <th>P</th>
                            <th>W</th>
                            <th>D</th>
                            <th>L</th>
                            <th>F</th>
                            <th>A</th>
                            <th>GD</th>
                            <th>PTS</th>
                        </tr>
                        <?php foreach ($service_teamInfo->stat_table[0]->list as $index => $item): ?>
                            <tr class="
                            <?php if ($index <= 1): ?> bg-team-goto-champion-league<?php endif; ?>
                            <?php if ($item->tid == $service_teamInfo->team->tid): ?> bg-team-selected<?php endif; ?>
                            <?php if ($index >= count($service_teamInfo->stat_table[0]->list)): ?> bg-team-degraded<?php endif; ?>
                            ">
                                <td class="order-table"><?php echo $item->no; ?></td>
                                <td class="name-team"><a href="/team.php?id=<?php echo $item->tid; ?>"><img src="{{ logoUrl}}<?php //echo $item->tid; ?>_32x32.png"/> <?php echo isset($service_allteam->{$item->tid}->name) ? $service_allteam->{$item->tid}->name : $item->tn; ?></a></td>
                                <td></td>
                                <td><?php echo $item->gp; ?></td>
                                <td><?php echo $item->w; ?></td>
                                <td><?php echo $item->d; ?></td>
                                <td><?php echo $item->l; ?></td>
                                <td><?php echo $item->gf; ?></td>
                                <td><?php echo $item->ga; ?></td>
                                <td><?php echo $item->plusminus; ?></td>
                                <td><?php echo $item->pts; ?></td>
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                        <tbody ng-show="tableTab == 'home'">
                        <tr>
                            <th colspan="2"></th>
                            <th></th>
                            <th>P</th>
                            <th>W</th>
                            <th>D</th>
                            <th>L</th>
                            <th>F</th>
                            <th>A</th>
                            <th>GD</th>
                            <th>PTS</th>
                        </tr>
                        <?php foreach (__::sortBy($service_teamInfo->stat_table[0]->list, function($n){ return $n->no_h; }) as $index => $item): ?>
                            <tr class="
                            <?php if ($index <= 1): ?> bg-team-goto-champion-league<?php endif; ?>
                            <?php if ($item->tid == $service_teamInfo->team->tid): ?> bg-team-selected<?php endif; ?>
                            <?php if ($index >= count($service_teamInfo->stat_table[0]->list)): ?> bg-team-degraded<?php endif; ?>
                            ">
                                <td class="order-table"><?php echo $item->no_h; ?></td>
                                <td class="name-team"><a href="/team.php?id=<?php echo $item->tid; ?>"><img src="{{ logoUrl}}<?php //echo $item->tid; ?>_32x32.png"/> <?php echo isset($service_allteam->{$item->tid}->name) ? $service_allteam->{$item->tid}->name : $item->tn; ?></a></td>
                                <td></td>
                                <td><?php echo $item->gp_h; ?></td>
                                <td><?php echo $item->w_h; ?></td>
                                <td><?php echo $item->d_h; ?></td>
                                <td><?php echo $item->l_h; ?></td>
                                <td><?php echo $item->gf_h; ?></td>
                                <td><?php echo $item->ga_h; ?></td>
                                <td><?php echo $item->plusminus_h; ?></td>
                                <td><?php echo $item->pts_h; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>

                        <tbody ng-show="tableTab == 'away'">
                        <tr>
                            <th colspan="2"></th>
                            <th></th>
                            <th>P</th>
                            <th>W</th>
                            <th>D</th>
                            <th>L</th>
                            <th>F</th>
                            <th>A</th>
                            <th>GD</th>
                            <th>PTS</th>
                        </tr>
                        <?php foreach (__::sortBy($service_teamInfo->stat_table[0]->list, function($n){ return $n->no_g; }) as $index => $item): ?>
                            <tr class="
                            <?php if ($index <= 1): ?> bg-team-goto-champion-league<?php endif; ?>
                            <?php if ($item->tid == $service_teamInfo->team->tid): ?> bg-team-selected<?php endif; ?>
                            <?php if ($index >= count($service_teamInfo->stat_table[0]->list)): ?> bg-team-degraded<?php endif; ?>
                            ">
                                <td class="order-table"><?php echo $item->no_g; ?></td>
                                <td class="name-team"><a href="/team.php?id=<?php echo $item->tid; ?>"><img src="{{ logoUrl}}<?php //echo $item->tid; ?>_32x32.png"/> <?php echo isset($service_allteam->{$item->tid}->name) ? $service_allteam->{$item->tid}->name : $item->tn; ?></a></td>
                                <td></td>
                                <td><?php echo $item->gp_g; ?></td>
                                <td><?php echo $item->w_g; ?></td>
                                <td><?php echo $item->d_g; ?></td>
                                <td><?php echo $item->l_g; ?></td>
                                <td><?php echo $item->gf_g; ?></td>
                                <td><?php echo $item->ga_g; ?></td>
                                <td><?php echo $item->plusminus_g; ?></td>
                                <td><?php echo $item->pts_g; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
        <?php endif; ?>
    </div>
    </div>

    </div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>