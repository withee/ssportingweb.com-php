<div class="wrapper-menu-right">
    <div class="menu-right">



        <div id="board-right" class="wrapperTopic" style="display: none;">

            <div class="wrapperDetailTopic">
                <div class="topic"><img src="images/icon/human-icon.png"> หัวข้อโดยทีมงาน</div>
                <div ng-repeat="item in topics.recommend.timelines| limitTo:5" class="topicDetail">
                    <table>
                        <tr>
                            <td>
                                <a ng-if="item.owner.content_type !== 'news' && item.owner.content_type !== 'highlight'" href="profile.php?id={{ item.owner.fb_uid}}"><img ng-src="https://graph.facebook.com/{{ item.owner.fb_uid}}/picture"></a>
                                <img ng-if="item.owner.content_type === 'news'" src="images/icon/news.png">
                                <img ng-if="item.owner.content_type === 'highlight'" src="images/icon/video-icon.png">
                            </td>
                            <td><b>
                                    <a ng-if="item.owner.content_type === 'image'" href="news.php?id={{ item.owner.id}}">เผยแพร่รูปภาพ</a>
                                    <a ng-if="item.owner.content_type === 'board'" href="news.php?id={{ item.owner.id}}">{{ item.content[0].title}}</a>
                                    <a ng-if="item.owner.content_type === 'game'" href="game.php?mid={{ item.content[0].mid}}">{{ item.content[0].title}}</a>
                                    <a ng-if="item.owner.content_type === 'news'" href="news.php?id={{ item.owner.id}}">{{ item.content[0].title<?php echo ucfirst(__LANGUAGE__); ?>}}</a>
                                    <a ng-if="item.owner.content_type === 'status'" href="news.php?id={{ item.owner.id}}">{{ item.content[0].message}}</a>
                                </b>{{ getTopicTypeFromId(item.owner.type).feed_name}}</td>
                        </tr>
                    </table>
                </div>
                <!--                <div class="topicDetail">-->
                <!--                    <table>-->
                <!--                        <tr>-->
                <!--                            <td><img src="images/sample.jpg"></td>-->
                <!--                            <td><b>ใครจะชนะมาดูกัน</b>ไนจีเรีย - ออสเตรเลีย</td>-->
                <!--                        </tr>-->
                <!--                    </table>-->
                <!--                </div>-->
                <!--                <div class="topicDetail">-->
                <!--                    <table>-->
                <!--                        <tr>-->
                <!--                            <td><img src="images/sample.jpg"></td>-->
                <!--                            <td><b>ฮากิรี แฮตทริกสวิสอัด 3-0</b>ข่าวฟุตบอล</td>-->
                <!--                        </tr>-->
                <!--                    </table>-->
                <!--                </div>-->
                <!--                <div class="topicDetail">-->
                <!--                    <table>-->
                <!--                        <tr>-->
                <!--                            <td><img src="images/sample.jpg"></td>-->
                <!--                            <td><b>คุณคิดว่าแชมป์โลกปีนี้คือใคร</b>ห้องบอลโลก</td>-->
                <!--                        </tr>-->
                <!--                    </table>-->
                <!--                </div>-->
                <!--                <div class="topicDetail">-->
                <!--                    <table>-->
                <!--                        <tr>-->
                <!--                            <td><img src="images/sample.jpg"></td>-->
                <!--                            <td><b>สาวสวยวันนี้</b>Sexy Football</td>-->
                <!--                        </tr>-->
                <!--                    </table>-->
                <!--                </div>-->
            </div>




            <div class="wrapperDetailTopic">
                <div class="topic"><img src="images/icon/star-icon.png"> หัวข้อยอดนิยม</div>
                <div ng-repeat="item in topics.most_view.timelines| limitTo:5" class="topicDetail">
                    <table>
                        <tr>
                            <td>
                                <a ng-if="item.owner.content_type !== 'news' && item.owner.content_type !== 'highlight'" href="profile.php?id={{ item.owner.fb_uid}}"><img ng-src="https://graph.facebook.com/{{ item.owner.fb_uid}}/picture"></a>
                                <img ng-if="item.owner.content_type === 'news'" src="images/icon/news.png">
                                <img ng-if="item.owner.content_type === 'highlight'" src="images/icon/video-icon.png">
                            </td>
                            <td><b>
                                    <a ng-if="item.owner.content_type === 'image'" href="news.php?id={{ item.owner.id}}">เผยแพร่รูปภาพ</a>
                                    <a ng-if="item.owner.content_type === 'board'" href="news.php?id={{ item.owner.id}}">{{ item.content[0].title}}</a>
                                    <a ng-if="item.owner.content_type === 'game'" href="game.php?mid={{ item.content[0].mid}}">{{ item.content[0].title}}</a>
                                    <a ng-if="item.owner.content_type === 'news'" href="news.php?id={{ item.owner.id}}">{{ item.content[0].title<?php echo ucfirst(__LANGUAGE__); ?>}}</a>
                                    <a ng-if="item.owner.content_type === 'status'" href="news.php?id={{ item.owner.id}}">{{ item.content[0].message}}</a>
                                </b>{{ getTopicTypeFromId(item.owner.type).feed_name}}</td>
                        </tr>
                    </table>
                </div>
                <!--                <div class="topicDetail">-->
                <!--                    <table>-->
                <!--                        <tr>-->
                <!--                            <td><img src="images/sample.jpg"></td>-->
                <!--                            <td><b>ใครจะชนะมาดูกัน</b>ไนจีเรีย - ออสเตรเลีย</td>-->
                <!--                        </tr>-->
                <!--                    </table>-->
                <!--                </div>-->
                <!--                <div class="topicDetail">-->
                <!--                    <table>-->
                <!--                        <tr>-->
                <!--                            <td><img src="images/sample.jpg"></td>-->
                <!--                            <td><b>ฮากิรี แฮตทริกสวิสอัด 3-0</b>ข่าวฟุตบอล</td>-->
                <!--                        </tr>-->
                <!--                    </table>-->
                <!--                </div>-->
                <!--                <div class="topicDetail">-->
                <!--                    <table>-->
                <!--                        <tr>-->
                <!--                            <td><img src="images/sample.jpg"></td>-->
                <!--                            <td><b>คุณคิดว่าแชมป์โลกปีนี้คือใคร</b>ห้องบอลโลก</td>-->
                <!--                        </tr>-->
                <!--                    </table>-->
                <!--                </div>-->
                <!--                <div class="topicDetail">-->
                <!--                    <table>-->
                <!--                        <tr>-->
                <!--                            <td><img src="images/sample.jpg"></td>-->
                <!--                            <td><b>สาวสวยวันนี้</b>Sexy Football</td>-->
                <!--                        </tr>-->
                <!--                    </table>-->
                <!--                </div>-->
            </div>
        </div>

        <div  class="sidebar-style">
            <div class="titleCountdown">จะแจกรางวัลในอีก !!</div>
            <!--        box แสดงเวลานับถอยหลัง Countdown-->

            <div class="timeCountdowns" style="display: snone;">
                <table>
                    <tr>
                        <td><b ng-bind="nextReset[0]"></b>day</td>
                        <td><b ng-bind="nextReset[1]">2</b>Hour</td>
                        <td><b ng-bind="nextReset[2]">30</b>Minute</td>
                        <td><b ng-bind="nextReset[3]">59</b>Second</td>
                    </tr>
                </table>
            </div>
            <!--end-->
        </div>

        <div id="latest-event-box" style="display: none;">
            <div class="right-menu" style="display: none;">
                <?php echo Utils::trans('Latest Events') ?>
                <div ng-repeat="event in lastEvents">
                    <span>{{ event.cm}}</span>
                    <span>{{ event.hn}}</span>
                    <span>{{ event.gn}}</span>
                    <span>{{ event.s}}</span>
                </div>
            </div>
            <div class="right-menu sidebar-style" id="box-latest-event" style="display: none;">
                <div class="tab-top-rank-type"><?php echo Utils::trans('Latest Events') ?></div>
                <div class="last-event">
                    <div class="nano">
                        <div class="contentS">
                            <div ng-repeat="event in lastEvents">
                                <a href="/match.php?mid={{ event.mid}}">
                                    <table>
                                        <tr>
                                            <td>{{ event.cm}}<br>
    <!--                                            <img src="images/icon/footballs.png">-->
                                                <img ng-if="event.zdrid === '1'" src="images/icon/footballs.png">
                                                <img ng-if="event.zdrid === '4'" src="images/icon/red-card.png">
<!--                                                <img ng-if="event.zdrid === '5'" src="images/icon/ball-p.png">-->
<!--                                                <img ng-if="event.zdrid === '6'" src="images/icon/ball-no-p.png">-->
                                                <span ng-if="event.zdrid === '8'">AET</span>
                                                <span ng-if="event.zdrid === '9'">AP</span>
                                            </td>
                                            <td class="nameLeague" style="display: snone;">{{ event.kkod}}</td>
                                            <td class="event-detail"><span ng-bind="event.hn"></span><br>
    <!--                                            <span ng-if="event.zdz === 'H'" ng-bind=""></span>-->
    <!--                                            <span ng-if="event.zdz === 'G'" ng-bind=""></span>-->
                                                <span ng-bind="event.gn"></span>
                                            </td>
                                            <td>{{ event.s}}</td>
                                        </tr>
                                    </table>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div  class="sidebar-style" id="news-feed-box" style="display: none;">
            <div class="right-menu tab-top-rank-type">
                <?php echo Utils::trans('News Feed') ?>
            </div>

            <div class="menu-feed-list">
                <div class="nano">
                    <div class="contentS">
                        <table>
                            <tbody>
                                <tr ng-repeat="item in newfeed">
                                    <td>
<!--                                        <a href="/profile.php?id={{ item.fb_uid}}"><img-->
<!--                                                ng-src="http://graph.facebook.com/{{ item.fb_uid}}/picture"/></a>-->
                                        <a href="/profile.php?id={{ item.fb_uid}}"><img
                                                ng-src="http://api.ssporting.com/cache/small/{{ item.fb_uid}}.jpg"/></a>
                                    </td>
                                    <td ng-click="openBetDialogFromId(item.mid, $event)"><span class="font-name">{{ item.display_name || item.displayname || item.fb_firstname}}</span>
                                        <br/>Play:
                                        <span ng-switch="item.choose">
                                            <a href="/game.php?mid={{ item.mid}}" ng-switch-when="home">{{
                                                        allTeams[item.hid].name}}</a>
                                            <a href="/game.php?mid={{ item.mid}}" ng-switch-when="away">{{
                                                        allTeams[item.gid].name}}</a>
                                        </span> {{ item.betValue}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>







        <!-- feed comment match แถบด้านขวา-->
        <!--<div id="feed-comment-match-box" style="display: none;">-->
        <!--    <div class="right-menu">-->
        <!--        --><?php //echo Utils::trans('Comments');                                                      ?>
        <!--    </div>-->
        <!--    <div class="section-comment-match">-->
        <!--        <div>-->
        <!--            <div class="contentS">-->
        <!--                <table>-->
        <!--                    <tbody>-->
        <!--                    <tr ng-repeat="item in currentMatchComments">-->
        <!--                        <td class="team-bet select-team-{{ item.choose}}"></td>-->
        <!--                        <td class="images-commented-user"><a href="/profile.php?id={{ item.fb_uid}}"><img-->
        <!--                                    ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></td>-->
        <!--                        <td class="commented-post"><b>{{ item.displayname}}</b> {{ item.message}}</td>-->
        <!--                    </tr>-->
        <!--                    </tbody>-->
        <!--                </table>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->

        <!-- end -->


        <!-- Ranking World Cup-->

        <!--        <div id="w14ranking" class="wrapper-new-ranking sidebar-style" style="display: none;">
                    <div class="tab-top-rank-type">
                        Top Ranking <span class="font-blue">World Cup</span>
                    </div>
                    <div class="menu-toprank-list">
                        <div class="nano">
                            <div class="contentS">
                                <table>
                                    <tbody>
                                        <tr ng-repeat="item in w14ranking| limitTo:10">
                                            <td class="ranking-icon"><img ng-src="images/icon/{{ item.rank}}.png"/></td>
                                            <td><span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"></a></span>
                                                <span class="font-name name-ranking">{{ item.display_name || item.fb_firstname }}</span>
                                                <span class="score-user name-ranking">{{ item.overall_gp}}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>-->
    </div>

    <!--End-->




    <div id="ranking-wrapper" class="wrapper-new-ranking" style="display: none;">

        <div class="toprank">
            <div class="tab-header-menu" style="display: none;">
                <?php echo Utils::trans('Top Ranking') ?>
            </div>

            <div class="sidebar-style" ng-show="topRanking.friend.now[0]">
                <div class="tab-top-rank-type">
                    <?php echo Utils::trans('Top Friends') ?>
                </div>

                <div ng-init="topRankingFriendsSelect = 'now'" class="tab-menu-rank twoColumn">
                    <table>
                        <td ng-click="topRankingFriendsSelect = 'now'"
                            ng-class="{'active-rank': topRankingFriendsSelect == 'now'}"><?php echo Utils::trans('Now') ?>
                        </td>
                        <td ng-click="topRankingFriendsSelect = '2week'"
                            ng-class="{'active-rank': topRankingFriendsSelect == '2week'}">
                                <?php echo Utils::trans('Last Champ') ?>
                        </td>
                    </table>
                </div>

                <div ng-show="topRankingFriendsSelect == 'now'" class="menu-toprank-list">
                    <div class="nano">
                        <div class="contentS">
                            <table>
                                <tbody>
                                    <tr ng-repeat="item in topRanking.friend.now| limitTo:10">
                                        <td class="ranking-icon"><img ng-src="images/icon/{{ $index + 1}}.png"/></td>
                                        <td>
<!--                                            <span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img-->
<!--                                                        ng-src="http://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></span> <span-->
<!--                                                class="font-name name-ranking">{{ item.display_name}}</span> <span-->
<!--                                                class="score-user name-ranking">{{ item.overall_sgold | toFixed }}</span>-->
                                            <span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                                        ng-src="http://api.ssporting.com/cache/small/{{ item.fb_uid}}.jpg"/></a></span> <span
                                                class="font-name name-ranking">{{ item.display_name}}</span> <span
                                                class="score-user name-ranking">{{ item.overall_sgold | toFixed }}</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div ng-show="topRankingFriendsSelect == '2week'" class="menu-toprank-list">
                    <div class="nano">
                        <div class="contentS">
                            <table>
                                <tbody>
                                    <tr ng-repeat="item in topRanking.friend.2week| limitTo:10">
                                        <td class="ranking-icon"><img ng-src="images/icon/{{ $index + 1}}.png"/></td>
                                        <td>
<!--                                            <span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img-->
<!--                                                        ng-src="http://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></span> <span-->
<!--                                                class="font-name name-ranking">{{ item.display_name}}</span> <span-->
<!--                                                class="score-user name-ranking">{{ item.overall_sgold | toFixed }}</span>-->
                                            <span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                                        ng-src="http://api.ssporting.com/cache/small/{{ item.fb_uid}}.jpg"/></a></span> <span
                                                class="font-name name-ranking">{{ item.display_name}}</span> <span
                                                class="score-user name-ranking">{{ item.overall_sgold | toFixed }}</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div class="sidebar-style" ng-show="topRanking.top_ranking.alltime[0]">
                <div class="tab-top-rank-type">
                    <?php echo Utils::trans('Top Ranking') ?>
                </div>
                <div ng-init="topRankingScoreSelect = 'all'" class="tab-menu-rank threeColumn">
                    <table>

                        <td ng-click="topRankingScoreSelect = 'all'"
                            ng-class="{'active-rank': topRankingScoreSelect == 'all'}"><?php echo Utils::trans('All') ?>
                        </td>
                        <td ng-click="topRankingScoreSelect = 'now'"
                            ng-class="{'active-rank': topRankingScoreSelect == 'now'}">
                                <?php echo Utils::trans('Now') ?>
                        </td>
                        <td ng-click="topRankingScoreSelect = '2week'"
                            ng-class="{'active-rank': topRankingScoreSelect == '2week'}">
                                <?php echo Utils::trans('Last Champ') ?>
                        </td>

                    </table>
                </div>

                <div ng-show="topRankingScoreSelect == 'all'" class="menu-toprank-list">
                    <div class="nano">
                        <div class="contentS">
                            <table>
                                <tbody>
                                    <tr ng-repeat="item in topRanking.top_ranking.alltime| limitTo:10">
                                        <td class="ranking-icon"><img ng-src="images/icon/{{ $index + 1}}.png"/></td>
                                        <td>
<!--                                            <span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img-->
<!--                                                        ng-src="http://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></span> <span-->
<!--                                                class="font-name name-ranking">{{ item.display_name}}</span> <span-->
<!--                                                class="score-user name-ranking">{{ item.real_gp | toFixed }}</span>-->
<span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}">
        <img ng-src="http://api.ssporting.com/cache/small/{{ item.fb_uid}}.jpg"/>
    </a></span> <span
                                                class="font-name name-ranking">{{ item.display_name}}</span> <span
                                                class="score-user name-ranking">{{ item.real_gp | toFixed }}</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div ng-show="topRankingScoreSelect == 'now'" class="menu-toprank-list">
                    <div class="nano">
                        <div class="contentS">
                            <table>
                                <tbody>
                                    <tr ng-repeat="item in topRanking.top_ranking.now| limitTo:10">
                                        <td class="ranking-icon"><img ng-src="images/icon/{{ $index + 1}}.png"/></td>
                                        <td>
<!--                                            <span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img-->
<!--                                                        ng-src="http://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></span> <span-->
<!--                                                class="font-name name-ranking">{{ item.display_name}}</span> <span-->
<!--                                                class="score-user name-ranking">{{ item.overall_gp | toFixed }}</span>-->
                                            <span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                                        ng-src="http://api.ssporting.com/cache/small/{{ item.fb_uid}}.jpg"/></a></span> <span
                                                class="font-name name-ranking">{{ item.display_name}}</span> <span
                                                class="score-user name-ranking">{{ item.overall_gp | toFixed }}</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div ng-show="topRankingScoreSelect == '2week'" class="menu-toprank-list">
                    <div class="nano">
                        <div class="contentS">
                            <table>
                                <tbody>
                                    <tr ng-repeat="item in topRanking.top_ranking.2week| limitTo:10">
                                        <td class="ranking-icon"><img ng-src="images/icon/{{ $index + 1}}.png"/></td>
                                        <td>
<!--                                            <span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img-->
<!--                                                        ng-src="http://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></span> <span-->
<!--                                                class="font-name name-ranking">{{ item.display_name}}</span> <span-->
<!--                                                class="score-user name-ranking">{{ item.overall_gp | toFixed }}</span>-->
                                            <span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                                        ng-src="http://api.ssporting.com/cache/small/{{ item.fb_uid}}.jpg"/></a></span> <span
                                                class="font-name name-ranking">{{ item.display_name}}</span> <span
                                                class="score-user name-ranking">{{ item.overall_gp | toFixed }}</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <div class="sidebar-style" ng-show="topRanking.top_sgold.alltime[0]">
                <div class="tab-top-rank-type">
                    <?php echo Utils::trans('Top Sgold') ?>
                </div>
                <div ng-init="topRankingSgoldSelect = 'all'" class="tab-menu-rank threeColumn">
                    <table>

                        <td ng-click="topRankingSgoldSelect = 'all'"
                            ng-class="{'active-rank': topRankingSgoldSelect == 'all'}"><?php echo Utils::trans('All') ?>
                        </td>
                        <td ng-click="topRankingSgoldSelect = 'now'"
                            ng-class="{'active-rank': topRankingSgoldSelect == 'now'}">
                                <?php echo Utils::trans('Now') ?>
                        </td>
                        <td ng-click="topRankingSgoldSelect = '2week'"
                            ng-class="{'active-rank': topRankingSgoldSelect == '2week'}">
                                <?php echo Utils::trans('Last Champ') ?>
                        </td>

                    </table>
                </div>

                <div ng-show="topRankingSgoldSelect == 'all'" class="menu-toprank-list">
                    <div class="nano">
                        <div class="contentS">
                            <table>
                                <tbody>
                                    <tr ng-repeat="item in topRanking.top_sgold.alltime| limitTo:10">
                                        <td class="ranking-icon"><img ng-src="images/icon/{{ $index + 1}}.png"/></td>
                                        <td>
<!--                                            <span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img-->
<!--                                                        ng-src="http://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></span> <span-->
<!--                                                class="font-name name-ranking">{{ item.display_name}}</span> <span-->
<!--                                                class="score-user name-ranking">{{ item.user_sgold | toFixed }}</span>-->
                                            <span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                                        ng-src="http://api.ssporting.com/cache/small/{{ item.fb_uid}}.jpg"/></a></span> <span
                                                class="font-name name-ranking">{{ item.display_name}}</span> <span
                                                class="score-user name-ranking">{{ item.user_sgold | toFixed }}</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div ng-show="topRankingSgoldSelect == 'now'" class="menu-toprank-list">
                    <div class="nano">
                        <div class="contentS">
                            <table>
                                <tbody>
                                    <tr ng-repeat="item in topRanking.top_sgold.now| limitTo:10">
                                        <td class="ranking-icon"><img ng-src="images/icon/{{ $index + 1}}.png"/></td>
                                        <td>
<!--                                            <span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img-->
<!--                                                        ng-src="http://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></span> <span-->
<!--                                                class="font-name name-ranking">{{ item.display_name}}</span> <span-->
<!--                                                class="score-user name-ranking">{{ item.overall_sgold | toFixed }}</span>-->
                                            <span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                                        ng-src="http://api.ssporting.com/cache/small/{{ item.fb_uid}}.jpg"/></a></span> <span
                                                class="font-name name-ranking">{{ item.display_name}}</span> <span
                                                class="score-user name-ranking">{{ item.overall_sgold | toFixed }}</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div ng-show="topRankingSgoldSelect == '2week'" class="menu-toprank-list">
                    <div class="nano">
                        <div class="contentS">
                            <table>
                                <tbody>
                                    <tr ng-repeat="item in topRanking.top_sgold.2week| limitTo:10">
                                        <td class="ranking-icon"><img ng-src="images/icon/{{ $index + 1}}.png"/></td>
                                        <td>
<!--                                            <span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img-->
<!--                                                        ng-src="http://graph.facebook.com/{{ item.fb_uid}}/picture"/></a></span> <span-->
<!--                                                class="font-name name-ranking">{{ item.display_name}}</span> <span-->
<!--                                                class="score-user name-ranking">{{ item.overall_sgold | toFixed }}</span>-->
                                            <span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img
                                                        ng-src="http://api.ssporting.com/cache/small/{{ item.fb_uid}}.jpg"/></a></span> <span
                                                class="font-name name-ranking">{{ item.display_name}}</span> <span
                                                class="score-user name-ranking">{{ item.overall_sgold | toFixed }}</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


        </div>

    </div>
    <!-- end -->

    <div ng-init="topRankingLeagueSelect = '34885'"  class="sidebar-style" id="news-feed-box" style="display:none;">
        <div class="right-menu tab-top-rank-type">
            TOP 5 LEAGUE
        </div>
        <div class="headingTop5">
            <table>
                <tr>
                    <td tooltip="'ไทย พรีเมียร์ลีก'" ng-class="{'activeRankingTop5':isTopRankingLeagueSelect('34885')}" ng-click="setTopRankingLeagueSelect('34885')"><img src="images/five-leagues/tpl.png"></td>
                    <td tooltip="'อังกฤษพรีเมียร์ลีก'" ng-class="{'activeRankingTop5':isTopRankingLeagueSelect('35909')}" ng-click="setTopRankingLeagueSelect('35909')"><img src="images/five-leagues/Englis-Premier-League.png"></td>
                    <td tooltip="'ลาลีกา สเปน'" ng-class="{'activeRankingTop5':isTopRankingLeagueSelect('37120')}" ng-click="setTopRankingLeagueSelect('37120')"><img src="images/five-leagues/La-Liga-Logo-1.png"></td>
                    <td tooltip="'ยูฟ่า แชมเปี้ยนลีก'" ng-class="{'activeRankingTop5':isTopRankingLeagueSelect('35980')}" ng-click="setTopRankingLeagueSelect('35980')"><img src="images/five-leagues/UEFA-Champions-League-1.png"></td>
                    <td tooltip="'ยูฟ่า ยูโรปาลีก'" ng-class="{'activeRankingTop5':isTopRankingLeagueSelect('35998')}" ng-click="setTopRankingLeagueSelect('35998')"><img src="images/five-leagues/UEFA-Europa-League-1.png"></td>
                </tr>
            </table>
        </div>
        <div ng-repeat="lg in highlightLeague" ng-show="isTopRankingLeagueSelect(lg)">
            <div class="menu-toprank-list">
                <div class="nano">
                    <div class="contentS">
                        <table>
                            <tbody>
                                <tr ng-repeat="item in topRanking.league[lg]| limitTo:10">
                                    <td class="ranking-icon"><img ng-src="/images/icon/{{ $index + 1}}.png"/></td>
                                    <td><span class="images-user"><a href="/profile.php?id={{ item.fb_uid}}"><img ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"></a></span>
                                        <span class="font-name name-ranking">{{ item.display_name}}</span>
                                        <span class="score-user name-ranking">{{ item.overall_gp}}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>




    </div>

    <div class="sidebar-style" id="bet-list-box" style="display: none;">
        <div class="tab-top-rank-type right-menu">
            <?php echo Utils::trans('Play Game') ?>
        </div>
        <div class="menu-bets-list">
            <div id="about" class="nano">
                <div class="contentS">
                    <table>
                        <tbody>
                        <!--                    <tr ng-repeat="(key,item) in odds.data" ng-if="getOddsHdp(key, 'hdp')">-->
                        <!--                        <td class="bg-gray"><span class="text-blue">HDP <span-->
                            <!--                                    ng-bind="getOddsHdp(key, 'hdp')"></span></span>-->
                            <!--                            <span class="time-race" ng-bind="getOddsHdp(key, 'kick_off') * 1000 | kickTime"></span>-->
                            <!--                        </td>-->
                            <!--                        <td><a href="/game.php?mid={{ getOddsHdp(key, 'match_id') }}">-->
                            <!--                            <span class="teambet"><img-->
                            <!--                                    ng-src="{{ odds.logos[item.SBOBET.tid1 || item.Bet365.tid1 || item.Ladbrokes.tid1]['32x32'] }}"/> <span-->
                            <!--                                    ng-bind="odds.lang_team1[item.SBOBET.tid1 || item.Bet365.tid1 || item.Ladbrokes.tid1][language]"></span></span>-->
                            <!--                            <span class="teambet"><img-->
                            <!--                                    ng-src="{{ odds.logos[item.SBOBET.tid2 || item.Bet365.tid2 || item.Ladbrokes.tid2]['32x32'] }}"/> <span-->
                            <!--                                    ng-bind="allTeams[item.tid2].name || odds.lang_team2[item.SBOBET.tid2 || item.Bet365.tid2 || item.Ladbrokes.tid2][language]"></span></span>-->
                            <!--                            </a>-->
                            <!--                        </td>-->
                            <!--                        <td><span ng-bind="getOddsHdp(key, 'hdp_home')"></span>-->
                            <!--                            <span ng-bind="getOddsHdp(key, 'hdp_away')"></span>-->
                            <!--                        </td>-->
                            <!--                    </tr>-->

                            <tr ng-repeat="(key,item) in live_match_wait_sorted.hdp">
                                <td class="bg-gray"><span class="text-blue">HDP <span
                                            ng-bind="item.hdp"></span></span>
                                    <span class="time-race" ng-bind="item.date | kickTime"></span>
                                </td>
                                <td><a href="/game.php?mid={{ key}}">
                                        <span class="teambet"><img
                                                ng-src="{{ live_match_wait_sorted.Teamlogos[item.hid]['32x32']}}"/> <span
                                                ng-bind="allTeams[item.hid].name || item.homename""></span></span>
                                        <span class="teambet"><img
                                                ng-src="{{ live_match_wait_sorted.Teamlogos[item.gid]['32x32']}}"/> <span
                                                ng-bind="allTeams[item.gid].name || item.awayname"></span></span>
                                    </a>
                                </td>
                                <td><span ng-bind="item.hdp_home"></span>
                                    <span ng-bind="item.hdp_away"></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div style="margin-bottom: 5px;" class="fb-like-box" onrender="fbLikeRendered()"
         data-href="https://www.facebook.com/9ssporting"
         data-layout="standard"
         data-action="like"
         data-show-faces="true"
         data-share="true"
         data-stream="true"
         data-height="520"
         data-width="216"
         ></div>


</div>



<div style="clear: both;"></div>

<div class="footer">
    <div class="more-footer">
        Ssporting.com All right reserved.
    </div>

    <table>
        <tr>
            <td><a href="/">Home</a></td>
            <td><a href="/">Live Score</a></td>
            <td><a href="/result.php">Score</a></td>
        </tr>
    </table>
</div>



<!-- modal คลิกดูรูปคอมเม้นท์รูปใหญ่-->

<div id="enlarge-image-modal" class="section-fullImage" style="display: none;">
    <div class="modal-backdrop fade in"></div>
    <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="false" style="display: block; border: none;background: none; box-shadow: none;width: 100%; left: 0; top: 5%; right: 0; margin: 0 auto;  border-radius: 5px !important;">
        <!--<div class="box-close"><img src="images/icon/close.png"/></div>-->
        <div class="header-fullImage">
            <!--            <div class="modal-header">
                            <button ng-click="enlargeImageClose()" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">&nbsp;{{ enlargeImageTitle }}</h4>
                        </div>-->
            <div class="closeBox"> <button ng-click="enlargeImageClose()" type="button" class="closes" data-dismiss="modal" aria-hidden="true">X Close</button></div>
            <div style="clear: both;"></div>

            <div class="fullImages">
                <img ng-src="{{ enlargeImageSrc}}">
            </div>


        </div>
    </div>
</div>


<!-- Modal Combo-->
<div id="modal-combo-single" class="Modal-resultGame" style="display: none;">
    <div class="fadeGame"></div>
    <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="false" style="display: block; z-index: 5; width: 471px; border: 0; background: none; box-shadow: none;">
        <div class="bgCombo">
            <div class="titleStep">คอมโบต่อเนื่อง</div>
            <div class="iconDiamond"><img src="images/icon/diamondall.png"></div>
            <div class="detailStepCombos">
                ยินดีด้วยคุณได้รับ <span class="bg">{{ comboToDiamondModal(userInfo.step_combo_count) }}</span> Diamond
                <div class="wrapperStepCombo">
                    <div class="graph-outer2">
                        <div class="inner-left-cap1" ng-if="userInfo.step_combo_count >= 2"></div>
                        <div class="inner-left-bar1" ng-if="userInfo.step_combo_count >= 2"><b>Combo {{userInfo.step_combo_count}}</b></div>
                        <div class="inner-left-cap2" ng-if="userInfo.step_combo_count >= 6"></div>
                        <div class="inner-left-bar1" ng-if="userInfo.step_combo_count >= 6"><b>Combo {{userInfo.step_combo_count}}</b></div>
                        <div class="inner-left-cap3" ng-if="userInfo.step_combo_count >= 8"></div>
                        <div class="inner-left-bar1" ng-if="userInfo.step_combo_count >= 8"><b>Combo {{userInfo.step_combo_count}}</b></div>
                        <div class="inner-left-cap4" ng-if="userInfo.step_combo_count >= 10"></div>
                        <div class="inner-left-bar1" ng-if="userInfo.step_combo_count >= 10"><b>Combo {{userInfo.step_combo_count}}</b></div>
                        <div class="inner-left-cap5" ng-if="userInfo.step_combo_count >= 15"></div>
                        <div class="inner-left-bar1" ng-if="userInfo.step_combo_count >= 15"><b>Combo {{userInfo.step_combo_count}}</b></div>
                        <div class="inner-left-cap6" ng-if="userInfo.step_combo_count >= 28"></div>
                        <div class="inner-left-bar1" ng-if="userInfo.step_combo_count >= 28"><b>Combo {{userInfo.step_combo_count}}</b></div>
                    </div>
                </div>

            </div>
            <div class="comboText"> คอมโบรวม  <b>{{ userInfo.step_combo_count }}</b> คู่ต่อเนื่อง</div>
            <div class="conditions">เงื่อนไข : คุณจะต้องชนะหรือแพ้ทุกคู่ที่เล่นต่อเนื่องกันทุกวันโดยคอมโบจะเริ่มต้นที่ 2 คู่ขึ้นไปสำหรับคอมโบวันแรก คุณสามารถแลกคอมโบเป็น Diamond ได้ทันทีหากต้องการ</div>
            <div class="buttonSend"><a href="exchangeCoin.php"><img src="/images/icon/buttonCombo.png"></a> <img ng-click="closeComboDiamondModal()" src="/images/icon/buttonClose.png"></div>
        </div>
    </div>
</div>
<!-- End-->



<!-- Modal Step-->

<div id="modal-step-single" class="Modal-resultGame" style="display: none;">
    <div class="fadeGame"></div>
    <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="false" style="display: block; z-index: 5; width: 471px; border: 0; background: none; box-shadow: none;">
        <div class="bgStep">
            <div class="titleStep">รางวัลคอมโบประจำวัน</div>
            <div class="iconDiamond"><img src="images/icon/diamondall.png"></div>
            <div class="detailStepCombos">
                ยินดีด้วยคุณได้รับ <span class="bg">{{ todayComboToDiamondModal(userInfo.step_combo_today_count) }}</span> Diamond
                <div class="step" ng-class="{
                    'step3': userInfo.step_combo_today_count == 3,
                    'step4': userInfo.step_combo_today_count == 4,
                    'step5': userInfo.step_combo_today_count == 5,
                    'step6': userInfo.step_combo_today_count == 6,
                    'step7': userInfo.step_combo_today_count == 7,
                    'step8': userInfo.step_combo_today_count >= 8,
                    }"></div>
                <div class="boxTextStep">
                    <table>
                        <tr>
                            <td>ชนะ 3 คู่</td>
                            <td>ชนะ 4 คู่</td>
                            <td>ชนะ 5 คู่</td>
                            <td>ชนะ 6 คู่</td>
                            <td>ชนะ 7 คู่</td>
                            <td>ชนะ 8 คู่</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="comboText"> คุณได้ <b>{{ userInfo.step_combo_today_count }}</b> คู่ต่อเนื่อง</div>
            <div class="conditions">เงื่อนไข : คุณจะต้องชนะทุกคู่ที่เล่นวันนั้น โดยคอมโบจะเริ่มที่ 3 คู่ขึ้นไป</div>
            <div class="buttonSend"><!--<a href="exchangeCoin.php"><img src="/images/icon/buttonCombo.png"></a>--> <img ng-click="closeTodayComboDiamondModal()" src="/images/icon/buttonClose.png"></div>
        </div>
    </div>
</div>

<!-- End-->


<!--    ส่วน modal สรุปผลการเล่นเกมส์-->

<div id="daily-report" class="Modal-resultGame" style="display: none;">
    <div class="fadeGame"></div>
    <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="false" style="display: block; z-index: 5;">

        <div ng-if="!dailyReport.play" class="header-resultGame">
            <div ng-click="closeDailyReport()" class="closed" style="background-color: #fff; color: #333; position: absolute; margin-left: 545px; margin-top: -10px;">X</div>
            <h5>ไม่มีข้อมูลการเล่นของเมื่อวาน</h5>

        </div>

        <div ng-if="dailyReport.play" class="header-resultGame">

            <div ng-click="closeDailyReport()" class="closed" style="background-color: #fff; color: #333; position: absolute; margin-left: 545px; margin-top: -10px;">X</div>

            <div class="resultGames">
                <h4>สรุปผลการเล่นเกมส์</h4>
                <table>
                    <tr>
                        <td><span class="result">{{ dailyReport.play}}</span> Play</td>
                        <td class="scorePta"><span class="result pta">{{ dailyReport.pta}}%</span> PTA</td>
                        <td>
                            <div class="wrapScore">
                                <div class="tscore" ng-class="{'tlose': dailyReport.score < 0}"><b><span ng-if="dailyReport.score >= 0">+</span>{{ dailyReport.score}}</b>S GOLD</div>
                            </div>
                        </td>
                        <td><span class="result twin">{{ dailyReport.win}}</span> Win</td>
                        <td><span class="result tdraw">{{ dailyReport.draw}}</span> Draw</td>
                        <td><span class="result tlose">{{ dailyReport.lose}}</span> Lose</td>
                    </tr>
                </table>
                <div class="tab">
                    <div class="tab1">{{ dailyReport.playdate}}</div>
                    <div class="tab2">Rating : {{ dailyReport.rating.toFixed(2)}}%  Ranking :  {{ dailyReport.rank}}</div>
                    <div style="clear: both;"></div>
                </div>
            </div>
        </div>

        <!--ส่วนแสดง Step , Combo-->
        <div ng-if="dailyReport.play" class="boxStep" style="display:none;">
            <table>
                <tr>
                    <td>Step : </td>
                    <td class="step" ng-class="{
                    'step3': userInfo.step_combo_today_count == 3,
                    'step4': userInfo.step_combo_today_count == 4,
                    'step5': userInfo.step_combo_today_count == 5,
                    'step6': userInfo.step_combo_today_count == 6,
                    'step7': userInfo.step_combo_today_count == 7,
                    'step8': userInfo.step_combo_today_count >= 8,
                    }"></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div class="boxTextStep">
                            <table>
                                <tr>
                                    <td style="text-align: left;">Step 3</td>
                                    <td>Step 4</td>
                                    <td>Step 5</td>
                                    <td>Step 6</td>
                                    <td>Step 7</td>
                                    <td style="text-align: right;">Step 8</td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Combo : </td>
                    <td>
                        <div class="graph-outer1">
                            <div class="inner-left-cap1" ng-if="userInfo.step_combo_count >= 2"></div>
                            <div class="inner-left-bar1" ng-if="userInfo.step_combo_count >= 2"><b>Combo 2</b></div>
                            <div class="inner-left-cap2" ng-if="userInfo.step_combo_count >= 6"></div>
                            <div class="inner-left-bar1" ng-if="userInfo.step_combo_count >= 6"><b>Combo 6</b></div>
                            <div class="inner-left-cap3" ng-if="userInfo.step_combo_count >= 8"></div>
                            <div class="inner-left-bar1" ng-if="userInfo.step_combo_count >= 8"><b>Combo 8</b></div>
                            <div class="inner-left-cap4" ng-if="userInfo.step_combo_count >= 10"></div>
                            <div class="inner-left-bar1" ng-if="userInfo.step_combo_count >= 10"><b>Combo 10</b></div>
                            <div class="inner-left-cap5" ng-if="userInfo.step_combo_count >= 15"></div>
                            <div class="inner-left-bar1" ng-if="userInfo.step_combo_count >= 15"><b>Combo 15</b></div>
                            <div class="inner-left-cap6" ng-if="userInfo.step_combo_count >= 28"></div>
                            <div class="inner-left-bar1" ng-if="userInfo.step_combo_count >= 28"><b>Combo 28</b></div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <!--        end-->

        <div ng-if="dailyReport.play" class="tabUtility">
            <div class="tab1 text-purple">สรุปคู่ที่เล่นวันนี้</div>
            <div class="tab2"><a href="profile.php?play"><span class="seeMore">ดูทั้งหมด</span></a></div>
            <div style="clear: both;"></div>
        </div>
        <div ng-if="dailyReport.play" class="tableResult-game">
            <table>
                <tr ng-repeat="item in dailyReport.betlist">
                    <td ng-class="{'team-played-na': item.choose == 'home'}"><span ng-class="{'team-favored':item.betHdp < 0}">{{ item.homeName<?php echo ucfirst(__LANGUAGE__); ?>}} ({{ item.oddsTimestamp | parseOdds:'home' }})</span></td>
                    <td>{{ item.oddsTimestamp | parseOdds:'hdp' }}</td>
                    <td ng-class="{'team-played-na': item.choose == 'away'}"><span ng-class="{'team-favored':item.betHdp > 0}">{{ item.awayName<?php echo ucfirst(__LANGUAGE__); ?>}} ({{ item.oddsTimestamp | parseOdds:'away' }})</span></td>
                    <td class="scored">
                        <span ng-if="item.sid >= 5">{{ item.FT_score}}</span>
                        <span ng-if="item.sid < 5">{{ item.kickOff | kickTime }}</span>
                    </td>
                    <td><span ng-if="item.betAmount >= 0">+</span>{{ item.betAmount}}</td>
                    <td>
                        <img ng-if="item.betResult === 'win'" src="images/icon/win.png">
                        <img ng-if="item.betResult === 'draw'" src="images/icon/draw.png">
                        <img ng-if="item.betResult === 'lose'" src="images/icon/lose.png">
                        <img ng-if="item.betResult === 'win_half'" src="images/icon/win_half.png">
                        <img ng-if="item.betResult === 'lose_half'" src="images/icon/lose_half.png">
                    </td>
                </tr>
<!--                <tr>-->
<!--                    <td>Honduras (2.06)</td>-->
<!--                    <td>0.75</td>-->
<!--                    <td><span class="team-favored">Equador (1.88)</span></td>-->
<!--                    <td class="scored">1 - 2</td>-->
<!--                    <td>+7.05</td>-->
<!--                    <td><img src="images/icon/win.png"></td>-->
                <!--                </tr>-->
                <!--                <tr>-->
                <!--                    <td>Honduras (2.06)</td>-->
                <!--                    <td>0.75</td>-->
                <!--                    <td><span class="team-favored">Equador (1.88)</span></td>-->
                <!--                    <td class="scored">1 - 2</td>-->
                <!--                    <td>+7.05</td>-->
                <!--                    <td><img src="images/icon/win.png"></td>-->
                <!--                </tr>-->
                <!--                <tr>-->
                <!--                    <td>Honduras (2.06)</td>-->
                <!--                    <td>0.75</td>-->
                <!--                    <td><span class="team-favored">Equador (1.88)</span></td>-->
                <!--                    <td class="scored">1 - 2</td>-->
                <!--                    <td>+7.05</td>-->
                <!--                    <td><img src="images/icon/win.png"></td>-->
                <!--                </tr>-->
                <!--                <tr>-->
                <!--                    <td>Honduras (2.06)</td>-->
                <!--                    <td>0.75</td>-->
                <!--                    <td><span class="team-favored">Equador (1.88)</span></td>-->
                <!--                    <td class="scored">1 - 2</td>-->
                <!--                    <td>+7.05</td>-->
                <!--                    <td><img src="images/icon/win.png"></td>-->
                <!--                </tr>-->
            </table>
        </div>
    </div>
</div>

<!--End-->


<!--modal daily coins-->
<div id="daily-bonus" class="Modal-resultGame" style="display: none;">
    <div class="fadeGame"></div>
    <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="false" style="display: block; z-index: 5; top: 200px; width: 700px; left:0; right: 0; margin:0 auto; border-radius: 5px;">

        <div class="dailyCoins">
            <div ng-click="closeDailyBonus()" class="closed" style="background-color: #fff; color: #333; position: absolute; margin-left: 685px; margin-top: -16px;">X</div>
            <table>
<!--                 <tr>-->
<!--                    <td><img ng-src="images/daily-coins/bure-1.png"></td>-->
<!--                    <td>-->
<!--                        <img ng-if="dailyBonus.login.day >= 2" src="images/daily-coins/bure-2.png">-->
<!--                        <img ng-if="dailyBonus.login.day <= 2" src="images/daily-coins/w-2.png">-->
                <!--                    </td>-->
                <!--                    <td>-->
                <!--                        <img ng-if="dailyBonus.login.day >= 3" src="images/daily-coins/bure-3.png">-->
                <!--                        <img ng-if="dailyBonus.login.day < 3" src="images/daily-coins/w-3.png">-->
                <!--                    </td>-->
                <!--                    <td>-->
                <!--                        <img ng-if="dailyBonus.login.day >= 4" src="images/daily-coins/bure-4.png">-->
                <!--                        <img ng-if="dailyBonus.login.day < 4" src="images/daily-coins/w-4.png">-->
                <!--                    </td>-->
                <!--                    <td>-->
                <!--                        <img ng-if="dailyBonus.login.day >= 5" src="images/daily-coins/bure-5.png">-->
                <!--                        <img ng-if="dailyBonus.login.day < 5" src="images/daily-coins/w-5.png">-->
                <!--                    </td>-->
                <!--                    <td>-->
                <!--                        <img ng-if="dailyBonus.login.day >= 6" src="images/daily-coins/bure-6.png">-->
                <!--                        <img ng-if="dailyBonus.login.day < 6" src="images/daily-coins/w-6.png">-->
                <!--                    </td>-->
                <!--                    <td>-->
                <!--                        <img ng-if="dailyBonus.login.day >= 7" src="images/daily-coins/bure-7.png">-->
                <!--                        <img ng-if="dailyBonus.login.day < 7" src="images/daily-coins/w-7.png">-->
                <!--                    </td>-->
                <!--                </tr>-->

                <!--                ส่วนได้รับเงินจากการเข้าประจำวัน แบบใหม่-->
                <tr style="display: snone;">
                    <td>
                        <div ng-class="{'currentActiveDay': dailyBonus.login.day === 1}" class="boxDailyCoins activeDay">
                            <table>
                                <tr>
                                    <td><b>วันที่ 1</b></td>
                                </tr>
                                <tr>
                                    <td><img src="/images/icon/scoin40.png" style="width: 40px;"></td>
                                </tr>
                                <tr>
                                    <td>Scoin</td>
                                </tr>
                                <tr>
                                    <td class="boxCoins"><b>${{ systemConfig.login_scoin[0]}}</b></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td>
                        <div ng-class="{'activeDay': dailyBonus.login.day >= 2, 'currentActiveDay': dailyBonus.login.day === 2}" class="boxDailyCoins">
                            <table>
                                <tr>
                                    <td><b>วันที่ 2</b></td>
                                </tr>
                                <tr>
                                    <td><img src="/images/icon/scoin40.png" style="width: 40px;"></td>
                                </tr>
                                <tr>
                                    <td>Scoin</td>
                                </tr>
                                <tr>
                                    <td class="boxCoins"><b>${{ systemConfig.login_scoin[1]}}</b></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td>
                        <div ng-class="{'activeDay': dailyBonus.login.day >= 3, 'currentActiveDay': dailyBonus.login.day === 3}" class="boxDailyCoins">
                            <table>
                                <tr>
                                    <td><b>วันที่ 3</b></td>
                                </tr>
                                <tr>
                                    <td><img src="/images/icon/scoin40.png" style="width: 40px;"></td>
                                </tr>
                                <tr>
                                    <td>Scoin</td>
                                </tr>
                                <tr>
                                    <td class="boxCoins"><b>${{ systemConfig.login_scoin[2]}}</b></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td>
                        <div ng-class="{'activeDay': dailyBonus.login.day >= 4, 'currentActiveDay': dailyBonus.login.day === 4}" class="boxDailyCoins">
                            <table>
                                <tr>
                                    <td><b>วันที่ 4</b></td>
                                </tr>
                                <tr>
                                    <td><img src="/images/icon/scoin40.png" style="width: 40px;"></td>
                                </tr>
                                <tr>
                                    <td>Scoin</td>
                                </tr>
                                <tr>
                                    <td class="boxCoins"><b>${{ systemConfig.login_scoin[3]}}</b></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td>
                        <div ng-class="{'activeDay': dailyBonus.login.day >= 5, 'currentActiveDay': dailyBonus.login.day === 5}" class="boxDailyCoins">
                            <table>
                                <tr>
                                    <td><b>วันที่ 5</b></td>
                                </tr>
                                <tr>
                                    <td><img src="/images/icon/scoin40.png" style="width: 40px;"></td>
                                </tr>
                                <tr>
                                    <td>Scoin</td>
                                </tr>
                                <tr>
                                    <td class="boxCoins"><b>${{ systemConfig.login_scoin[4]}}</b></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td>
                        <div ng-class="{'activeDay': dailyBonus.login.day >= 6, 'currentActiveDay': dailyBonus.login.day === 6}" class="boxDailyCoins">
                            <table>
                                <tr>
                                    <td><b>วันที่ 6</b></td>
                                </tr>
                                <tr>
                                    <td><img src="/images/icon/scoin40.png" style="width: 40px;"></td>
                                </tr>
                                <tr>
                                    <td>Scoin</td>
                                </tr>
                                <tr>
                                    <td class="boxCoins"><b>${{ systemConfig.login_scoin[5]}}</b></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td>
                        <div ng-class="{'activeDay': dailyBonus.login.day >= 7, 'currentActiveDay': dailyBonus.login.day === 7}" class="boxDailyCoins">
                            <table>
                                <tr>
                                    <td><b>วันที่ 7</b></td>
                                </tr>
                                <tr>
                                    <td><img src="/images/icon/scoin40.png" style="width: 40px;"></td>
                                </tr>
                                <tr>
                                    <td>Scoin</td>
                                </tr>
                                <tr>
                                    <td class="boxCoins"><b>${{ systemConfig.login_scoin[6]}}</b></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>

                <!--               end-->
            </table>
            <div ng-click="closeDailyBonus()" class="reciveCoins"><img src="/images/daily-coins/login-button.png"></div>
            <div class="labelText">รับทันที <img src="images/icon/sgold30.png"> SGOLD <img src="images/icon/scoin30.png"> SCOIN เมื่อเข้าเว็บต่อเนื่อง</div>
        </div>

    </div>
</div>





<!-- modal แสดง 10 อันดับผู้ทำคะแนนสูงสุดใน 14 วัน-->

<div id="two-week-report" class="section-video" style="display: none;">
    <div class="modal-backdrop fade in"></div>
    <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="false" style="display: block;">
        <div class="box-close" ng-click="close2weekReport()"><img src="images/icon/close.png"/></div>
        <div class="header-video">
            <div class="logos"><img src="images/icon/logo.png"/></div>
            <div class="heading-modal">10 อันดับผู้ทำคะแนนสูงสุดใน 14 วัน</div>
            <div class="textAll-ranking"><a href="/profile.php?ranking">Ranking ทั้งหมด</a></div>
            <div style="clear: both;"></div>
        </div>
        <div class="boxTopRanking">
            <div class="boxMenuTrophy">
                <table>
                    <tr ng-class="{'activeTrophy': twoWeekTab === 'all'}" ng-click="setTwoWeekTab('all')">
                        <td><img src="images/icon-profile/all-ranking.png"><br>Top Ranking<br><b>All</b></td>
                    </tr>
                    <tr ng-class="{'activeTrophy': twoWeekTab === 'now'}" ng-click="setTwoWeekTab('now')">
                        <td><img src="images/icon-profile/2week.png"><br> Top Ranking<br><b>Now</b></td>
                    </tr>
<!--                    <tr ng-class="{'activeTrophy': twoWeekTab === '15days'}" ng-click="setTwoWeekTab('15days')">
                        <td><img src="images/icon-profile/2week.png"><br> 15 Day</td>
                    </tr>-->
                    <tr ng-class="{'activeTrophy': twoWeekTab === 'alltime'}" ng-click="setTwoWeekTab('alltime')">
                        <td><img src="images/icon-profile/rating-100.png"><br>  Top Sgold<br><b>All</b></td>
                    </tr>
                    <tr ng-class="{'activeTrophy': twoWeekTab === 'sgoldnow'}" ng-click="setTwoWeekTab('sgoldnow')">
                        <td><img src="images/icon-profile/rating-200.png"><br> Top Sgold<br><b>Now</b></td>
                    </tr>
<!--                    <tr ng-class="{'activeTrophy': twoWeekTab === 'sgold15days'}" ng-click="setTwoWeekTab('sgold15days')">
                        <td><img src="images/icon/sglod15.png"><br> Sgold 15 Day</td>
                    </tr>-->
                </table>
            </div>
            <div class="detailTrophy" ng-show="twoWeekTab === 'all'">
                <h6>Top Ranking (Now)</h6>
                <table>
                    <tr ng-repeat="item in topRanking.top_ranking.alltime">
                        <td><img ng-src="images/icon/{{ $index + 1 }}.png"></td>
                        <td class="imgProfile"><img ng-src="https://graph.facebook.com/{{ item.fb_uid }}/picture"></td>
                        <td>{{ item.display_name }}</td>
                        <td class="trophyBox"><img src="images/icon-profile/3001.png"></td>
                        <td>{{ item.real_gp }}</td>
                    </tr>
                </table>
            </div>
            <div class="detailTrophy" ng-show="twoWeekTab === 'now'">
                <h6>Top Ranking (Now)</h6>
                <table>
                    <tr ng-repeat="item in topRanking.top_ranking.now">
                        <td><img ng-src="images/icon/{{ $index + 1 }}.png"></td>
                        <td class="imgProfile"><img ng-src="https://graph.facebook.com/{{ item.fb_uid }}/picture"></td>
                        <td>{{ item.display_name }}</td>
                        <td class="trophyBox"><img src="images/icon-profile/3001.png"></td>
                        <td>{{ item.overall_gp }}</td>
                    </tr>
                </table>
            </div>
            <div class="detailTrophy" ng-show="twoWeekTab === '15days'">
                <h6>Top Ranking (Now)</h6>
                <table>
                    <tr ng-repeat="item in topRanking.top_ranking.2week">
                        <td><img ng-src="images/icon/{{ $index + 1 }}.png"></td>
                        <td class="imgProfile"><img ng-src="https://graph.facebook.com/{{ item.fb_uid }}/picture"></td>
                        <td>{{ item.display_name }}</td>
                        <td class="trophyBox"><img src="images/icon-profile/3001.png"></td>
                        <td>{{ item.overall_gp }}</td>
                    </tr>
                </table>
            </div>
            <div class="detailTrophy" ng-show="twoWeekTab === 'alltime'">
                <h6>Top Ranking (Now)</h6>
                <table>
                    <tr ng-repeat="item in topRanking.top_sgold.alltime">
                        <td><img ng-src="images/icon/{{ $index + 1 }}.png"></td>
                        <td class="imgProfile"><img ng-src="https://graph.facebook.com/{{ item.fb_uid }}/picture"></td>
                        <td>{{ item.display_name }}</td>
                        <td class="trophyBox"><img src="images/icon-profile/3001.png"></td>
                        <td>{{ item.user_sgold }}</td>
                    </tr>
                </table>
            </div>
            <div class="detailTrophy" ng-show="twoWeekTab === 'sgoldnow'">
                <h6>Top Ranking (Now)</h6>
                <table>
                    <tr ng-repeat="item in topRanking.top_sgold.now">
                        <td><img ng-src="images/icon/{{ $index + 1 }}.png"></td>
                        <td class="imgProfile"><img ng-src="https://graph.facebook.com/{{ item.fb_uid }}/picture"></td>
                        <td>{{ item.display_name }}</td>
                        <td class="trophyBox"><img src="images/icon-profile/3001.png"></td>
                        <td>{{ item.overall_sgold }}</td>
                    </tr>
                </table>
            </div>
            <div class="detailTrophy" ng-show="twoWeekTab === 'sgold15days'">
                <h6>Top Ranking (Now)</h6>
                <table>
                    <tr ng-repeat="item in topRanking.top_sgold.2week">
                        <td><img ng-src="images/icon/{{ $index + 1 }}.png"></td>
                        <td class="imgProfile"><img ng-src="https://graph.facebook.com/{{ item.fb_uid }}/picture"></td>
                        <td>{{ item.display_name }}</td>
                        <td class="trophyBox"><img src="images/icon-profile/3001.png"></td>
                        <td>{{ item.overall_sgold }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>



<!-- modal Alert ประกาศปิดปรับปรุงระบบชั่วคราว -->

<div id="ma-dialog" class="section-video" style="display: none;">
    <div class="modal-backdrop fade in"></div>
    <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="false" style="display: block;">
        <div ng-click="closeMaDialog()" class="box-close"><img src="images/icon/close.png"/></div>
        <div class="header-video">
            <div class="logos"><img src="images/icon/logo.png"/></div>
            <div class="heading-modal">&nbsp;</div>
            <div style="clear: both;"></div>
        </div>
        <div class="AlertMaintenance">

            <h2>ประกาศปิดปรับปรุงระบบชั่วคราว</h2>
            <img src="images/maintenance.png" style="width: 100px;">
            <h5>ขออภัยในความไม่สะดวก เนื่องจากทางเราจะปิดปรับปรุงระบบตั้งแต่เวลา</h5>
            <div class="timeClose">
                <b style="color: #008ee2;">06:00 น.</b>  ถึงเวลา <b style="color: #008ee2;">15:00 น.</b> 
                <div style="font-size: 22px;">ของวันที่ <b>21 กรกฏาคม 2557</b> (GMT +7)</div>
            </div>
            <div class="text">ขอบคุณทุกท่านที่เข้าใช้งาน Scorspot.com</div>

        </div>
    </div>
</div>




<!--modal รายงานกระทุ้ไม่เหมาะสม-->

<div id="report-dialog-modal" class="alertReport" style="display: none;">
    <div class="modal-backdrop fade in"></div>
    <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="false" style="display: block;">
        <div class="modal-content">
            <div class="modal-header">
                <button ng-click="closeReportDialog()" type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">รายงานกระทู้ไม่เหมาะสม</h4>
            </div>
            <div class="modal-body">
                คุณต้องการรายงานว่ากระทู้นี้ไม่เหมาะสมหรือไม่ ?
            </div>
            <div class="modal-footer">
                <button ng-click="closeReportDialog()" type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button ng-click="sendReport()" type="button" class="btn btn-primary">Yes</button>
            </div>
        </div>
    </div>
</div>


<!--Modal ส่ง PM-->
<script type="text/ng-template" id="customTemplate.html">
    <a>
    <img ng-src="https://graph.facebook.com/{{match.model.fb_uid}}/picture" width="16">
    <span bind-html-unsafe="match.label | typeaheadHighlight:query"></span>
    </a>
</script>

<div id="pm-dialog-modal" class="section-video" style="display: none;">
    <div class="modal-backdrop fade in"></div>
    <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="false" style="display: block;">
        <div class="modal-content">
            <div class="modal-header">
                <button ng-click="closePmDialog()" type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ข้อความใหม่</h4>
            </div>
            <div class="modal-body">
                <div class="boxMessage">
                    <table>
                        <tr>
                            <td>
                                <input id="pmUserSelectedBox" class="span6" type="text" ng-model="pmToUserSelected" placeholder="ค้นหารายชื่อ"
                                       typeahead="user as user.display_name for user in getUserByFilter($viewValue)"
                                       typeahead-template-url="customTemplate.html"
                                       typeahead-loading="loadingLocations"
                                       typeahead-min-length="3"
                                       typeahead-wait-ms="300"
                                       typeahead-on-select="selectUserToSend($item, $model, $label); pmToUserSelected='';"
                                       class="form-control">
                                <i ng-show="loadingLocations" class="glyphicon glyphicon-refresh"></i>
                                <div ng-repeat="item in usersToSend"  class="wrapperBoxUserSend">
                                    <div ng-click="userToSendRemove(item)" class="boxUserSend">
                                        <div class="boxImages"><img ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"></div>
                                        <div class="boxUsername">{{ item.display_name}}</div>
                                    </div>
                                    <div style="clear: both;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td> <textarea id="pm-message-box" ng-model="pmMessage" placeholder="เขียนข้อความ..."></textarea></td>
                        </tr>
                    </table>
                </div>
                <div class="sectionAttrachment" style="display: snone;">
                    <div class="image-post-to-wall" ng-repeat="item in pmImage.thumbnails">
                        <span class="delete-img-postwall" ng-click="pmRemovePicture($index)">x</span>
                        <img ng-src="{{ item}}">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <img ng-click="pmBrowsePicture()" src="images/icon/attrach.png" style="width: 32px; float: left; cursor: pointer;" title="แนบรูปภาพ">
                <button ng-click="closePmDialog()" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!--                <button ng-click="sendPm(pmMessage, pmReceiverUID)" type="button" class="btn btn-primary">Send</button>-->
                <button ng-click="sendPmTo(pmMessage)" type="button" class="btn btn-primary">Send</button>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input id="pm-upload-browse" type="file" multiple ng-file-select="pmUploadPicture($files)">
    <button ng-click="upload.abort()">Cancel Upload</button>
</div>

<div ng-include="dialog"></div>
<div id="app-config"
     language="<?php echo __LANGUAGE__; ?>"
     gmtOffset="<?php echo __GMT_OFFSET__; ?>"
     >
</div>

<div id="boxMessage" ng-show="(!statusMessage)&&(serverStatus.news=='Y' || serverStatus.server_online=='N')" style="display: none;position: fixed;top: 0px;right: 0px;bottom: 0px;left: 0px;z-index: 1050;">
    <div style="border-radius: 10px;border: solid 5px #002a80;margin: 0px auto;width: 1000px;position: relative;height: 97%;background-color: #FFFFFF;opacity: 1;">
        <div style="text-align: center; width: 100%;font-weight: bolder;font-size: 24px;padding: 2%;">
            {{serverStatus.title}}
        </div>
        <div style="background-image: url('/images/bg_statusServer7.png');background-repeat: no-repeat;height: 80%;width: 100%;border-top: solid 2px#cccccc;border-bottom: solid 2px#cccccc;font-size: 16px;">
            <div style="line-height: 160%;padding: 20px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{serverStatus.message}}</div>
        </div>
        <div ng-if="serverStatus.news=='Y' && serverStatus.server_online=='Y' " style="text-align:center; width: 100%;font-weight: bolder;font-size: 20px;padding: 1%;">
           <button ng-click="changeStatusMessage()" style="font-size:18px;background-color:#444444;padding:5px;border:solid 1px #cccccc;width: 20%;color: #FFFFFF;border-radius: 5px;" class="" >ตกลง</button>
        </div>
    </div>
</div>
<div id="BgBoxMessage" ng-show="(!statusMessage)&&(serverStatus.news=='Y' || serverStatus.server_online=='N')" style="display: none;position: fixed;top: 0px;right: 0px;bottom: 0px;left: 0px;z-index: 1040;background-color: #000;opacity: 0.9;"></div>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID -->

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-45968275-1', 'auto');
    ga('send', 'pageview');

</script>


<script src="scripts/vendors/jquery/jquery-1.11.0.min.js"></script>
<script src="scripts/vendors/purl/purl.js"></script>
<script src="scripts/vendors/bootstrap/bootstrap.min.js"></script>
<script src="scripts/vendors/momentjs/moment.min.js"></script>
<script src="scripts/vendors/momentjs/countdown.min.js"></script>
<script src="scripts/vendors/momentjs/moment-countdown.min.js"></script>
<script src="scripts/vendors/underscore/underscore-min.js"></script>
<script src="scripts/vendors/angularjs/angular-file-upload-shim.min.js"></script>
<script src="scripts/vendors/angularjs/angular.min.js"></script>
<script src="scripts/vendors/angularjs/angular-file-upload.min.js"></script>
<script src="scripts/vendors/angularjs/angular-route.min.js"></script>
<script src="scripts/vendors/angularjs/angular-cookies.min.js"></script>
<script src="scripts/vendors/angularjs/angular-easyfb.min.js"></script>
<script src="scripts/vendors/angularjs/angular-embedplayer.js"></script>
<script src="scripts/vendors/angularjs/angular.audio.js"></script>
<script src="scripts/vendors/angularjs/naturalSortVersion.min.js"></script>
<script src="scripts/vendors/angularjs/ui-bootstrap-tpls-0.11.0.min.js"></script>
<script src="scripts/vendors/angularjs/angular-local-storage.min.js"></script>
<script src="scripts/___.js"></script>
<?php echo $footerScript; ?>

<?php require_once(__DOCS_ROOT__ . '/includes/checkGmtOffsetSaved.php'); ?>

<script>

            $('.btn-change-lang-to').click(function(e){
    e.preventDefault();
            var lang = $(e.currentTarget).attr('data-lang');
            $.ajax({
            method: 'get',
                    url: 'settings.php?item=saveLanguage&language=' + lang,
                    success: function (response) {
                    window.location.reload();
                    }
            });
    });
            $('.btn-change-gmt-offset-to').click(function(e){
    e.preventDefault();
            var offset = $(e.currentTarget).attr('data-offset');
            $.ajax({
            method: 'get',
                    url: 'settings.php?item=saveGmtOffset&offset=' + offset,
                    success: function (response) {
                    window.location.reload();
                    }
            });
    });
            $('#gmt-box-btn').click(function(e){
    e.preventDefault();
            if ($('#gmt-box').is(':not(:visible)')) {
    $('#gmt-box').fadeIn();
    } else {
    $('#gmt-box').fadeOut();
    }
    });

</script>
</body>
</html>