<?php if (!__GMT_OFFSET__): ?>
    <script>
        var date = new Date();
        $.ajax({
            type: 'GET',
            url: 'settings.php',
            data: 'item=saveGmtOffset&offset=' + ((-date.getTimezoneOffset() / 60)),
            success: function (response) {
                location.reload();
            }
        });
    </script>
<?php endif; ?>