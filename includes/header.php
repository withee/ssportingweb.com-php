<?php
$scriptName = explode('/', $_SERVER['SCRIPT_NAME']);
$scriptName = end($scriptName);
$service_competition = Services::getCompetition();
?>
<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" ng-app="scorspot.comApp"> <!--<![endif]-->
    <head>
<!--        <style>-->
<!--            img {-->
<!--                -webkit-filter: grayscale(100%);-->
<!--                filter: grayscale(100%);-->
<!--            }-->
<!--            html {-->
<!--                -webkit-filter: grayscale(100%);-->
<!--            }-->
<!--        </style>-->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $title; ?></title>
        <meta name="viewport" content="width=device-width">
        <?php echo $meta; ?>
        <base href="/">
        <link rel="stylesheet" type="text/css" href="styles/reset.css"/>
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <!-- build:css(.tmp) styles/main.css -->

        <link rel="stylesheet" type="text/css" href="styles/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="styles/style.css"/>
        <link rel="stylesheet" type="text/css" href="styles/custom.css"/>
        <link rel="stylesheet" type="text/css" href="styles/follow.css"/>
        <link rel="stylesheet" type="text/css" href="styles/new-feed.css"/>
        <link rel="stylesheet" type="text/css" href="styles/statistics.css"/>
        <link rel="stylesheet" type="text/css" href="styles/match-result.css"/>
        <link rel="stylesheet" type="text/css" href="styles/bet.css"/>
        <link rel="stylesheet" type="text/css" href="styles/team-favorite.css"/>
        <link rel="stylesheet" type="text/css" href="styles/team-favorite-stat.css"/>
        <link rel="stylesheet" type="text/css" href="styles/ranking.css"/>
        <link rel="stylesheet" type="text/css" href="styles/bet-page.css"/>
        <link rel="stylesheet" type="text/css" href="styles/nanoscrollbar.css"/>
        <link rel="stylesheet" type="text/css" href="styles/expand-feed.css"/>
        <link rel="stylesheet" type="text/css" href="styles/league-level.css"/>
        <link rel="stylesheet" type="text/css" href="styles/worldcup-match.css" />
        <link rel="stylesheet" type="text/css" href="styles/forum.css" />
        <link rel="stylesheet" type="text/css" href="styles/mini-games.css" />
        <!-- endbuild -->

        <meta property="og:image" content="https://ssporting.com/images/1200x627.png"/>
        <meta property="og:image" content="https://ssporting.com/images/560x292.png"/>
        <meta property="og:image" content="https://ssporting.com/images/manu-swan-1.png"/>
        <?php echo $headerScript; ?>
    </head>
    <body ng-app ng-controller="mainCtrl">
        <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser</p>
        <![endif]-->

        <!--[if lt IE 9]>
        <script src="bower_components/es5-shim/es5-shim.js"></script>
        <script src="bower_components/json3/lib/json3.min.js"></script>
        <![endif]-->

        <div class="navbar navbar-fixed-top nav">

            <div class="nav-topbar pop-wrapper">

                <div class="wrapper-menu-top">
<!--                    <a href="/" class="brand logo"><img src="images/icon/logo.png"/></a>-->

                    <div class="section-tabmenu-top">
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <span><?php echo Utils::trans('Choose your language') ?></span> :
                                        <div class="btn-group">
                                            <!--                                                                                        <button class="btn btn-mini btn-inverse dropdown-toggle" data-toggle="dropdown">
                                            <?php echo Utils::trans('__label__'); ?>
                                                                                                                                        <span class="caret"></span>
                                                                                                                                    </button>-->
                                            <div class="chooseLanguage">
                                                <a class="btn-change-lang-to" data-lang="th" href= "lang/th"><input type="radio"<?php if (__LANGUAGE__ === 'th') echo ' checked'; ?>> <img src="images/icon/thai-flag.jpg"/></a>
                                                <a class="btn-change-lang-to" data-lang="en" href= "lang/en"><input type="radio"<?php if (__LANGUAGE__ === 'en') echo ' checked'; ?>> <img src="images/icon/english-flag.gif"/></a>
                                            </div>
                                            <ul class="dropdown-menu list-menu-top" style="color: #000;">
                                                <li><a class="btn-change-lang-to" data-lang="en" href="lang/en"><img src="images/icon/english-flag.gif"/> English</a></li>
                                                <li><a class="btn-change-lang-to" data-lang="th" href="lang/th"><img src="images/icon/thai-flag.jpg"/> ภาษาไทย</a></li>
                                            </ul>
                                        </div>
                                        <!--<span style="  right: 0; z-index: 10; top: 100px"><iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2F9ssporting&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px; margin-top: 3px; margin-left: 5px; position:absolute;" allowTransparency="true"></iframe></span>-->
                                    </td>
                                    <td style=" width: 50%; text-align: center;">
                                        <div class="boxLogo"><a href="/"><img src="images/logo-ssporting.png"></a></div>
                                    </td>
                                    <td>
                                        <div id="notifications-button" style="display:none;" ng-click="toggleNotifications()">
                                            <div class="iconNoti activeNotificate"></div>
                                            <div class="boxNoti">
                                                <span ng-if="notifications.unseen < 100" class="boxNotifi" ng-bind="notifications.unseen || 0"></span>
                                                <span ng-if="notifications.unseen >= 100" class="boxNotifi">99+</span>
                                            </div>
                                        </div>
                                        <div class="boxMessageNoti" ng-click="togglePmNotifications()">
                                            <div class="iconPM" ng-class="{'activeNotificate':pms.unseen > 0}"></div>
                                            <div class="boxNoti messageNoti">
                                                <span class="boxNotifi" ng-bind="pms.unseen || 0"></span>
                                            </div>
                                        </div>

                                    </td>
                                    <td>

                                        <div class="btn-group pop-wrapper" style=" margin-right: -2px; ">
                                            <img ng-show="loginStatus.status == 'unknown' || loginStatus.status == 'not_authorized'"
                                                 ng-click="login();"
                                                 src="images/icon/login-face180.png" style="display: snone; margin-top: 2px; position: relative; z-index: 5; margin-left: -1px;"/>
                                            <button id="gmt-box-btn" class="btn btn-mini btn-inverse dropdown-toggle">
                                                <?php echo Utils::getCurrentGmtOffset(); ?> <span class="caret"></span>
                                            </button>
                                            <ul id="gmt-box" class="dropdown-menu list-menu-top" style="color: #000;">
                                                <?php foreach (Utils::$availableGmtOffset as $val): ?>
                                                    <li><a class="btn-change-gmt-offset-to" data-offset="<?php echo $val['offset']; ?>" href="gmt/<?php echo $val['offset']; ?>"><?php echo $val['label'] ?></a>
                                                    </li>
                                                <?php endforeach; ?>

                                            </ul>
                                        </div>

                                        <button ng-if="facebookInfo.id" ng-click="logout()" class="btn btn-mini btn-inverse" style="position: relative; z-index: 5; border-radius: 0 4px 4px 0 ">Log Out</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="tab-apps">
                            <!--                    <ul>-->
                            <!--                        <li class="facebook-icon"><a href="http://apps.facebook.com/scorspot/"><img-->
                            <!--                                    src="images/icon/facebook-icon.png"/></a></li>-->
                            <!--                        <li class="android-icon"><a-->
                            <!--                                href="https://play.google.com/store/apps/details?id=com.app.livescore"-->
                            <!--                               ><img src="images/icon/android-icon.png"/></a></li>-->
                            <!--                        <li class="apple-icon"><a href=""><img src="images/icon/apple-icon.png"/></a></li>-->
                            <!--                        <li class="window-icon"><a href=""><img src="images/icon/window-icon.png"/></a></li>-->
                            <!--                    </ul>-->
                        </div>
                    </div>

                </div>

                <!-- ส่วนแสดงรายละเอียด PM-->
                <div id="pm-notifications-box" class="detailNoti" style="display: none; margin: 2px 0px 0px 515px;">
                    <table>
                        <tr ng-repeat="item in pms.messagelist">
                            <td><a href="profile.php?id={{ item.fb_uid}}"><img ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"></a></td>
                            <td class="infoNoti"><span class="userName" ng-bind="item.display_name"></span> : <a href="message.php?id={{ item.id}}">ได้ส่งข้อความหาคุณ</a> <span class="visionComment">"<a href="message.php?id={{ item.id}}" ng-bind="item.message"></a>"</span></td>
                            <td><span class="closeNotificate" style="display: none;">x</span><br><img src="images/icon/mail-1-w.png"></td>
                        </tr>

                        <tr>
                            <td colspan="3" class="seeMore-Noti"><a href="message.php">ดูทั้งหมด</a></td>
                        </tr>
                    </table>
                </div>

                <!-- ส่วนแสดงรายละเอียด notifications-->
                <div id="notifications-box" class="detailNoti" style="display:none;">
                    <table>
                        <tbody ng-repeat="item in notificationsM| limitTo:10">
                            <tr ng-if="item.link_type === 'game' && item.type === 'play'" data-noti-id="{{ item.noti_id}}">
                                <td><a href="profile.php?id={{ item.fb_uid}}"><img src="https://graph.facebook.com/{{ item.fb_uid}}/picture"></a></td>
                                <td class="infoNoti"><a href="game.php?mid={{ item.link_key}}"><span class="userName" ng-bind="item.display_name"></span> : <span ng-bind="item.sys_msg"></span> <span class="visionComment" ng-bind="item.message"></span></a></td>
                                <td><span ng-click="removeNoti(item.rfb_uid, item.noti_id)" class="closeNotificate">x</span><br><img src="images/icon/bet2.png"></td>
                            </tr>
                            <tr ng-if="item.link_type === 'follow'" data-noti-id="{{ item.noti_id}}">
                                <td><a href="profile.php?id={{ item.fb_uid}}"><img src="https://graph.facebook.com/{{ item.fb_uid}}/picture"></a></td>
                                <td class="infoNoti"><span class="userName" ng-bind="item.display_name"></span><br><span ng-bind="item.sys_msg"></span></td>
                                <td><span ng-click="removeNoti(item.rfb_uid, item.noti_id)" class="closeNotificate">x</span><br><img src="images/icon/follow.png"></td>
                            </tr>
                            <tr ng-if="item.link_type === 'timeline' && item.type === 'like'" data-noti-id="{{ item.noti_id}}">
                                <td><a href="profile.php?id={{ item.fb_uid}}"><img src="https://graph.facebook.com/{{ item.fb_uid}}/picture"></a></td>
                                <td class="infoNoti"><a href="news.php?id={{ item.link_key}}"><span class="userName" ng-bind="item.display_name"></span><br><span ng-bind="item.sys_msg"></span>: <span ng-bind="item.message"></span></a></td>
                                <td><span ng-click="removeNoti(item.rfb_uid, item.noti_id)" class="closeNotificate">x</span><br><img src="images/icon/heart.png"></td>
                            </tr>
                            <tr ng-if="item.link_type === 'timeline' && item.type === 'post'" data-noti-id="{{ item.noti_id}}">
                                <td><a href="profile.php?id={{ item.fb_uid}}"><img src="https://graph.facebook.com/{{ item.fb_uid}}/picture"></a></td>
                                <td class="infoNoti"><a href="news.php?id={{ item.link_key}}"><span class="userName" ng-bind="item.display_name"></span><br><span ng-bind="item.sys_msg"></span>: <span ng-bind="item.message"></span></a></td>
                                <td><span ng-click="removeNoti(item.rfb_uid, item.noti_id)" class="closeNotificate">x</span><br><img src="images/icon/commenting.png"></td>
                            </tr>
                            <tr ng-if="item.link_type === 'message' && item.type === 'message'" data-noti-id="{{ item.noti_id}}">
                                <td><a href="profile.php?id={{ item.fb_uid}}"><img src="https://graph.facebook.com/{{ item.fb_uid}}/picture"></a></td>
                                <td class="infoNoti"><a href="message.php?id={{ item.link_key}}"><span class="userName" ng-bind="item.display_name"></span><br><span ng-bind="item.sys_msg"></span>: <span ng-bind="item.message"></span></a></td>
                                <td><span ng-click="removeNoti(item.rfb_uid, item.noti_id)" class="closeNotificate">x</span><br><img src="images/icon/commenting.png"></td>
                            </tr>
                        </tbody>


<!--                        <tr>-->
<!--                            <td><img src="images/55.jpg"></td>-->
<!--                            <td class="infoNoti">ผลการเล่นเกมส์ : <b>Uruguay</b> <span class="text-red">2-0</span> <b>England</b><b> Win ที่</b> <span class="text-red">+0.5</span> : คะแนน <span class="text-red">+8.30</span></td>-->
<!--                            <td><span class="closeNotificate">x</span><br><img src="images/icon/total1.png"></td>-->
                        <!--                        </tr>-->
                        <!--                        <tr>-->
                        <!--                            <td><img src="images/bg.png"></td>-->
                        <!--                            <td class="infoNoti"><span class="userName">นินจา</span>  <br> กำลังติดตามคุณ</td>-->
                        <!--                            <td><span class="closeNotificate">x</span><br><img src="images/icon/follow.png"></td>-->
                        <!--                        </tr>-->
                        <!--                        <tr>-->
                        <!--                            <td><img src="images/bracket.png"></td>-->
                        <!--                            <td class="infoNoti"><span class="userName">diarylovely</span> <br> ได้ถูกใจโพสต์ของคุณ</td>-->
                        <!--                            <td><span class="closeNotificate">x</span><br><img src="images/icon/heart.png"></td>-->
                        <!--                        </tr>-->
                        <!--                        <tr>-->
                        <!--                            <td><img src="images/bracket.png"></td>-->
                        <!--                            <td class="infoNoti"><span class="userName">diarylovely</span> : ได้แสดงความคิดเห็นบนรูปภาพของคุณ <span class="visionComment">"ขอทีเด็ดหน่อยคับวันนี้ ขอเด็ดๆ คับ"</span></td>-->
                        <!--                            <td><span class="closeNotificate">x</span><br><img src="images/icon/commenting.png"></td>-->
                        <!--                        </tr>-->
                        <tr>
                            <td colspan="3" class="seeMore-Noti"><a href="notifications.php">ดูทั้งหมด</a></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="topMenubar">
            <div class="wrapperBoxHeader">
<!--                <div class="boxLogo"><a href="/"><img src="images/logo-ssporting.png"></a></div>-->
                <div class="wrapperTopmenubar menu-nt1">
                    <table>
                        <td class="<?php if ($scriptName === 'timeline.php'): ?>activeMenu<?php endif; ?> wait-for-facebook-session" style="<?php if (!__FACEBOOK_ID__): ?>display: none;<?php endif; ?>"><a href="/timeline.php"><span><img src="images/icon/ico_timeline.png"></span><?php echo Utils::trans('Timeline'); ?></a></td>
                        <td class="<?php if ($scriptName === 'index.php'): ?>activeMenu<?php endif; ?>"><a href="/"><span><img src="images/icon/ico_live.png"></span><?php echo Utils::trans('Livescore'); ?></a></td>
                        <td class="<?php if ($scriptName === 'result.php'): ?>activeMenu<?php endif; ?>"><a href="/result.php"><span><img src="images/icon/ico_result.png"></span><?php echo Utils::trans('Results'); ?></a></td>
                        <td class="<?php if ($scriptName === 'w14index.php'): ?>activeMenu<?php endif; ?> menuWorldcup" style="display: none;"><a href="/w14index.php"><img src="images/icon/menu.png"></a></td>
                        <td class="<?php if ($scriptName === 'ranking.php'): ?>activeMenu<?php endif; ?> wait-for-facebook-session" style="<?php if (!__FACEBOOK_ID__): ?>display: none;<?php endif; ?>"><a href="/ranking.php"><span><img src="images/icon/ico_ranking.png"></span><?php echo Utils::trans('Ranking'); ?></a></td>
                        <!--<td class="<?php// if ($scriptName === 'favorite.php'): ?>activeMenu<?php// endif; ?>"><a href="/favorite.php"><span><img src="images/icon/ico_fav.png"></span><?php// echo Utils::trans('Favorites'); ?></a></td>-->
                        <td class="<?php if ($scriptName === 'favorite.php'): ?>activeMenu<?php endif; ?>"><a href="/favorite.php"><span><img src="images/icon/ico_table.png"></span>League/Table</a></td>
                        <td class="<?php if ($scriptName === 'all-games.php'): ?>activeMenu<?php endif; ?>">
                            <a href="/all-games.php">
                                <div class="hot-minigame"></div>
                                <span><img src="images/icon/ico_minigame.png"></span>
                                Mini Games
                            </a></td>
                        <td class="<?php if ($scriptName === 'howto.php'): ?>activeMenu<?php endif; ?>"><a href="/howto.php"><span><img src="images/icon/ico_how-to.png"></span>How to play</a></td>

                    </table>

<!--                    <table>
    <td class="activeMenu wait-for-facebook-session" style="<?php // if (!__FACEBOOK_ID__):          ?>display: none;<?php // endif;          ?>"><img src="images/icon/i19-b.png"> <a href="/timeline.php"><?php // echo Utils::trans('Timeline');          ?></a></td>
    <td><img src="images/icon/i24-b.png"> <a href="/"><?php // echo Utils::trans('Matches');          ?></a></td>
    <td><img src="images/icon/i18b.png"> <a href="/result.php"><?php // echo Utils::trans('Results');          ?></a></td>
    <td class="menuWorldcup"><a href="/w14index.php"><img src="images/icon/menu.png"></a></td>
    <td class="wait-for-facebook-session" style="<?php // if (!__FACEBOOK_ID__):          ?>display: none;<?php // endif;          ?>"><img src="images/icon/i6-b.png"> <a href="/profile.php?ranking"><?php //echo Utils::trans('Ranking');          ?></a></td>
    <td><img src="images/icon/i53-b.png"> <a href="/favorite.php"><?php // echo Utils::trans('Favorites');          ?></a></td>
    <td><a href="/all-games.php"><img src="images/icon/i40-b.png">Games</a></td>
    <td><img src="images/icon/book-icon.png"><a href="/howto.php">How to play</a></td>
</table>-->
                </div>
            </div>
        </div>
        <!--
                        เมนูมินิเกมส์
                <div id="mini-games-menu" class="gamesMenu" style="z-index: 6; left: 900px; display: none;">
                    <table>
                        <tbody>
                            <tr>
                                <td><a href="game-pao-ying-choob.php"><img src="images/mini-game/paoyingchoob/title_icon_g1.png"> เกมส์เป่ายิงฉุบ</a></td>
                            </tr>
                            <tr>
                                <td><img src="images/mini-game/hightlow/title_icon_g2.png"> เกมส์ไฮโล</td>
                            </tr>
                            <tr>
                                <td><a href="game-pao-ying-choob.php"><img src="images/mini-game/card/card-icon.png"> เกมส์ไพ่ดำ-แดง</a></td>
                            </tr>
                            <tr>
                                <td><img src="images/mini-game/card/card-icon.png"> เกมส์ไพ่ป็อกเด้ง</td>
                            </tr>
                            <tr>
                                <td><img src="images/mini-game/chest/chest-icon.png"> เกมส์หีบสมบัติ</td>
                            </tr>
                            <tr>
                                <td>เกมส์น้ำเต้าปูปลา</td>
                            </tr>
                        </tbody>
                    </table>
                </div>-->


        <div id="wrapper">

            <div class="wrapper-menu-left">
                <div class="fixed-menu">
                    <?php if (__FACEBOOK_ID__): ?>
                        <div id="facebook-info-box-temp" class="section-registered">
                            <div class="box-user-login">
                                <table>
                                    <tr>
                                        <td><a href="/profile.php"><img src="https://graph.facebook.com/<?php echo reset(explode('-', __FACEBOOK_ID__)); ?>/picture"/></a></td>
                                        <td>
                                            <div class="name-user-login"><a href="/profile.php"><?php echo end(explode('-', __FACEBOOK_ID__)); ?></a></div>
                                            <div class="boxGolden"><a href="/exchangeCoin.php"><img src="images/icon/diamond30.png"></a> <b ng-bind="userInfo.diamond"></b></div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    <?php endif; ?>
                    <div id="facebook-info-box" class="section-registered" style="display: none;">
                        <div class="box-user-login">
                            <table>
                                <tr>
                                    <td><a href="/profile.php">
                                            <?php if (__FACEBOOK_ID__): ?>
                                                <img src="https://graph.facebook.com/<?php echo reset(explode('-', __FACEBOOK_ID__)); ?>/picture"/>
                                            <?php else: ?>
                                                <img ng-if="facebookInfo.id" ng-src="https://graph.facebook.com/{{ facebookInfo.id}}/picture"/>
                                            <?php endif; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <div class="name-user-login"><a href="/profile.php">{{ facebookInfo.name}}</a></div>
                                        <div class="boxGolden"><a href="/exchangeCoin.php"><img src="images/icon/diamond30.png"></a> <b ng-bind="userInfo.diamond"></b></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    <div class="sectionCoin">
                        <ul>
                            <li>
                                <table>
                                    <tr>
                                        <td><a href="/exchangeCoin.php"><img src="images/icon/sgold30.png"></a> <span ng-bind="userInfo.sgold || 0 | number"></span> </td>
                                        <td><a href="/exchangeCoin.php"><img src="images/icon/scoin30.png"></a> <span ng-bind="userInfo.scoin || 0 | number"></span> </td>
                                    </tr>
                                </table>

                            </li>
                        </ul>
                    </div>

                    <div id="board-filter-loading" class="beforeLoading" style="display:none;"><img src="images/icon/loader.gif"></div>
                    <div class="section-menulist">
                        <!--                                                <ul id="basic-left-menu">
                                                                            <li class="wait-for-facebook-session" style="<?php //if (!__FACEBOOK_ID__):                 ?>display: none;<?php //endif;                 ?>"><img src="images/icon/i19.png"/> <a href="/timeline.php"><?php //echo Utils::trans('Timeline');                 ?></a></li>
                                                                            <li><img src="images/icon/i24.png"/> <a href="/"><?php //echo Utils::trans('Matches');                 ?></a></li>
                                                                            <li><img src="images/icon/i18.png"/> <a href="/result.php"><?php //echo Utils::trans('Results');                 ?></a></li>
                                                                            <li class="menu-worldcup"><a href="/w14index.php"><img src="images/menu-worldcup.png"></a></li>
                                                                            <li class="wait-for-facebook-session" style="<?php //if (!__FACEBOOK_ID__):                 ?>display: none;<?php //endif;                 ?>"><img src=" images/icon/i6.png"/> <a href="/profile.php?ranking"><?php //echo Utils::trans('Ranking');                 ?></a></li>
                                                                            <li><img src="images/icon/i53.png"/> <a href="/favorite.php"><?php //echo Utils::trans('Favorites');                 ?></a></li>
                                                                            <li><img src="images/icon/i40.png"/> <a href="/howto.php">How to play</a></li>
                                                                        </ul>-->
                        <ul id="webboard-filter" style="display: none;">
                            <li ng-repeat="item in topicType" ng-class="{'offMenu':!isActiveTopicType(item.fid)}"><a href="board.php?id={{ item.fid}}">{{ item.feed_name}}</a> <img ng-click="toggleActiveTopicType(item.fid)" ng-if="isActiveTopicType(item.fid)" src="images/icon/on.png"><img ng-click="toggleActiveTopicType(item.fid)" ng-if="!isActiveTopicType(item.fid)" src="images/icon/off.png"></li>
                        </ul>
                    </div>

                    <div id="favorite-list-box" style="display: none;" class="section-menusub">
                        <div class="tab-header-menu"><?php echo Utils::trans('Favorites') ?></div>
                        <div class="menu-teams-list">
                            <table id="favorite-league-list-box">
                                <tr ng-repeat="league in favoriteLeague" ng-if="league.name">
                                    <td><img ng-src="images/countries/{{ league.competition_id}}.png"/></td>
                                    <td><span class="name-country"><a href="/league.php?id={{ league.id}}" ng-bind="allLeagues[league.id].name || league.name || '(null)'"></a></span></td>
                                    <td></td>
                                </tr>
                            </table>
                            <table id="favorite-team-list-box">
                                <tr ng-repeat="team in favoriteTeam">
                                    <td><img ng-src="{{ logoUrl}}/{{ team.id}}_32x32.png"/></td>
                                    <td><span class="name-country"><a href="/team.php?id={{ team.id}}" ng-bind="allTeams[team.id].name || team.name"></a></span></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                        <div style="clear: both;"></div>
                    </div>

                    <div class="section-menusub" id="highlight-league-list-box">
                        <div class="tab-header-menu league-head"><?php echo Utils::trans('Leagues'); ?></div>
                        <div class="menu-teams-list box-left-menu">
                            <div class="league">
                                <ul>
                                    <li class="flag-icon">
                                        <img ng-src="images/countries/152.png"/>
                                    </li>
                                    <li class="name-league"><a href="league.php?id=34885">Thailand Premier League</a></li>
                                    <li class="right-icon"></li>
                                </ul>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="league">
                                <ul>
                                    <li class="flag-icon">
                                        <img ng-src="images/countries/59.png"/>
                                    </li>
                                    <li class="name-league"><a href="league.php?id=35909">English Premier League</a></li>
                                    <li class="right-icon"></li>
                                </ul>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="league">
                                <ul>
                                    <li class="flag-icon">
                                        <img ng-src="images/countries/70.png"/>
                                    </li>
                                    <li class="name-league"><a href="league.php?id=37120">Spanish Primera Division</a></li>
                                    <li class="right-icon"></li>
                                </ul>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="league">
                                <ul>
                                    <li class="flag-icon">
                                        <img ng-src="images/countries/78.png"/>
                                    </li>
                                    <li class="name-league"><a href="league.php?id=36172">Bundesliga</a></li>
                                    <li class="right-icon"></li>
                                </ul>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="league">
                                <ul>
                                    <li class="flag-icon">
                                        <img ng-src="images/countries/80.png"/>
                                    </li>
                                    <li class="name-league"><a href="league.php?id=37049">Italian Serie A</a></li>
                                    <li class="right-icon"></li>
                                </ul>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="league">
                                <ul>
                                    <li class="flag-icon">
                                        <img ng-src="images/countries/43.png"/>
                                    </li>
                                    <li class="name-league"><a href="league.php?id=35797">Ligue 1</a></li>
                                    <li class="right-icon"></li>
                                </ul>
                                <div style="clear: both;"></div>
                            </div>
                        </div>
                    </div>

                    <!--                    <div class="section-menusub" id="competition-list-box">-->
                    <!--                        <div class="tab-header-menu league-head">--><?php //echo Utils::trans('Leagues/Tables');  ?><!--</div>-->
                    <!--                        <div class="menu-teams-list box-left-menu">-->
                    <!---->
                    <!--                            --><?php //foreach ($service_competition->competitions as $key => $item):  ?>
                    <!---->
                    <!--                                --><?php //if (Utils::isHighlightCompetition($item->competitionId)):  ?>
                    <!--                                    <div class="league">-->
                    <!--                                        <ul ng-click="toggleCompetition(--><?php //echo $item->competitionId;  ?><!--)">-->
                    <!--                                            <li class="flag-icon"><img-->
                    <!--                                                    ng-src="images/countries/--><?php //echo $item->competitionId;  ?><!--.png"/>-->
                    <!--                                            </li>-->
                    <!--                                            <li class="name-league">--><?php //echo $item->competitionName;  ?><!--</li>-->
                    <!--                                            <li class="right-icon"><img src="images/icon/right-icon.png"/></li>-->
                    <!--                                        </ul>-->
                    <!--                                        <div style="clear: both;"></div>-->
                    <!---->
                    <!--                                        <div class="subcate-league" ng-show="competitionToggleStatus[--><?php //echo $item->competitionId;  ?><!--]">-->
                    <!--                                            --><?php //foreach ($item->leagueList as $sub):  ?>
                    <!--                                                <ul>-->
                    <!--                                                    <li class="flag-icon sub-icon"><img src="images/icon/list-icon.png"/></li>-->
                    <!--                                                    <li class="league-sub"><a-->
                    <!--                                                            href="/league.php?id=--><?php //echo $sub->leagueId;  ?><!--">--><?php //echo $sub->leagueName;  ?><!--</a>-->
                    <!--                                                    </li>-->
                    <!--                                                    <div style="clear: both;"></div>-->
                    <!--                                                </ul>-->
                    <!--                                            --><?php //endforeach;  ?>
                    <!--                                            <div style="clear: both;"></div>-->
                    <!--                                        </div>-->
                    <!--                                    </div>-->
                    <!--                                --><?php //endif;  ?>
                    <!--                            --><?php //endforeach;  ?>
                    <!---->
                    <!--                            <div>-->
                    <!--                                <span class="section-more" ng-click="setHighlightCompetitionOnly(false)"-->
                    <!--                                      ng-show="highlightCompetitionOnly"><a href="/favorite.php">more..</a></span>-->
                    <!--                                <span class="section-more" ng-click="setHighlightCompetitionOnly(true)"-->
                    <!--                                      ng-show="!highlightCompetitionOnly">less..</span>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <div style="clear: both;"></div>-->
                    <!--                    </div>-->
                </div>
                <!--<div style="clear: both;"></div>-->
            </div>
            <!--<div style="clear: both;"></div>-->

            <div class="box-loading" style="display: none;">
                <img src="images/blank_loading.gif"/>
            </div>
