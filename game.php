<?php
require_once(dirname(__FILE__) . '/_init_.php');

define('__MID__', $_REQUEST['mid']);

$title = 'เกมทายผลฟุตบอลและรวมทรรศนะจากบรรดาเซียนบอล';
$meta = '<meta name="description" content="เกมทายผลฟุตบอลที่มีกติกาสุดเร้าใจ มีการเก็บคะแนนเป็นลีก เพื่อหาผู้เล่นที่มีความแม่นยำที่สูงสุด และรวบรวมทรรศนะจากเซียนบอลทั้งหลาย">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,เกมทายผลฟุตบอล,เกมฟุตบอลออนไลน์">' . "\n";
$meta .= '<meta property="og:image" content="https://ssporting.com/WebObject/images/m' . __MID__ . '.jpg"/>' . "\n";
$meta .= '<meta property="og:type" content="website"/>' . "\n";

$service_matchInfo = Services::getMatchInfo(__MID__);
$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();
//$service_w14 = Services::getW14();

//echo '<pre>';
//print_r($service_matchInfo);
//echo '</pre>';
//$headerScript .= '<meta property="og:image" content="http://scorspot.com/WebObject/images/m' . __MID__ . '.jpg"/>';

$footerScript .= '<script id="gameScript" src="scripts/game.js"
mid="' . __MID__ . '"
match_id="' . $service_matchInfo->game->match_id . '"
betType="' . @$service_matchInfo->game->type . '"
betHdp="' . @$service_matchInfo->game->hdp . '"
hdp_home="' . @$service_matchInfo->game->hdp_home . '"
hdp_away="' . @$service_matchInfo->game->hdp_away . '"
hid="' . @$service_matchInfo->game->hid . '"
gid="' . @$service_matchInfo->game->gid . '"
leagueId="' . @$service_matchInfo->game->_lid . '"
></script>';

use Carbon\Carbon;

require_once(__INCLUDE_DIR__ . '/header.php')
?>


<div id="news-top-slide-box" class="wrapper-slide-comment-top" style="display: none;">

    <div class="box-comment-top" ng-repeat="item in newsTopSlide">
        <a href="/news.php?id={{ news.ontimelines[item.newsId]}}">
            <table>
                <tr>
                    <td><img ng-src="{{ item.imageLink}}"></td>
                    <td>
                        <b ng-bind="news.titles[item.newsId]"></b>
                        <span class="detail-news" ng-bind="news.desc[item.newsId]"></span>
                    </td>
                </tr>
            </table>
        </a>
    </div>
</div>

<div class="wrapper-content content-profile">
    <div class="banner" style="padding: 5px;">
        <a href="/w14index.php"><img src="images/banner.png" style="width: 560px;"></a>
    </div>

    <div class="teamPlay-Match" style="display: none;">
        <?php
        $ds = Carbon::now('Africa/Abidjan')->format('Y-m-d');
        if ($ds < '2014-06-12')
            $ds = '2014-06-12';
        else if ($ds > '2014-07-13')
            $ds = '2014-07-13';

        $dm = Carbon::createFromFormat('Y-m-d', $ds)->addDays(1)->format('Y-m-d');
        $dmd = Carbon::createFromFormat('Y-m-d', $dm);
        $dmw = $dmd->day . ' ' . Utils::monthOfYear($dmd->month) . ' ' . $dmd->year;

        $d = Carbon::createFromFormat('Y-m-d', $ds)->addHours(__GMT_OFFSET__);
        $dw = $d->day . ' ' . Utils::monthOfYear($d->month) . ' ' . $d->year;
        ?>
        <?php foreach ($service_w14->matches->{$ds} as $idx => $item): ?>
            <div class="Box1 upcoming-match" data-idx="<?php echo $idx; ?>" style="display: none;">
                <?php if (!empty($item->mid)): ?><a href="/game.php?mid=<?php echo $item->mid; ?>"><?php endif; ?>
                    <table>
                        <tr>
                            <td><img src="http://ws.1ivescore.com/worldcup/<?php echo $item->hid; ?>_.png"><div class="nameTeams"><?php echo $service_w14->team->{$item->hid}->{__LANGUAGE__}; ?></div></td>
                            <td><img src="images/icon/bet-icon.png"><br><span class="fontYellow"><?php echo Carbon::createFromTimestamp($item->datetime)->addHours(__GMT_OFFSET__)->format('H:i'); ?></span><br><?php echo $dw; ?></td>
                            <td><img src="http://ws.1ivescore.com/worldcup/<?php echo $item->gid; ?>_.png"><div class="nameTeams"><?php echo $service_w14->team->{$item->gid}->{__LANGUAGE__}; ?></div></td>
                        </tr>
                    </table>
                    <?php if (!empty($item->mid)): ?></a><?php endif; ?>
            </div>
        <?php endforeach; ?>
        <?php foreach ($service_w14->matches->{$dm} as $idx => $item): ?>
            <div class="Box1 upcoming-match" data-idx="<?php echo $idx + count($service_w14->matches->{$ds}); ?>" style="display: none;">
                <?php if (!empty($item->mid)): ?><a href="/game.php?mid=<?php echo $item->mid; ?>"><?php endif; ?>
                    <table>
                        <tr>
                            <td><img src="http://ws.1ivescore.com/worldcup/<?php echo $item->hid; ?>_.png"><div class="nameTeams"><?php echo $service_w14->team->{$item->hid}->{__LANGUAGE__}; ?></div></td>
                            <td><img src="images/icon/bet-icon.png"><br><span class="fontYellow"><?php echo Carbon::createFromTimestamp($item->datetime)->addHours(__GMT_OFFSET__)->format('H:i'); ?></span><br><?php echo $dmw; ?></td>
                            <td><img src="http://ws.1ivescore.com/worldcup/<?php echo $item->gid; ?>_.png"><div class="nameTeams"><?php echo $service_w14->team->{$item->gid}->{__LANGUAGE__}; ?></div></td>
                        </tr>
                    </table>
                    <?php if (!empty($item->mid)): ?></a><?php endif; ?>
            </div>
        <?php endforeach; ?>
        <div style="clear: both;"></div>
    </div>

    <div class="wrapper-box-feed-expand">

        <div class="wrapper-feed-expand">
            <?php if ($service_matchInfo->game->hdp != '' && $service_matchInfo->game->hdp != '""'): ?>
                <div class="box-wrap">

                    <table class="bg-head">

                        <tr>
                            <td colspan="2" class="team-feed-play">
                                <table>
                                    <tr>
                                        <td class="name-league-game" colspan="3"><?php echo (isset($service_allleague->{$service_matchInfo->game->_lid}) && isset($service_allleague->{$service_matchInfo->game->_lid}->name)) ? $service_allleague->{$service_matchInfo->game->_lid}->name : $service_matchInfo->game->leagueName; ?> <span ng-bind="currentMatch.show_date | kickTime"><?php echo Utils::kickTime($service_matchInfo->game->show_date); ?></span></td>
                                    </tr>
                                    <tr>
                                        <td ng-class="{'default-cursor':!canPlay, 'select-success':myBet[<?php echo __MID__; ?>].choose === 'home'}" ng-click="chooseTeam('home', false, $event)" style="cursor: pointer;" class="sec-teams1"><div class="logoCenter"><img ng-class="{'default-cursor':!canPlay}" src="<?php echo $service_matchInfo->game->h64x64; ?>"/></div></td>
                                        <td class="hdpr">

                                            <?php if ($service_matchInfo->game->sid != '1'): ?>
                                                <br/><br/><span class="success-score-game"><?php echo $service_matchInfo->game->score; ?></span>
                                            <?php else: ?>
                                                <div class="tab-hdpr">
                                                    <span class="hdp-home"><?php echo isset($service_matchInfo->game->hdp_home) ? $service_matchInfo->game->hdp_home : '-' ?></span>
                                                    <span class="gamehdp"><span class="game-hdp"><b><?php echo isset($service_matchInfo->game->hdp) ? $service_matchInfo->game->hdp : '-' ?></b> </span><h6 ng-if="canPlay">PLAY</h6></span>
                                                    <span class="hdp-away"><?php echo isset($service_matchInfo->game->hdp_away) ? $service_matchInfo->game->hdp_away : '-' ?></span>
                                                </div>
                                            <?php endif; ?>
                                        </td>
                                        <td ng-class="{'default-cursor':!canPlay, 'select-success':myBet[<?php echo __MID__; ?>].choose === 'away'}" ng-click="chooseTeam('away', false, $event)" style="cursor: pointer;" class="sec-teams2"><div class="logoCenter2"><img ng-class="{'default-cursor':!canPlay}" src="<?php echo $service_matchInfo->game->a64x64; ?>"/></div></td>
                                    </tr>

                                    <tr style="display: none;">
                                        <td colspan="3">
                                            <table>
                                                <tr>
                                                    <td class="teams1">
                                                        <span class="<?php if ($service_matchInfo->game->hdp < 0): ?>team-favored<?php endif; ?>"><?php echo (isset($service_allteam->{$service_matchInfo->game->hid}) && isset($service_allteam->{$service_matchInfo->game->hid}->name)) ? $service_allteam->{$service_matchInfo->game->hid}->name : $service_matchInfo->game->homeName; ?></span>
                                                        <br/>
                                                        <div ng-class="{'default-cursor':!canPlay}" style="display: none;" class="bet-select-button bet-select-button-home<?php if ($service_matchInfo->game->hdp < 0): ?> bet-select-button-favored<?php endif; ?>" ng-click="chooseTeam('home', $event)"><?php echo Utils::trans('Choose Home') ?></div>
                                                    </td>
                                                    <td class="teams2">
                                                        <span class="<?php if ($service_matchInfo->game->hdp > 0): ?>team-favored<?php endif; ?>"><?php echo (isset($service_allteam->{@$service_matchInfo->game->gid}) && isset($service_allteam->{@$service_matchInfo->game->gid}->name)) ? $service_allteam->{$service_matchInfo->game->gid}->name : $service_matchInfo->game->awayName; ?></span>
                                                        <br/>
                                                        <div ng-class="{'default-cursor':!canPlay}" style="display: none;" class="bet-select-button bet-select-button-away<?php if ($service_matchInfo->game->hdp > 0): ?> bet-select-button-favored<?php endif; ?>" ng-click="chooseTeam('away', $event)"><?php echo Utils::trans('Choose Away') ?></div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <span class="t1"><b style="font-weight: normal; font-size: 14px;">[1]</b> <b><span style="xcursor: pointer;" ng-class="{'default-cursor':!canPlay}" ng-click="chooseTeam('home', false, $event)" class="<?php if ($service_matchInfo->game->hdp < 0): ?>team-favored<?php endif; ?>"><?php echo isset($service_allteam->{$service_matchInfo->game->hid}->name) ? $service_allteam->{$service_matchInfo->game->hid}->name : $service_matchInfo->game->homeName; ?></span></b></span>
                                            <span class="t2"><b><span style="xcursor: pointer;" ng-class="{'default-cursor':!canPlay}" ng-click="chooseTeam('away', false, $event)" style="cursor: pointer;" class="<?php if ($service_matchInfo->game->hdp > 0): ?>team-favored<?php endif; ?>"><?php echo isset($service_allteam->{@$service_matchInfo->game->gid}->name) ? $service_allteam->{$service_matchInfo->game->gid}->name : $service_matchInfo->game->awayName; ?></span></b> <b style="font-weight: normal; font-size: 14px;">[1]</b></span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="left-team">
                                            <b></b>
    <!--                                        <b><span style="xcursor: pointer;" ng-class="{'default-cursor':!canPlay}" ng-click="chooseTeam('home', false, $event)" class="<?php if ($service_matchInfo->game->hdp < 0): ?>team-favored<?php endif; ?>"><?php echo isset($service_allteam->{$service_matchInfo->game->hid}->name) ? $service_allteam->{$service_matchInfo->game->hid}->name : $service_matchInfo->game->homeName; ?></span></b>-->
                                            <span ng-if="canPlay" ng-class="{'default-cursor':!canPlay, 'bet-left':canPlay, 'bet-left-h':!canPlay}" class="bet-select-button-set" ng-click="chooseTeam('home', false, $event)"></span>
                                            <span ng-if="myBet[mid].choose === 'home'" class="selected-home-success"  style="display: xnone;"></span>
                                        </td>
                                        <td class="vs">
                                            <div ng-if="canPlay" class="bet-select-button-set bg-vsbet">Select Team</div>
                                        </td>
                                        <td class="right-team">
                                            <b></b>
    <!--                                        <b><span style="xcursor: pointer;" ng-class="{'default-cursor':!canPlay}" ng-click="chooseTeam('away', false, $event)" style="cursor: pointer;" class="<?php if ($service_matchInfo->game->hdp > 0): ?>team-favored<?php endif; ?>"><?php echo isset($service_allteam->{@$service_matchInfo->game->gid}->name) ? $service_allteam->{$service_matchInfo->game->gid}->name : $service_matchInfo->game->awayName; ?></span></b>-->
                                            <span ng-if="canPlay" ng-class="{'default-cursor':!canPlay, 'bet-right':canPlay, 'bet-right-h':!canPlay}" class="bet-select-button-set" ng-click="chooseTeam('away', false, $event)"></span>
                                            <span ng-if="myBet[mid].choose === 'away'" class="selected-away-success" style="display: xnone;"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                </div>

            <?php else: ?>

                <!--1X2-->
                <div class="box-wrap" style="display: snone;">

                    <table class="bg-head">

                        <tr>
                            <td colspan="2" class="team-feed-play">
                                <table>
                                    <tr>
                                        <td class="name-league-game" colspan="3"><?php echo (isset($service_allleague->{$service_matchInfo->game->_lid}) && isset($service_allleague->{$service_matchInfo->game->_lid}->name)) ? $service_allleague->{$service_matchInfo->game->_lid}->name : $service_matchInfo->game->leagueName; ?> <span ng-bind="currentMatch.show_date | kickTime"><?php echo Utils::kickTime($service_matchInfo->game->show_date); ?></span></td>
                                    </tr>
                                    <tr>
                                        <td ng-class="{'default-cursor':!canPlay}" ng-click="chooseTeam('home', false, $event)" style="cursor: pointer;" class="sec-teams1"><div class="logoVote1"><img ng-class="{'default-cursor':!canPlay}" src="<?php echo $service_matchInfo->game->h64x64; ?>"/></div> </td>
                                        <td class="hdpr">

                                            <?php if ($service_matchInfo->game->sid != '1'): ?>
                                                <br/><br/><span class="success-score-game"><?php echo $service_matchInfo->game->score; ?></span>
                                            <?php else: ?>
                                                <div class="tab-hdpr">
                                                    <span class="hdp-home">1&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    <span class="gamehdp"><span class="game-hdp"><b>X</b> </span><h6>VOTE</h6></span>
                                                    <span class="hdp-away">&nbsp;&nbsp;&nbsp;&nbsp;2</span>
                                                </div>
                                            <?php endif; ?>
                                        </td>
                                        <td ng-class="{'default-cursor':!canPlay}" ng-click="chooseTeam('away', false, $event)" style="cursor: pointer;" class="sec-teams2"><div class="logoVote2"><img ng-class="{'default-cursor':!canPlay}" src="<?php echo $service_matchInfo->game->a64x64; ?>"/></div></td>
                                    </tr>

                                    <tr style="display: none;">
                                        <td colspan="3">
                                            <table>
                                                <tr>
                                                    <td class="teams1">
                                                        <span class="<?php if ($service_matchInfo->game->hdp < 0): ?>team-favored<?php endif; ?>"><?php echo (isset($service_allteam->{$service_matchInfo->game->hid}) && isset($service_allteam->{$service_matchInfo->game->hid}->name)) ? $service_allteam->{$service_matchInfo->game->hid}->name : $service_matchInfo->game->homeName; ?></span>
                                                        <br/>
                                                        <div ng-class="{'default-cursor':!canPlay}" style="display: none;" class="bet-select-button bet-select-button-home<?php if ($service_matchInfo->game->hdp < 0): ?> bet-select-button-favored<?php endif; ?>" ng-click="chooseTeam('home', $event)"><?php echo Utils::trans('Choose Home') ?></div>
                                                    </td>
                                                    <td class="teams2">
                                                        <span class="<?php if ($service_matchInfo->game->hdp > 0): ?>team-favored<?php endif; ?>"><?php echo (isset($service_allteam->{@$service_matchInfo->game->gid}) && isset($service_allteam->{@$service_matchInfo->game->gid}->name)) ? $service_allteam->{$service_matchInfo->game->gid}->name : $service_matchInfo->game->awayName; ?></span>
                                                        <br/>
                                                        <div ng-class="{'default-cursor':!canPlay}" style="display: none;" class="bet-select-button bet-select-button-away<?php if ($service_matchInfo->game->hdp > 0): ?> bet-select-button-favored<?php endif; ?>" ng-click="chooseTeam('away', $event)"><?php echo Utils::trans('Choose Away') ?></div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <span class="t1"> <b><span style="xcursor: pointer;" ng-class="{'default-cursor':!canPlay}" ng-click="chooseTeam('home', false, $event)" class="<?php if ($service_matchInfo->game->hdp < 0): ?>team-favored<?php endif; ?>"><?php echo (isset($service_allteam->{$service_matchInfo->game->hid}) && isset($service_allteam->{$service_matchInfo->game->hid}->name)) ? $service_allteam->{$service_matchInfo->game->hid}->name : $service_matchInfo->game->homeName; ?></span></b></span>
                                            <span class="t2"><b><span style="xcursor: pointer;" ng-class="{'default-cursor':!canPlay}" ng-click="chooseTeam('away', false, $event)" style="cursor: pointer;" class="<?php if ($service_matchInfo->game->hdp > 0): ?>team-favored<?php endif; ?>"><?php echo (isset($service_allteam->{@$service_matchInfo->game->gid}) && isset($service_allteam->{@$service_matchInfo->game->gid}->name)) ? $service_allteam->{$service_matchInfo->game->gid}->name : $service_matchInfo->game->awayName; ?></span></b></span>
                                        </td>
                                    </tr>

                                    <tr class="vote">
                                        <td>
                                            <span ng-if="canVote" ng-click="chooseTeam('home', false, $event)" class="vote-home"></span>
                                        </td>
                                        <td>
                                            <span ng-if="canVote" ng-click="chooseTeam('draw', false, $event)" class="vote-draw"></span>
                                        </td>
                                        <td>
                                            <span ng-if="canVote" ng-click="chooseTeam('away', false, $event)" class="vote-away"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                </div>
                <!--end-->
            <?php endif; ?>

            <div style="clear: both;"></div>
        </div>

        <?php if ($service_matchInfo->game->hdp != '' && $service_matchInfo->game->hdp != '""'): ?>
            <div ng-show="playHomePercent != '0' || playDrawPercent != '0' || playAwayPercent != '0'" class="progress-vote-game">
                <div class="graph-outer">
                    <div class="inner-left-cap"></div>
                    <div xng-show="playHomePercent !== 'NaN'" class="inner-left-bar" ng-style="{ width: playHomePercent + '%' }"><span ng-bind="playHomePercent"></span>%</div>
                    <div xng-show="playAwayPercent !== 'NaN'" class="inner-right-bar" ng-style="{ width: playAwayPercent + '%' }"><span ng-bind="playAwayPercent"></span>%</div>
                    <div class="inner-right-cap"></div>
                </div>
            </div>
        <?php else: ?>
            <!-- 1x2-->
            <div ng-show="voteHomePercent != '0' || voteDrawPercent != '0' || voteAwayPercent != '0'" class="progress-vote-game" style="display: snone;">
                <div class="graph-outer">
                    <div class="inner-left-cap"></div>
                    <div class="inner-left-bar" ng-style="{ width: voteHomePercent + '%' }"><span ng-bind="voteHomePercent"></span>%</div>
                    <div class="inner-center-bar" ng-style="{ width: voteDrawPercent + '%' }"><span ng-bind="voteDrawPercent"></span>%</div>
                    <div class="inner-center-cap"></div>
                    <div class="inner-right-bar" ng-style="{ width: voteAwayPercent + '%' }"><span ng-bind="voteAwayPercent"></span>%</div>
                    <div class="inner-right-cap"></div>
                </div>
            </div>
            <!--end-->
        <?php endif; ?>


        <div id="game-comment-list-box" class="wrapper-box-feed-comment">

            <?php if ($service_matchInfo->game->hdp != '' && $service_matchInfo->game->hdp != '""'): ?>
                <div class="box-feed-comment">
                    <table>
                        <tr>
                            <td class="people-like" id="game-comment-list-box-home" style="display: none;">{{ commentsInfo.home.count || '0' }}</td>

                            <td class="user-like">
                                <span ng-repeat="item in commentsInfo.home.list| limitTo:5" class="img-people-like"><img
                                        ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"/></span>
                            </td>

                            <td class="vs-like">VS</td>
                            <td class="user-like">
                                <span ng-repeat="item in commentsInfo.away.list| limitTo:5" class="img-people-like"><img
                                        ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"/></span>
                            </td>
                            <td class="people-like" id="game-comment-list-box-away" style="display: none;">{{ commentsInfo.away.count || '0' }}</td>
                        </tr>
                    </table>
                </div>
            <?php else: ?>
                <!--1X2-->
                <div class="box-feed-comment vote-game" style="display: snone;">
                    <table>
                        <tr>
                            <td><span ng-bind="votes.home.count"></span> <img ng-repeat="fb_uid in votes.home.people| limitTo:5" src="https://graph.facebook.com/{{ fb_uid}}/picture"> </td>
                            <td><span ng-bind="votes.draw.count"></span> <img ng-repeat="fb_uid in votes.draw.people| limitTo:5" src="https://graph.facebook.com/{{ fb_uid}}/picture"> </td>
                            <td><span ng-bind="votes.away.count"></span> <img ng-repeat="fb_uid in votes.away.people| limitTo:5" src="https://graph.facebook.com/{{ fb_uid}}/picture"> </td>
                        </tr>
                    </table>
                </div>
                <!--end-->
            <?php endif; ?>

            <!-- section-stat 5 -->
            <div class="section-macth-stat-game">
                <a href="/match.php?mid=<?php echo __MID__; ?>">
                    <table>
                        <tr>
                            <td><img src="images/icon/lastmatch.png"></td>
                            <td>
                                <div class="section-math-stat-tournament">
                                    <table>
                                        <tbody><tr>
                                                <td class="head-stat">All</td>
                                                <td>
                                                    <?php foreach ($service_matchInfo->last_result->home->all as $key => $val): ?>
                                                        <img ng-src="images/icon/<?php echo Utils::wldStr($val) ?>.png"/>
                                                        <?php if ($key >= 4) break; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                                <td>
                                                    <?php foreach ($service_matchInfo->last_result->away->all as $key => $val): ?>
                                                        <img ng-src="images/icon/<?php echo Utils::wldStr($val) ?>.png"/>
                                                        <?php if ($key >= 4) break; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="head-stat">League</td>
                                                <td>
                                                    <?php foreach ($service_matchInfo->last_result->home->league as $key => $val): ?>
                                                        <img ng-src="images/icon/<?php echo Utils::wldStr($val) ?>.png"/>
                                                        <?php if ($key >= 4) break; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                                <td>
                                                    <?php foreach ($service_matchInfo->last_result->away->league as $key => $val): ?>
                                                        <img ng-src="images/icon/<?php echo Utils::wldStr($val) ?>.png"/>
                                                        <?php if ($key >= 4) break; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                </div>
                            </td>
                            <td>ดูสถิติทั้งหมด</td>
                        </tr>
                    </table></a>
            </div>

            <!-- comment input -->
            <div class="box-show-comment">
                <table>
                    <tr>
                        <td class="user-comment-img" style="display: none;">
                            <!--<div class="box-like-post">-->
                            <!--<img src="images/icon/heart.png"/> 134-->
                            <!--</div>-->
                        </td>
                        <td class="comment-box-like-match">
                            <div ng-click="browsePicture()" class="upimages-box-match" style="display:none;"><img src="images/icon/cam15.png"
                                                                                                                  title="แนบรูปภาพ"/></div>
                            <textarea id="bet-comment-box" ng-model="newComment.message" ng-click="hasBetOn($event)"
                                      ng-enter="addNewComment(comments, $event)" placeholder="แสดงความคิดเห็น..."></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab-ui-post">
                            <div ng-click="browsePicture()" class="tab-upload-img"><img src="images/icon/cam15.png" title="แนบรูปภาพ"/> รูปภาพ</div>
                            <span class="loading" ng-if="inProgress.comment"><img src="images/icon/loading_icon.gif"></span><div class="button-post-img" ng-click="addNewComment(comments, $event)">โพสต์</div>
                        </td>
                    </tr>
                </table>

                <div class="wall" ng-show="newComment.thumbnail">
                    <div class="img-thumbnail-upload">
                        <span class="delete-img" ng-click="removeImage('comment')">x</span>
                        <img class="new-comment-thumbnail" ng-src="{{ newComment.thumbnail}}"/>
                    </div>
                    <button style="display: none;" class="btn" ng-click="addNewComment(comments, $event)">โพสต์</button>
                </div>
            </div>

            <!-- end -->
            <div id="box-show-comment-all" style="display: none;">
            <div class="box-show-comment" ng-repeat="(index,comment) in comments">
                <span class="remove-ment ment-1" ng-if="facebookInfo.id == comment.fb_uid || facebookInfo.id == sup" ng-click="removeReply(comment.id, false)" title="ลบ">x</span>
                <table>
                    <tr>
                        <td class="user-comment-img userCommentGame"><a href="/profile.php?id={{ comment.fb_uid}}"><img
                                    ng-src="https://graph.facebook.com/{{ comment.fb_uid}}/picture"/></a></td>
                        <td class="user-comment-post">
                            <span ng-click="openReportDialog(comment.id, 'reply')" class="reportPost"><img src="images/icon/downicon.png"></span>
                            <a href="/profile.php?id={{ comment.fb_uid}}"><b>{{
                                        comment.display_name || comment.displayname || comment.fb_firstname}}</b></a> <span
                                class="team-user-selected" ng-switch="comment.choose">
                                <span ng-switch-when="home">@{{ allTeams[currentMatch.hid].name || currentMatch.hn }}</span>
                                <span ng-switch-when="away">@{{ allTeams[currentMatch.gid].name || currentMatch.gn }}</span></span>
                            {{ comment.message}}
                            <div ng-show="comment.thumbnail">
                                <img ng-src="{{ comment.thumbnail}}"/>
                            </div>
                            <span class="time-post">
                                <ul>
                                    <li class="time-post-comment-user">{{ comment.comment_at | timeago }}</li>
                                    <li class="user-liked"><img
                                            ng-click="likeComment(comment.id, comment.match_id, index)"
                                            src="images/icon/heart.png"/> ({{
                                                        comment.like}}) <span
                                            class="reply" ng-click="openReplyBox(comment.id)">{{ replies[comment.id].length}} reply</span></li>

                                </ul>
                            </span>
                        </td>
                    </tr>
                </table>

                <div class="wrapper-reply-comment" data-comment-id="{{ comment.id}}">
                    <div class="wrapper-reply">
                        <div ng-repeat="(r_index, reply) in replies[comment.id]"
                             class="box-show-comment sub-user-comment">
                            <span class="remove-ment ment-2" ng-if="facebookInfo.id == reply.fb_uid || facebookInfo.id == sup" ng-click="removeReply(reply.id, comment.id)" title="ลบ">x</span>
                            <table>
                                <tr>
                                    <td class="user-comment-img userCommentGame"><img
                                            ng-src="https://graph.facebook.com/{{ reply.fb_uid}}/picture"/></td>
                                    <td class="user-comment-reply">
                                        <span ng-click="openReportDialog(reply.id, 'reply')" class="reportPost"><img src="images/icon/downicon.png"></span>
                                        <b>{{ reply.display_name || reply.displayname || reply.fb_firstname}}</b> {{ reply.message}}
                                        <div class="reply-image-upload" ng-show="reply.thumbnail">
                                            <img ng-src="{{ reply.thumbnail}}"/>
                                        </div>
                                        <span class="time-post">
                                            <ul>
                                                <li>{{ reply.comment_at | timeago }}</li>
                                                <li><img
                                                        ng-click="likeReply(reply.id, comment.id, r_index)"
                                                        src="images/icon/heart.png"/> ({{ reply.like}})

                                                </li>
                                            </ul>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="clear: both;"></div>

                    <!-- reply comment input -->
                    <div class="box-show-comment sub-user-comment reply-box" style="display: none;">
                        <table>
                            <tr>
                                <td class="user-comment-img userCommentGame"><img ng-if="facebookInfo.id"
                                                                                  ng-src="https://graph.facebook.com/{{ facebookInfo.id}}/picture"/></td>
                                <td class="user-comment-reply">

                                    <div ng-click="browsePictureReply(comment.id)" class="upimages-box reply-upload">

                                        <img src="images/icon/cam15.png"
                                             title="แนบรูปภาพ"/></div>

                                    <input ng-model="newReply.message[comment.id]"
                                           ng-click="hasBetOn($event, comment.id)"
                                           ng-enter="addNewReply(comment.id, $event)" type="text"
                                           placeholder="reply..."/>
                                    <span class="loading loading-reply" ng-if="inProgress.reply"><img src="images/icon/loading_icon.gif"/></span>

                                </td>
                            </tr>
                        </table>
                        <div class="img-post-comment" ng-show="newReply.thumbnail">
                            <div ng-click="removeImage('reply')" class="removeImg">x</div>
                            <img class="new-reply-thumbnail" ng-src="{{ newReply.thumbnail}}"/>
                            <div class="tab-reply">
                                <div class="button-post-img" ng-click="addNewReply(comment.id, $event)">โพสต์</div>
                                <div style="clear: both;"></div>
                            </div>
                            <button style="display: none;" class="btn" ng-click="addNewReply(comment.id, $event)">โพสต์</button>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>
            </div>


        </div>

    </div>
</div>



<div style="display: none;">
    <input id="upload-browse" type="file" ng-file-select="uploadPicture($files)">
    <input id="upload-browse-reply" type="file" ng-file-select="uploadPictureReply($files)">
    <button ng-click="upload.abort()">Cancel Upload</button>
</div>


<!-- modal confirm bet -->


<div class="section-confirm-bet" id="team-select-dialog" style="display: none;">
    <div class="modal-backdrop fade in"></div>
    <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: block;">
        <!--<div class="box-close"><img src="images/icon/close.png"/></div>-->
        <div class="wrapper-box-confirm">
            <div class="box-confirm-bet">
                <table>
                    <tr>
                        <td class="photo-user"><img ng-if="facebookInfo.id" ng-src="https://graph.facebook.com/{{ facebookInfo.id}}/picture"></td>
                        <td class="box-user-comment-input">
                            <textarea ng-model="newComment.message" placeholder="แสดงทรรศนะ..."></textarea>
                            <div style="clear: both;"></div>
                            <span class="box-utility">
<!--                                <select ng-model="teamSelect">
                                    <option value="home"> อุรุกวัย (<?php echo Utils::trans('Home'); ?>)</option>
                                    <option value="away"> คาซัคสถาน(<?php echo Utils::trans('Away'); ?>)</option>
                                    </select> Select Team   -->

                                <div class="dropdown" style="float:left;">
                                    <button class="btn dropdown-toggle sr-only" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                        <span ng-if="teamSelect == 'home'"><img src="<?php echo $service_matchInfo->game->h32x32; ?>" style="width: 15px; padding-left: 5px;"> <?php echo (isset($service_allteam->{$service_matchInfo->game->hid}) && isset($service_allteam->{$service_matchInfo->game->hid}->name)) ? $service_allteam->{$service_matchInfo->game->hid}->name : $service_matchInfo->game->homeName; ?> (<?php echo Utils::trans('Home'); ?>)</span>
                                        <span ng-if="teamSelect == 'away'"><img src="<?php echo $service_matchInfo->game->a32x32; ?>" style="width: 15px; padding-left: 5px;"> <?php echo (isset($service_allteam->{$service_matchInfo->game->gid}) && isset($service_allteam->{$service_matchInfo->game->gid}->name)) ? $service_allteam->{$service_matchInfo->game->gid}->name : $service_matchInfo->game->awayName; ?> (<?php echo Utils::trans('Away'); ?>)</span>
                                        <span ng-if="teamSelect == 'draw'"><?php echo Utils::trans('Draw'); ?></span>
                                        <span ng-if="!teamSelect">Please Select Team</span>
                                        <span class="caret"></span>
                                    </button> 
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                        <li style="cursor: pointer;" ng-click="teamSelect = 'home'"><img src="<?php echo $service_matchInfo->game->h32x32; ?>" style="width: 15px; padding-left: 5px;"> <?php echo (isset($service_allteam->{$service_matchInfo->game->hid}) && isset($service_allteam->{$service_matchInfo->game->hid}->name)) ? $service_allteam->{$service_matchInfo->game->hid}->name : $service_matchInfo->game->homeName; ?> (<?php echo Utils::trans('Home'); ?>)</li>
                                        <?php if (!(!empty($service_matchInfo->game->hdp) && $service_matchInfo->game->hdp != '""') && $service_matchInfo->game->hdp . '' !== '0'): ?>
                                            <li role="presentation" class="divider"></li>
                                            <li style="cursor: pointer;" ng-click="teamSelect = 'draw'"><?php echo Utils::trans('Draw'); ?></li>
                                        <?php endif; ?>
                                        <li role="presentation" class="divider"></li>
                                        <li style="cursor: pointer;" ng-click="teamSelect = 'away'"><img src="<?php echo $service_matchInfo->game->a32x32; ?>" style="width: 15px; padding-left: 5px;"> <?php echo (isset($service_allteam->{$service_matchInfo->game->gid}) && isset($service_allteam->{$service_matchInfo->game->gid}->name)) ? $service_allteam->{$service_matchInfo->game->gid}->name : $service_matchInfo->game->awayName; ?> (<?php echo Utils::trans('Away'); ?>)</li>
                                    </ul>
                                </div>

                                <!-- เลือกจำนวนเงินในการเล่น-->
                                <div class="dropdown clearfix" style="float: right; display: snone; margin-right: 8px;">
                                    <img src="images/icon/scoin30.png">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" style="width: 100px;">
                                        <span>{{ scoin}}</span>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu menuCoinSelect" role="menu" aria-labelledby="dropdownMenu2">
                                        <li ng-click="setScoin(item)" ng-repeat="item in systemConfig.ball_game_scoin" role="presentation"><span role="menuitem" tabindex="-1" href="#">{{ item}}</span></li>
<!--                                        <li ng-click="scoin = 200" role="presentation"><span role="menuitem" tabindex="-1" href="#">200</span></li>-->
<!--                                        <li ng-click="scoin = 500" role="presentation"><span role="menuitem" tabindex="-1" href="#">500</span></li>-->
<!--                                        <li ng-click="scoin = 1000" role="presentation"><span role="menuitem" tabindex="-1" href="#">1,000</span></li>-->
<!--                                        <li ng-click="scoin = 1500" role="presentation"><span role="menuitem" tabindex="-1" href="#">1,500</span></li>-->
<!--                                        <li ng-click="scoin = 2000" role="presentation"><span role="menuitem" tabindex="-1" href="#">2,000</span></li>-->
                                    </ul>
                                </div>


                            </span>

                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <span class="Boxloading" ng-if="inProgress.play"><img src="images/icon/loader.gif"/></span>
                <button class="btn btn-info" ng-click="chooseTeam(teamSelect, true, $event)"><?php echo Utils::trans('Confirm'); ?></button>
                <button class="btn" ng-click="chooseTeam(false, false, $event)"><?php echo Utils::trans('Cancel'); ?></button>
            </div>
            <div style="clear: both;"></div>
        </div>
    </div>
</div>

<div onrender="rendered()" class="fb-share-button" data-href="<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>" data-type="button_count"></div>

<div style="display: none;">
    <img src="/WebObject/images/m<?php echo __MID__; ?>.jpg">
</div>

<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>