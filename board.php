<?php
require_once(dirname(__FILE__) . '/_init_.php');

$footerScript .= '<script id="boardScript" src="scripts/board.js" data-id="' . $_REQUEST['id'] . '"></script>';

require_once(__INCLUDE_DIR__ . '/header.php');
?>


<div id="news-top-slide-box" class="wrapper-slide-comment-top" style="display: none;">

    <div class="box-comment-top" ng-repeat="item in newsTopSlide">
        <a href="/news.php?id={{ news.ontimelines[item.newsId]}}">
            <table>
                <tr>
                    <td><img ng-src="{{ item.imageLink}}"></td>
                    <td>
                        <b ng-bind="news.titles[item.newsId]"></b>
                        <span class="detail-news" ng-bind="news.desc[item.newsId]"></span>
                    </td>
                </tr>
            </table>
        </a>
    </div>
</div>


<div class="wrapper-content content-profile">

    <div class="tab-heading-title" id="title-board" style="display: none;">{{ getTopicTypeFromId(forceId).feed_name || 'หัวข้อทั้งหมด'}}</div>
    <div class="wrapper-feed">
        <div class="box-post-to-wall">
            <div class="feed-box">
                <div class="img-post-wall">
                    <img src="images/icon/post.png"></img>
                </div>
            </div>
            <div class="feed-head-comment-wall">
                <table>
                    <tr>
                        <td class="comment-to-wall">
                            <div class="wrapper-ultility">
                                <div class="img-profile">
<!--                                    <img style="width: 25px; height: 25px;" ng-if="facebookInfo.id" ng-src="https://graph.facebook.com/{{ facebookInfo.id}}/picture"/>-->
                                    <img style="width: 25px; height: 25px;" ng-if="facebookInfo.id" ng-src="http://api.ssporting.com/cache/small/{{ facebookInfo.id}}.jpg"/>
                                </div>
                                <div class="tab-utility">
                                    <b>โพสต์ข้อความ</b>
                                    <img ng-click="browsePictureNewTopic()" src="images/icon/cam15.png" title="แนบรูปภาพ"> <span
                                        ng-click="browsePictureNewTopic()">เพิ่มรูปภาพ</span>
                                    <img src="images/icon/video.png" title="แนบวีดีโอ" ng-click="toggleVideoLinkBox()"> <span
                                        ng-click="toggleVideoLinkBox()">เพิ่มวีดีโอ</span> <img src="images/icon/icon_youtube_sm.png"> <img src="images/icon/dailymotion.png">
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="tabPost">
                                <table>
                                    <tr>
                                        <td>หัวข้อ :</td>
                                        <td><input ng-model="newTopic.title" type="text" placeholder="title"></td>
                                    </tr>
                                    <tr>
                                        <td>รายละเอียด :</td>
<!--                                        <td><textarea class="" ng-focus="focusBox($event)" ng-blur="blurBox($event)" type="text"
                                                      placeholder="comment..." xng-enter="addNewTopic()"
                                                      ng-model="newTopic.message">
                                            </textarea></td>-->
                                        <td>
                                            <textarea id="txtarea"n class="myclass" spellcheck="false"  ng-blur="blurBox($event)" type="text"
                                                      placeholder="comment..." xng-enter="addNewTopic()"
                                                      ng-model="newTopic.message">
                                            </textarea>
                                        </td>
                                    </tr>
                                    <tr ng-show="videoLinkBox">
                                        <td>Video:</td>
                                        <td><input ng-model="newTopic.video" type="text" placeholder="Video url"> </td>
                                    </tr>
                                </table>
                                <!--<textarea class="" ng-focus="focusBox($event)" ng-blur="blurBox($event)" type="text" placeholder="comment..." xng-enter="addNewTimeline()"
                                          ng-model="newTimeline.message">
                                </textarea>-->
                            </div>
                            <div class="tab-utility2">
                                <div class="titleType"><b>ประเภท :</b>
                                    <select ng-model="selectedTopicType" ng-options="o.feed_name for o in topicType" class="span2">
                                    </select>
                                </div>
                                <span class="loading-post" ng-if="inProgress.newTopic"><img
                                        src="images/icon/loading_icon.gif"></span>

                                <div class="buttons-post" ng-click="addNewTopic()" class="btn btn-small btn-primary">โพสต์
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                        </td>
                        <td class="post-to-wall" style="display: none;">
                            <span class="loading-post" ng-if="inProgress.newTopic"><img
                                    src="images/icon/loading_icon.gif"></span>
                            <button ng-click="addNewTimeline()" class="btn btn-small btn-primary">โพสต์</button>
                        </td>
                    </tr>
                </table>
                <div>
                    <!--<span ng-if="inProgress.uploadTimeline">......</span>-->
                    <div class="image-post-to-wall" ng-repeat="(idx, img) in newTopic.thumbnail">
                        <span class="delete-img-postwall" ng-click="removeNewTopicUploadPicture(idx)">x</span>
                        <img ng-src="{{ img}}"/>
                    </div>
                    <div ng-show="isVideo(newTopic.video)">
                        <a href="{{ newTopic.video}}" embed-video width="500" height="290"></a>
                    </div>
                </div>
            </div>
            <!--<div style="display: none;">-->
            <!--<img ng-src="{{ newTimeline.thumbnail }}"/>-->
            <!--</div>-->
            <div style="clear: both;"></div>
        </div>

        <div ng-if="topics.recommend.timelines.length > 0 && (forceId == '4' || forceId == '5' || forceId == '6' || forceId == '8')" ng-show="activeTopicType.length === 0" id="box-recommend-topics" style="display: none;">
            <div class="box">
                <img src="images/icon/hot.png"> <b>หัวข้อแนะนำ</b>
            </div>

            <div class="box-recommend">
                <div class="box recommend1">
                    <div class="imageRecommend"> <a ng-if="recommendTopics[0].owner.content_type !== 'game'" href="news.php?id={{ recommendTopics[0].owner.id}}"><img ng-src="{{ getTopicCover(recommendTopics[0])}}"></a>
                        <a ng-if="recommendTopics[0].owner.content_type === 'game'" href="game.php?mid={{ recommendTopics[0].content[0].mid}}"><img ng-src="{{ getTopicCover(recommendTopics[0])}}"></a></div>

                    <div class="infoBox">
                        <div ng-if="recommendTopics[0].owner.content_type === 'game'">
                            <b>{{ recommendTopics[0].content[0].homeName<?php echo ucfirst(__LANGUAGE__); ?>}} {{ recommendTopics[0].content[0].hdp_home}} ({{ recommendTopics[0].content[0].hdp}}) {{ recommendTopics[0].content[0].hdp_away}} {{ recommendTopics[0].content[0].awayName<?php echo ucfirst(__LANGUAGE__); ?>}}</b>
                            {{ recommendTopics[0].content[0].betdata.betlist[0].message}}
                        </div>
                        <div ng-if="recommendTopics[0].owner.content_type === 'board'">
                            <b>{{ recommendTopics[0].content[0].title}}</b>
                            {{ recommendTopics[0].content[0].desc}}
                        </div>
                        <div ng-if="recommendTopics[0].owner.content_type === 'status'">
                            <b>{{ recommendTopics[0].content[0].message}}</b>
                        </div>
                        <div ng-if="recommendTopics[0].owner.content_type === 'image'">

                        </div>
                        <div ng-if="recommendTopics[0].owner.content_type === 'news'">
                            <b>{{ recommendTopics[0].content[0].title<?php echo ucfirst(__LANGUAGE__); ?>}}</b>
                            {{ recommendTopics[0].content[0].shortDescription<?php echo ucfirst(__LANGUAGE__); ?>}}
                        </div>
                        <div ng-if="recommendTopics[0].owner.content_type === 'highlight'">
                            <b>{{ recommendTopics[0].content[0].title}}</b>
                        </div>
                    </div>
                    <div class="userPost-info">
                        <span ng-if="recommendTopics[0].owner.content_type !== 'news' && recommendTopics[0].owner.content_type !== 'highlight'">
<!--                            <img ng-src="https://graph.facebook.com/{{ recommendTopics[0].owner.fb_uid}}/picture">-->
                            <img ng-src="http://api.ssporting.com/cache/small/{{ recommendTopics[0].owner.fb_uid}}.jpg">
                            <b>{{ recommendTopics[0].owner.display_name || recommendTopics[0].owner.fb_firstname }}
                        </span>
                        </b>{{ recommendTopics[0].owner.created_at | timeagoByDateTime }}
                    </div>
                </div>
                <div class="box recommend2">
                    <div class="imageRecommend"> <a href="news.php?id={{ recommendTopics[1].owner.id}}">
                            <img ng-src="{{ getTopicCover(recommendTopics[1])}}">
                        </a></div>

                    <div class="infoBox">
                        <div ng-if="recommendTopics[1].owner.content_type === 'game'">
                            <b>{{ recommendTopics[1].content[0].homeName<?php echo ucfirst(__LANGUAGE__); ?>}} {{ recommendTopics[1].content[0].hdp_home}} ({{ recommendTopics[1].content[0].hdp}}) {{ recommendTopics[1].content[0].hdp_away}} {{ recommendTopics[1].content[0].awayName<?php echo ucfirst(__LANGUAGE__); ?>}}</b>
                            {{ recommendTopics[1].content[0].betdata.betlist[0].message}}
                        </div>
                        <div ng-if="recommendTopics[1].owner.content_type === 'board'">
                            <b>{{ recommendTopics[1].content[0].title}}</b>
                            {{ recommendTopics[1].content[0].desc}}
                        </div>
                        <div ng-if="recommendTopics[1].owner.content_type === 'status'">
                            <b>{{ recommendTopics[1].content[0].message}}</b>
                        </div>
                        <div ng-if="recommendTopics[1].owner.content_type === 'image'">

                        </div>
                        <div ng-if="recommendTopics[1].owner.content_type === 'news'">
                            <b>{{ recommendTopics[1].content[0].title<?php echo ucfirst(__LANGUAGE__); ?>}}</b>
                            {{ recommendTopics[1].content[0].shortDescription<?php echo ucfirst(__LANGUAGE__); ?>}}
                        </div>
                        <div ng-if="recommendTopics[0].owner.content_type === 'highlight'">
                            <b>{{ recommendTopics[1].content[0].title}}</b>
                        </div>
                    </div>
                    <div class="userPost-info">
                        <span ng-if="recommendTopics[1].owner.content_type !== 'news' && recommendTopics[1].owner.content_type !== 'highlight'">
<!--                            <img ng-src="https://graph.facebook.com/{{ recommendTopics[1].owner.fb_uid}}/picture">-->
                            <img ng-src="http://api.ssporting.com/cache/small/{{ recommendTopics[1].owner.fb_uid}}.jpg">
                            <b>{{ recommendTopics[1].owner.display_name || recommendTopics[1].owner.fb_firstname }}
                        </span>
                        </b>{{ recommendTopics[1].owner.created_at | timeagoByDateTime }}
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>

        <div ng-if="topics.recommend.timelines.length > 0 && (forceId == '2' || forceId == '3' || forceId == '7')">
            <div class="box">
                <img src="images/icon/hot.png"> <b>หัวข้อแนะนำ</b>
            </div>

            <div style="display: none;" id="topics-recommend"  ng-if="item.owner.content_type !== 'game'" ng-repeat="(idx, item) in topics.recommend.timelines" class="ForumWrap">
                <div class="box-feedForum">
                    <div class="feed-situations">
                        <div class="img-user-feed">
                            <a ng-if="item.owner.content_type === 'board' || item.owner.content_type === 'status' || item.owner.content_type === 'image'" href="profile.php?id={{ item.owner.fb_uid}}">
<!--                                <img ng-src="https://graph.facebook.com/{{ item.owner.fb_uid}}/picture">-->
                                <img ng-src="http://api.ssporting.com/cache/small/{{ item.owner.fb_uid}}.jpg">
                            </a>
                            <span ng-if="item.owner.content_type === 'news'"><img src="images/icon/news.png"></span>
                            <span ng-if="item.owner.content_type === 'highlight'"><img src="images/icon/video-icon.png"></span>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                    <div class="tableForum">
                        <table ng-if="item.owner.content_type === 'board'">
                            <tr>
                                <td>
                                    <div class="titleFor"><a href="news.php?id={{ item.owner.id}}">{{ item.content[0].title}}</a></div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span> 
                                    <span ng-click="openReportDialog(item.owner.id)" class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="postNewsForum"><a href="news.php?id={{ item.owner.id}}"><span ng-bind-html="item.content[0].desc | nl2br"></span></div>
                                </td>
                            </tr>
                            <tr ng-if="hasVideo(item.content[0].medialist)">
                                <td>
                                    <a href="{{ item.content[0].medialist[0].path}}" embed-video width="500" height="290"></a>
                                </td>
                            </tr>
                            <tr ng-if="hasImage(item.content[0].medialist)">
                                <td>
                                    <div class="wrapper-showimg-wall">
                                        <a href="/news.php?id={{ item.owner.id}}">
                                            <div ng-if="item.content[0].medialist.length === 1" class="one-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                            </div>

                                            <div ng-if="item.content[0].medialist.length === 2" class="two-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[1].path}}"></span>
                                            </div>

                                            <div ng-if="item.content[0].medialist.length >= 3" class="three-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[1].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[2].path}}"></span>
                                            </div>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}}
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'status'">
                            <tr>
                                <td>
                                    <div class="titleFor"><a href="news.php?id={{ item.owner.id}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span> 
                                    <span ng-click="openReportDialog(item.owner.id)" class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="postNewsForum"><a href="news.php?id={{ item.owner.id}}"><span ng-bind-html="item.content[0].message | nl2br"></span></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}}
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'image'">
                            <tr>
                                <td>
                                    <div class="titleFor"><a href="news.php?id={{ item.owner.id}}">{{ item.content[0].title}}</a></div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span>
                                    <span ng-click="openReportDialog(item.owner.id)" class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="wrapper-showimg-wall">
                                        <a href="/news.php?id={{ item.owner.id}}">
                                            <div ng-if="item.content.length === 1" class="one-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                            </div>

                                            <div ng-if="item.content.length === 2" class="two-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[1].path}}"></span>
                                            </div>

                                            <div ng-if="item.content.length >= 3" class="three-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[1].path}}"></span>
                                                <span><img ng-src="{{ item.content[2].path}}"></span>
                                            </div>
                                        </a>

                                        <div style="clear: both;"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}}
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'news'" class="news-section">
                            <tbody>
                                <tr>
                                    <td><span class="font-name user-name-feed">Ssporting.com</span></td>
                                    <td class="time-to-post ng-binding">{{ item.owner.created_at | timeagoByDateTime }}</td>
                                </tr>
                                <tr>
                                    <td class="img-news" style="width: 5%;"><a href="/news.php?id={{ item.owner.id}}"><img ng-src="{{ item.content[0].imageLink}}"/> </a></td>

                                    <td class="detail-newss">
                                        <div class="title-news"><a href="news.php?id={{ item.owner.id}}" class="ng-binding">{{ item.content[0].title<?php echo ucfirst(__LANGUAGE__); ?>}}</a>
                                        </div>
                                        <span ng-bind-html="item.content[0].title<?php echo ucfirst(__LANGUAGE__); ?> | toTrusted"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="section-like-post-wall ng-binding">
                                        <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | <a href="news.php?id={{ item.owner.id}}">ดูรายละเอียดข่าว</a> | <a href="news.php?id={{ item.owner.id}}">แสดงความคิดเห็น</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table ng-if="item.owner.content_type === 'highlight'">
                            <tr>
                                <td>
                                    <div class="titleFor">{{ item.content[0].title}}</div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span>
                                    <span class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ item.content[0].content}}" embed-video width="500" height="290"></a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="section-like-post-wall ng-binding">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | <a href="news.php?id={{ item.owner.id}}">ดูรายละเอียดข่าว</a> | <a href="news.php?id={{ item.owner.id}}">แสดงความคิดเห็น</a>

                                    <div class="postBy"><span ng-if="item.owner.display_name || item.owner.fb_firstname" class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a> |</span> {{ getTopicTypeFromId(item.owner.type).feed_name}}</div>
                                </td>
                            </tr>
                        </table>
                        <div ng-if="item.owner.content_type === 'game'" class="wrapper-feed-wall">
                            <a href="game.php?mid={{ item.content[0].mid}}">
                                <div class="feed-situations">
                                    <div class="img-user-feed">
                                        <img ng-src="/images/countries/{{ item.content[0].competitionId}}.png">
                                        <div xng-show="item.content[0].hdp"><img src="images/icon/bet2.png" style="width: 30px; height: 30px;">
                                            <br/> <b>{{ item.content[0].hdp}}</b><br/>HDP
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>
                                </div>
                                <div class="tableGames">
                                    <table>

                                        <tr class="head-feed">
                                            <td class="time-date-post">{{ item.content[0].show_date | formatDate:'DD MMMM YYYY HH:mm'}}</td>
                                            <td class="league-name-title">{{ allLeagues[item.content[0]._lid].name || item.content[0].LeagueName}}</td>
                                        </tr>
                                        <span class="remove-comment" ng-if="item.owner.fb_uid == facebookInfo.id" ng-click="removeTimeline(item.owner.id, $event)">x</span>
                                        <tr class="teamplay">
                                            <td colspan="2" class="team-playing">

                                                <table>
                                                    <tr>
                                                        <td class="sec-team1"><img ng-src="{{ item.content[0]['h256x256']}}"/></td>
                                                        <td class="hdp-match">
                                                            <!-- {{ allLeagues[item.content[0]._lid].name || item.content[0].LeagueName}}-->
                                                            <!--<br/> {{ item.content[0].show_date | formatDate:'DD MMMM YYYY HH:mm'}}-->
                                                            <span class="success-score" ng-if="item.content[0].sid != '1'">
                                                                <span>{{ item.content[0].score | scoreSelect:'home' }}</span>
                                                                <span>:</span>
                                                                <span>{{ item.content[0].score | scoreSelect:'away' }}</span>
                                                            </span>
                                                            <span class="hdp-game" ng-if="item.content[0].sid == '1'">
                                                                <b>{{item.content[0].hdp}}</b><br/> <span class="macth-hdp">({{ item.content[0].hdp_home}}) : ({{ item.content[0].hdp_away}})</span>
                                                            </span>
                                                        </td>
                                                        <td class="sec-team2"><img ng-src="{{ item.content[0]['a256x256']}}"/> </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table>
                                                                <tr>
                                                                    <td class="teams1">
                                                                        <div class="boxTeam teamLeft">
                                                                            <b ng-bind="parseTeamStat(item.content[0].stat, item.content[0].hid, 'number')">[1]</b> <span class="displayNameTeam" ng-class="{'team-favored': item.content[0].hdp < 0}">{{ allTeams[item.content[0].hid].name || item.content[0].homeName }}</span>
                                                                        </div>
                                                                        <div style="clear: both;"></div>
                                                                        <div class="last-stat-fivematch" ng-init="ssh = parseTeamStat(item.content[0].stat, item.content[0].hid, 'lastplay')">
                                                                            <span ng-repeat="s in ssh track by $index" ng-switch="s">
                                                                                <span ng-switch-when="w" class="text-green">W</span>
                                                                                <span ng-switch-when="l" class="text-red">L</span>
                                                                                <span ng-switch-when="d" class="text-black">D</span>
                                                                                <span ng-if="$index < (ssh.length - 1)"> | </span>
                                                                            </span>
                                                                        </div>
                                                                    </td>
                                                                    <td class="vs vss">
                                                                        <span ng-if="item.content[0].sid == '1'">
                                                                            <a ng-if="canPlay(item.content[0].mid, item.content[0].sid, item.content[0].hdp)" href="/game.php?mid={{ item.content[0].mid}}"><img src="images/icon/bet-play.png"></a>
                                                                            <a ng-if="!canPlay(item.content[0].mid, item.content[0].sid, item.content[0].hdp)" href="/game.php?mid={{ item.content[0].mid}}"><img src="images/icon/bet-view.png"></a>
                                                                        </span>
                                                                    </td>
                                                                    <td class="teams2">
                                                                        <div class="boxTeam teamRight">
                                                                            <span class="displayNameTeam" ng-class="{'team-favored': item.content[0].hdp > 0}">{{ allTeams[item.content[0].gid].name || item.content[0].awayName }}</span><b ng-bind="parseTeamStat(item.content[0].stat, item.content[0].gid, 'number')">[3]</b>
                                                                        </div>
                                                                        <div style="clear: both;"></div>
                                                                        <div class="last-stat-fivematch" ng-init="ssg = parseTeamStat(item.content[0].stat, item.content[0].gid, 'lastplay')">
                                                                            <span ng-repeat="s in ssg track by $index" ng-switch="s">
                                                                                <span ng-switch-when="w" class="text-green">W</span>
                                                                                <span ng-switch-when="l" class="text-red">L</span>
                                                                                <span ng-switch-when="d" class="text-black">D</span>
                                                                                <span ng-if="$index < (ssg.length - 1)"> | </span>
                                                                            </span>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </a>
                        </div>


                    </div>
                    <div style="clear: both;"></div>


                    <div class="wrapper-box-feed-comment">
                        <div class="box-show-comment">
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="comment">
                                            <div class="upimages-box">
                                                <a href="news.php?id={{ item.owner.id}}">
                                                    <img src="images/icon/cam15.png" title="แนบรูปภาพ"></div>
<!--                                                    <img style="width: 25px; height: 25px;" ng-src="https://graph.facebook.com/{{ facebookInfo.id}}/picture">-->
                                                    <img style="width: 25px; height: 25px;" ng-src="http://api.ssporting.com/cache/small/{{ facebookInfo.id}}.jpg">
                                                    <input type="text" placeholder="comment...">
                                                    <span class="hover"></span>
                                                </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
                <div style="clear: both;"></div>
            </div>
        </div>


        <div class="box">
            <img src="images/icon/post-all.png"> <b id="title-all" style="display: none;">{{ getTopicTypeFromId(forceId).feed_name || 'หัวข้อทั้งหมด'}}</b>
        </div>

        <div class="wrapForums" id="wrapForums" style="display: none;">
            <div class="tab-situations forumLine"></div>
<!--            <div ng-if="item.owner.content_type !== 'game'" ng-repeat="(idx, item) in topics.timelines" class="ForumWrap">-->

            <div ng-repeat="(idx, item) in topics.timelines" class="ForumWrap">
                <div class="box-feedForum">
                    <div class="feed-situations">
                        <div class="img-user-feed">
                            <a ng-if="item.owner.content_type === 'board' || item.owner.content_type === 'status' || item.owner.content_type === 'image'" href="profile.php?id={{ item.owner.fb_uid}}">
<!--                                <img ng-src="https://graph.facebook.com/{{ item.owner.fb_uid}}/picture">-->
                                <img ng-src="http://api.ssporting.com/cache/small/{{ item.owner.fb_uid}}.jpg">
                            </a>
                            <span ng-if="item.owner.content_type === 'news'"><img src="images/icon/news.png"></span>
                            <span ng-if="item.owner.content_type === 'highlight'"><img src="images/icon/video-icon.png"></span>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                    <div class="tableForum">
                        <table ng-if="item.owner.content_type === 'board'">
                            <tr>
                                <td>
                                    <div class="titleFor"><a href="news.php?id={{ item.owner.id}}">{{ item.content[0].title}}</a></div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span>
                                    <span ng-click="openReportDialog(item.owner.id)" class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="postNewsForum"><a href="news.php?id={{ item.owner.id}}"><span ng-bind-html="item.content[0].desc | nl2br"></span></div>
                                </td>
                            </tr>
                            <tr ng-if="hasVideo(item.content[0].medialist)">
                                <td>
                                    <a href="{{ item.content[0].medialist[0].path}}" embed-video width="500" height="290"></a>
                                </td>
                            </tr>
                            <tr ng-if="hasImage(item.content[0].medialist)">
                                <td>
                                    <div class="wrapper-showimg-wall">
                                        <a href="/news.php?id={{ item.owner.id}}">
                                            <div ng-if="item.content[0].medialist.length === 1" class="one-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                            </div>

                                            <div ng-if="item.content[0].medialist.length === 2" class="two-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[1].path}}"></span>
                                            </div>

                                            <div ng-if="item.content[0].medialist.length >= 3" class="three-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[1].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[2].path}}"></span>
                                            </div>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}}
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'status'">
                            <tr>
                                <td>
                                    <div class="postNewsForum"><a href="news.php?id={{ item.owner.id}}"><span ng-bind-html="item.content[0].message | nl2br"></span></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}} | {{ item.owner.created_at | timeagoByDateTime }}  <span ng-click="openReportDialog(item.owner.id)" class="reportPost boxRep"><img src="images/icon/downicon.png"></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'image'">
                            <tr>
                                <td>
                                    <div class="wrapper-showimg-wall">
                                        <a href="/news.php?id={{ item.owner.id}}">
                                            <div ng-if="item.content.length === 1" class="one-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                            </div>

                                            <div ng-if="item.content.length === 2" class="two-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[1].path}}"></span>
                                            </div>

                                            <div ng-if="item.content.length >= 3" class="three-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[1].path}}"></span>
                                                <span><img ng-src="{{ item.content[2].path}}"></span>
                                            </div>
                                        </a>

                                        <div style="clear: both;"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                       <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}} | {{ item.owner.created_at | timeagoByDateTime }}  <span ng-click="openReportDialog(item.owner.id)" class="reportPost boxRep"><img src="images/icon/downicon.png"></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'news'" class="news-section">
                            <tbody>
                                <tr>
                                    <td><span class="font-name user-name-feed">Ssporting.com</span></td>
                                    <td class="time-to-post ng-binding">{{ item.owner.created_at | timeagoByDateTime }}</td>
                                </tr>
                                <tr>
                                    <td class="img-news" style="width: 5%;"><a href="/news.php?id={{ item.owner.id}}"><img ng-src="{{ item.content[0].imageLink}}"/> </a></td>

                                    <td class="detail-newss">
                                        <div class="title-news"><a href="news.php?id={{ item.owner.id}}" class="ng-binding">{{ item.content[0].title<?php echo ucfirst(__LANGUAGE__); ?>}}</a>
                                        </div>
                                        <span ng-bind-html="item.content[0].shortDescription<?php echo ucfirst(__LANGUAGE__); ?> | toTrusted"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="section-like-post-wall ng-binding">
                                        <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | <a href="news.php?id={{ item.owner.id}}">ดูรายละเอียดข่าว</a> | <a href="news.php?id={{ item.owner.id}}">แสดงความคิดเห็น</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table ng-if="item.owner.content_type === 'highlight'">
                            <tr>
                                <td>
                                    <div class="titleFor">{{ item.content[0].title}}</div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span>
                                    <span ng-click="openReportDialog(item.owner.id)" class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ item.content[0].content}}" embed-video width="500" height="290"></a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="section-like-post-wall ng-binding">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | <a href="news.php?id={{ item.owner.id}}">ดูรายละเอียดข่าว</a> | <a href="news.php?id={{ item.owner.id}}">แสดงความคิดเห็น</a>

                                    <div class="postBy"><span ng-if="item.owner.display_name || item.owner.fb_firstname" class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a> |</span> {{ getTopicTypeFromId(item.owner.type).feed_name}}</div>
                                </td>
                            </tr>
                        </table>
                        <div ng-if="item.owner.content_type === 'game'" class="wrapper-feed-wall">
                            <a href="game.php?mid={{ item.content[0].mid}}">
                                <div class="feed-situations">
                                    <div class="img-user-feed">
                                        <img ng-src="/images/countries/{{ item.content[0].competitionId}}.png">
                                        <div xng-show="item.content[0].hdp"><img src="images/icon/bet2.png" style="width: 30px; height: 30px;">
                                            <br/> <b>{{ item.content[0].hdp}}</b><br/>HDP
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>
                                </div>
                                <div class="tableGames">
                                    <table>

                                        <tr class="head-feed">
                                            <td class="time-date-post">{{ item.content[0].show_date | formatDate:'DD MMMM YYYY HH:mm'}}</td>
                                            <td class="league-name-title">{{ allLeagues[item.content[0]._lid].name || item.content[0].LeagueName}}</td>
                                        </tr>
                                        <span class="remove-comment" ng-if="item.owner.fb_uid == facebookInfo.id" ng-click="removeTimeline(item.owner.id, $event)">x</span>
                                        <tr class="teamplay">
                                            <td colspan="2" class="team-playing">

                                                <table>
                                                    <tr>
                                                        <td class="sec-team1"><img ng-src="{{ item.content[0]['h256x256']}}"/></td>
                                                        <td class="hdp-match">
                                                            <!-- {{ allLeagues[item.content[0]._lid].name || item.content[0].LeagueName}}-->
                                                            <!--<br/> {{ item.content[0].show_date | formatDate:'DD MMMM YYYY HH:mm'}}-->
                                                            <span class="success-score" ng-if="item.content[0].sid != '1'">
                                                                <span>{{ item.content[0].score | scoreSelect:'home' }}</span>
                                                                <span>:</span>
                                                                <span>{{ item.content[0].score | scoreSelect:'away' }}</span>
                                                            </span>
                                                            <span class="hdp-game" ng-if="item.content[0].sid == '1'">
                                                                <b>{{item.content[0].hdp}}</b><br/> <span class="macth-hdp">({{ item.content[0].hdp_home}}) : ({{ item.content[0].hdp_away}})</span>
                                                            </span>
                                                        </td>
                                                        <td class="sec-team2"><img ng-src="{{ item.content[0]['a256x256']}}"/> </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table>
                                                                <tr>
                                                                    <td class="teams1">
                                                                        <div class="boxTeam teamLeft">
                                                                            <b ng-bind="parseTeamStat(item.content[0].stat, item.content[0].hid, 'number')">[1]</b> <span class="displayNameTeam" ng-class="{'team-favored': item.content[0].hdp < 0}">{{ allTeams[item.content[0].hid].name || item.content[0].homeName }}</span>
                                                                        </div>
                                                                        <div style="clear: both;"></div>
                                                                        <div class="last-stat-fivematch" ng-init="ssh = parseTeamStat(item.content[0].stat, item.content[0].hid, 'lastplay')">
                                                                            <span ng-repeat="s in ssh track by $index" ng-switch="s">
                                                                                <span ng-switch-when="w" class="text-green">W</span>
                                                                                <span ng-switch-when="l" class="text-red">L</span>
                                                                                <span ng-switch-when="d" class="text-black">D</span>
                                                                                <span ng-if="$index < (ssh.length - 1)"> | </span>
                                                                            </span>
                                                                        </div>
                                                                    </td>
                                                                    <td class="vs vss">
                                                                        <span ng-if="item.content[0].sid == '1'">
                                                                            <a ng-if="canPlay(item.content[0].mid, item.content[0].sid, item.content[0].hdp)" href="/game.php?mid={{ item.content[0].mid}}"><img src="images/icon/bet-play.png"></a>
                                                                            <a ng-if="!canPlay(item.content[0].mid, item.content[0].sid, item.content[0].hdp)" href="/game.php?mid={{ item.content[0].mid}}"><img src="images/icon/bet-view.png"></a>
                                                                        </span>
                                                                    </td>
                                                                    <td class="teams2">
                                                                        <div class="boxTeam teamRight">
                                                                            <span class="displayNameTeam" ng-class="{'team-favored': item.content[0].hdp > 0}">{{ allTeams[item.content[0].gid].name || item.content[0].awayName }}</span><b ng-bind="parseTeamStat(item.content[0].stat, item.content[0].gid, 'number')">[3]</b>
                                                                        </div>
                                                                        <div style="clear: both;"></div>
                                                                        <div class="last-stat-fivematch" ng-init="ssg = parseTeamStat(item.content[0].stat, item.content[0].gid, 'lastplay')">
                                                                            <span ng-repeat="s in ssg track by $index" ng-switch="s">
                                                                                <span ng-switch-when="w" class="text-green">W</span>
                                                                                <span ng-switch-when="l" class="text-red">L</span>
                                                                                <span ng-switch-when="d" class="text-black">D</span>
                                                                                <span ng-if="$index < (ssg.length - 1)"> | </span>
                                                                            </span>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div style="clear: both;"></div>


                    <div class="wrapper-box-feed-comment">
                        <div class="box-show-comment">
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="comment">
                                            <div class="upimages-box">
                                                <a href="news.php?id={{ item.owner.id}}">
                                                    <img src="images/icon/cam15.png" title="แนบรูปภาพ"></div>
<!--                                                    <img style="width: 25px; height: 25px;" ng-src="https://graph.facebook.com/{{ facebookInfo.id}}/picture">-->
                                                    <img style="width: 25px; height: 25px;" ng-src="http://api.ssporting.com/cache/small/{{ facebookInfo.id}}.jpg">
                                                    <input type="text" placeholder="comment...">
                                                    <span class="hover"></span>
                                                </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div ng-repeat="(idx, item) in topics1.timelines" class="ForumWrap">
                <div class="box-feedForum">
                    <div class="feed-situations">
                        <div class="img-user-feed">
                            <a ng-if="item.owner.content_type === 'board' || item.owner.content_type === 'status' || item.owner.content_type === 'image'" href="profile.php?id={{ item.owner.fb_uid}}">
                                <!--                                <img ng-src="https://graph.facebook.com/{{ item.owner.fb_uid}}/picture">-->
                                <img ng-src="http://api.ssporting.com/cache/small/{{ item.owner.fb_uid}}.jpg">
                            </a>
                            <span ng-if="item.owner.content_type === 'news'"><img src="images/icon/news.png"></span>
                            <span ng-if="item.owner.content_type === 'highlight'"><img src="images/icon/video-icon.png"></span>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                    <div class="tableForum">
                        <table ng-if="item.owner.content_type === 'board'">
                            <tr>
                                <td>
                                    <div class="titleFor"><a href="news.php?id={{ item.owner.id}}">{{ item.content[0].title}}</a></div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span>
                                    <span ng-click="openReportDialog(item.owner.id)" class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="postNewsForum"><a href="news.php?id={{ item.owner.id}}"><span ng-bind-html="item.content[0].desc | nl2br"></span></div>
                                </td>
                            </tr>
                            <tr ng-if="hasVideo(item.content[0].medialist)">
                                <td>
                                    <a href="{{ item.content[0].medialist[0].path}}" embed-video width="500" height="290"></a>
                                </td>
                            </tr>
                            <tr ng-if="hasImage(item.content[0].medialist)">
                                <td>
                                    <div class="wrapper-showimg-wall">
                                        <a href="/news.php?id={{ item.owner.id}}">
                                            <div ng-if="item.content[0].medialist.length === 1" class="one-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                            </div>

                                            <div ng-if="item.content[0].medialist.length === 2" class="two-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[1].path}}"></span>
                                            </div>

                                            <div ng-if="item.content[0].medialist.length >= 3" class="three-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[1].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[2].path}}"></span>
                                            </div>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}}
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'status'">
                            <tr>
                                <td>
                                    <div class="postNewsForum"><a href="news.php?id={{ item.owner.id}}"><span ng-bind-html="item.content[0].message | nl2br"></span></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}} | {{ item.owner.created_at | timeagoByDateTime }}  <span ng-click="openReportDialog(item.owner.id)" class="reportPost boxRep"><img src="images/icon/downicon.png"></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'image'">
                            <tr>
                                <td>
                                    <div class="wrapper-showimg-wall">
                                        <a href="/news.php?id={{ item.owner.id}}">
                                            <div ng-if="item.content.length === 1" class="one-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                            </div>

                                            <div ng-if="item.content.length === 2" class="two-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[1].path}}"></span>
                                            </div>

                                            <div ng-if="item.content.length >= 3" class="three-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[1].path}}"></span>
                                                <span><img ng-src="{{ item.content[2].path}}"></span>
                                            </div>
                                        </a>

                                        <div style="clear: both;"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}} | {{ item.owner.created_at | timeagoByDateTime }}  <span ng-click="openReportDialog(item.owner.id)" class="reportPost boxRep"><img src="images/icon/downicon.png"></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'news'" class="news-section">
                            <tbody>
                            <tr>
                                <td><span class="font-name user-name-feed">Ssporting.com</span></td>
                                <td class="time-to-post ng-binding">{{ item.owner.created_at | timeagoByDateTime }}</td>
                            </tr>
                            <tr>
                                <td class="img-news" style="width: 5%;"><a href="/news.php?id={{ item.owner.id}}"><img ng-src="{{ item.content[0].imageLink}}"/> </a></td>

                                <td class="detail-newss">
                                    <div class="title-news"><a href="news.php?id={{ item.owner.id}}" class="ng-binding">{{ item.content[0].title<?php echo ucfirst(__LANGUAGE__); ?>}}</a>
                                    </div>
                                    <span ng-bind-html="item.content[0].shortDescription<?php echo ucfirst(__LANGUAGE__); ?> | toTrusted"></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="section-like-post-wall ng-binding">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | <a href="news.php?id={{ item.owner.id}}">ดูรายละเอียดข่าว</a> | <a href="news.php?id={{ item.owner.id}}">แสดงความคิดเห็น</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table ng-if="item.owner.content_type === 'highlight'">
                            <tr>
                                <td>
                                    <div class="titleFor">{{ item.content[0].title}}</div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span>
                                    <span ng-click="openReportDialog(item.owner.id)" class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ item.content[0].content}}" embed-video width="500" height="290"></a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="section-like-post-wall ng-binding">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | <a href="news.php?id={{ item.owner.id}}">ดูรายละเอียดข่าว</a> | <a href="news.php?id={{ item.owner.id}}">แสดงความคิดเห็น</a>

                                    <div class="postBy"><span ng-if="item.owner.display_name || item.owner.fb_firstname" class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a> |</span> {{ getTopicTypeFromId(item.owner.type).feed_name}}</div>
                                </td>
                            </tr>
                        </table>
                        <div ng-if="item.owner.content_type === 'game'" class="wrapper-feed-wall">
                            <a href="game.php?mid={{ item.content[0].mid}}">
                                <div class="feed-situations">
                                    <div class="img-user-feed">
                                        <img ng-src="/images/countries/{{ item.content[0].competitionId}}.png">
                                        <div xng-show="item.content[0].hdp"><img src="images/icon/bet2.png" style="width: 30px; height: 30px;">
                                            <br/> <b>{{ item.content[0].hdp}}</b><br/>HDP
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>
                                </div>
                                <div class="tableGames">
                                    <table>

                                        <tr class="head-feed">
                                            <td class="time-date-post">{{ item.content[0].show_date | formatDate:'DD MMMM YYYY HH:mm'}}</td>
                                            <td class="league-name-title">{{ allLeagues[item.content[0]._lid].name || item.content[0].LeagueName}}</td>
                                        </tr>
                                        <span class="remove-comment" ng-if="item.owner.fb_uid == facebookInfo.id" ng-click="removeTimeline(item.owner.id, $event)">x</span>
                                        <tr class="teamplay">
                                            <td colspan="2" class="team-playing">

                                                <table>
                                                    <tr>
                                                        <td class="sec-team1"><img ng-src="{{ item.content[0]['h256x256']}}"/></td>
                                                        <td class="hdp-match">
                                                            <!-- {{ allLeagues[item.content[0]._lid].name || item.content[0].LeagueName}}-->
                                                            <!--<br/> {{ item.content[0].show_date | formatDate:'DD MMMM YYYY HH:mm'}}-->
                                                            <span class="success-score" ng-if="item.content[0].sid != '1'">
                                                                <span>{{ item.content[0].score | scoreSelect:'home' }}</span>
                                                                <span>:</span>
                                                                <span>{{ item.content[0].score | scoreSelect:'away' }}</span>
                                                            </span>
                                                            <span class="hdp-game" ng-if="item.content[0].sid == '1'">
                                                                <b>{{item.content[0].hdp}}</b><br/> <span class="macth-hdp">({{ item.content[0].hdp_home}}) : ({{ item.content[0].hdp_away}})</span>
                                                            </span>
                                                        </td>
                                                        <td class="sec-team2"><img ng-src="{{ item.content[0]['a256x256']}}"/> </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table>
                                                                <tr>
                                                                    <td class="teams1">
                                                                        <div class="boxTeam teamLeft">
                                                                            <b ng-bind="parseTeamStat(item.content[0].stat, item.content[0].hid, 'number')">[1]</b> <span class="displayNameTeam" ng-class="{'team-favored': item.content[0].hdp < 0}">{{ allTeams[item.content[0].hid].name || item.content[0].homeName }}</span>
                                                                        </div>
                                                                        <div style="clear: both;"></div>
                                                                        <div class="last-stat-fivematch" ng-init="ssh = parseTeamStat(item.content[0].stat, item.content[0].hid, 'lastplay')">
                                                                            <span ng-repeat="s in ssh track by $index" ng-switch="s">
                                                                                <span ng-switch-when="w" class="text-green">W</span>
                                                                                <span ng-switch-when="l" class="text-red">L</span>
                                                                                <span ng-switch-when="d" class="text-black">D</span>
                                                                                <span ng-if="$index < (ssh.length - 1)"> | </span>
                                                                            </span>
                                                                        </div>
                                                                    </td>
                                                                    <td class="vs vss">
                                                                        <span ng-if="item.content[0].sid == '1'">
                                                                            <a ng-if="canPlay(item.content[0].mid, item.content[0].sid, item.content[0].hdp)" href="/game.php?mid={{ item.content[0].mid}}"><img src="images/icon/bet-play.png"></a>
                                                                            <a ng-if="!canPlay(item.content[0].mid, item.content[0].sid, item.content[0].hdp)" href="/game.php?mid={{ item.content[0].mid}}"><img src="images/icon/bet-view.png"></a>
                                                                        </span>
                                                                    </td>
                                                                    <td class="teams2">
                                                                        <div class="boxTeam teamRight">
                                                                            <span class="displayNameTeam" ng-class="{'team-favored': item.content[0].hdp > 0}">{{ allTeams[item.content[0].gid].name || item.content[0].awayName }}</span><b ng-bind="parseTeamStat(item.content[0].stat, item.content[0].gid, 'number')">[3]</b>
                                                                        </div>
                                                                        <div style="clear: both;"></div>
                                                                        <div class="last-stat-fivematch" ng-init="ssg = parseTeamStat(item.content[0].stat, item.content[0].gid, 'lastplay')">
                                                                            <span ng-repeat="s in ssg track by $index" ng-switch="s">
                                                                                <span ng-switch-when="w" class="text-green">W</span>
                                                                                <span ng-switch-when="l" class="text-red">L</span>
                                                                                <span ng-switch-when="d" class="text-black">D</span>
                                                                                <span ng-if="$index < (ssg.length - 1)"> | </span>
                                                                            </span>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div style="clear: both;"></div>


                    <div class="wrapper-box-feed-comment">
                        <div class="box-show-comment">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="comment">
                                        <div class="upimages-box">
                                            <a href="news.php?id={{ item.owner.id}}">
                                                <img src="images/icon/cam15.png" title="แนบรูปภาพ"></div>
                                        <!--                                                    <img style="width: 25px; height: 25px;" ng-src="https://graph.facebook.com/{{ facebookInfo.id}}/picture">-->
                                        <img style="width: 25px; height: 25px;" ng-src="http://api.ssporting.com/cache/small/{{ facebookInfo.id}}.jpg">
                                        <input type="text" placeholder="comment...">
                                        <span class="hover"></span>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div ng-repeat="(idx, item) in topics2.timelines" class="ForumWrap">
                <div class="box-feedForum">
                    <div class="feed-situations">
                        <div class="img-user-feed">
                            <a ng-if="item.owner.content_type === 'board' || item.owner.content_type === 'status' || item.owner.content_type === 'image'" href="profile.php?id={{ item.owner.fb_uid}}">
                                <!--                                <img ng-src="https://graph.facebook.com/{{ item.owner.fb_uid}}/picture">-->
                                <img ng-src="http://api.ssporting.com/cache/small/{{ item.owner.fb_uid}}.jpg">
                            </a>
                            <span ng-if="item.owner.content_type === 'news'"><img src="images/icon/news.png"></span>
                            <span ng-if="item.owner.content_type === 'highlight'"><img src="images/icon/video-icon.png"></span>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                    <div class="tableForum">
                        <table ng-if="item.owner.content_type === 'board'">
                            <tr>
                                <td>
                                    <div class="titleFor"><a href="news.php?id={{ item.owner.id}}">{{ item.content[0].title}}</a></div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span>
                                    <span ng-click="openReportDialog(item.owner.id)" class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="postNewsForum"><a href="news.php?id={{ item.owner.id}}"><span ng-bind-html="item.content[0].desc | nl2br"></span></div>
                                </td>
                            </tr>
                            <tr ng-if="hasVideo(item.content[0].medialist)">
                                <td>
                                    <a href="{{ item.content[0].medialist[0].path}}" embed-video width="500" height="290"></a>
                                </td>
                            </tr>
                            <tr ng-if="hasImage(item.content[0].medialist)">
                                <td>
                                    <div class="wrapper-showimg-wall">
                                        <a href="/news.php?id={{ item.owner.id}}">
                                            <div ng-if="item.content[0].medialist.length === 1" class="one-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                            </div>

                                            <div ng-if="item.content[0].medialist.length === 2" class="two-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[1].path}}"></span>
                                            </div>

                                            <div ng-if="item.content[0].medialist.length >= 3" class="three-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[1].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[2].path}}"></span>
                                            </div>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}}
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'status'">
                            <tr>
                                <td>
                                    <div class="postNewsForum"><a href="news.php?id={{ item.owner.id}}"><span ng-bind-html="item.content[0].message | nl2br"></span></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}} | {{ item.owner.created_at | timeagoByDateTime }}  <span ng-click="openReportDialog(item.owner.id)" class="reportPost boxRep"><img src="images/icon/downicon.png"></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'image'">
                            <tr>
                                <td>
                                    <div class="wrapper-showimg-wall">
                                        <a href="/news.php?id={{ item.owner.id}}">
                                            <div ng-if="item.content.length === 1" class="one-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                            </div>

                                            <div ng-if="item.content.length === 2" class="two-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[1].path}}"></span>
                                            </div>

                                            <div ng-if="item.content.length >= 3" class="three-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[1].path}}"></span>
                                                <span><img ng-src="{{ item.content[2].path}}"></span>
                                            </div>
                                        </a>

                                        <div style="clear: both;"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}} | {{ item.owner.created_at | timeagoByDateTime }}  <span ng-click="openReportDialog(item.owner.id)" class="reportPost boxRep"><img src="images/icon/downicon.png"></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'news'" class="news-section">
                            <tbody>
                            <tr>
                                <td><span class="font-name user-name-feed">Ssporting.com</span></td>
                                <td class="time-to-post ng-binding">{{ item.owner.created_at | timeagoByDateTime }}</td>
                            </tr>
                            <tr>
                                <td class="img-news" style="width: 5%;"><a href="/news.php?id={{ item.owner.id}}"><img ng-src="{{ item.content[0].imageLink}}"/> </a></td>

                                <td class="detail-newss">
                                    <div class="title-news"><a href="news.php?id={{ item.owner.id}}" class="ng-binding">{{ item.content[0].title<?php echo ucfirst(__LANGUAGE__); ?>}}</a>
                                    </div>
                                    <span ng-bind-html="item.content[0].shortDescription<?php echo ucfirst(__LANGUAGE__); ?> | toTrusted"></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="section-like-post-wall ng-binding">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | <a href="news.php?id={{ item.owner.id}}">ดูรายละเอียดข่าว</a> | <a href="news.php?id={{ item.owner.id}}">แสดงความคิดเห็น</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table ng-if="item.owner.content_type === 'highlight'">
                            <tr>
                                <td>
                                    <div class="titleFor">{{ item.content[0].title}}</div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span>
                                    <span ng-click="openReportDialog(item.owner.id)" class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ item.content[0].content}}" embed-video width="500" height="290"></a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="section-like-post-wall ng-binding">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | <a href="news.php?id={{ item.owner.id}}">ดูรายละเอียดข่าว</a> | <a href="news.php?id={{ item.owner.id}}">แสดงความคิดเห็น</a>

                                    <div class="postBy"><span ng-if="item.owner.display_name || item.owner.fb_firstname" class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a> |</span> {{ getTopicTypeFromId(item.owner.type).feed_name}}</div>
                                </td>
                            </tr>
                        </table>
                        <div ng-if="item.owner.content_type === 'game'" class="wrapper-feed-wall">
                            <a href="game.php?mid={{ item.content[0].mid}}">
                                <div class="feed-situations">
                                    <div class="img-user-feed">
                                        <img ng-src="/images/countries/{{ item.content[0].competitionId}}.png">
                                        <div xng-show="item.content[0].hdp"><img src="images/icon/bet2.png" style="width: 30px; height: 30px;">
                                            <br/> <b>{{ item.content[0].hdp}}</b><br/>HDP
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>
                                </div>
                                <div class="tableGames">
                                    <table>

                                        <tr class="head-feed">
                                            <td class="time-date-post">{{ item.content[0].show_date | formatDate:'DD MMMM YYYY HH:mm'}}</td>
                                            <td class="league-name-title">{{ allLeagues[item.content[0]._lid].name || item.content[0].LeagueName}}</td>
                                        </tr>
                                        <span class="remove-comment" ng-if="item.owner.fb_uid == facebookInfo.id" ng-click="removeTimeline(item.owner.id, $event)">x</span>
                                        <tr class="teamplay">
                                            <td colspan="2" class="team-playing">

                                                <table>
                                                    <tr>
                                                        <td class="sec-team1"><img ng-src="{{ item.content[0]['h256x256']}}"/></td>
                                                        <td class="hdp-match">
                                                            <!-- {{ allLeagues[item.content[0]._lid].name || item.content[0].LeagueName}}-->
                                                            <!--<br/> {{ item.content[0].show_date | formatDate:'DD MMMM YYYY HH:mm'}}-->
                                                            <span class="success-score" ng-if="item.content[0].sid != '1'">
                                                                <span>{{ item.content[0].score | scoreSelect:'home' }}</span>
                                                                <span>:</span>
                                                                <span>{{ item.content[0].score | scoreSelect:'away' }}</span>
                                                            </span>
                                                            <span class="hdp-game" ng-if="item.content[0].sid == '1'">
                                                                <b>{{item.content[0].hdp}}</b><br/> <span class="macth-hdp">({{ item.content[0].hdp_home}}) : ({{ item.content[0].hdp_away}})</span>
                                                            </span>
                                                        </td>
                                                        <td class="sec-team2"><img ng-src="{{ item.content[0]['a256x256']}}"/> </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table>
                                                                <tr>
                                                                    <td class="teams1">
                                                                        <div class="boxTeam teamLeft">
                                                                            <b ng-bind="parseTeamStat(item.content[0].stat, item.content[0].hid, 'number')">[1]</b> <span class="displayNameTeam" ng-class="{'team-favored': item.content[0].hdp < 0}">{{ allTeams[item.content[0].hid].name || item.content[0].homeName }}</span>
                                                                        </div>
                                                                        <div style="clear: both;"></div>
                                                                        <div class="last-stat-fivematch" ng-init="ssh = parseTeamStat(item.content[0].stat, item.content[0].hid, 'lastplay')">
                                                                            <span ng-repeat="s in ssh track by $index" ng-switch="s">
                                                                                <span ng-switch-when="w" class="text-green">W</span>
                                                                                <span ng-switch-when="l" class="text-red">L</span>
                                                                                <span ng-switch-when="d" class="text-black">D</span>
                                                                                <span ng-if="$index < (ssh.length - 1)"> | </span>
                                                                            </span>
                                                                        </div>
                                                                    </td>
                                                                    <td class="vs vss">
                                                                        <span ng-if="item.content[0].sid == '1'">
                                                                            <a ng-if="canPlay(item.content[0].mid, item.content[0].sid, item.content[0].hdp)" href="/game.php?mid={{ item.content[0].mid}}"><img src="images/icon/bet-play.png"></a>
                                                                            <a ng-if="!canPlay(item.content[0].mid, item.content[0].sid, item.content[0].hdp)" href="/game.php?mid={{ item.content[0].mid}}"><img src="images/icon/bet-view.png"></a>
                                                                        </span>
                                                                    </td>
                                                                    <td class="teams2">
                                                                        <div class="boxTeam teamRight">
                                                                            <span class="displayNameTeam" ng-class="{'team-favored': item.content[0].hdp > 0}">{{ allTeams[item.content[0].gid].name || item.content[0].awayName }}</span><b ng-bind="parseTeamStat(item.content[0].stat, item.content[0].gid, 'number')">[3]</b>
                                                                        </div>
                                                                        <div style="clear: both;"></div>
                                                                        <div class="last-stat-fivematch" ng-init="ssg = parseTeamStat(item.content[0].stat, item.content[0].gid, 'lastplay')">
                                                                            <span ng-repeat="s in ssg track by $index" ng-switch="s">
                                                                                <span ng-switch-when="w" class="text-green">W</span>
                                                                                <span ng-switch-when="l" class="text-red">L</span>
                                                                                <span ng-switch-when="d" class="text-black">D</span>
                                                                                <span ng-if="$index < (ssg.length - 1)"> | </span>
                                                                            </span>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div style="clear: both;"></div>


                    <div class="wrapper-box-feed-comment">
                        <div class="box-show-comment">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="comment">
                                        <div class="upimages-box">
                                            <a href="news.php?id={{ item.owner.id}}">
                                                <img src="images/icon/cam15.png" title="แนบรูปภาพ"></div>
                                        <!--                                                    <img style="width: 25px; height: 25px;" ng-src="https://graph.facebook.com/{{ facebookInfo.id}}/picture">-->
                                        <img style="width: 25px; height: 25px;" ng-src="http://api.ssporting.com/cache/small/{{ facebookInfo.id}}.jpg">
                                        <input type="text" placeholder="comment...">
                                        <span class="hover"></span>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div ng-repeat="(idx, item) in topics3.timelines" class="ForumWrap">
                <div class="box-feedForum">
                    <div class="feed-situations">
                        <div class="img-user-feed">
                            <a ng-if="item.owner.content_type === 'board' || item.owner.content_type === 'status' || item.owner.content_type === 'image'" href="profile.php?id={{ item.owner.fb_uid}}">
                                <!--                                <img ng-src="https://graph.facebook.com/{{ item.owner.fb_uid}}/picture">-->
                                <img ng-src="http://api.ssporting.com/cache/small/{{ item.owner.fb_uid}}.jpg">
                            </a>
                            <span ng-if="item.owner.content_type === 'news'"><img src="images/icon/news.png"></span>
                            <span ng-if="item.owner.content_type === 'highlight'"><img src="images/icon/video-icon.png"></span>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                    <div class="tableForum">
                        <table ng-if="item.owner.content_type === 'board'">
                            <tr>
                                <td>
                                    <div class="titleFor"><a href="news.php?id={{ item.owner.id}}">{{ item.content[0].title}}</a></div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span>
                                    <span ng-click="openReportDialog(item.owner.id)" class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="postNewsForum"><a href="news.php?id={{ item.owner.id}}"><span ng-bind-html="item.content[0].desc | nl2br"></span></div>
                                </td>
                            </tr>
                            <tr ng-if="hasVideo(item.content[0].medialist)">
                                <td>
                                    <a href="{{ item.content[0].medialist[0].path}}" embed-video width="500" height="290"></a>
                                </td>
                            </tr>
                            <tr ng-if="hasImage(item.content[0].medialist)">
                                <td>
                                    <div class="wrapper-showimg-wall">
                                        <a href="/news.php?id={{ item.owner.id}}">
                                            <div ng-if="item.content[0].medialist.length === 1" class="one-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                            </div>

                                            <div ng-if="item.content[0].medialist.length === 2" class="two-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[1].path}}"></span>
                                            </div>

                                            <div ng-if="item.content[0].medialist.length >= 3" class="three-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[1].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[2].path}}"></span>
                                            </div>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}}
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'status'">
                            <tr>
                                <td>
                                    <div class="postNewsForum"><a href="news.php?id={{ item.owner.id}}"><span ng-bind-html="item.content[0].message | nl2br"></span></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}} | {{ item.owner.created_at | timeagoByDateTime }}  <span ng-click="openReportDialog(item.owner.id)" class="reportPost boxRep"><img src="images/icon/downicon.png"></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'image'">
                            <tr>
                                <td>
                                    <div class="wrapper-showimg-wall">
                                        <a href="/news.php?id={{ item.owner.id}}">
                                            <div ng-if="item.content.length === 1" class="one-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                            </div>

                                            <div ng-if="item.content.length === 2" class="two-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[1].path}}"></span>
                                            </div>

                                            <div ng-if="item.content.length >= 3" class="three-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[1].path}}"></span>
                                                <span><img ng-src="{{ item.content[2].path}}"></span>
                                            </div>
                                        </a>

                                        <div style="clear: both;"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}} | {{ item.owner.created_at | timeagoByDateTime }}  <span ng-click="openReportDialog(item.owner.id)" class="reportPost boxRep"><img src="images/icon/downicon.png"></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'news'" class="news-section">
                            <tbody>
                            <tr>
                                <td><span class="font-name user-name-feed">Ssporting.com</span></td>
                                <td class="time-to-post ng-binding">{{ item.owner.created_at | timeagoByDateTime }}</td>
                            </tr>
                            <tr>
                                <td class="img-news" style="width: 5%;"><a href="/news.php?id={{ item.owner.id}}"><img ng-src="{{ item.content[0].imageLink}}"/> </a></td>

                                <td class="detail-newss">
                                    <div class="title-news"><a href="news.php?id={{ item.owner.id}}" class="ng-binding">{{ item.content[0].title<?php echo ucfirst(__LANGUAGE__); ?>}}</a>
                                    </div>
                                    <span ng-bind-html="item.content[0].shortDescription<?php echo ucfirst(__LANGUAGE__); ?> | toTrusted"></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="section-like-post-wall ng-binding">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | <a href="news.php?id={{ item.owner.id}}">ดูรายละเอียดข่าว</a> | <a href="news.php?id={{ item.owner.id}}">แสดงความคิดเห็น</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table ng-if="item.owner.content_type === 'highlight'">
                            <tr>
                                <td>
                                    <div class="titleFor">{{ item.content[0].title}}</div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span>
                                    <span ng-click="openReportDialog(item.owner.id)" class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ item.content[0].content}}" embed-video width="500" height="290"></a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="section-like-post-wall ng-binding">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | <a href="news.php?id={{ item.owner.id}}">ดูรายละเอียดข่าว</a> | <a href="news.php?id={{ item.owner.id}}">แสดงความคิดเห็น</a>

                                    <div class="postBy"><span ng-if="item.owner.display_name || item.owner.fb_firstname" class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a> |</span> {{ getTopicTypeFromId(item.owner.type).feed_name}}</div>
                                </td>
                            </tr>
                        </table>
                        <div ng-if="item.owner.content_type === 'game'" class="wrapper-feed-wall">
                            <a href="game.php?mid={{ item.content[0].mid}}">
                                <div class="feed-situations">
                                    <div class="img-user-feed">
                                        <img ng-src="/images/countries/{{ item.content[0].competitionId}}.png">
                                        <div xng-show="item.content[0].hdp"><img src="images/icon/bet2.png" style="width: 30px; height: 30px;">
                                            <br/> <b>{{ item.content[0].hdp}}</b><br/>HDP
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>
                                </div>
                                <div class="tableGames">
                                    <table>

                                        <tr class="head-feed">
                                            <td class="time-date-post">{{ item.content[0].show_date | formatDate:'DD MMMM YYYY HH:mm'}}</td>
                                            <td class="league-name-title">{{ allLeagues[item.content[0]._lid].name || item.content[0].LeagueName}}</td>
                                        </tr>
                                        <span class="remove-comment" ng-if="item.owner.fb_uid == facebookInfo.id" ng-click="removeTimeline(item.owner.id, $event)">x</span>
                                        <tr class="teamplay">
                                            <td colspan="2" class="team-playing">

                                                <table>
                                                    <tr>
                                                        <td class="sec-team1"><img ng-src="{{ item.content[0]['h256x256']}}"/></td>
                                                        <td class="hdp-match">
                                                            <!-- {{ allLeagues[item.content[0]._lid].name || item.content[0].LeagueName}}-->
                                                            <!--<br/> {{ item.content[0].show_date | formatDate:'DD MMMM YYYY HH:mm'}}-->
                                                            <span class="success-score" ng-if="item.content[0].sid != '1'">
                                                                <span>{{ item.content[0].score | scoreSelect:'home' }}</span>
                                                                <span>:</span>
                                                                <span>{{ item.content[0].score | scoreSelect:'away' }}</span>
                                                            </span>
                                                            <span class="hdp-game" ng-if="item.content[0].sid == '1'">
                                                                <b>{{item.content[0].hdp}}</b><br/> <span class="macth-hdp">({{ item.content[0].hdp_home}}) : ({{ item.content[0].hdp_away}})</span>
                                                            </span>
                                                        </td>
                                                        <td class="sec-team2"><img ng-src="{{ item.content[0]['a256x256']}}"/> </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table>
                                                                <tr>
                                                                    <td class="teams1">
                                                                        <div class="boxTeam teamLeft">
                                                                            <b ng-bind="parseTeamStat(item.content[0].stat, item.content[0].hid, 'number')">[1]</b> <span class="displayNameTeam" ng-class="{'team-favored': item.content[0].hdp < 0}">{{ allTeams[item.content[0].hid].name || item.content[0].homeName }}</span>
                                                                        </div>
                                                                        <div style="clear: both;"></div>
                                                                        <div class="last-stat-fivematch" ng-init="ssh = parseTeamStat(item.content[0].stat, item.content[0].hid, 'lastplay')">
                                                                            <span ng-repeat="s in ssh track by $index" ng-switch="s">
                                                                                <span ng-switch-when="w" class="text-green">W</span>
                                                                                <span ng-switch-when="l" class="text-red">L</span>
                                                                                <span ng-switch-when="d" class="text-black">D</span>
                                                                                <span ng-if="$index < (ssh.length - 1)"> | </span>
                                                                            </span>
                                                                        </div>
                                                                    </td>
                                                                    <td class="vs vss">
                                                                        <span ng-if="item.content[0].sid == '1'">
                                                                            <a ng-if="canPlay(item.content[0].mid, item.content[0].sid, item.content[0].hdp)" href="/game.php?mid={{ item.content[0].mid}}"><img src="images/icon/bet-play.png"></a>
                                                                            <a ng-if="!canPlay(item.content[0].mid, item.content[0].sid, item.content[0].hdp)" href="/game.php?mid={{ item.content[0].mid}}"><img src="images/icon/bet-view.png"></a>
                                                                        </span>
                                                                    </td>
                                                                    <td class="teams2">
                                                                        <div class="boxTeam teamRight">
                                                                            <span class="displayNameTeam" ng-class="{'team-favored': item.content[0].hdp > 0}">{{ allTeams[item.content[0].gid].name || item.content[0].awayName }}</span><b ng-bind="parseTeamStat(item.content[0].stat, item.content[0].gid, 'number')">[3]</b>
                                                                        </div>
                                                                        <div style="clear: both;"></div>
                                                                        <div class="last-stat-fivematch" ng-init="ssg = parseTeamStat(item.content[0].stat, item.content[0].gid, 'lastplay')">
                                                                            <span ng-repeat="s in ssg track by $index" ng-switch="s">
                                                                                <span ng-switch-when="w" class="text-green">W</span>
                                                                                <span ng-switch-when="l" class="text-red">L</span>
                                                                                <span ng-switch-when="d" class="text-black">D</span>
                                                                                <span ng-if="$index < (ssg.length - 1)"> | </span>
                                                                            </span>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div style="clear: both;"></div>


                    <div class="wrapper-box-feed-comment">
                        <div class="box-show-comment">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="comment">
                                        <div class="upimages-box">
                                            <a href="news.php?id={{ item.owner.id}}">
                                                <img src="images/icon/cam15.png" title="แนบรูปภาพ"></div>
                                        <!--                                                    <img style="width: 25px; height: 25px;" ng-src="https://graph.facebook.com/{{ facebookInfo.id}}/picture">-->
                                        <img style="width: 25px; height: 25px;" ng-src="http://api.ssporting.com/cache/small/{{ facebookInfo.id}}.jpg">
                                        <input type="text" placeholder="comment...">
                                        <span class="hover"></span>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div ng-repeat="(idx, item) in topics4.timelines" class="ForumWrap">
                <div class="box-feedForum">
                    <div class="feed-situations">
                        <div class="img-user-feed">
                            <a ng-if="item.owner.content_type === 'board' || item.owner.content_type === 'status' || item.owner.content_type === 'image'" href="profile.php?id={{ item.owner.fb_uid}}">
                                <!--                                <img ng-src="https://graph.facebook.com/{{ item.owner.fb_uid}}/picture">-->
                                <img ng-src="http://api.ssporting.com/cache/small/{{ item.owner.fb_uid}}.jpg">
                            </a>
                            <span ng-if="item.owner.content_type === 'news'"><img src="images/icon/news.png"></span>
                            <span ng-if="item.owner.content_type === 'highlight'"><img src="images/icon/video-icon.png"></span>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                    <div class="tableForum">
                        <table ng-if="item.owner.content_type === 'board'">
                            <tr>
                                <td>
                                    <div class="titleFor"><a href="news.php?id={{ item.owner.id}}">{{ item.content[0].title}}</a></div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span>
                                    <span ng-click="openReportDialog(item.owner.id)" class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="postNewsForum"><a href="news.php?id={{ item.owner.id}}"><span ng-bind-html="item.content[0].desc | nl2br"></span></div>
                                </td>
                            </tr>
                            <tr ng-if="hasVideo(item.content[0].medialist)">
                                <td>
                                    <a href="{{ item.content[0].medialist[0].path}}" embed-video width="500" height="290"></a>
                                </td>
                            </tr>
                            <tr ng-if="hasImage(item.content[0].medialist)">
                                <td>
                                    <div class="wrapper-showimg-wall">
                                        <a href="/news.php?id={{ item.owner.id}}">
                                            <div ng-if="item.content[0].medialist.length === 1" class="one-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                            </div>

                                            <div ng-if="item.content[0].medialist.length === 2" class="two-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[1].path}}"></span>
                                            </div>

                                            <div ng-if="item.content[0].medialist.length >= 3" class="three-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[1].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[2].path}}"></span>
                                            </div>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}}
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'status'">
                            <tr>
                                <td>
                                    <div class="postNewsForum"><a href="news.php?id={{ item.owner.id}}"><span ng-bind-html="item.content[0].message | nl2br"></span></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}} | {{ item.owner.created_at | timeagoByDateTime }}  <span ng-click="openReportDialog(item.owner.id)" class="reportPost boxRep"><img src="images/icon/downicon.png"></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'image'">
                            <tr>
                                <td>
                                    <div class="wrapper-showimg-wall">
                                        <a href="/news.php?id={{ item.owner.id}}">
                                            <div ng-if="item.content.length === 1" class="one-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                            </div>

                                            <div ng-if="item.content.length === 2" class="two-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[1].path}}"></span>
                                            </div>

                                            <div ng-if="item.content.length >= 3" class="three-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[1].path}}"></span>
                                                <span><img ng-src="{{ item.content[2].path}}"></span>
                                            </div>
                                        </a>

                                        <div style="clear: both;"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}} | {{ item.owner.created_at | timeagoByDateTime }}  <span ng-click="openReportDialog(item.owner.id)" class="reportPost boxRep"><img src="images/icon/downicon.png"></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'news'" class="news-section">
                            <tbody>
                            <tr>
                                <td><span class="font-name user-name-feed">Ssporting.com</span></td>
                                <td class="time-to-post ng-binding">{{ item.owner.created_at | timeagoByDateTime }}</td>
                            </tr>
                            <tr>
                                <td class="img-news" style="width: 5%;"><a href="/news.php?id={{ item.owner.id}}"><img ng-src="{{ item.content[0].imageLink}}"/> </a></td>

                                <td class="detail-newss">
                                    <div class="title-news"><a href="news.php?id={{ item.owner.id}}" class="ng-binding">{{ item.content[0].title<?php echo ucfirst(__LANGUAGE__); ?>}}</a>
                                    </div>
                                    <span ng-bind-html="item.content[0].shortDescription<?php echo ucfirst(__LANGUAGE__); ?> | toTrusted"></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="section-like-post-wall ng-binding">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | <a href="news.php?id={{ item.owner.id}}">ดูรายละเอียดข่าว</a> | <a href="news.php?id={{ item.owner.id}}">แสดงความคิดเห็น</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table ng-if="item.owner.content_type === 'highlight'">
                            <tr>
                                <td>
                                    <div class="titleFor">{{ item.content[0].title}}</div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span>
                                    <span ng-click="openReportDialog(item.owner.id)" class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ item.content[0].content}}" embed-video width="500" height="290"></a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="section-like-post-wall ng-binding">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | <a href="news.php?id={{ item.owner.id}}">ดูรายละเอียดข่าว</a> | <a href="news.php?id={{ item.owner.id}}">แสดงความคิดเห็น</a>

                                    <div class="postBy"><span ng-if="item.owner.display_name || item.owner.fb_firstname" class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a> |</span> {{ getTopicTypeFromId(item.owner.type).feed_name}}</div>
                                </td>
                            </tr>
                        </table>
                        <div ng-if="item.owner.content_type === 'game'" class="wrapper-feed-wall">
                            <a href="game.php?mid={{ item.content[0].mid}}">
                                <div class="feed-situations">
                                    <div class="img-user-feed">
                                        <img ng-src="/images/countries/{{ item.content[0].competitionId}}.png">
                                        <div xng-show="item.content[0].hdp"><img src="images/icon/bet2.png" style="width: 30px; height: 30px;">
                                            <br/> <b>{{ item.content[0].hdp}}</b><br/>HDP
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>
                                </div>
                                <div class="tableGames">
                                    <table>

                                        <tr class="head-feed">
                                            <td class="time-date-post">{{ item.content[0].show_date | formatDate:'DD MMMM YYYY HH:mm'}}</td>
                                            <td class="league-name-title">{{ allLeagues[item.content[0]._lid].name || item.content[0].LeagueName}}</td>
                                        </tr>
                                        <span class="remove-comment" ng-if="item.owner.fb_uid == facebookInfo.id" ng-click="removeTimeline(item.owner.id, $event)">x</span>
                                        <tr class="teamplay">
                                            <td colspan="2" class="team-playing">

                                                <table>
                                                    <tr>
                                                        <td class="sec-team1"><img ng-src="{{ item.content[0]['h256x256']}}"/></td>
                                                        <td class="hdp-match">
                                                            <!-- {{ allLeagues[item.content[0]._lid].name || item.content[0].LeagueName}}-->
                                                            <!--<br/> {{ item.content[0].show_date | formatDate:'DD MMMM YYYY HH:mm'}}-->
                                                            <span class="success-score" ng-if="item.content[0].sid != '1'">
                                                                <span>{{ item.content[0].score | scoreSelect:'home' }}</span>
                                                                <span>:</span>
                                                                <span>{{ item.content[0].score | scoreSelect:'away' }}</span>
                                                            </span>
                                                            <span class="hdp-game" ng-if="item.content[0].sid == '1'">
                                                                <b>{{item.content[0].hdp}}</b><br/> <span class="macth-hdp">({{ item.content[0].hdp_home}}) : ({{ item.content[0].hdp_away}})</span>
                                                            </span>
                                                        </td>
                                                        <td class="sec-team2"><img ng-src="{{ item.content[0]['a256x256']}}"/> </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table>
                                                                <tr>
                                                                    <td class="teams1">
                                                                        <div class="boxTeam teamLeft">
                                                                            <b ng-bind="parseTeamStat(item.content[0].stat, item.content[0].hid, 'number')">[1]</b> <span class="displayNameTeam" ng-class="{'team-favored': item.content[0].hdp < 0}">{{ allTeams[item.content[0].hid].name || item.content[0].homeName }}</span>
                                                                        </div>
                                                                        <div style="clear: both;"></div>
                                                                        <div class="last-stat-fivematch" ng-init="ssh = parseTeamStat(item.content[0].stat, item.content[0].hid, 'lastplay')">
                                                                            <span ng-repeat="s in ssh track by $index" ng-switch="s">
                                                                                <span ng-switch-when="w" class="text-green">W</span>
                                                                                <span ng-switch-when="l" class="text-red">L</span>
                                                                                <span ng-switch-when="d" class="text-black">D</span>
                                                                                <span ng-if="$index < (ssh.length - 1)"> | </span>
                                                                            </span>
                                                                        </div>
                                                                    </td>
                                                                    <td class="vs vss">
                                                                        <span ng-if="item.content[0].sid == '1'">
                                                                            <a ng-if="canPlay(item.content[0].mid, item.content[0].sid, item.content[0].hdp)" href="/game.php?mid={{ item.content[0].mid}}"><img src="images/icon/bet-play.png"></a>
                                                                            <a ng-if="!canPlay(item.content[0].mid, item.content[0].sid, item.content[0].hdp)" href="/game.php?mid={{ item.content[0].mid}}"><img src="images/icon/bet-view.png"></a>
                                                                        </span>
                                                                    </td>
                                                                    <td class="teams2">
                                                                        <div class="boxTeam teamRight">
                                                                            <span class="displayNameTeam" ng-class="{'team-favored': item.content[0].hdp > 0}">{{ allTeams[item.content[0].gid].name || item.content[0].awayName }}</span><b ng-bind="parseTeamStat(item.content[0].stat, item.content[0].gid, 'number')">[3]</b>
                                                                        </div>
                                                                        <div style="clear: both;"></div>
                                                                        <div class="last-stat-fivematch" ng-init="ssg = parseTeamStat(item.content[0].stat, item.content[0].gid, 'lastplay')">
                                                                            <span ng-repeat="s in ssg track by $index" ng-switch="s">
                                                                                <span ng-switch-when="w" class="text-green">W</span>
                                                                                <span ng-switch-when="l" class="text-red">L</span>
                                                                                <span ng-switch-when="d" class="text-black">D</span>
                                                                                <span ng-if="$index < (ssg.length - 1)"> | </span>
                                                                            </span>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div style="clear: both;"></div>


                    <div class="wrapper-box-feed-comment">
                        <div class="box-show-comment">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="comment">
                                        <div class="upimages-box">
                                            <a href="news.php?id={{ item.owner.id}}">
                                                <img src="images/icon/cam15.png" title="แนบรูปภาพ"></div>
                                        <!--                                                    <img style="width: 25px; height: 25px;" ng-src="https://graph.facebook.com/{{ facebookInfo.id}}/picture">-->
                                        <img style="width: 25px; height: 25px;" ng-src="http://api.ssporting.com/cache/small/{{ facebookInfo.id}}.jpg">
                                        <input type="text" placeholder="comment...">
                                        <span class="hover"></span>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div ng-repeat="(idx, item) in topics5.timelines" class="ForumWrap">
                <div class="box-feedForum">
                    <div class="feed-situations">
                        <div class="img-user-feed">
                            <a ng-if="item.owner.content_type === 'board' || item.owner.content_type === 'status' || item.owner.content_type === 'image'" href="profile.php?id={{ item.owner.fb_uid}}">
                                <!--                                <img ng-src="https://graph.facebook.com/{{ item.owner.fb_uid}}/picture">-->
                                <img ng-src="http://api.ssporting.com/cache/small/{{ item.owner.fb_uid}}.jpg">
                            </a>
                            <span ng-if="item.owner.content_type === 'news'"><img src="images/icon/news.png"></span>
                            <span ng-if="item.owner.content_type === 'highlight'"><img src="images/icon/video-icon.png"></span>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                    <div class="tableForum">
                        <table ng-if="item.owner.content_type === 'board'">
                            <tr>
                                <td>
                                    <div class="titleFor"><a href="news.php?id={{ item.owner.id}}">{{ item.content[0].title}}</a></div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span>
                                    <span ng-click="openReportDialog(item.owner.id)" class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="postNewsForum"><a href="news.php?id={{ item.owner.id}}"><span ng-bind-html="item.content[0].desc | nl2br"></span></div>
                                </td>
                            </tr>
                            <tr ng-if="hasVideo(item.content[0].medialist)">
                                <td>
                                    <a href="{{ item.content[0].medialist[0].path}}" embed-video width="500" height="290"></a>
                                </td>
                            </tr>
                            <tr ng-if="hasImage(item.content[0].medialist)">
                                <td>
                                    <div class="wrapper-showimg-wall">
                                        <a href="/news.php?id={{ item.owner.id}}">
                                            <div ng-if="item.content[0].medialist.length === 1" class="one-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                            </div>

                                            <div ng-if="item.content[0].medialist.length === 2" class="two-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[1].path}}"></span>
                                            </div>

                                            <div ng-if="item.content[0].medialist.length >= 3" class="three-images">
                                                <span><img ng-src="{{ item.content[0].medialist[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[1].path}}"></span>
                                                <span><img ng-src="{{ item.content[0].medialist[2].path}}"></span>
                                            </div>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}}
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'status'">
                            <tr>
                                <td>
                                    <div class="postNewsForum"><a href="news.php?id={{ item.owner.id}}"><span ng-bind-html="item.content[0].message | nl2br"></span></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}} | {{ item.owner.created_at | timeagoByDateTime }}  <span ng-click="openReportDialog(item.owner.id)" class="reportPost boxRep"><img src="images/icon/downicon.png"></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'image'">
                            <tr>
                                <td>
                                    <div class="wrapper-showimg-wall">
                                        <a href="/news.php?id={{ item.owner.id}}">
                                            <div ng-if="item.content.length === 1" class="one-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                            </div>

                                            <div ng-if="item.content.length === 2" class="two-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[1].path}}"></span>
                                            </div>

                                            <div ng-if="item.content.length >= 3" class="three-images">
                                                <span><img ng-src="{{ item.content[0].path}}"></span>
                                                <span><img ng-src="{{ item.content[1].path}}"></span>
                                                <span><img ng-src="{{ item.content[2].path}}"></span>
                                            </div>
                                        </a>

                                        <div style="clear: both;"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="section-like-post-wall">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | ({{ item.owner.view}}) ดูแล้ว | <a href="news.php?id={{ item.owner.id}}">({{ topics.main_reply[item.owner.id].length || 0 }}) ความคิดเห็น</a>
                                    <div ng-if="item.owner.content_type !== 'news'" class="postBy">
                                        <span class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a></span> | {{ getTopicTypeFromId(item.owner.type).feed_name}} | {{ item.owner.created_at | timeagoByDateTime }}  <span ng-click="openReportDialog(item.owner.id)" class="reportPost boxRep"><img src="images/icon/downicon.png"></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table ng-if="item.owner.content_type === 'news'" class="news-section">
                            <tbody>
                            <tr>
                                <td><span class="font-name user-name-feed">Ssporting.com</span></td>
                                <td class="time-to-post ng-binding">{{ item.owner.created_at | timeagoByDateTime }}</td>
                            </tr>
                            <tr>
                                <td class="img-news" style="width: 5%;"><a href="/news.php?id={{ item.owner.id}}"><img ng-src="{{ item.content[0].imageLink}}"/> </a></td>

                                <td class="detail-newss">
                                    <div class="title-news"><a href="news.php?id={{ item.owner.id}}" class="ng-binding">{{ item.content[0].title<?php echo ucfirst(__LANGUAGE__); ?>}}</a>
                                    </div>
                                    <span ng-bind-html="item.content[0].shortDescription<?php echo ucfirst(__LANGUAGE__); ?> | toTrusted"></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="section-like-post-wall ng-binding">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | <a href="news.php?id={{ item.owner.id}}">ดูรายละเอียดข่าว</a> | <a href="news.php?id={{ item.owner.id}}">แสดงความคิดเห็น</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table ng-if="item.owner.content_type === 'highlight'">
                            <tr>
                                <td>
                                    <div class="titleFor">{{ item.content[0].title}}</div>
                                    <span class="timePostForum">{{ item.owner.created_at | timeagoByDateTime }}</span>
                                    <span ng-click="openReportDialog(item.owner.id)" class="reportPost"><img src="images/icon/downicon.png"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ item.content[0].content}}" embed-video width="500" height="290"></a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="section-like-post-wall ng-binding">
                                    <img ng-click="likeTopic(item.owner.id, idx)" src="images/icon/heart.png"> ({{ item.content[0].like}}) ถูกใจ | <a href="news.php?id={{ item.owner.id}}">ดูรายละเอียดข่าว</a> | <a href="news.php?id={{ item.owner.id}}">แสดงความคิดเห็น</a>

                                    <div class="postBy"><span ng-if="item.owner.display_name || item.owner.fb_firstname" class="by">โดย : <a href="profile.php?id={{ item.owner.fb_uid}}">{{ item.owner.display_name || item.owner.fb_firstname }}</a> |</span> {{ getTopicTypeFromId(item.owner.type).feed_name}}</div>
                                </td>
                            </tr>
                        </table>
                        <div ng-if="item.owner.content_type === 'game'" class="wrapper-feed-wall">
                            <a href="game.php?mid={{ item.content[0].mid}}">
                                <div class="feed-situations">
                                    <div class="img-user-feed">
                                        <img ng-src="/images/countries/{{ item.content[0].competitionId}}.png">
                                        <div xng-show="item.content[0].hdp"><img src="images/icon/bet2.png" style="width: 30px; height: 30px;">
                                            <br/> <b>{{ item.content[0].hdp}}</b><br/>HDP
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>
                                </div>
                                <div class="tableGames">
                                    <table>

                                        <tr class="head-feed">
                                            <td class="time-date-post">{{ item.content[0].show_date | formatDate:'DD MMMM YYYY HH:mm'}}</td>
                                            <td class="league-name-title">{{ allLeagues[item.content[0]._lid].name || item.content[0].LeagueName}}</td>
                                        </tr>
                                        <span class="remove-comment" ng-if="item.owner.fb_uid == facebookInfo.id" ng-click="removeTimeline(item.owner.id, $event)">x</span>
                                        <tr class="teamplay">
                                            <td colspan="2" class="team-playing">

                                                <table>
                                                    <tr>
                                                        <td class="sec-team1"><img ng-src="{{ item.content[0]['h256x256']}}"/></td>
                                                        <td class="hdp-match">
                                                            <!-- {{ allLeagues[item.content[0]._lid].name || item.content[0].LeagueName}}-->
                                                            <!--<br/> {{ item.content[0].show_date | formatDate:'DD MMMM YYYY HH:mm'}}-->
                                                            <span class="success-score" ng-if="item.content[0].sid != '1'">
                                                                <span>{{ item.content[0].score | scoreSelect:'home' }}</span>
                                                                <span>:</span>
                                                                <span>{{ item.content[0].score | scoreSelect:'away' }}</span>
                                                            </span>
                                                            <span class="hdp-game" ng-if="item.content[0].sid == '1'">
                                                                <b>{{item.content[0].hdp}}</b><br/> <span class="macth-hdp">({{ item.content[0].hdp_home}}) : ({{ item.content[0].hdp_away}})</span>
                                                            </span>
                                                        </td>
                                                        <td class="sec-team2"><img ng-src="{{ item.content[0]['a256x256']}}"/> </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table>
                                                                <tr>
                                                                    <td class="teams1">
                                                                        <div class="boxTeam teamLeft">
                                                                            <b ng-bind="parseTeamStat(item.content[0].stat, item.content[0].hid, 'number')">[1]</b> <span class="displayNameTeam" ng-class="{'team-favored': item.content[0].hdp < 0}">{{ allTeams[item.content[0].hid].name || item.content[0].homeName }}</span>
                                                                        </div>
                                                                        <div style="clear: both;"></div>
                                                                        <div class="last-stat-fivematch" ng-init="ssh = parseTeamStat(item.content[0].stat, item.content[0].hid, 'lastplay')">
                                                                            <span ng-repeat="s in ssh track by $index" ng-switch="s">
                                                                                <span ng-switch-when="w" class="text-green">W</span>
                                                                                <span ng-switch-when="l" class="text-red">L</span>
                                                                                <span ng-switch-when="d" class="text-black">D</span>
                                                                                <span ng-if="$index < (ssh.length - 1)"> | </span>
                                                                            </span>
                                                                        </div>
                                                                    </td>
                                                                    <td class="vs vss">
                                                                        <span ng-if="item.content[0].sid == '1'">
                                                                            <a ng-if="canPlay(item.content[0].mid, item.content[0].sid, item.content[0].hdp)" href="/game.php?mid={{ item.content[0].mid}}"><img src="images/icon/bet-play.png"></a>
                                                                            <a ng-if="!canPlay(item.content[0].mid, item.content[0].sid, item.content[0].hdp)" href="/game.php?mid={{ item.content[0].mid}}"><img src="images/icon/bet-view.png"></a>
                                                                        </span>
                                                                    </td>
                                                                    <td class="teams2">
                                                                        <div class="boxTeam teamRight">
                                                                            <span class="displayNameTeam" ng-class="{'team-favored': item.content[0].hdp > 0}">{{ allTeams[item.content[0].gid].name || item.content[0].awayName }}</span><b ng-bind="parseTeamStat(item.content[0].stat, item.content[0].gid, 'number')">[3]</b>
                                                                        </div>
                                                                        <div style="clear: both;"></div>
                                                                        <div class="last-stat-fivematch" ng-init="ssg = parseTeamStat(item.content[0].stat, item.content[0].gid, 'lastplay')">
                                                                            <span ng-repeat="s in ssg track by $index" ng-switch="s">
                                                                                <span ng-switch-when="w" class="text-green">W</span>
                                                                                <span ng-switch-when="l" class="text-red">L</span>
                                                                                <span ng-switch-when="d" class="text-black">D</span>
                                                                                <span ng-if="$index < (ssg.length - 1)"> | </span>
                                                                            </span>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div style="clear: both;"></div>


                    <div class="wrapper-box-feed-comment">
                        <div class="box-show-comment">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="comment">
                                        <div class="upimages-box">
                                            <a href="news.php?id={{ item.owner.id}}">
                                                <img src="images/icon/cam15.png" title="แนบรูปภาพ"></div>
                                        <!--                                                    <img style="width: 25px; height: 25px;" ng-src="https://graph.facebook.com/{{ facebookInfo.id}}/picture">-->
                                        <img style="width: 25px; height: 25px;" ng-src="http://api.ssporting.com/cache/small/{{ facebookInfo.id}}.jpg">
                                        <input type="text" placeholder="comment...">
                                        <span class="hover"></span>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>

<!--            ******************moreloading-->
            <button style="margin-left:9%;margin-top:10px;background-color: #333136;float: left;border:solid 1px #666;width: 91%;" class="buttons-post" ng-click="MoreTimeLine()">
                โหลดเพิ่ม
                <span ng-if="moreloading">
                    <img src="images/icon/loading_icon.gif">
                </span>
            </button>

        </div>
    </div>
</div>


<div style="display: none;">
    <input id="upload-browse-new-topic" type="file" multiple ng-file-select="uploadPictureNewTopic($files)">
    <input id="upload-browse-new-comment" type="file" ng-file-select="uploadPictureNewComment($files)">
    <input id="upload-browse-new-reply" type="file" ng-file-select="uploadPictureNewReply($files)">
    <button ng-click="upload.abort()">Cancel Upload</button>
</div>

<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>


<script>
            function expandTextarea(id) {
            var $element = $('.myclass').get(0);
                    $element.addEventListener('keyup', function() {
                    this.style.overflow = 'hidden';
                            this.style.height = 0;
                            this.style.height = this.scrollHeight + 'px';
                    }, false);
            }

    expandTextarea('txtarea');
</script>