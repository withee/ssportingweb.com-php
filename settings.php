<?php

require_once(dirname(__FILE__) . '/_init_.php');


switch ($_REQUEST['item']) {
    case 'getGmtOffset':
        echo json_encode(array('offset' => __GMT_OFFSET__));
        break;
    case 'saveGmtOffset':
        setcookie('scorspot-gmt_offset', $_REQUEST['offset']);
//        $_SESSION['scorspot']['gmt_offset'] = $_REQUEST['offset'];
        break;
    case 'getLanguage':
        echo __LANGUAGE__;
        break;
    case 'saveLanguage':
        setcookie('scorspot-language', $_REQUEST['language']);
//        $_SESSION['scorspot']['language'] = $_REQUEST['language'];
        break;
    case 'saveFacebookId':
        setcookie('scorspot-facebook-id', $_REQUEST['facebookId']);
        break;
    case 'removeFacebookId':
        setcookie('scorspot-facebook-id', '', time()-3600);
}