<?php
require_once(dirname(__FILE__) . '/_init_.php');

$service_timeline_content = Services::getTimelineContent($_REQUEST['id']);

if (!$service_timeline_content->success) {
    echo $service_timeline_content->desc;

    exit;
}

//echo '<pre>';
//print_r($service_timeline_content->timelines[0]->content);
//echo '</pre>';

$footerScript .= '<script id="newsScript" src="scripts/news.js" data-id="' . $_REQUEST['id'] . '"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>

<div id="news-top-slide-box" class="wrapper-slide-comment-top" style="display: none;">

    <div class="box-comment-top" ng-repeat="item in newsTopSlide">
        <a href="/news.php?id={{ news.ontimelines[item.newsId]}}">
            <table>
                <tr>
                    <td><img ng-src="{{ item.imageLink}}"></td>
                    <td>
                        <b ng-bind="news.titles[item.newsId]"></b>
                        <span class="detail-news" ng-bind="news.desc[item.newsId]"></span>
                    </td>
                </tr>
            </table>
        </a>
    </div>
</div>



<!--Content-->
<div class="wrapper-content content-profile">
    <div class="tab-heading">
        <?php if ($service_timeline_content->timelines[0]->owner->content_type === 'board') { ?>
            <div class="info-user-post">
                <img src="https://graph.facebook.com/<?php echo $service_timeline_content->timelines[0]->owner->fb_uid; ?>/picture" style="width: 40px;">
                <b ng-bind="contents.owner.display_name || contents.owner.fb_firstname"></b><span class="timePost" style="float:right;" ng-bind="contents.owner.created_at | timeagoByDateTime"></span><br>
                <div style="float: left;">
                    <img ng-click="likeTopic(<?php echo $service_timeline_content->timelines[0]->owner->id; ?>)" style="width: 13px; height: 13px; margin-top: 3px; cursor: pointer;" src="images/icon/heart.png"> <span id="like-count"><?php echo $service_timeline_content->timelines[0]->content[0]->like; ?></span>
                </div>
                <div style="clear: both;"></div>
            </div> 

        <?php } else if ($service_timeline_content->timelines[0]->owner->content_type === 'highlight') { ?>
            <img src="images/icon/video-icon.png">  Highlight: <?php echo $service_timeline_content->timelines[0]->content[0]->title ?>
        <?php } else if ($service_timeline_content->timelines[0]->owner->content_type === 'news') { ?>
            <img src="images/icon/news.png"> News: <?php echo $service_timeline_content->timelines[0]->content[0]->{'title' . ucfirst(Utils::getCurrentLanguage())}; ?>
        <?php } ?>
        <div style="clear: both;"></div>
    </div>
    <div class="wrapper-box-feed-expand">
        <?php if ($service_timeline_content->timelines[0]->owner->content_type === 'board') { ?>
            <div style="font-size: 18px; color: #008ae2; font-weight: bold;"><?php echo $service_timeline_content->timelines[0]->content[0]->title ?></div>
            <div class="boardDescription">
                <?php if (Utils::hasImage($service_timeline_content->timelines[0]->content[0]->medialist)): ?>
                    <div class="photoPagePrevNav">
                        <table>
                            <tr>
                                <td></td>
                                <td style="text-align: right; padding-right: 10px;"><a ng-if="lengthIdx > 1" href="" ng-click="gotoImg('prev', $event)"><?php echo Utils::trans('prev'); ?></a></td>
                                <td><a ng-if="lengthIdx > 1" href="" ng-click="gotoImg('next', $event)"><?php echo Utils::trans('next'); ?></a></td>
                            </tr>
                        </table>
                    </div>

                    <?php foreach ($service_timeline_content->timelines[0]->content[0]->medialist as $idx => $item): ?>
                        <div style="margin-bottom: 10px; text-align: center;"><img style="<?php if ($idx > 0): ?>display: none;<?php endif; ?>" class="idx-img" data-idx="<?php echo $idx; ?>" src="<?php echo $item->path; ?>"/></div>
                    <?php endforeach; ?>
                    <div style="clear: both;"></div>
                <?php endif; ?>
                <?php if (Utils::isVideo($service_timeline_content->timelines[0]->content[0]->medialist[0]->path)): ?>
                    <div class="details-video">
                        <a href="<?php echo $service_timeline_content->timelines[0]->content[0]->medialist[0]->path; ?>" embed-video width="540" height="280"></a>
                    </div>
                <?php endif; ?>
                <?php echo nl2br($service_timeline_content->timelines[0]->content[0]->desc); ?>
            </div>



        <?php } else if ($service_timeline_content->timelines[0]->owner->content_type === 'highlight') { ?>
            <div class="details-video">
                <a href="<?php echo $service_timeline_content->timelines[0]->content[0]->content; ?>" embed-video width="540" height="280"></a>
            </div>
        <?php } else if ($service_timeline_content->timelines[0]->owner->content_type === 'news') { ?>
            <div
                class="tab-heading tab-news-title"><?php echo $service_timeline_content->timelines[0]->content[0]->{'shortDescription' . ucfirst(Utils::getCurrentLanguage())}; ?></div>
            <div
                class="image-news"><?php if (!empty($service_timeline_content->timelines[0]->content[0]->imageLink)): ?>
                    <img src="<?php echo $service_timeline_content->timelines[0]->content[0]->imageLink; ?>"/>
                <?php endif; ?></div>
            <div
                class="details-news"><?php echo $service_timeline_content->timelines[0]->content[0]->{'content' . ucfirst(Utils::getCurrentLanguage())}; ?></div>
            <?php } else if ($service_timeline_content->timelines[0]->owner->content_type === 'image') { ?>
            <div class="info-user-post">
                <img src="https://graph.facebook.com/<?php echo $service_timeline_content->timelines[0]->owner->fb_uid; ?>/picture">
                <b ng-bind="contents.owner.display_name || contents.owner.fb_firstname"></b><br><span class="timePost" ng-bind="contents.owner.created_at | timeagoByDateTime"></span>
                <div>
                    <img ng-click="likeTopic(<?php echo $service_timeline_content->timelines[0]->owner->id; ?>)" style="width: 16px; height: 16px;" src="images/icon/heart.png"> <span id="like-count"><?php echo $service_timeline_content->timelines[0]->content[0]->like; ?></span>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div class="images-timeline-post">
                <div class="album-name"><?php echo nl2br(htmlspecialchars($service_timeline_content->timelines[0]->owner->desc)); ?></div>
                <div class="photoPagePrevNav">
                    <table>
                        <tr>
                            <td ng-bind="contents.owner.created_at | timeagoByDateTime"></td>
                            <td style="text-align:right; padding-right: 10px"><a ng-if="lengthIdx > 1" href="" ng-click="gotoImg('prev', $event)"><?php echo Utils::trans('prev'); ?></a></td>
                            <td><a ng-if="lengthIdx > 1" href="" ng-click="gotoImg('next', $event)"><?php echo Utils::trans('next'); ?></a></td>
                        </tr>
                    </table>
                </div>

                <?php foreach ($service_timeline_content->timelines[0]->content as $idx => $item): ?>
                    <img style="<?php if ($idx > 0): ?>display: none;<?php endif; ?>" class="idx-img" data-idx="<?php echo $idx; ?>" src="<?php echo $item->path; ?>"/>
                <?php endforeach; ?>
                <div style="clear: both;"></div>

            </div>
        <?php } else if ($service_timeline_content->timelines[0]->owner->content_type === 'status') { ?>
            <div class="info-user-post">
                <img src="https://graph.facebook.com/<?php echo $service_timeline_content->timelines[0]->owner->fb_uid; ?>/picture">
                <b ng-bind="contents.owner.display_name || contents.owner.fb_firstname"></b><br><span class="timePost" ng-bind="contents.owner.created_at | timeagoByDateTime"></span>
                <div style="clear: both;"></div>
            </div> 
            <div class="details-news"><?php echo $service_timeline_content->timelines[0]->content[0]->message; ?></div>
        <?php } else { ?>

        <?php } ?>
        <div style="clear:both;"></div>

        <div id="comments-block" class="wrapper-box-feed-comment" style="display: none;">
            <!-- comment input -->
            <div class="box-show-comment" style="margin-top: 20px;">
                <table>
                    <tr>
                        <td class="user-comment-img" style="display: none;">
                            <!--<div class="box-like-post">-->
                            <!--<img src="images/icon/heart.png"/> 134-->
                            <!--</div>-->
                        </td>
                        <td class="comment-box-like-match">
                            <div ng-click="browsePicture()" class="upimages-box-match" style="display: none;"><img
                                    src="images/icon/cam15.png"
                                    title="แนบรูปภาพ"/></div>
                            <textarea id="bet-comment-box" ng-model="newComment.message" ng-click="hasBetOn($event)"
                                      ng-enter="addNewComment(comments, $event)" type="text" placeholder="comment..."/></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="tab-ui-post">
                            <div ng-click="browsePicture()" class="tab-upload-img"><img src="images/icon/cam15.png" title="แนบรูปภาพ"/> รูปภาพ</div>
                            <span class="loading" ng-if="inProgress.comment"><img src="images/icon/loading_icon.gif"></span><div class="button-post-img" ng-click="addNewComment(comments, $event)">โพสต์</div>
                        </td>
                    </tr>
                </table>

                <div class="wall" ng-show="newComment.thumbnail">
                    <div class="img-thumbnail-upload">
                        <span class="delete-img" ng-click="removeImage('comment')">x</span>
                        <img class="new-comment-thumbnail" ng-src="{{ newComment.thumbnail}}"/>
                    </div>
                    <button style="display: none;" class="btn" ng-click="addNewComment(comments, $event)">โพสต์</button>
                </div>

                <div class="img-post-comment" ng-show="newComment.thumbnail" style="display: none;">
                    <img class="new-comment-thumbnail" ng-src="{{ newComment.thumbnail}}"/>
                    <button class="btn" ng-click="addNewComment(comments, $event)">โพสต์</button>
                </div>
            </div>

            <!-- end -->

            <div class="box-show-comment" ng-repeat="(index,comment) in comments">
                <span class="remove-ment ment-1" ng-if="facebookInfo.id == comment.fb_uid || facebookInfo.id == sup" ng-click="removeReply(comment.id, false)">x</span>
                <table>
                    <tr>
                        <td class="user-comment-img"><a href="/profile.php?id={{ comment.fb_uid}}"><img
                                    ng-src="https://graph.facebook.com/{{ comment.fb_uid}}/picture"/></a></td>
                        <td class="user-comment-post">
                            <span ng-click="openReportDialog(comment.id, 'reply')" class="reportPost"><img src="images/icon/downicon.png"></span>
                            <a href="/profile.php?id={{ comment.fb_uid}}"><b>{{
                                    comment.display_name}}</b></a> <span
                                class="team-user-selected" ng-switch="comment.choose">
                                <span
                                    ng-switch-when="home">@{{ allTeams[currentMatch.hid].name || currentMatch.hn }}</span>
                                <span
                                    ng-switch-when="away">@{{ allTeams[currentMatch.gid].name || currentMatch.gn }}</span></span>
                            {{ comment.message}}
                            <div ng-show="comment.thumbnail">
                                <img ng-src="{{ comment.thumbnail}}" style="max-width: 500px;"/>
                            </div>
                            <span class="time-post">
                                <ul>
                                    <li class="time-post-comment-user">{{ comment.comment_at | timeago }}</li>
                                    <li class="user-liked"><img
                                            ng-click="likeComment(comment.id, comment.match_id, index)"
                                            src="images/icon/heart.png"/> ({{
                                                    comment.like}}) <span
                                            class="reply" ng-click="openReplyBox(comment.id)">{{ replies[comment.id].length}} reply</span>

                                    </li>
                                </ul>
                            </span>
                        </td>
                    </tr>
                </table>

                <div class="wrapper-reply-comment" data-comment-id="{{ comment.id}}">
                    <div class="wrapper-reply">
                        <div ng-repeat="(r_index, reply) in replies[comment.id]"
                             class="box-show-comment sub-user-comment">
                            <span class="remove-ment ment-2" ng-if="facebookInfo.id == reply.fb_uid || facebookInfo.id == sup" ng-click="removeReply(reply.id, comment.id)">x</span>
                            <table>
                                <tr>
                                    <td class="user-comment-img"><img
                                            ng-src="https://graph.facebook.com/{{ reply.fb_uid}}/picture"/></td>
                                    <td class="user-comment-reply">
                                        <span ng-click="openReportDialog(reply.id, 'reply')" class="reportPost"><img src="images/icon/downicon.png"></span>
                                        <b>{{ reply.displayname}}</b> {{ reply.message}}
                                        <div ng-show="reply.thumbnail">
                                            <img ng-src="{{ reply.thumbnail}}" style="width:300px;"/>
                                        </div>
                                        <span class="time-post">
                                            <ul>
                                                <li>{{ reply.comment_at | timeago }}</li>
                                                <li><img
                                                        ng-click="likeReply(reply.id, comment.id, r_index)"
                                                        src="images/icon/heart.png"/> ({{ reply.like}})

                                                </li>
                                            </ul>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="clear: both;"></div>

                    <!-- reply comment input -->
                    <div class="box-show-comment sub-user-comment reply-box" style="display: none;">
                        <table>
                            <tr>
                                <td class="user-comment-img"><img ng-if="facebookInfo.id"
                                                                  ng-src="https://graph.facebook.com/{{ facebookInfo.id}}/picture"/></td>
                                <td class="user-comment-reply">
                                    <div ng-click="browsePictureReply(comment.id)"
                                         class="upimages-box reply-upload">

                                        <img src="images/icon/cam15.png"
                                             title="แนบรูปภาพ"/></div>
                                    <input ng-model="newReply.message[comment.id]"
                                           ng-click="hasBetOn($event, comment.id)"
                                           ng-enter="addNewReply(comment.id, $event)" type="text"
                                           placeholder="reply..."/>
                                    <span class="loading loading-reply" ng-if="inProgress.reply"><img src="images/icon/loading_icon.gif"/></span>
                                </td>
                            </tr>
                        </table>
                        <div class="img-post-comment" ng-show="newReply.thumbnail">
                            <div ng-click="removeImage('reply')" class="removeImg">x</div>
                            <img class="new-reply-thumbnail" ng-src="{{ newReply.thumbnail}}"/>
                            <div class="tab-reply">
                                <div class="button-post-img" ng-click="addNewReply(comment.id, $event)">โพสต์</div>
                                <div style="clear: both;"></div>
                            </div>
                            <button style="display: none;" class="btn" ng-click="addNewReply(comment.id, $event)">โพสต์</button>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>

            <!-- end -->

        </div>


    </div>
</div>

<!--End-->

<div style="display: none;">
    <input id="upload-browse" type="file" ng-file-select="uploadPicture($files)">
    <input id="upload-browse-reply" type="file" ng-file-select="uploadPictureReply($files)">
    <button ng-click="upload.abort()">Cancel Upload</button>
</div>

<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>