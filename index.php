<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$service_liveMatch = Services::getLiveMatch();
$service_liveWait = Services::getLiveWait();

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

$footerScript .= '<script src="scripts/main.js"></script>';
$host = 'http://api.ssporting.com';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div>
    <div id="display-Ads" class="display-Ads" style="display: none;">
        <div style="top: 120px;" class="wrapperAds">
            <div ng-click="closeAds()" class="closeAds">x</div>
            <img src="images/ads.jpg">
        </div>
    </div>

    <div class="wrapper-content">
        <div class="tab-menu">
            <table>
                <tbody>
                    <tr>
                        <td ng-click="matchFilter = false"
                            ng-class="{'activated': matchFilter == false}"><?php echo Utils::trans('All matches') ?> (<span
                                class="font-orange"
                                ng-bind="live_match_wait_sorted.count.live + live_match_wait_sorted.count.wait || 0"><?php echo $service_liveMatch->count->live + $service_liveMatch->count->wait; ?>
                            </span>)
                        </td>
                        <td ng-click="matchFilter = 'live'" ng-class="{'activated': matchFilter == 'live'}"><img
                                src="images/icon/live-icon.png"/> (<span
                                class="font-orange"
                                ng-bind="live_match_wait_sorted.count.live || 0"><?php echo $service_liveMatch->count->live; ?></span>)
                        </td>
                        <td ng-click="matchFilter = 'bet'"
                            ng-class="{'activated': matchFilter == 'bet'}"><?php echo Utils::trans('Play'); ?> (<span
                                class="font-orange" ng-bind="matchOddsCount || 0">0</span>)
                        </td>
                        <td class="pop-wrapper"
                            ng-class="{'activated': matchFilter == 'selected'}">
                            <span class="sound">
                                <img ng-click="fxSetToggle()" ng-if="fx1 == 'true' || fx2 == 'true' || fx3 == 'true'" ng-src="images/icon/sound.png">
                                <img ng-click="fxSetToggle()" ng-if="fx1 != 'true' && fx2 != 'true' && fx3 != 'true'" ng-src="images/icon/sound-off.png">
                            </span>

                            <div id="fx-set" class="soundSetting" style="display: none;" xng-show="fxSet">
                                <h6>ตั้งค่าเสียง</h6>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>ยิงประตูเข้า</td>
                                            <td>
                                                <img ng-click="fxCtrl('fx1', 'false')" ng-if="fx1 == 'true'" ng-src="images/icon/sound.png">
                                                <img ng-click="fxCtrl('fx1', 'true')" ng-if="fx1 != 'true'" ng-src="images/icon/sound-off.png">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>จบการแข่งขัน</td>
                                            <td>
                                                <img ng-click="fxCtrl('fx2', 'false')" ng-if="fx2 == 'true'" ng-src="images/icon/sound.png">
                                                <img ng-click="fxCtrl('fx2', 'true')" ng-if="fx2 != 'true'" ng-src="images/icon/sound-off.png">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>ได้ใบแดง</td>
                                            <td>
                                                <img ng-click="fxCtrl('fx3', 'false')" ng-if="fx3 == 'true'" ng-src="images/icon/sound.png">
                                                <img ng-click="fxCtrl('fx3', 'true')" ng-if="fx3 != 'true'" ng-src="images/icon/sound-off.png">
                                            </td>
                                        </tr>
                                    </tbody>

                                </table>
                            </div>

                            <span ng-click="matchFilter = 'selected'"><?php echo Utils::trans('Selected'); ?></span>
                            <span class="font-orange">(<span ng-bind="matchNotificationsCount || 0">0</span>)</span></td>
                    </tr>

                </tbody>
            </table>
        </div>


        <div class="content">          
            <div class="box-all-livemath">
                <div class="tab-situations"></div>

                <div id="live_match_table" style="display: none;">
                    <div ng-repeat="league in live_match_sorted.live_league"
                         ng-show="league.matches.length > 0 && (!matchFilter || matchFilter == 'live' || (matchFilter == 'selected' && hasMatchNotification(league.data.subleagueId)))">
                        <div class="box-speech">
                            <div class="box-situations">
                                <img src="images/icon/live.png"/>
                            </div>
                            <div class="speech-left speech-left-red"></div>
                            <div class="speech bg-red"><img ng-src="images/countries/{{ league.data.competitionId}}.png"/> {{
                                            allLeagues[league.data.leagueId].name || league.data.ln }} <span ng-click="hasMatchNotification(league.data.subleagueId)"></span>
                            </div>
                        </div>

                        <div class="box-speech" ng-repeat="match in league.matches"
                             ng-show="(!matchFilter || matchFilter == 'live' || (matchFilter == 'selected' && matchNotifications[match.mid]))">
                            <div class="speech-left speech-left-white"></div>
                            <div class="speech bg-white">
                                <table>
                                    <tr>
                                        <td><span class="font-red-live" data-sid="{{ match.sid}}">{{ currentMin(match.c0, match.c1, match.c2, match.cx, match.sid)}}</span>
                                        </td>
                                        <td class="team1"><img style="padding-right: 2px;" src="images/icon/red-card.png"
                                                               ng-repeat="n in []| range: match.hrci"/> {{ allTeams[match.hid].name
                                                                               || match.hn }}
                                        </td>
                                        <td class="logo-team1"><img ng-src="{{apiUrl}}/teams_clean/team_default_32x32.png"/></td>
                                        <td class="scores"><a href="/match.php?mid={{ match.mid}}"><span class="score-live">{{ match.s1 | formatScore }}</span></a></td>
                                        <td class="logo-team2"><img ng-src="{{apiUrl}}/teams_clean/team_default_32x32.png"/></td>
                                        <td class="team2">{{ allTeams[match.gid].name || match.gn }} <img
                                                style="padding-right: 2px;"
                                                src="images/icon/red-card.png"
                                                ng-repeat="n in []| range: match.grci"/></td>
                                        <td class="scores-team"><span ng-if="match.s2 != '-'">( {{ match.s2 | formatScore}} )</span></td>
                                        <td class="status-team">
                                            <span>
                                                <img ng-click="matchNotification(match, true)" ng-if="matchNotifications[match.mid]"
                                                     ng-src="images/icon/speech.png" style="display: snone;"/>
                                                <img ng-click="matchNotification(match)" ng-if="!matchNotifications[match.mid]"
                                                     ng-src="images/icon/speech-white.png" style="display: snone;"/>
                                            </span><a href="/match.php?mid={{match.mid}}"><img src="images/icon/stat.png"/></a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="live_match_wait_table" style="display: none;">
                    <div ng-repeat="league in live_match_wait_sorted.live_sequence"
                         ng-show="live_match_wait_sorted.live_league[league].matches.length > 0 && (!matchFilter || (matchFilter == 'bet' && live_match_wait_sorted.live_league[league].playable) || (matchFilter == 'selected' && hasMatchNotification(live_match_wait_sorted.live_league[league].data.subleagueId, true)))"
                         ng-if="league != '34885Q'">
                        <div class="box-speech">
                            <div class="box-situations">
                                <img src="images/icon/today.png"/>
                            </div>
                            <div class="speech-left speech-left-blue"></div>
                            <div class="speech bg-blue"><img ng-src="images/countries/{{ live_match_wait_sorted.live_league[league].data.competitionId}}.png"/> {{
                                            allLeagues[live_match_wait_sorted.live_league[league].data.leagueId].name || live_match_wait_sorted.live_league[league].data.ln }}
                            </div>
                        </div>
                        <div class="box-speech" ng-repeat="match in live_match_wait_sorted.live_league[league].matches"
                             ng-show="!matchFilter || (matchFilter == 'bet' && live_match_wait_sorted.hdp[match.mid].hdp_home) || (matchFilter == 'selected' && matchNotifications[match.mid])">
                            <div class="box-situations" ng-if="live_match_wait_sorted.hdp[match.mid].hdp_home">
                                <a href="/game.php?mid={{ match.mid}}"><img src="images/icon/bet2.png"
                                                                            style="width: 25px; height: 25px; margin-left: 2px;  margin-top: 2px; cursor: pointer;"/></a>
                            </div>
                            <div class="speech-left speech-left-white"></div>
                            <div class="speech bg-white">
                                <table>
                                    <tr>
                                        <td><span class="font-red">{{ match.date | kickTime }}</span></td>
                                        <td ng-click="openBetDialog(match, $event)" class="team1"><img
                                                style="padding-right: 2px;" src="images/icon/red-card.png"
                                                ng-repeat="n in []| range: match.hrci"/> {{ allTeams[match.hid].name || match.hn }}
                                        </td>
                                        <td ng-click="openBetDialog(match, $event)" class="logo-team1"><img
                                                ng-src="{{apiUrl}}/teams_clean/team_default_32x32.png"/></td>
                                        <td class="scores">
                                            <a href="/game.php?mid={{ match.mid}}">&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;</a>
                                            <!--                            <a ng-if="live_match_wait_sorted.hdp[match.mid].hdp_home" href="/game.php?mid={{ match.mid }}">&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;</a>-->
                                            <!--                            <a ng-if="!live_match_wait_sorted.hdp[match.mid].hdp_home" href="/match.php?mid={{ match.mid }}">&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;</a>-->
                                        </td>
                                        <td ng-click="openBetDialog(match, $event)" class="logo-team2"><img
                                                ng-src="{{apiUrl}}/teams_clean/team_default_32x32.png"/></td>
                                        <td ng-click="openBetDialog(match, $event)" class="team2">{{ allTeams[match.gid].name ||
                            match.gn }} <img
                                                style="padding-right: 2px;" src="images/icon/red-card.png"
                                                ng-repeat="n in []| range: match.grci"/></td>
                                        <td ng-click="openBetDialog(match, $event)" class="scores-team"></td>
                                        <td class="status-team">
                                            <span>
                                                <img ng-click="matchNotification(match, true)" ng-if="matchNotifications[match.mid]"
                                                     ng-src="images/icon/speech.png" style="display: snone;"/>
                                                <img ng-click="matchNotification(match)" ng-if="!matchNotifications[match.mid]"
                                                     ng-src="images/icon/speech-white.png" style="display: snone;"/>
                                            </span> <a href="/match.php?mid={{ match.mid}}"><img src="images/icon/stat.png"/></a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="temp-live-match">
                    <?php foreach ($service_liveMatch->live_league as $league): ?>
                        <?php
                        if (__::any($service_liveMatch->live_match, function ($val) use ($league) {
                                    return $val->lid == $league->subleagueId;
                                })
                        ):
                            ?>
                            <div
                                ng-show="!matchFilter || matchFilter == 'live' || (matchFilter == 'selected' && league.hasNotifications)">
                                <div class="box-speech">
                                    <div class="box-situations">
                                        <img src="images/icon/live.png"/>
                                    </div>
                                    <div class="speech-left speech-left-red"></div>
                                    <div class="speech bg-red"><img
                                            src="images/countries/<?php echo $league->competitionId; ?>.png"/>
                                            <?php echo isset($service_allleague->{$league->leagueId}->name) ? $service_allleague->{$league->leagueId}->name : $league->ln; ?>
                                    </div>
                                </div>

                                <?php foreach ($service_liveMatch->live_match as $match): ?>
                                    <?php if ($match->lid == $league->subleagueId): ?>
                                        <div class="box-speech"
                                             ng-show="(!matchFilter || matchFilter == 'live' || (matchFilter == 'selected' && league.hasNotifications))">
                                            <div class="speech-left speech-left-white"></div>
                                            <div class="speech bg-white">
                                                <table>
                                                    <tr>
                                                        <td><span class="font-red-live">?</span></td>
                                                        <td class="team1"><img style="display: none; padding-right: 2px;"
                                                                               src="images/icon/red-card.png"
                                                                               ng-repeat="n in []| range: match.hrci"/>
                                                                               <?php echo isset($service_allteam->{$match->hid}->name) ? $service_allteam->{$match->hid}->name : $match->hn; ?>
                                                        </td>
                                                        <td class="logo-team1"><img
                                                                src="{{apiUrl}}/teams_clean/team_default_32x32.png"/>
                                                        </td>
                                                        <td class="scores"><span
                                                                class="score-live"><?php echo $match->s1; ?></span></td>
                                                        <td class="logo-team2"><img
                                                                src="{{apiUrl}}/teams_clean/team_default_32x32.png"/>
                                                        </td>
                                                        <td class="team2"><?php echo isset($service_allteam->{$match->gid}->name) ? $service_allteam->{$match->gid}->name : $match->gn; ?>
                                                            <img
                                                                style="display: none; padding-right: 2px;"
                                                                src="images/icon/red-card.png"
                                                                ng-repeat="n in []| range: match.grci"/></td>
                                                        <td class="scores-team"><?php if ($match->s2 != '-'): ?>( <?php echo $match->s2; ?> )<?php endif; ?></td>
                                                        <td class="status-team">
                                                            <span>
                                                                <img ng-click="matchNotification(match)" ng-show="!matchNotifications[match.mid]" ng-src="images/icon/speech-white.png"/>
                                                            </span>
                                                            <a href="/match.php?mid=<?php echo $match->mid; ?>"><img src="images/icon/stat.png"/></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                <?php endforeach; ?>

                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>

                <div id="temp-live-match-wait">
                    <?php foreach ($service_liveWait->live_league as $league): ?>
                        <?php
                        if (__::any($service_liveWait->live_match, function ($val) use ($league) {
                                    return $val->lid == $league->subleagueId;
                                })
                        ):
                            ?>

                            <div
                                ng-show="(!matchFilter || (matchFilter == 'bet' && league.hasOdds) || (matchFilter == 'selected' && league.hasNotifications))">
                                <div class="box-speech">
                                    <div class="box-situations">
                                        <img src="images/icon/today.png"/>
                                    </div>
                                    <div class="speech-left speech-left-blue"></div>
                                    <div class="speech bg-blue"><img
                                            src="images/countries/<?php echo $league->competitionId; ?>.png"/>
                                            <?php echo isset($service_allleague->{$league->leagueId}->name) ? $service_allleague->{$league->leagueId}->name : $league->ln; ?>
                                    </div>
                                </div>

                                <?php foreach ($service_liveWait->live_match as $match): ?>
                                    <?php if ($match->lid == $league->subleagueId): ?>
                                        <div class="box-speech"
                                             ng-show="!matchFilter || (matchFilter == 'bet' && match.hasOdds) || (matchFilter == 'selected' && match.hasNotification)">
                                            <div class="box-situations" ng-show="match.hasOdds"
                                                 ng-click="openBetDialog(match, $event)">
                                                <img src="images/icon/bet2.png"
                                                     style="display: none; width: 25px; height: 25px; margin-left: 2px;  margin-top: 2px; cursor: pointer;"/>
                                            </div>
                                            <div class="speech-left speech-left-white"></div>
                                            <div class="speech bg-white">
                                                <table>
                                                    <tr>
                                                        <td><span
                                                                class="font-red"><?php echo Utils::kickTime($match->date); ?></span>
                                                        </td>
                                                        <td ng-click="openBetDialog(match, $event)" class="team1"><img
                                                                style="display: none; padding-right: 2px;"
                                                                src="images/icon/red-card.png"
                                                                ng-repeat="n in []| range: match.hrci"/> <?php echo isset($service_allteam->{$match->hid}->name) ? $service_allteam->{$match->hid}->name : $match->hn; ?>
                                                        </td>
                                                        <td ng-click="openBetDialog(match, $event)" class="logo-team1"><img
                                                                src="{{apiUrl}}/teams_clean/team_default_32x32.png"/>
                                                        </td>
                                                        <td ng-click="openBetDialog(match, $event)" class="scores">-
                                                        </td>
                                                        <td ng-click="openBetDialog(match, $event)" class="logo-team2"><img
                                                                src="{{apiUrl}}/teams_clean/team_default_32x32.png"/>
                                                        </td>
                                                        <td ng-click="openBetDialog(match, $event)"
                                                            class="team2"><?php echo isset($service_allteam->{$match->gid}->name) ? $service_allteam->{$match->gid}->name : $match->gn; ?>
                                                            <img
                                                                style="display: none; padding-right: 2px;"
                                                                src="images/icon/red-card.png"
                                                                ng-repeat="n in []| range: match.grci"/></td>
                                                        <td ng-click="openBetDialog(match, $event)" class="scores-team"></td>
                                                        <td class="status-team">
                                                            <span>
                                                                <img ng-click="matchNotification(match)" ng-show="!matchNotifications[match.mid]" ng-src="images/icon/speech-white.png" style="display: none;"/>
                                                            </span> <a href="/match.php?mid=<?php echo $match->mid; ?><img src="images/icon/stat.png"/></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>


                            </div>

                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>

            </div>
            <div style="clear: both;"></div>
        </div>

    </div>
</div>


<audio id="fx1" src="sounds/fx1.mp3"></audio>
<audio id="fx2" src="sounds/fx2.mp3"></audio>
<audio id="fx3" src="sounds/fx3.mp3"></audio>
<audio id="fx4" src="sounds/fx4.mp3"></audio>
<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
