<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$service_liveMatch = Services::getLiveMatch();
$service_liveWait = Services::getLiveWait();

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

$footerScript .= '<script src="scripts/main.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div ng-controller="mainCtrl">


    <!--Content-->
    <div class="wrapper-content content-profile">

        <div class="wrapper-miniGames">
            <div class="tab-heading-title tabTypeGames"><img src="images/mini-game/card/title_icon_g3.png" width="40px;" style="float: left;"> เกมส์ไพ่ป๊อกเด้ง</div>
            <div class="totalPoint correct poker" style="display: none;">
                <b>คุณชนะ</b>
            </div>
            <div class="bodyGames">
                <table>
                    <tr>
                        <td>
                            <span class="player"><img src="https://graph.facebook.com/100001037283424/picture" style="width: 40px;"></span>
                            <div class="cards">
                                <img src="images/mini-game/card/card_red_front.png">
                                <img src="images/mini-game/card/card_red_front.png">
                                <img src="images/mini-game/card/card_red_front.png">
                            </div>
                            <span class="scoreCard">ป๊อก 9</span>
                        </td>
                        <td>
                            <span class="player"><img src="images/mini-game/paoyingchoob/avata_GM_g1.png" style="width: 40px;"></span>
                            <div class="cards">
                                <img src="images/mini-game/card/card_red_front.png">
                                <img src="images/mini-game/card/card_red_front.png">
                                <img src="images/mini-game/card/card_red_front.png">
                            </div>
                            <span class="scoreCard">5 แต้ม</span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="selectCoin">
                <table>
                    <tr>
                        <td class="selectCoin-100 activeCoin-100"></td>
                        <td class="selectCoin-200"></td>
                        <td class="selectCoin-500"></td>
                    </tr>
                </table>
            </div>

            <div class="PlayGames">
                <div class="Play"></div>
            </div>


            <div class="titleGames">วิธีการเล่นเกมส์</div>
            <div class="boxHowToPlay-Games">
                <ul>
                    <li><b>เกมส์ไพ่ป๊อกเด้ง เป็นเกมส์ที่ผู้เล่นแข่งกับคอมพิวเตอร์ว่าฝ่ายไหนจะมีแต้มสูงกว่ากัน โดยมีวิธีการเล่นดังนี้</b></li>
                    <li>- เลือกจำนวนเหรียญที่ต้องการเดิมพัน โดยมีให้เลือก 100, 200, 500 เหรียญ Scoin</li>
                    <li>- จากนั้นผู้เล่นจะได้รับไพ่  2  ใบ โดยจะเมื่อนำตัวเลขไพ่ทั้งสองมาบวกกันแล้วจะเป็นแต้มของผู้เล่น โดยนำแต้มของผู้เล่นมาเปรียบเทียบกับแต้มของคอมพิวเตอร์ แต้มฝ่ายสูงกว่าก็จะเป็นผู้ชนะ โดยมีวิธีคิดคะแนนดังนี้</li>
                </ul>

                <div class="mores">
                    <input type="checkbox" id="toggle">
                    <label for="toggle">ดูเพิ่มเติม</label>
                    <div class="to-be-changed">
                        - แต้มที่นำมาเปรียบเทียบกัน คือ  1-9  แต้ม โดยเมื่อตัวเลขบวกกันได้ 10 แต้ม จะนับเป็น 0 แต้ม หรือ ถ้าตัวเลขบวกกัน เกิน 10 แต้ม ให้นับแต้มจากตัวเลขหลักหน่วย เช่น 9+8 = 17  จะได้เท่ากับ 7 แต้ม <br>
                        - เมื่อนำตัวเลขไพ่ทั้งสองใบบวกกันแล้วได้น้อยกว่าหรือเท่ากับ 4 แต้ม ระบบจะให้ไพ่ใบที่ 3 อัตโนมัติ แล้วนำตัวเลขไพ่ทั้ง 3 มาบวกกัน แล้วนำแต้มที่ได้มาเปรียบเทียบกับแต้มของคอมพิวเตอร์ แต้มฝ่ายไหนสูงกว่าก็จะเป็นผู้ชนะ เมื่อผู้เล่นชนะจะได้รับเหรียญ 1 เท่า<br>
                        - หรือถ้าไพ่ของผู้เล่นทั้ง 2 ใบมีดอกที่เหมือนกัน และแต้มสูงกว่าคอมพิวเตอร์ ผู้เล่นชนะจะได้รับเหรียญ 2 เท่า เช่น ไพ่โพดำ 5  ไพ่โพดำ 2  เมื่อนำมาบวกกันจะได้ 7 แต้ม 2 เด้ง <br>
                        - หรือถ้าไพ่ของผู้เล่นทั้ง 3 ใบมีดอกที่เหมือนกัน และแต้มสูงกว่าคอมพิวเตอร์ ผู้เล่นชนะจะได้รับเหรียญ 3 เท่า<br>
                        เช่น ไพ่โพดำ 7  ไพ่โพดำ 5  ไพ่โพดำ 3  เมื่อนำมาบวกกันจะได้ 5 แต้ม 3 เด้ง<br><br>
                        <b>เงื่อนไขที่ได้คะแนนพิเศษ</b><br>
                        -	ถ้าไพ่ 3 ใบ เป็น J,Q,K เช่น J,J,K  จะได้รับเหรียญ 3 เท่า<br>
                        -	ถ้าไพ่ 3 ใบ เรียงกันทั้ง 3 ใบ เช่น 5,6,7 หรือ J,Q,K  จะได้รับเหรียญ 4 เท่า<br>
                        -	ถ้าไพ่ 3 ใบ เรียงกันทั้ง 3 ใบ และดอกเหมือนกัน จะได้รับเหรียญ 5 เท่า<br>
                        -	ถ้าไพ่ 3 ใบ เหมือนกันทุกใบ เช่น 3,3,3 / J,J,J  เรียกว่า ไพ่ตอง จะได้รับเหรียญ 5 เท่า
                    </div>
                </div>

            </div>


            <!--            ตารางสรุปผลการเล่นเกมส์-->
            <div class="titleGames">ตารางสรุปผลการเล่นเกมส์</div>

            <div class="table-ResultMiniGames">
                <div class="tabs-tableResult">
                    <ul>
                        <li class="active">เฉพาะฉัน</li>
                        <li>ทุกคน</li>
                    </ul>
                    <div style="clear: both;"></div>
                </div>

                <table>
                    <thead>
                        <tr>
                            <th>ชื่อผู้เล่น</th>
                            <th>ชื่อเกมส์</th>
                            <th>ผู้เล่น</th>
                            <th>คอมพ์</th>
                            <th>ผลการเล่น</th>
                            <th>จำนวน</th>
                            <th>ได้/เสีย</th>
                            <th>วันที่/เวลา</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><img src="images/imguser.png"> หลวงปู่เณรคำ</td>
                            <td>ป๊อกเด้ง</td>
                            <td>ป๊อก 9</td>
                            <td>5 แต้ม</td>
                            <td>แพ้</td>
                            <td><img src="images/icon/scoin.png"> 100</td>
                            <td><img src="images/icon/scoin.png"> 200</td>
                            <td>22/6/57 10:00</td>
                        </tr>
                        <tr class="bgGray">
                            <td><img src="images/imguser.png"> หลวงปู่เณรคำ</td>
                            <td>ป๊อกเด้ง</td>
                            <td>ป๊อก 9</td>
                            <td>5 แต้ม</td>
                            <td>แพ้</td>
                            <td><img src="images/icon/scoin.png"> 100</td>
                            <td><img src="images/icon/scoin.png"> 200</td>
                            <td>22/6/57 10:00</td>
                        </tr>
                        <tr>
                            <td><img src="images/imguser.png"> หลวงปู่เณรคำ</td>
                            <td>ป๊อกเด้ง</td>
                            <td>ป๊อก 9</td>
                            <td>5 แต้ม</td>
                            <td>แพ้</td>
                            <td><img src="images/icon/scoin.png"> 100</td>
                            <td><img src="images/icon/scoin.png"> 200</td>
                            <td>22/6/57 10:00</td>
                        </tr>
                        <tr class="bgGray">
                            <td><img src="images/imguser.png"> หลวงปู่เณรคำ</td>
                            <td>ป๊อกเด้ง</td>
                            <td>ป๊อก 9</td>
                            <td>5 แต้ม</td>
                            <td>แพ้</td>
                            <td><img src="images/icon/scoin.png"> 100</td>
                            <td><img src="images/icon/scoin.png"> 200</td>
                            <td>22/6/57 10:00</td>
                        </tr>


                    </tbody>
                </table>
            </div>

        </div>






    </div>
</div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
