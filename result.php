<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ผลบอลสด อัพเดทรวดเร็วที่สุดและแม่นยำที่สุด';
$meta = '<meta name="description" content="เช็คผลบอลสด ผลบอลเมื่อคืนและผลบอลย้อนหลังได้ที่นี่ ข้อมูลแม่นยำอัพเดทผลรวดเร็ว ซึ่งจะทำให้คุณไม่พลาดทุกวินาทีสำคัญ">' . "\n";
$meta .= '<meta name="keyword" content="ผลบอลสด,ผลบอล,ผลบอลเมื่อคืน,ผลบอลย้อนหลัง,ไฮไลท์ฟุตบอล">' . "\n";

$service_todayResult = Services::getTodayResult();
$service_yesterdayResult = Services::getYesterdayResult();

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

$footerScript .= '<script src="scripts/result.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>


    <div class="wrapper-content">
        <div class="tab-menu-result">
            <table>
                <tbody>
                <tr>
                    <td ng-class="{'activated': currentTab == 'today'}" ng-click="currentTab = 'today'"><?php echo Utils::trans('Today Result'); ?> <span class="font-orange">(<?php echo $service_todayResult->count->result; ?>)</span></td>
                    <td ng-class="{'activated': currentTab == 'yesterday'}" ng-click="currentTab = 'yesterday'"><?php echo Utils::trans('Yesterday Result'); ?> <span class="font-orange">(<?php echo $service_todayResult->count->yesterday; ?>)</span></td>
                </tr>

                </tbody>
            </table>
        </div>

        <div class="content">
            <div class="box-all-livemath">
                <div class="tab-situations"></div>
                <?php foreach ($service_todayResult->live_league as $league): ?>
                    <?php if (__::any($service_todayResult->live_match, function ($val) use ($league) {
                        return $val->lid == $league->subleagueId;
                    })
                    ): ?>
                        <div id="today-result-box-list" ng-show="currentTab == 'today'">
                            <div class="box-speech">
                                <div class="box-situations">
                                    <img src="images/icon/result-icon.png"/>
                                </div>
                                <div class="speech-left speech-left-green"></div>
                                <div class="speech bg-green"><img
                                        src="images/countries/<?php echo $league->competitionId; ?>.png"/>
                                    <?php echo isset($service_allleague->{$league->leagueId}->name) ? $service_allleague->{$league->leagueId}->name : $league->ln; ?>
                                </div>
                            </div>
                            <?php foreach ($service_todayResult->live_match as $match): ?>
                                <?php if ($match->lid == $league->subleagueId): ?>
                                    <div class="box-speech">
                                        <div class="speech-left speech-left-white"></div>
                                        <div class="speech bg-white">
                                            <table>
                                                <tr>
                                                    <td><span class="font-red"><?php echo Utils::$sidLabel[$match->sid]; ?></span></td>
                                                    <td class="team1">
                                                        <?php for($i = 0; $i < (int)$match->hrci; $i++): ?>
                                                            <img style="padding-right: 2px;" src="images/icon/red-card.png"/>
                                                        <?php endfor; ?>
                                                        <?php echo isset($service_allteam->{$match->hid}->name) ? $service_allteam->{$match->hid}->name : $match->hn; ?>
                                                    </td>
                                                    <td class="logo-team1"><img src="{{apiUrl}}/teams_clean/team_default_32x32.png"/></td>
                                                    <td class="scores"><a href="/match.php?mid=<?php echo $match->mid; ?>"><span class="font-red"><?php echo $match->s1; ?></span></a></td>
                                                    <td class="logo-team2"><img src="{{apiUrl}}/teams_clean/team_default_32x32.png"/></td>
                                                    <td class="team2"><?php echo isset($service_allteam->{$match->gid}->name) ? $service_allteam->{$match->gid}->name : $match->gn; ?>
                                                        <?php for($i = 0; $i < (int)$match->grci; $i++): ?>
                                                            <img style="padding-right: 2px;" src="images/icon/red-card.png"/>
                                                        <?php endfor; ?>
                                                    </td>
                                                    <td class="scores-team" style="width: 62px;"><?php if(!empty($match->s2)): ?>( <?php echo $match->s2; ?> )<?php endif; ?></td>
                                                    <td class="status-team" style="width: 20px;"><a href="/match.php?mid=<?php echo $match->mid; ?>"><img src="images/icon/stat.png"/></a> </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
                <?php foreach ($service_yesterdayResult->live_league as $league): ?>
                    <?php if (__::any($service_yesterdayResult->live_match, function ($val) use ($league) {
                        return $val->lid == $league->subleagueId;
                    })
                    ): ?>
                        <div id="yesterday-result-box-list" ng-show="currentTab == 'yesterday'">
                            <div class="box-speech">
                                <div class="box-situations">
                                    <img src="images/icon/result-icon.png"/>
                                </div>
                                <div class="speech-left speech-left-green"></div>
                                <div class="speech bg-green"><img
                                        src="images/countries/<?php echo $league->competitionId; ?>.png"/>
                                    <?php echo isset($service_allleague->{$league->leagueId}->name) ? $service_allleague->{$league->leagueId}->name : $league->ln; ?>
                                </div>
                            </div>
                        <?php foreach ($service_yesterdayResult->live_match as $match): ?>
                            <?php if ($match->lid == $league->subleagueId): ?>
                                <div class="box-speech">
                                    <div class="speech-left speech-left-white"></div>
                                    <div class="speech bg-white">
                                        <table>
                                            <tr>
                                                <td><span class="font-red"><?php echo Utils::$sidLabel[$match->sid]; ?></span></td>
                                                <td class="team1">
                                                    <?php for($i = 0; $i < (int)$match->hrci; $i++): ?>
                                                        <img style="padding-right: 2px;" src="images/icon/red-card.png"/>
                                                    <?php endfor; ?>

                                                    <?php echo isset($service_allteam->{$match->hid}->name) ? $service_allteam->{$match->hid}->name : $match->hn; ?>
                                                </td>
                                                <td class="logo-team1"><img src="{{apiUrl}}/teams_clean/team_default_32x32.png"/></td>
                                                <td class="scores"><a href="/match.php?mid=<?php echo $match->mid; ?>"><span class="font-red"><?php echo $match->s1; ?></span></a></td>
                                                <td class="logo-team2"><img src="{{apiUrl}}/teams_clean/team_default_32x32.png"/></td>
                                                <td class="team2"><?php echo isset($service_allteam->{$match->gid}->name) ? $service_allteam->{$match->gid}->name : $match->gn; ?>
                                                    <?php for($i = 0; $i < (int)$match->grci; $i++): ?>
                                                        <img style="padding-right: 2px;" src="images/icon/red-card.png"/>
                                                    <?php endfor; ?>
                                                </td>
                                                <td class="scores-team" style="width: 62px;"><?php if(!empty($match->s2)): ?>( <?php echo $match->s2; ?> )<?php endif; ?></td>
                                                <td class="status-team" style="width: 20px;"><a href="/match.php?mid=<?php echo $match->mid; ?>"><img src="images/icon/stat.png"/></a> </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>

                        </div>

                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <div style="clear: both;"></div>
        </div>
    </div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>