<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$service_liveMatch = Services::getLiveMatch();
$service_liveWait = Services::getLiveWait();

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

$footerScript .= '<script src="scripts/main.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div ng-controller="mainCtrl">


    <!--Content-->
    <div class="wrapper-content content-profile">

        <div class="wrapper-miniGames">
            <div class="tab-heading-title"><img src="images/mini-game/hightlow/title_icon_g2.png" width="25px;"> เกมส์ไฮโล</div>
            <div class="totalPoint">
                <b>11 แต้ม</b> <br>
                <div class="resultAlert correct">คุณทายถูก</div>
                <div class="resultAlert wrong" style="display: none;">คุณทายผิด</div>    
            </div>
            <div class="gameHightLow">
                <table>
                    <tr>
                        <td>
                            <span class="point">1</span>
                            <img src="images/mini-game/hightlow/dice_No_01.png">
                        </td>
                        <td>
                            <span class="point">4</span>
                            <img src="images/mini-game/hightlow/dice_No_04.png">
                        </td>
                        <td>
                            <span class="point">6</span>
                            <img src="images/mini-game/hightlow/dice_No_06.png">
                        </td>
                    </tr>
                </table>
            </div>
            <div class="selectCoin">
                <table>
                    <tr>
                        <td class="selectCoin-100 activeCoin-100"></td>
                        <td class="selectCoin-200"></td>
                        <td class="selectCoin-500"></td>
                    </tr>
                </table>
            </div>

            <div class="selectWeapon-HightLow">
                <table>
                    <tr>
                        <td class="weaponLow"></td>                      
                        <td class="weaponEleven activeEleven"></td>
                        <td class="weaponHight"></td>
                    </tr>
                </table>
            </div>



            <!--            ตารางสรุปผลการเล่นเกมส์-->
            <div class="titleGames">ตารางสรุปผลการเล่นเกมส์</div>

            <div class="table-ResultMiniGames">
                <div class="tabs-tableResult">
                    <ul>
                        <li class="active">เฉพาะฉัน</li>
                        <li>ทุกคน</li>
                    </ul>
                    <div style="clear: both;"></div>
                </div>

                <table>
                    <thead>
                        <tr>
                            <th>ชื่อผู้เล่น</th>
                            <th>ชื่อเกมส์</th>
                            <th>ผู้เล่น</th>
                            <th>คอมพ์</th>
                            <th>ผลการเล่น</th>
                            <th>จำนวน</th>
                            <th>ได้/เสีย</th>
                            <th>วันที่/เวลา</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><img src="images/imguser.png"> หลวงปู่เณรคำ</td>
                            <td>ไฮโล</td>
                            <td><img src="images/mini-game/hightlow/btn_low_active.png"></td>
                            <td><img src="images/mini-game/hightlow/btn_hight_active.png"></td>
                            <td>แพ้</td>
                            <td><img src="images/icon/scoin.png"> 100</td>
                            <td><img src="images/icon/scoin.png"> 200</td>
                            <td>22/6/57 10:00</td>
                        </tr>
                        <tr class="bgGray">
                            <td><img src="images/imguser.png"> หลวงปู่เณรคำ</td>
                            <td>ไฮโล</td>
                            <td><img src="images/mini-game/hightlow/btn_low_active.png"></td>
                            <td><img src="images/mini-game/hightlow/btn_hight_active.png"></td>
                            <td>แพ้</td>
                            <td><img src="images/icon/scoin.png"> 100</td>
                            <td><img src="images/icon/scoin.png"> 200</td>
                            <td>22/6/57 10:00</td>
                        </tr>
                        <tr>
                            <td><img src="images/imguser.png"> หลวงปู่เณรคำ</td>
                            <td>ไฮโล</td>
                            <td><img src="images/mini-game/hightlow/btn_low_active.png"></td>
                            <td><img src="images/mini-game/hightlow/btn_hight_active.png"></td>
                            <td>แพ้</td>
                            <td><img src="images/icon/scoin.png"> 100</td>
                            <td><img src="images/icon/scoin.png"> 200</td>
                            <td>22/6/57 10:00</td>
                        </tr>
                        <tr class="bgGray">
                            <td><img src="images/imguser.png"> หลวงปู่เณรคำ</td>
                            <td>ไฮโล</td>
                            <td><img src="images/mini-game/hightlow/btn_low_active.png"></td>
                            <td><img src="images/mini-game/hightlow/btn_hight_active.png"></td>
                            <td>แพ้</td>
                            <td><img src="images/icon/scoin.png"> 100</td>
                            <td><img src="images/icon/scoin.png"> 200</td>
                            <td>22/6/57 10:00</td>
                        </tr>


                    </tbody>
                </table>
            </div>

        </div>






    </div>
</div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
