<?php

switch ($_REQUEST['type']) {
    case 'match':
        $id = $_REQUEST['id'];
        $content = file_get_contents('http://' . $_SERVER['SERVER_NAME'] . ':3801/match/' . $id);

        $dest = dirname(__FILE__) . '/WebObject/images';
        if (!is_dir($dest)) {
            mkdir($dest, 0777, true);

        }

        file_put_contents($dest . '/m' . $id . '.jpg', $content);
        header('Content-type: image/png');
        echo $content;
        break;
    case 'square':
        $id = $_REQUEST['id'];
        $content = file_get_contents('http://' . $_SERVER['SERVER_NAME'] . ':3801/square/' . $id);

        $dest = dirname(__FILE__) . '/WebObject/images';
        if (!is_dir($dest)) {
            mkdir($dest, 0777, true);

        }

        file_put_contents($dest . '/s' . $id . '.jpg', $content);
        header('Content-type: image/png');
        echo $content;
        break;
    default:
}