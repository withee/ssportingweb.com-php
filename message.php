<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";


$footerScript .= '<script id="mainScript" src="scripts/message.js" data-id="' . $_REQUEST['id'] . '"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div>


    <div class="wrapper-content content-profile">
        <div class="boxSendMessage">
            <div class="tab-heading-title" style="float: left;"><img src="images/icon/letter.png" style="width: 20px;"> กล่องข้อความ</div>
            <button ng-click="openPmDialog()" class="btn btn-danger btn-small" style="float: right; margin-right: 5px;">ส่งข้อความ</button>
            <div style="clear: both;"></div>
        </div>
        <div class="wrapper-feed">

            <div class="wrapperBoxMessage">
                <div class="boxOtherChat">
                    <div class="selectMailType">
                        <div ng-class="{'activeMessageType':currentBox === 'inbox'}" class="MailType1" ng-click="currentBox = 'inbox'">กล่องขาเข้า ({{ inbox.quantity || 0 }})</div>
                        <div ng-class="{'activeMessageType':currentBox === 'sendbox'}" class="MailType2" ng-click="currentBox = 'sendbox'">กล่องขาออก ({{ sendbox.quantity || 0 }})</div>
                        <div style="clear: both;"></div>
                    </div>
                    <table ng-if="currentBox === 'inbox'">
                        <tr ng-repeat="item in inbox.messagelist" ng-click="setCurrentView(item)">
                            <td><img ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"></td>
                            <td><b>{{ item.display_name}}</b><span class="textMessage">{{ item.message}}</span></td>
                            <td>{{ item.send_at | timeagoByDateTime }}</td>
                        </tr>
                    </table>
                    <table ng-if="currentBox === 'sendbox'">
                        <tr ng-repeat="item in sendbox.messagelist" ng-click="setCurrentView(item)">
                            <td><img ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"></td>
                            <td><b>{{ item.display_name}}</b><span class="textMessage">{{ item.message}}</span></td>
                            <td>{{ item.send_at | timeagoByDateTime }}</td>
                        </tr>
                    </table>
                </div>

                <div class="boxAllChat" ng-if="currentView">
                    <table>
                        <tr>
                            <td><img ng-src="https://graph.facebook.com/{{ currentView.fb_uid}}/picture"></td>
                            <td>
                                <div class="tabUserMessage">
                                    <b>{{ currentView.display_name}}</b> <span class="timeChat">{{ currentView.send_at | timeagoByDateTime }}</span>
                                </div>
                                <div class="message" ng-bind-html="currentView.message | nl2br"></div>
                                <div ng-repeat="item in currentView.medialist" class="imageAttrach" style="display: snone;"><img ng-src="{{ item.path }}"></div>
                            </td>
                        </tr>


                    </table>
                    <div style="clear: both;"></div>

                    <div class="boxReplyMessage">
                        <textarea placeholder="เขียนข้อความตอบกลับ" ng-model="pmMessage" ng-enter="sendPm(pmMessage, currentView.sender); pmMessage = ''"></textarea>
                        <span class="textReply">กด Enter เพื่อส่ง</span>
                        <div style="clear: both;"></div>
                    </div>


                </div>


                <div style="clear: both;"></div>
            </div>








        </div>


        <!--Modal ส่ง PM-->
        <div class="section-video" style="display: none;">
            <div class="modal-backdrop fade in"></div>
            <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="false" style="display: block;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">ข้อความใหม่</h4>
                    </div>
                    <div class="modal-body">
                        <div class="boxMessage">
                            <table>
                                <tr style="border-bottom: solid 1px #ccc;">
                                    <td>ถึง : <input type="text" class="span6"></td>
                                </tr>
                                <tr>
                                    <td> <textarea ng-model="pmMessage" placeholder="เขียนข้อความ..."></textarea></td>
                                </tr>
                            </table>
                        </div>
                        <div class="sectionAttrachment" style="display: none;">
                            <div class="image-post-to-wall">
                                <span class="delete-img-postwall">x</span>
                                <img src="images/test2.jpg">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <img src="images/icon/attrach.png" style="width: 32px; float: left; cursor: pointer;" title="แนบรูปภาพ">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Send</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--End-->
</div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
