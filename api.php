<?php

set_error_handler(
    create_function(
        '$severity, $message, $file, $line',
        'throw new ErrorException($message, $severity, $severity, $file, $line);'
    )
);


$host = 'http://api.ssporting.com';
$service = false;
$uri = false;

if (preg_match("/.php$/", $_SERVER['REQUEST_URI'])) {
    if (preg_match("#^ws#", $_SERVER['QUERY_STRING'])) {
        $service = 'ws';
        $uri = preg_replace('#^ws/#', "", $_SERVER['QUERY_STRING']);
    } else if (preg_match("#^upload#", $_SERVER['QUERY_STRING'])) {
        $service = 'upload';
        $uri = preg_replace('#^upload/#', "", $_SERVER['QUERY_STRING']);
    }
} else {
    preg_match("#^/api/(.+)/(.+)#is", $_SERVER['REQUEST_URI'], $match);

//    print_r($match);exit;

    if (preg_match("#^ws#", $match[1])) {
        $service = 'ws';
        $uri = preg_replace("#^/api/ws/#", "", $match[0]);
    } else if (preg_match("#^upload#", $match[1])) {
        $service = 'upload';
        $uri = preg_replace("#^/api/upload/#", "", $match[0]);
    } else if (preg_match("#^post#", $match[1])) {
        $service = 'post';
        $uri = preg_replace("#^/api/post/#", "", $match[0]);

//        print_r($uri);
//        exit;
    } else if (preg_match("#^put#", $match[1])) {
        $service = 'put';
        $uri = preg_replace("#^/api/put/#", "", $match[0]);
    }
}

//echo '<pre>';
//print_r($service);
//echo '<br/>';
//print_r($uri);
//exit;


try {

    if ($service && $uri) {
        switch ($service) {
            case 'ws':
                $uri = $host . '/' . $uri;
                if (preg_match("#\?#is", $uri)) {
                    $uri .= '&' . date('U');
                } else {
                    $uri .= '?' . date('U');
                }

                $d = file_get_contents($uri, false, stream_context_create(array('http' => array(
                    'header' => array('Connection: close', 'Cache-Control: no-cache'),
                    'timeout' => 30
                ))));
                echo $d;
                break;
            case 'post':
                $postdata = file_get_contents("php://input");
                $request = json_decode($postdata, true);

                $d = file_get_contents($host . '/' . $uri, false, stream_context_create(
                        array(
                            'http' => array(
                                'header' => array('Content-type: application/x-www-form-urlencoded', 'Connection: close;'),
                                'method' => 'POST',
                                'timeout' => 30,
                                'content' => http_build_query($request)
                            )
                        )
                    )
                );
                echo $d;
                break;
            case 'put':
                $postdata = file_get_contents("php://input");
                $request = json_decode($postdata, true);
                if (empty($request)) $request = array();
                $query = http_build_query($request);

                $d = file_get_contents($host . '/' . $uri, false, stream_context_create(
                        array(
                            'http' => array(
                                'header' => array('Content-type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($query), 'Connection: close;'),
                                'method' => 'PUT',
                                'content' => $query
                            )
                        )
                    )
                );
                echo $d;
                break;
            case 'upload':
                //$file = realpath($_FILES['file']['tmp_name']);
                $x = rand(0,99);
                move_uploaded_file($_FILES['file']['tmp_name'],__DIR__."/cache/$x.jpg");
                $file = new CURLFile(realpath(__DIR__."/cache/$x.jpg"));
                //$file = new CURLFile(realpath(__DIR__ . "images/ads.jpg"));
                $post = array('fb_uid' => $_REQUEST['fb_uid'], 'file' => $file);
                //var_dump($_FILES['file']);
                //curl_file_create($file);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $host . '/FileMan');
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                $result = curl_exec($ch);
                if (curl_error($ch)) {
                    echo "curl error:" . curl_error($ch);
                }
                curl_close($ch);
                //var_dump($result);
                echo $result;
                break;
            default:
        }
    } else {
        echo ';';
    }
} catch (Exception $e) {
//    print_r($e->getMessage());
    echo json_encode(array(
        'error' => $e->getMessage()
    ));
}

//echo '<pre>';
//print_r(join('/', $match));