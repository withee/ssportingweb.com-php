<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$footerScript .= '<script src="scripts/games/all.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>

<!--Content-->
<div class="wrapper-content content-profile">

    <div class="wrapper-miniGames allgamePage">
        <div class="tab-heading-title tabTypeGames"><img src="images/mini-game/allgames/icon_All-game.png" width="25px;" style="float: left; margin-right: 5px;"> All GAMES</div>

        <div class="all-games" style="display: none;">
            <table>
                <tr>
                    <td>
                        <img src="images/mini-game/allgames/all-game_pao-ying-chup_logo.png">
                        <div class="infoGames">
                            <b>เกมส์เป่ายิงฉุบ</b>
                            <div class="detailGames">เลือกอาวุธเพื่อสู้กับคอมพิวเตอร์</div>
                            <a href="/game-pao-ying-choob.php"><div class="play-games"></div></a>
                        </div>

                    </td>
                    <td>
                        <img src="images/mini-game/allgames/all-game_dice_logo.png">
                        <div class="infoGames">
                            <b>เกมส์ไฮโล</b>
                            <div class="detailGames">ทายผลสูงต่ำเพื่อรับเหรียญ..</div>
                            <a href="/game-high-low.php"><div class="play-games"></div></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="images/mini-game/allgames/all-game_pi-pog-deng_logo.png">
                        <div class="infoGames">
                            <b>เกมส์ไพ่ป๊อกเด้ง</b>
                            <div class="detailGames">เป็นเกมส์ที่ผู้เล่นแข่งกับคอมพิวเตอร์ว่าฝ่ายไหนจะมีแต้มสูงกว่ากัน </div>
                            <a href="/game-pok-deng.php"><div class="play-games"></div></a>
                        </div>

                    </td>
                    <td>
                        <img src="images/mini-game/allgames/all-game_2color_logo.png">
                        <div class="infoGames">
                            <b>เกมส์ทายสีไพ่</b>
                            <div class="detailGames">ทายว่าไพ่ที่จะออกเป็นสีแดงหรือสีดำ</div>
                            <a href="/game-card-black-red.php"><div class="play-games"></div></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="images/mini-game/allgames/all-game_treasure-Chest_logo1.png">
                        <div class="infoGames">
                            <b>เกมส์เปิดหีบสมบัติ</b>
                            <div class="detailGames">เปิดหีบรับเหรียญ..</div>
                            <a href="/game-treasure.php"><div class="play-games"></div></a>
                        </div>

                    </td>
                    <td>
                        <img src="images/mini-game/allgames/all-game_lotto_logo.png">
                        <div class="infoGames">
                            <b>เกมส์น้ำเต้าปูปลา</b>
                            <div class="detailGames">ทายว่าจะออกตรงกับที่เลือกหรือไม่</div>
                            <a href="/game-slot.php"><div class="play-games"></div></a>
                        </div>
                    </td>
                </tr>
            </table>
        </div>



        <div class="wrapperAll-Minigames">
            <table>
                <tr>
                    <td class="game-pao-ying-choob">
                        <div class="detailMiniGames">
                            <span>เลือกอาวุธเพื่อสู้กับคอมพิวเตอร์</span>
                            <a href="/game-pao-ying-choob.php"><div class="playGames"></div></a>
                        </div>
                    </td>
                    <td class="game-hi-lo">
                        <div class="detailMiniGames">
                            <span>ทายผลสูงต่ำเพื่อรับเหรียญ</span>
                            <a href="/game-high-low.php"><div class="playGames"></div></a>
                        </div>
                    </td>
                    <td class="game-pok-deng">
                        <div class="detailMiniGames">
                            <span>มาดูว่าฝ่ายไหนจะมีแต้มสูงกว่ากัน </span>
                            <a href="/game-pok-deng.php"><div class="playGames"></div></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="game-redwhite-card">
                        <div class="detailMiniGames">
                            <span>ทายว่าไพ่ที่จะออกเป็นสีแดงหรือสีดำ</span>
                            <a href="/game-card-black-red.php"><div class="playGames"></div></a>
                        </div>
                    </td>
                    <td class="game-treasure">
                        <div class="detailMiniGames">
                            <span>เกมส์เปิดหีบสมบัติ</span>
                            <a href="/game-treasure.php"><div class="playGames"></div></a>
                        </div>
                    </td>
                    <td class="game-slot">
                        <div class="detailMiniGames">
                            <span>ทายว่าจะออกตรงกับที่เลือกหรือไม่</span>
                            <a href="/game-slot.php"><div class="playGames"></div></a>
                        </div>
                    </td>
                </tr>

            </table>
        </div>



        <!-- ตารางสรุปผลการเล่นเกมส์ทั้งหมดทุกคน ทุกเกมส์-->
        <div class="titleGames" style="display: snone;">ตารางสรุปผลการเล่นเกมส์ทั้งหมด</div>

        <div class="table-ResultMiniGames" style="display: snone;">
            <div class="tabs-tableResult" style="display: snone;">
                <ul>
                    <li ng-click="stateTab = 'user'" ng-class="{'active':stateTab === 'user'}">เฉพาะฉัน</li>
                    <li ng-click="stateTab = 'all'" ng-class="{'active':stateTab === 'all'}">ทุกคน</li>
                </ul>
                <div style="clear: both;"></div>
            </div>

            <table>
                <thead>
                    <tr>
                        <th>ชื่อผู้เล่น</th>
                        <th>ชื่อเกมส์</th>
                        <th>ผู้เล่น</th>
                        <th>คอมพ์</th>
                        <th>ผลการเล่น</th>
                        <th>จำนวน</th>
                        <th>ได้/เสีย</th>
                        <th>วันที่/เวลา</th>
                    </tr>
                </thead>
                <tbody ng-show="stateTab === 'user'">
                    <tr ng-repeat="item in statement.user">
                        <td><a href="profile.php?id={{ item.fb_uid}}"><img
                                    ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"> {{ item.display_name ||
                                item.fb_firstname }}</a></td>
                        <td>
                            <span ng-switch="item.game_type">
                                <span ng-switch-when="hi_low">ไฮโล</span>
                                <span ng-switch-when="rock_paper_scissors">เป่ายิงฉุบ</span>
                                <span ng-switch-when="treasure_high">หีบสมบัติ</span>
                                <span ng-switch-when="treasure_medium">หีบสมบัติ</span>
                                <span ng-switch-when="treasure_low">หีบสมบัติ</span>
                                <span ng-switch-when="red_black">ทายสีไพ่</span>
                                <span ng-switch-when="calabash_crap_fish">น้ำเต้าปูปลา</span>
                                <span ng-switch-when="pok_deng">ป๊อกเด้ง</span>
                                <span ng-switch-default>{{ item.game_type}}</span>
                            </span>
                        </td>

                        <td ng-if="item.game_type === 'calabash_crap_fish'">
                            <span ng-switch="item.player1">
                                <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                            </span>
                        </td>

                        <td ng-if="item.game_type === 'hi_low'">
                            <span ng-switch="item.player1">
                                <img ng-switch-when="l" src="images/mini-game/hightlow/btn_low_defualt.png">
                                <img ng-switch-when="11" src="images/mini-game/hightlow/btn_11_defualt.png">
                                <img ng-switch-when="h" src="images/mini-game/hightlow/btn_hight_defualt.png">
                            </span>
                        </td>

                        <td ng-if="item.game_type === 'pok_deng'">
                            {{ item.player1 | parseScore:'1' }}
                        </td>

                        <td ng-if="item.game_type === 'calabash_crap_fish'">
                            <span ng-switch="item.player2 | parseOdds:0">
                                <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                            </span>
                            <span class="slot2" ng-switch="item.player2 | parseOdds:1">
                                <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                            </span>
                            <span ng-switch="item.player2 | parseOdds:2">
                                <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                            </span>
                        </td>
                        <td ng-if="item.game_type === 'hi_low'">
                            {{ item.player2 | parseOdds:'sum'}}
                        </td>

                        <td ng-if="item.game_type === 'pok_deng'">
                            {{ item.player2 | parseScore:'2' }}
                        </td>

                        <td ng-if="item.game_type === 'rock_paper_scissors'">
                            <span ng-switch="item.player1">
                                <img ng-switch-when="s" src="images/mini-game/paoyingchoob/L_cut_g1.png">
                                <img ng-switch-when="r" src="images/mini-game/paoyingchoob/L_hammer_g1.png">
                                <img ng-switch-when="p" src="images/mini-game/paoyingchoob/L_cut_g1.png">
                            </span>
                        </td>
                        <td ng-if="item.game_type === 'rock_paper_scissors'">
                            <span ng-switch="item.player2">
                                <img ng-switch-when="s" src="images/mini-game/paoyingchoob/R_cut_g1.png">
                                <img ng-switch-when="r" src="images/mini-game/paoyingchoob/R_hammer_g1.png">
                                <img ng-switch-when="p" src="images/mini-game/paoyingchoob/R_paper_g1.png">
                            </span>
                        </td>

                        <td ng-if="item.game_type === 'treasure_high' || item.game_type === 'treasure_medium' || item.game_type === 'treasure_low'">
                            <span ng-switch="item.game_type">
                                <span ng-switch-when="treasure_low">ธรรมดา</span>
                                <span ng-switch-when="treasure_high">สูงสุด</span>
                                <span ng-switch-when="treasure_medium">สูง</span>
                            </span>
                        </td>
                        <td ng-if="item.game_type === 'treasure_high' || item.game_type === 'treasure_medium' || item.game_type === 'treasure_low'">
                            {{ item.player2}}
                        </td>

                        <td ng-if="item.game_type === 'red_black'">
                            <span ng-switch="item.player1">
                                <img ng-switch-when="b" src="/images/mini-game/card/1_card_01_spade.png">
                                <img ng-switch-when="r" src="/images/mini-game/card/2_card_01_Hearts.png">
                            </span>
                        </td>
                        <td ng-if="item.game_type === 'red_black'">
                            <span ng-switch="item.player2">
                                <img ng-switch-when="b" src="/images/mini-game/card/1_card_01_spade.png">
                                <img ng-switch-when="r" src="/images/mini-game/card/2_card_01_Hearts.png">
                            </span>
                        </td>


                        <td>
                            <span ng-switch="item.result">
                                <span class="correct" ng-switch-when="win">ชนะ</span>
                                <span class="wrong" ng-switch-when="lose">แพ้</span>
                                <span class="draw" ng-switch-when="draw">เสมอ</span>
                                <span ng-switch-default>เสมอ</span>
                            </span>
                        </td>
                        <td><img src="images/icon/scoin30.png"> {{ item.scoin_amount}}</td>
                        <td><img src="images/icon/sgold30.png"> <span ng-class="{'correct':item.sgold_result_amount > 0, 'wrong':item.sgold_result_amount < 0 }">{{ item.sgold_result_amount}}</span></td>
                        <td>{{ item.play_timestamp | timeagoByDateTime }}</td>
                    </tr>
                </tbody>
                <tbody ng-show="stateTab === 'all'">
                    <tr ng-repeat="item in statement.all">
                        <td><a href="profile.php?id={{ item.fb_uid}}"><img
                                    ng-src="https://graph.facebook.com/{{ item.fb_uid}}/picture"> {{ item.display_name ||
                                item.fb_firstname }}</a></td>
                        <td>
                            <span ng-switch="item.game_type">
                                <span ng-switch-when="hi_low">ไฮโล</span>
                                <span ng-switch-when="rock_paper_scissors">เป่ายิงฉุบ</span>
                                <span ng-switch-when="treasure_high">หีบสมบัติ</span>
                                <span ng-switch-when="treasure_medium">หีบสมบัติ</span>
                                <span ng-switch-when="treasure_low">หีบสมบัติ</span>
                                <span ng-switch-when="red_black">ทายสีไพ่</span>
                                <span ng-switch-when="calabash_crap_fish">น้ำเต้าปูปลา</span>
                                <span ng-switch-when="pok_deng">ป๊อกเด้ง</span>
                                <span ng-switch-default>{{ item.game_type}}</span>
                            </span>
                        </td>

                        <td ng-if="item.game_type === 'calabash_crap_fish'">
                            <span ng-switch="item.player1">
                                <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                            </span>
                        </td>

                        <td ng-if="item.game_type === 'hi_low'">
                            <span ng-switch="item.player1">
                                <img ng-switch-when="l" src="images/mini-game/hightlow/btn_low_defualt.png">
                                <img ng-switch-when="11" src="images/mini-game/hightlow/btn_11_defualt.png">
                                <img ng-switch-when="h" src="images/mini-game/hightlow/btn_hight_defualt.png">
                            </span>
                        </td>

                        <td ng-if="item.game_type === 'pok_deng'">
                            {{ item.player1 | parseScore:'1' }}
                        </td>

                        <td ng-if="item.game_type === 'calabash_crap_fish'">
                            <span ng-switch="item.player2 | parseOdds:0">
                                <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                            </span>
                            <span class="slot2" ng-switch="item.player2 | parseOdds:1">
                                <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                            </span>
                            <span ng-switch="item.player2 | parseOdds:2">
                                <img ng-switch-when="ca" src="/images/mini-game/slot/slot_001.png">
                                <img ng-switch-when="cr" src="/images/mini-game/slot/slot_002.png">
                                <img ng-switch-when="f" src="/images/mini-game/slot/slot_003.png">
                                <img ng-switch-when="t" src="/images/mini-game/slot/slot_004.png">
                                <img ng-switch-when="s" src="/images/mini-game/slot/slot_005.png">
                                <img ng-switch-when="ch" src="/images/mini-game/slot/slot_006.png">
                            </span>
                        </td>
                        <td ng-if="item.game_type === 'hi_low'">
                            {{ item.player2 | parseOdds:'sum'}}
                        </td>

                        <td ng-if="item.game_type === 'pok_deng'">
                            {{ item.player2 | parseScore:'2' }}
                        </td>

                        <td ng-if="item.game_type === 'rock_paper_scissors'">
                            <span ng-switch="item.player1">
                                <img ng-switch-when="s" src="images/mini-game/paoyingchoob/L_cut_g1.png">
                                <img ng-switch-when="r" src="images/mini-game/paoyingchoob/L_hammer_g1.png">
                                <img ng-switch-when="p" src="images/mini-game/paoyingchoob/L_paper_g1.png">
                            </span>
                        </td>
                        <td ng-if="item.game_type === 'rock_paper_scissors'">
                            <span ng-switch="item.player2">
                                <img ng-switch-when="s" src="images/mini-game/paoyingchoob/R_cut_g1.png">
                                <img ng-switch-when="r" src="images/mini-game/paoyingchoob/R_hammer_g1.png">
                                <img ng-switch-when="p" src="images/mini-game/paoyingchoob/R_paper_g1.png">
                            </span>
                        </td>

                        <td ng-if="item.game_type === 'treasure_high' || item.game_type === 'treasure_medium' || item.game_type === 'treasure_low'">
                            <span ng-switch="item.game_type">
                                <span ng-switch-when="treasure_low">ธรรมดา</span>
                                <span ng-switch-when="treasure_high">สูงสุด</span>
                                <span ng-switch-when="treasure_medium">สูง</span>
                            </span>
                        </td>
                        <td ng-if="item.game_type === 'treasure_high' || item.game_type === 'treasure_medium' || item.game_type === 'treasure_low'">
                            {{ item.player2}}
                        </td>

                        <td ng-if="item.game_type === 'red_black'">
                            <span ng-switch="item.player1">
                                <img ng-switch-when="b" src="/images/mini-game/card/1_card_01_spade.png">
                                <img ng-switch-when="r" src="/images/mini-game/card/2_card_01_Hearts.png">
                            </span>
                        </td>
                        <td ng-if="item.game_type === 'red_black'">
                            <span ng-switch="item.player2">
                                <img ng-switch-when="b" src="/images/mini-game/card/1_card_01_spade.png">
                                <img ng-switch-when="r" src="/images/mini-game/card/2_card_01_Hearts.png">
                            </span>
                        </td>


                        <td>
                            <span ng-switch="item.result">
                                <span class="correct" ng-switch-when="win">ชนะ</span>
                                <span class="wrong" ng-switch-when="lose">แพ้</span>
                                <span class="draw" ng-switch-when="draw">เสมอ</span>
                                <span ng-switch-default>เสมอ</span>
                            </span>
                        </td>
                        <td><img src="images/icon/scoin30.png"> {{ item.scoin_amount}}</td>
                        <td><img src="images/icon/sgold30.png"> <span ng-class="{'correct':item.sgold_result_amount > 0, 'wrong':item.sgold_result_amount < 0 }">{{ item.sgold_result_amount}}</span></td>
                        <td>{{ item.play_timestamp | timeagoByDateTime }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>

</div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
