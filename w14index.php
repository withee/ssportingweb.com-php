<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ผลบอลสด อัพเดทรวดเร็วที่สุดและแม่นยำที่สุด';
$meta = '<meta name="description" content="เช็คผลบอลสด ผลบอลเมื่อคืนและผลบอลย้อนหลังได้ที่นี่ ข้อมูลแม่นยำอัพเดทผลรวดเร็ว ซึ่งจะทำให้คุณไม่พลาดทุกวินาทีสำคัญ">' . "\n";
$meta .= '<meta name="keyword" content="ผลบอลสด,ผลบอล,ผลบอลเมื่อคืน,ผลบอลย้อนหลัง,ไฮไลท์ฟุตบอล">' . "\n";

$service_w14 = Services::getW14();
$service_news = Services::getNews();
$service_w14ranking = Services::getW14ranking();

//echo '<pre>';
//print_r($service_w14ranking);
//echo '</pre>';
//exit;

$footerScript .= '<script src="scripts/w14index.js"></script>';

use Carbon\Carbon;

require_once(__INCLUDE_DIR__ . '/header.php')
?>

<div id="news-top-slide-box" class="wrapper-slide-comment-top" style="display: none;">

    <div class="box-comment-top" ng-repeat="item in newsTopSlide">
        <a href="/news.php?id={{ news.ontimelines[item.newsId]}}">
            <table>
                <tr>
                    <td><img ng-src="{{ item.imageLink}}"></td>
                    <td>
                        <b ng-bind="news.titles[item.newsId]"></b>
                        <span class="detail-news" ng-bind="news.desc[item.newsId]"></span>
                    </td>
                </tr>
            </table>
        </a>
    </div>
</div>



<div class="wrapper-content content-profile">

    <div class="banner" style="padding-left: 5px;">
        <a href="/w14index.php"><img src="images/banner.jpg"></a>
    </div>
    <div class="teamPlay-Match">
        <?php
        $ds = Carbon::now('Africa/Abidjan')->format('Y-m-d');
        if ($ds < '2014-06-12')
            $ds = '2014-06-12';
        else if ($ds > '2014-07-13')
            $ds = '2014-07-13';

        $dm = Carbon::createFromFormat('Y-m-d', $ds)->addDays(1)->format('Y-m-d');
        $dmd = Carbon::createFromFormat('Y-m-d', $dm);
        $dmw = $dmd->day . ' ' . Utils::monthOfYear($dmd->month) . ' ' . $dmd->year;

        $d = Carbon::createFromFormat('Y-m-d', $ds)->addHours(__GMT_OFFSET__);
        $dw = $d->day . ' ' . Utils::monthOfYear($d->month) . ' ' . $d->year;
        ?>
        <?php foreach ($service_w14->matches->{$ds} as $idx => $item): ?>
            <div class="Box1 upcoming-match" data-idx="<?php echo $idx; ?>" style="display: none;">
                <?php if (!empty($item->mid)): ?><a href="/game.php?mid=<?php echo $item->mid; ?>"><?php endif; ?>
                    <table>
                        <tr>
                            <td><img src="http://ws.1ivescore.com/worldcup/<?php echo $item->hid; ?>_.png"><div class="nameTeams"><?php echo $service_w14->team->{$item->hid}->{__LANGUAGE__}; ?></div></td>
                            <td><img src="images/icon/bet-icon.png"><br><span class="fontYellow"><?php echo Carbon::createFromTimestamp($item->datetime)->addHours(__GMT_OFFSET__)->format('H:i'); ?></span><br><?php echo $dw; ?></td>
                            <td><img src="http://ws.1ivescore.com/worldcup/<?php echo $item->gid; ?>_.png"><div class="nameTeams"><?php echo $service_w14->team->{$item->gid}->{__LANGUAGE__}; ?></div></td>
                        </tr>
                    </table>
                    <?php if (!empty($item->mid)): ?></a><?php endif; ?>
            </div>
        <?php endforeach; ?>
        <?php foreach ($service_w14->matches->{$dm} as $idx => $item): ?>
            <div class="Box1 upcoming-match" data-idx="<?php echo $idx + count($service_w14->matches->{$ds}); ?>" style="display: none;">
                <?php if (!empty($item->mid)): ?><a href="/game.php?mid=<?php echo $item->mid; ?>"><?php endif; ?>
                    <table>
                        <tr>
                            <td><img src="http://ws.1ivescore.com/worldcup/<?php echo $item->hid; ?>_.png"><div class="nameTeams"><?php echo $service_w14->team->{$item->hid}->{__LANGUAGE__}; ?></div></td>
                            <td><img src="images/icon/bet-icon.png"><br><span class="fontYellow"><?php echo Carbon::createFromTimestamp($item->datetime)->addHours(__GMT_OFFSET__)->format('H:i'); ?></span><br><?php echo $dmw; ?></td>
                            <td><img src="http://ws.1ivescore.com/worldcup/<?php echo $item->gid; ?>_.png"><div class="nameTeams"><?php echo $service_w14->team->{$item->gid}->{__LANGUAGE__}; ?></div></td>
                        </tr>
                    </table>
                    <?php if (!empty($item->mid)): ?></a><?php endif; ?>
            </div>
        <?php endforeach; ?>
        <div style="clear: both;"></div>
    </div>

    <div class="wrapper-content-worldcup">
        <div class="boxRight">
            <div class="tabs">
                <img src="images/icon/fix.png"> Result/Fixtures
                <div style="clear: both;"></div>
            </div>

            <div class="wrapperBox" style="margin-top: 3px;">
                <div class="boxMenu-date">
                    <?php
                    $now = Carbon::now('Africa/Abidjan');
                    $start = Carbon::createFromFormat('Y-m-d', '2014-06-12');
                    ?>
                    <table>
                        <tr data-d="all" class="dm-select <?php  if ($now->format('Y-m-d') < $start->format('Y-m-d')) echo 'activeDate' ?>">
                            <td>
                                <b>All</b><br>Match
                            </td>
                        </tr>
                        <?php foreach ($service_w14->matches as $key => $item): ?>
                            <?php
                            $kickdate = Carbon::createFromFormat('Y-m-d', $key);
                            if ($now > $kickdate && $now->diffInDays($kickdate) > 3)
                                continue;

                            $d = Carbon::createFromFormat('Y-m-d', $key)->addHours(__GMT_OFFSET__);
                            ?>
                            <tr class="dm-select <?php  if ($now->format('Y-m-d') == $kickdate->format('Y-m-d')) echo 'activeDate';  ?>" data-d="<?php echo $key; ?>">
                                <td>
                                    <b><?php echo $d->day; ?></b><br><?php echo Utils::monthOfYear($d->month, 'short'); ?> <?php echo $d->year; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>

                <div class="boxDetails-fixure">
                    <table>
                        <?php foreach ($service_w14->matches as $key => $item): ?>
                            <?php
                            $kickdate = Carbon::createFromFormat('Y-m-d', $key);

                            $d = Carbon::createFromFormat('Y-m-d', $key)->addHours(__GMT_OFFSET__);
                            $dw = $d->day . ' ' . Utils::monthOfYear($d->month) . ' ' . $d->year;

//                            var_dump($now->format('Y-m-d') === $kickdate->format('Y-m-d')); exit;
                            ?>
                            <tr class="dm-data" style="<?php  if ($now->format('Y-m-d') != $kickdate->format('Y-m-d') && $now->format('Y-m-d') >= $start->format('Y-m-d')) echo 'display: none;';  ?>" data-ds="<?php echo $key; ?>">
                                <td class="tab-dateTime" colspan="5"><?php echo Utils::dayOfWeek($d->dayOfWeek); ?> <?php echo $dw; ?></td>
                            </tr>
                            <?php foreach ($item as $match): ?>
                                <tr class="dm-data" style="<?php  if ($now->format('Y-m-d') != $kickdate->format('Y-m-d') && $now->format('Y-m-d') >= $start->format('Y-m-d')) echo 'display: none;';  ?>" data-ds="<?php echo $key; ?>">
                                    <td>
                                        <?php if (!empty($match->mid)): ?><a href="/game.php?mid=<?php echo $match->mid ?>"><?php endif; ?>
                                            <span><?php echo (isset($match->hid) && isset($service_w14->team->{$match->hid})) ? $service_w14->team->{$match->hid}->{__LANGUAGE__} : $match->teamA; ?></span>
                                            <?php if (!empty($match->mid)): ?></a><?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if (!empty($match->mid)): ?><a href="/game.php?mid=<?php echo $match->mid ?>"><?php endif; ?>
                                            <?php if ($match->hid): ?>
                                                <img src="http://ws.1ivescore.com/worldcup/<?php echo $match->hid; ?>_.png">
                                            <?php else: ?>
                                                <img src="http://ws.1ivescore.com/teams_clean/team_default_32x32.png">
                                            <?php endif; ?>
                                            <?php if (!empty($match->mid)): ?></a><?php endif; ?>
                                    </td>
                                    <td class="resultFixure">
                                        <?php if (!empty($match->mid)): ?><a href="/game.php?mid=<?php echo $match->mid ?>"><?php endif; ?>
                                            <?php if ($match->sid > 1): ?>
                                                <b><?php echo $match->s1; ?></b>
                                            <?php else: ?>
                                                <?php echo Carbon::createFromTimestamp($match->datetime)->addHours(__GMT_OFFSET__)->format('H:i'); ?>
                                            <?php endif; ?>
                                            <?php if (!empty($match->mid)): ?></a><?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if (!empty($match->mid)): ?><a href="/game.php?mid=<?php echo $match->mid ?>"><?php endif; ?>
                                            <?php if ($match->gid): ?>
                                                <img src="http://ws.1ivescore.com/worldcup/<?php echo $match->gid; ?>_.png">
                                            <?php else: ?>
                                                <img src="http://ws.1ivescore.com/teams_clean/team_default_32x32.png">
                                            <?php endif; ?>
                                            <?php if (!empty($match->mid)): ?></a><?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if (!empty($match->mid)): ?><a href="/game.php?mid=<?php echo $match->mid ?>"><?php endif; ?>
                                            <span><?php echo (isset($match->gid) && isset($service_w14->team->{$match->gid})) ? $service_w14->team->{$match->gid}->{__LANGUAGE__} : $match->teamB; ?></span>
                                            <?php if (!empty($match->mid)): ?></a><?php endif; ?>
                                    </td>
                                </tr>

                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </table>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div style="clear: both;"></div>
            <div class="tab-moreNews">
                <b><a href="/w14match.php">More Fixure >></a></b>
            </div>

        </div>

        <div class="boxRight boxBracket">
            <div class="tabs">
                <img src="images/icon/bracket.png"> Brackets
                <div style="clear: both;"></div>
            </div>
            <a href="w14bracket.php"><img src="images/bracket2.png" style="width: 460px; height: 430px;"></a>
            <div class="tab-moreNews">
                <a href="/w14bracket.php"><b>More Bracket >></b> </a>
            </div>
        </div>
    </div>

    <div style="clear: both;"></div>
    <div class="tableRound">
        <div class="tabs tab-round">
            <img src="images/icon/table-round.png"> Table Round 32 Team
            <div style="clear: both;"></div>
        </div>

        <div class="wrapper-round32">
            <?php for ($i = 0; $i < 4; $i++): ?>
                <div class="box-bracket bracket">
                    <div class="nameGroup group"><?php echo preg_replace("#-#", ' ', $service_w14->tables[$i][0]->subLeagueNamePk); ?></div>
                    <table>
                        <thead>
                            <tr>
                                <th>Team</th>
                                <th>Pts</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($service_w14->tables[$i] as $item): ?>
                                <tr>
                                    <td>
                                        <?php if ($item->tid): ?>
                                            <img src="http://ws.1ivescore.com/worldcup/<?php echo $item->tid; ?>_.png">
                                        <?php else: ?>
                                            <img src="http://ws.1ivescore.com/teams_clean/team_default_32x32.png">
                                        <?php endif; ?>
                                        <?php echo $service_w14->team->{$item->tid}->{__LANGUAGE__}; ?></td>
                                    <td><?php echo $item->pts; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?php endfor; ?>
            <div style="clear: both;"></div>
        </div>
        <div class="tab-moreNews moreTable"><b><a href="/w14table.php">More Table >></a></b></div>
        <div style="clear: both;"></div>
    </div>


    <div class="boxLeft">

        <div class="tabs">
            <img src="images/icon/news.png"> News World Cup 2014
            <div style="clear: both;"></div>
        </div>
        <div class="wrapperBox">
            <?php for ($i = 0; $i <= 1; $i++): ?>
                <?php if (isset($service_news->data[$i])): ?>
                    <?php
                    $d = Carbon::createFromTimestamp($service_news->data[$i]->createDatetime)->addHours(__GMT_OFFSET__);
                    $dw = $d->day . ' ' . Utils::monthOfYear($d->month) . ' ' . $d->year;
                    ?>
                    <div class="boxNews">

                        <img src="<?php echo $service_news->data[$i]->imageLink; ?>">
                        <div class="boxDetails">
                            <a href="/news.php?id=<?php echo $service_news->ontimelines->{$service_news->data[$i]->newsId}; ?>"><b><?php echo $service_news->titles->{$service_news->data[$i]->newsId} ?></b></a>
                            <?php echo Utils::dayOfWeek($d->dayOfWeek); ?> <?php echo $dw; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endfor; ?>
        </div>

        <div class="tab-moreNews">
            <b><a href="/w14news.php">More News >></a></b>
        </div>
    </div>



    <div class="boxLeft boxTopscore">
        <div class="tabs">
            <img src="images/icon/soccer1.png"> Top Scorers
            <div style="clear: both;"></div>
        </div>
        <div class="wrapperScore">
            <table>
                <?php for ($i = 0; $i < 10; $i++): ?>
                    <?php if (empty($service_w14->top_scorer[$i])) break; ?>
                    <tr>
                        <td class="userImage"><img src="http://dowebsite.com:2999/player-logo/<?php echo $service_w14->top_scorer[$i]->playerId; ?>.jpg"></td>
                        <td><b><?php echo $service_w14->top_scorer[$i]->{'playerName' . ucfirst(__LANGUAGE__)}; ?></b><br><?php echo $service_w14->top_scorer[$i]->{'teamName' . ucfirst(__LANGUAGE__)}; ?></td>
                        <td><?php echo $service_w14->top_scorer[$i]->totalScore; ?></td>
                    </tr>
                <?php endfor; ?>
            </table>
        </div>
        <div class="tab-moreNews">
            <b><a href="/w14topscore.php">More Top Scorers >></a></b>
        </div>
    </div>


    <div style="clear: both;"></div>



    <!--    Top score world cup-->


    <div id="w14ranking-guest" class="tabRanking" style="display: snone;">
        <div class="tabs tabs2">
            <img src="images/icon/logo-worldcup.png" style="width: 22px; height: 20px;"> Top Ranking World Cup
            <div style="clear: both;"></div>
        </div>
        <table>
            <?php foreach ($service_w14ranking->ranks as $item): ?>
                <?php
                $rating = number_format(($item->w / ($item->w + $item->d + $item->l) * 100), 2) + 0;
                ?>
                <tr>
                    <td><?php echo $item->rank; ?></td>
                    <td class="imgRank"><a href="/profile.php?id=<?php echo $item->fb_uid; ?>"><img src="https://graph.facebook.com/<?php echo $item->fb_uid; ?>/picture"></a></td>
                    <td class="nameRank"><?php echo!empty($item->display_name) ? $item->display_name : $item->fb_firstname; ?> <br><b>Rating</b><?php echo $rating; ?> % <b>Play</b> <?php echo $item->pts; ?> <b>Match</b></td>
                    <td><div class="boxTrophy"><img style="display: none;" src="images/icon-profile/3001.png"></div></td>
                    <td class="pointRank"><?php echo $item->overall_gp; ?> Point</td>
                    <td>
                        <?php
                        if ((int) $item->rank_change > 0)
                            echo '<img src="images/icon/top-icon.png">';
                        else if ((int) $item->rank_change < 0)
                            echo '<img src="images/icon/red-icon-bottom.png">';
                        else
                            echo '<img src="images/icon/right-green-icon.png">';
                        ?>
                    </td>
                </tr>
                
            <?php endforeach; ?>
        </table>
    </div>

    <div id="w14ranking-member" class="tabRanking" style="display: none;">
        <div class="tabs tabs2">
            <img src="images/icon/logo-worldcup.png" style="width: 22px; height: 20px;"> Top Ranking World Cup
            <div style="clear: both;"></div>
        </div>
        <table>
            <tr ng-repeat="item in w14ranking" ng-class="{'own-ranking':item.fb_uid == facebookInfo.id}">
                <td>{{ item.rank }}</td>
                <td class="imgRank"><a href="/profile.php?id={{ item.fb_uid }}"><img src="https://graph.facebook.com/{{ item.fb_uid }}/picture"></a></td>
                <td class="nameRank">{{ item.display_name || item.fb_firstname }} <br><b>Rating</span> </b>{{ rankRating(item.w, item.d, item.l) }} % <b>Play</b> {{ item.pts }} <b>Match</b></td>
                <td><div class="boxTrophy"><img style="display: none;" src="images/icon-profile/3001.png"></div></td>
                <td class="pointRank">{{ item.overall_gp }} Point</td>
                <td>
                    <img ng-if="item.rank_change > 0" src="images/icon/top-icon.png">
                    <img ng-if="item.rank_change < 0" src="images/icon/red-icon-bottom.png">
                    <img ng-if="item.rank_change == 0" src="images/icon/right-green-icon.png">
                </td>
            </tr>
            <tr ng-if="w14ownRanking.rank > 40" class="own-ranking">
                <td>{{ w14ownRanking.rank }}</td>
                <td class="imgRank"><a href="/profile.php?id={{ w14ownRanking.fb_uid }}"><img src="https://graph.facebook.com/{{ w14ownRanking.fb_uid }}/picture"></a></td>
                <td class="nameRank">{{ w14ownRanking.display_name || w14ownRanking.fb_firstname }} <br><b>Rating</span> </b>{{ rankRating(w14ownRanking.w, w14ownRanking.d, w14ownRanking.l) }} % <b>Play</b> {{ w14ownRanking.pts }} <b>Match</b></td>
                <td><div class="boxTrophy"><img style="display: none;" src="images/icon-profile/3001.png"></div></td>
                <td class="pointRank">{{ w14ownRanking.overall_gp }} Point</td>
                <td>
                    <img ng-if="w14ownRanking.rank_change > 0" src="images/icon/top-icon.png">
                    <img ng-if="w14ownRanking.rank_change < 0" src="images/icon/red-icon-bottom.png">
                    <img ng-if="w14ownRanking.rank_change == 0" src="images/icon/right-green-icon.png">
                </td>
            </tr>
        </table>
    </div>



</div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>