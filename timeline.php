<?php
require_once(dirname(__FILE__) . '/_init_.php');

define('__FB_UID__', isset($_REQUEST['id']) ? $_REQUEST['id'] : false);

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

$footerScript .= '<script id="timelineScript" src="scripts/timeline.js" fb_uid="' . __FB_UID__ . '"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>


<!--    <div id="feed-top-slide-box" class="wrapper-slide-comment-top" style="display: none;">-->
<!---->
<!--        <div class="box-comment-top" ng-repeat="item in feedTopSlide">-->
<!--            <table>-->
<!--                <tr>-->
<!--                    <td><a href="/profile.php?id={{ item.fb_uid }}"><img-->
<!--                                ng-src="http://graph.facebook.com/{{ item.fb_uid }}/picture"/></a></td>-->
<!--                    <td ng-click="openBetDialogFromId(item.mid, $event)">-->
<!--                        <b>{{ item.fb_firstname }} {{ item.fb_lastname }}</b>-->
<!--                        <span class="text-blue">Play</span> : <span ng-switch="item.choose">-->
<!--                        <a href="#!/team/{{ item.hid }}" ng-switch-when="home">{{-->
<!--                            allTeams[item.hid].name }}</a>-->
<!--                        <a href="#!/team/{{ item.gid }}" ng-switch-when="away">{{-->
<!--                            allTeams[item.gid].name }}</a>-->
<!--                    </span> {{ item.betValue }}-->
<!--                    </td>-->
<!--                </tr>-->
<!--            </table>-->
<!--        </div>-->
<!---->
<!--    </div>-->

<div id="news-top-slide-box" class="wrapper-slide-comment-top" style="display: none;">

    <div class="box-comment-top" ng-repeat="item in newsTopSlide">
        <a href="/news.php?id={{ news.ontimelines[item.newsId]}}">
            <table>
                <tr>
                    <td><img ng-src="{{ item.imageLink}}"></td>
                    <td>
                        <b ng-bind="news.titles[item.newsId]"></b>
                        <span class="detail-news" ng-bind="news.desc[item.newsId]"></span>
                    </td>
                </tr>
            </table>
        </a>
    </div>
</div>


<div class="wrapper-content content-profile">
     <div class="banner" style="padding: 5px;">
        <img src="images/banner.png" style="width: 560px;">
    </div>
    <div ng-show="facebookInfo.id == localFacebookInfo.fb_uid" class="button-change-cover" style="display: none;"><button ng-click="uploadCover()" style="display: none;" class="btn btn-mini"><img src="images/icon/edit-icon-1.png"/> Edit Cover</button></div>
    <div class="section-header-profile" style="background-image: url('{{ localFacebookInfo.cover || '/images/test-bg2.jpg' }}'); display: none;">
        <div class="wrapper-header-profile">
            <table>
                <tr>
                    <td class="profile-user-info">
                        <div class="box-img-profiles">
                            <div class="profile-img">
                                <img ng-src="http://graph.facebook.com/{{ userInfo.id}}/picture"/>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="menu-info-follow">
                                <span ng-switch on="isFollowing">
                                    <button ng-switch-when="true" ng-click="follow(userInfo.id, false)" class="btn btn-small"><img src="images/icon/follow-1.png"/> <?php echo Utils::trans('Unfollow'); ?></button>
                                    <button ng-switch-when="false" ng-click="follow(userInfo.id, true)" class="btn btn-small btn-info"><img src="images/icon/follow-white.png"/> <?php echo Utils::trans('Follow'); ?></button>
                                </span>
                            </div>
                        </div>

                        <div class="box-info-user">
                            <span><input class="span2" ng-show="edited_displayname" ng-enter="updateDisplayname()" type="text" ng-model="edited_displayname"></span>
                            <span ng-show="!edited_displayname" class="name-user">{{ localFacebookInfo.display_name || localFacebookInfo.user_status || localFacebookInfo.fb_name }} <img ng-show="facebookInfo.id == localFacebookInfo.fb_uid" ng-click="edited_displayname = localFacebookInfo.user_status" src="images/icon/edit-icon.png"/></span>
                            <div class="name-user-profile">
                                <span ng-show="edited_status"><input ng-enter="updateStatus()" class="span2" type="text" ng-model="edited_status"></span>
                                <span ng-show="!edited_status">{{ localFacebookInfo.mind || 'Edit Your Status Here...' }} <img ng-show="facebookInfo.id == localFacebookInfo.fb_uid" ng-click="edited_status = localFacebookInfo.mind" src="images/icon/edit-icon-1.png"/></span>
                            </div>
                            <div class="edit-name">
                                <span ng-show="edited_site"><input type="text" ng-enter="updateSite()" ng-model="edited_site"></span>
                                <span ng-show="!edited_site">{{ localFacebookInfo.site || 'Site Url' }} <img ng-show="facebookInfo.id == localFacebookInfo.fb_uid" ng-click="edited_site = localFacebookInfo.site" src="images/icon/edit-icon.png"/></span>
                            </div>

                            <div style="display: none;">
                                <input id="upload-browse" type="file" ng-file-select="updateCover($files)" >
                                <button ng-click="upload.abort()">Cancel Upload</button>
                            </div>


                        </div>
                    </td>
                    <td class="bg-cover-timeline">
                        <div class="tab-stat" ng-show="localFacebookInfo">
                            <table>
                                <tr>
                                    <td class="three-coin" style="display: none;">
                                        <ul>
                                            <li>0</li>
                                            <li>0</li>
                                            <li>0</li>
                                        </ul>
                                    </td>
                                    <td ng-show="localFacebookInfo"> <h5>{{ localFacebookInfo.gp || 0 | toFixed }}</h5><?php echo Utils::trans('Score'); ?> </td>
                                    <td><h5>{{ localFacebookInfo.spirit || 0 }}</h5> <?php echo Utils::trans('Spirit'); ?></td>
                                    <td><h5>{{ localFacebookInfo.pta || 0 | toFixed }}% </h5>  <?php echo Utils::trans('PTA'); ?></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="section-new-feed-user" ng-include="tabInclude"></div>
</div>






<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>