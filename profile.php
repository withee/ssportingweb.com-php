<?php
require_once(dirname(__FILE__) . '/_init_.php');

define('__FB_UID__', isset($_REQUEST['id']) ? $_REQUEST['id'] : false);

$service_allleague = Services::getAllLeague();
$service_allteam = Services::getAllTeam();

//$footerScript .= '<script id="profileScript" src="scripts/profile.js" fb_uid="'.__FB_UID__.'" q="' . $_SERVER['QUERY_STRING'] . '"></script>';
$footerScript .= '<script id="timelineScript" src="scripts/timeline.js" fb_uid="' . __FB_UID__ . '" q="' . $_SERVER['QUERY_STRING'] . '" data-own="true"></script>';

require_once(__INCLUDE_DIR__ . '/header.php');
?>

<!--    <div id="feed-tops-lide-box" class="wrapper-slide-comment-top" style="display: none;">-->
<!---->
<!--        <div class="box-comment-top" ng-repeat="item in feedTopSlide">-->
<!--            <table>-->
<!--                <tr>-->
<!--                    <td><a href="/profile.php?id={{ item.fb_uid }}"><img-->
<!--                                ng-src="http://graph.facebook.com/{{ item.fb_uid }}/picture"/></a></td>-->
<!--                    <td ng-click="openBetDialogFromId(item.mid, $event)">-->
<!--                        <b>{{ item.fb_firstname }} {{ item.fb_lastname }}</b>-->
<!--                        <span class="text-blue">Play</span> : <span ng-switch="item.choose">-->
<!--                        <a href="#!/team/{{ item.hid }}" ng-switch-when="home">{{-->
<!--                            allTeams[item.hid].name }}</a>-->
<!--                        <a href="#!/team/{{ item.gid }}" ng-switch-when="away">{{-->
<!--                            allTeams[item.gid].name }}</a>-->
<!--                    </span> {{ item.betValue }}-->
<!--                    </td>-->
<!--                </tr>-->
<!--            </table>-->
<!--        </div>-->
<!---->
<!--    </div>-->

<div id="news-top-slide-box" class="wrapper-slide-comment-top" style="display: none;">

    <div class="box-comment-top" ng-repeat="item in newsTopSlide">
        <a href="/news.php?id={{ news.ontimelines[item.newsId]}}">
            <table>
                <tr>
                    <td><img ng-src="{{ item.imageLink}}"></td>
                    <td>
                        <b ng-bind="news.titles[item.newsId]"></b>
                        <span class="detail-news" ng-bind="news.desc[item.newsId]"></span>
                    </td>
                </tr>
            </table>
        </a>
    </div>
</div>



<div class="wrapper-content content-profile">
    <div id="fetching-data" style="text-align: center;">
        <div id="facebookG">
            <div id="blockG_1" class="facebook_blockG">
            </div>
            <div id="blockG_2" class="facebook_blockG">
            </div>
            <div id="blockG_3" class="facebook_blockG">
            </div>
        </div>
        <?php echo Utils::trans('Checking facebook session, please wait ...'); ?> <a href="javascript:window.location.reload();"><?php echo Utils::trans('If you wait too long, click here to reload.'); ?></a>
    </div>
    <div id="fetched-data" style="display: none;">
        <div class="button-sendPM"><button class="btn btn-mini" ng-click="openPmDialog(localFacebookInfo.uid, {uid: localFacebookInfo.uid, fb_uid: localFacebookInfo.fb_uid, display_name: localFacebookInfo.display_name})"><img src="images/icon/letter.png"> Message</button></div>
        <div ng-show="facebookInfo.id == localFacebookInfo.fb_uid" class="button-change-cover"> <button ng-click="uploadCover()"  class="btn btn-mini"><img src="images/icon/edit-icon-1.png"/> <?php echo Utils::trans('Edit Cover'); ?></button></div>
        <div class="section-header-profile" ng-style="{ 'background-image': 'url(' + (localFacebookInfo.cover || '/images/test-bg2.jpg') + ')' }">
            <div class="wrapper-header-profile">
                <table>
                    <tr>
                        <td class="profile-user-info">
                            <div class="box-img-profiles">
                                <div class="profile-img">
                                    <img ng-src="https://graph.facebook.com/{{ fb_uid}}/picture"/>
                                </div>
                                <div style="clear: both;"></div>
                                <div class="menu-info-follow">
                                    <span ng-show="following && fb_uid != facebookInfo.id">
                                        <button ng-if="currentUserFollowing.user[fb_uid]" ng-click="follow(false)" class="btn btn-small"><img src="images/icon/follow-1.png"/> <?php echo Utils::trans('Unfollow'); ?></button>
                                        <button ng-if="!currentUserFollowing.user[fb_uid]" ng-click="follow(true)" class="btn btn-small btn-info"><img src="images/icon/follow-white.png"/> <?php echo Utils::trans('Follow'); ?></button>
                                    </span>
                                </div>
                            </div>

                            <div class="box-info-user">
                                <span><input class="span2" ng-show="editing_displayname" ng-enter="updateDisplayname()" type="text" ng-model="edited_displayname"></span>
                                <span ng-show="!editing_displayname" class="name-user">
                                    <span ng-bind="localFacebookInfo.display_name || localFacebookInfo.user_status || localFacebookInfo.fb_name"></span>
                                    <img ng-show="facebookInfo.id == localFacebookInfo.fb_uid" ng-click="editDisplayname()" src="images/icon/edit-icon.png"/>
                                </span>
                                <div class="name-user-profile">
                                    <span ng-show="editing_status"><input ng-enter="updateStatus()" class="span2" type="text" ng-model="edited_status"></span>
                                    <span ng-show="!editing_status">
                                        <img style="float: right; padding-top: 10px;" ng-show="facebookInfo.id == localFacebookInfo.fb_uid" ng-click="editStatus()" src="images/icon/edit-icon-1.png"/>
                                        <span style="width: 130px;float: left;padding: 2px 0px 0px 0px;height: 25px;overflow: hidden;line-height: 13px;" ng-bind="localFacebookInfo.mind || 'Edit Your Status Here...'"></span>

                                    </span>
                                </div>
                                <div class="edit-name">
                                    <span ng-show="editing_site"><input type="text" ng-enter="updateSite()" ng-model="edited_site"></span>
                                    <span ng-show="!editing_site">
                                        <span>{{ localFacebookInfo.site || 'Site Url' }}</span>
                                        <img ng-show="facebookInfo.id == localFacebookInfo.fb_uid" ng-click="editSite()" src="images/icon/edit-icon.png"/>
                                    </span>
                                </div>

                                <div style="display: none;">
                                    <input id="upload-browse" type="file" ng-file-select="updateCover($files)" >
                                    <button ng-click="upload.abort()">Cancel Upload</button>
                                </div>
                            </div>
                        </td>
                        <td class="bg-cover-timeline">
                            <div class="tab-stat" ng-show="localFacebookInfo">
                                <table>
                                    <tr>
                                        <td class="three-coin" style="display: none;">
                                            <ul>
                                                <li>0</li>
                                                <li>0</li>
                                                <li>0</li>
                                            </ul>
                                        </td>
                                        <td ng-show="localFacebookInfo"> <h5>{{ localFacebookInfo.gp || 0 | toFixed }}</h5><?php echo Utils::trans('Score'); ?> </td>
                                        <td><h5>{{ localFacebookInfo.spirit || 0 }}</h5> <?php echo Utils::trans('Spirit'); ?></td>
                                        <td><h5>{{ localFacebookInfo.accurate }}% </h5>  <?php echo Utils::trans('PTA'); ?></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <div ng-if="achieve.length > 0" class="box-trophy">
                <div ng-if="trophyPages > 1" class="boxs">
                    <div ng-click="getTrophy('prev')" class="prev"><img src="images/icon/left-sign.png"/></div>
                    <div ng-click="getTrophy('next')" class="nextt"><img src="images/icon/right-sign.png"/></div>
                </div>
                <table>
                    <tr>
                        <td ng-click="viewTrophy(item)" ng-repeat="item in trophyList| limitTo:8"><img ng-src="{{ item.picture}}"/></td>
<!--                            <td></td>-->
<!--                            <td></td>-->
<!--                            <td></td>-->
<!--                            <td></td>-->
<!--                            <td></td>-->
<!--                            <td></td>-->
<!--                            <td></td>-->
                    </tr>
                </table>

            </div>
        </div>

        <div class="section-result-score">
            <table>
                <tr>
                    <td class="tab-followr"><span class="tab-follow"><img src="images/icon/peple1.png"/> <?php echo Utils::trans('Follower'); ?> ({{ localFacebookInfo.follow_count || '??' }})</span></td>
                    <td><h4>{{ localFacebookInfo.pts}}</h4> <?php echo Utils::trans('Play'); ?></td>
                    <td><h4>{{ localFacebookInfo.w}}</h4> <?php echo Utils::trans('Win'); ?></td>
                    <td><h4>{{ localFacebookInfo.d}}</h4> <?php echo Utils::trans('Draw'); ?></td>
                    <td><h4>{{ localFacebookInfo.l}}</h4> <?php echo Utils::trans('Lose'); ?></td>
                    <td><h4>{{ localFacebookInfo.level}}</h4> <?php echo Utils::trans('Level'); ?></td>
                </tr>
            </table>
        </div>

        <!--        ระบบเหรียญ-->
        <div ng-if="facebookInfo.id == localFacebookInfo.fb_uid" class="tabCoins" style="display: snone;">

            <table>
                <tr>
                    <td><a href="/exchangeCoin.php"><img src="images/icon/diamond50.png"></a> <div class="coin"><b ng-bind="userInfo.diamond"></b><br> Diamond</div></td>
                    <td><a href="/exchangeCoin.php"><img src="images/icon/sgold100.png"></a> <div class="coin"><b ng-bind="userInfo.sgold || 0 | number">0</b><br> SGold</div></td>
                    <td><a href="/exchangeCoin.php"><img src="images/icon/scoin100.png"></a> <div class="coin"><b ng-bind="userInfo.scoin || 0 | number">0</b><br> SCoin</div></td>
                    <td><a href="/exchangeCoin.php"><img src="images/icon/reset.png"></a> แลกเปลี่ยนเหรียญ</a></td>
                </tr>
            </table>
        </div>

        <div class="section-match-result">
            <div class="section-menu-match-profile">
                <table>
                    <tr>
                        <td ng-click="changeTab('timeline')" ng-class="{'actived-menu': currentTab == 'timeline'}">
<!--                            <img
                                src="images/icon-profile/com.png"/>-->
                            <?php echo Utils::trans('News Feed'); ?>
                        </td>
<!--                        <td ng-click="changeTab('ranking')" ng-class="{'actived-menu': currentTab == 'ranking'}">
                            <img
                                src="images/icon-profile/rank.png"/>
                            <?php echo Utils::trans('Ranking'); ?>
                        </td>-->
                        <td ng-click="changeTab('play')" ng-class="{'actived-menu': currentTab == 'play'}">
<!--                            <img
                                src="images/icon-profile/paly.png"/>-->
                            <?php echo Utils::trans('Play'); ?>
                        </td>

                        <td ng-click="changeTab('following')" ng-class="{'actived-menu': currentTab == 'following'}">
<!--                            <img
                                src="images/icon-profile/fol.png"/>-->
                            <?php echo Utils::trans('Following'); ?>
                        </td>
                        <td ng-show="facebookInfo.id == localFacebookInfo.fb_uid"><a href="/statement.php">Statement</a></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="section-menutop-feed" style="display: none;">
            <div class="bg-menu-feed">
                <table>
                    <tr>
                        <td ng-click="changeTab('feed')" ng-class="{'actived-menu': currentTab == 'feed'}" class="menu-feed"></td>
                        <td ng-click="changeTab('ranking')" ng-class="{'actived-menu': currentTab == 'ranking'}" class="menu-ranking"></td>
                        <td ng-click="changeTab('play')" ng-class="{'actived-menu': currentTab == 'play'}" class="menu-play"></td>
                        <td ng-click="changeTab('following')" ng-class="{'actived-menu': currentTab == 'following'}" class="menu-following"></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="section-new-feed-user" ng-include="tabInclude">

        </div>

    </div>

</div>

<div id="selected-trophy" class="section-video" style="display: none;">
    <div class="modal-backdrop fade in"></div>
    <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="false" style="display: block;">
        <!--<div class="box-close"><img src="images/icon/close.png"/></div>-->
        <div class="header-video">
            <div class="logos"><img src="images/icon/logo.png"/></div>
            <div ng-click="closeTrophy()" class="closed">X</div>
            <div style="clear: both;"></div>

            <div class="bg-topscore">
                <table>
                    <tr>
                        <td><h1><span ng-bind="selectedTrophy.desc"></span></h1>    
                            Type : <span ng-bind="selectedTrophy.type"></span><br/>
                            Rank : <span ng-bind="selectedTrophy.sequence"></span><br/>
                            Date : <span ng-bind="selectedTrophy.earn_at"></span><br/>
                            <span class="boxQuantity">Quantity : 2</span>
                        </td>
                        <td><img ng-src="{{ selectedTrophy.picture}}"/></td>
                    </tr>
                </table>
            </div>


        </div>
    </div>
</div>

<div id="selected-trophy-wc" class="section-video" style="display: none;">
    <div class="modal-backdrop fade in"></div>
    <div id="Modal" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="false" style="display: block;">
        <div class="header-video">
            <div class="logos"><img src="images/icon/logo.png"/></div>
            <div ng-click="closeTrophy()" class="closed">X</div>
            <div style="clear: both;"></div>

            <div class="fullImages-winner-trophy">
                <img src="images/icon-profile/winner-trophy.png">
            </div>
        </div>
    </div>
</div>



<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>