<?php
require_once(dirname(__FILE__) . '/_init_.php');

define('__LEAGUE_ID__', $_REQUEST['id']);

$title = 'รวบรวมรายละเอียดข้อมูลของแต่ละลีกทั่วโลก';
$meta = '<meta name="description" content="รวบรวมข้อมูลรายละเอียดของแต่ละลีกทั่วโลก ทั้งตารางคะแนน ตารางการแข่งล่วงหน้า ผลการแข่งขัน">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอลสด,ผลบอล,livescore,โปรแกรมแข่งขันล่วงหน้า,ตารางคะแนน">' . "\n";

$service_leagueInfo = Services::getLeagueInfo(__LEAGUE_ID__);
$service_allteam = Services::getAllTeam();

$footerScript .= '<script id="leagueScript" src="scripts/league.js" leagueId="'.__LEAGUE_ID__.'"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>


    <div class="wrapper-content">

        <div class="content">
            <div class="section-header-profile league-cover">
                <table>
                    <tr>
                        <td class="tab-logo-header">
                            <div class="logo-team-stat"><img src="{{apiUrl}}/uploads/media/leagues/<?php echo $service_leagueInfo->info[0]->leagueId; ?>_256x256.png"/></div>
                        </td>
                        <td class="tab-nameteam-header">
                            <span class="name-user league-name-title"><?php echo isset($service_allleague->{$service_leagueInfo->info[0]->leagueId}->name) ? $service_allleague->{$service_leagueInfo->info[0]->leagueId}->name : $service_leagueInfo->info[0]->leagueName; ?></span>

                            <div class="country-stat"><img src="images/countries/<?php echo $service_leagueInfo->info[0]->competitionId; ?>.png"/> </div>
                        </td>

                        <td class="tab-like-this-team">
                        <span ng-click='favorite("league", <?php echo json_encode($service_leagueInfo->info[0]); ?>)'>
                            <img ng-if="favoriteLeague[<?php echo $service_leagueInfo->info[0]->leagueId; ?>]" src="images/icon/heart.png"/>
                            <img ng-if="!favoriteLeague[<?php echo $service_leagueInfo->info[0]->leagueId; ?>]" src="images/icon/heart-hide.png"/>
                        </span>
                        </td>

                    </tr>
                </table>
            </div>

            <div class="wrapper-lastest-match-result">

                <?php if (!empty($service_leagueInfo->stat_table[0])): ?>
                <div>
                    <div class="header-match-title"><?php echo Utils::trans('Stats Table'); ?></div>

                    <div class="box-table">
                        <div class="section-table">

                            <div class="tab-menu-table">
                                <table>
                                    <tr class="heading-table">
                                        <th ng-class="{'tab-total': tableTab == 'total'}" ng-click="tableTab = 'total'">
                                            <?php echo Utils::trans('Total'); ?>
                                        </th>
                                        <th ng-class="{'tab-total': tableTab == 'home'}" ng-click="tableTab = 'home'"><?php echo Utils::trans('Home'); ?></th>
                                        <th ng-class="{'tab-total': tableTab == 'away'}" ng-click="tableTab = 'away'"><?php echo Utils::trans('Away'); ?></th>
                                    </tr>
                                </table>
                            </div>
                            <table>
                                <tbody ng-show="tableTab == 'total'">
                                <tr>
                                    <th colspan="2"></th>
                                    <th></th>
                                    <th>P</th>
                                    <th>W</th>
                                    <th>D</th>
                                    <th>L</th>
                                    <th>F</th>
                                    <th>A</th>
                                    <th>GD</th>
                                    <th>PTS</th>
                                </tr>
                                <?php foreach ($service_leagueInfo->stat_table as $index => $item): ?>
                                    <tr class="<?php if ($index <= 1) echo ' bg-team-goto-champion-league'; ?><?php if ($index >= (count($service_leagueInfo->stat_table) - 3)) echo ' bg-team-degraded'; ?>">
                                        <td class="order-table"><?php echo $item->no; ?></td>
                                        <td class="name-team"><a href="/team.php?id=<?php echo $item->tid; ?>"><img src="<?php echo $service_leagueInfo->logos->{$item->tid}->{'32x32'}; ?>"> <?php echo isset($service_allteam->{$item->tid}->name) ? $service_allteam->{$item->tid}->name : $item->tn; ?></a></td>
                                        <td></td>
                                        <td><?php echo $item->gp; ?></td>
                                        <td><?php echo $item->w; ?></td>
                                        <td><?php echo $item->d; ?></td>
                                        <td><?php echo $item->l; ?></td>
                                        <td><?php echo $item->gf; ?></td>
                                        <td><?php echo $item->ga; ?></td>
                                        <td><?php echo $item->plusminus; ?></td>
                                        <td><span class="font-gray"><?php echo $item->pts; ?></span></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>

                                <tbody ng-show="tableTab == 'home'">
                                <tr>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td>P</td>
                                    <td>W</td>
                                    <td>D</td>
                                    <td>L</td>
                                    <td>F</td>
                                    <td>A</td>
                                    <td>GD</td>
                                    <td>PTS</td>
                                </tr>
                                <?php foreach (__::sortBy($service_leagueInfo->stat_table, function ($n) { return $n->no_h; }) as $index => $item): ?>
                                    <tr class="<?php if ($index <= 1) echo ' bg-team-goto-champion-league'; ?><?php if ($index >= (count($service_leagueInfo->stat_table) - 3)) echo ' bg-team-degraded'; ?>">
                                        <td class="order-table"><?php echo $item->no_h; ?></td>
                                        <td class="name-team"><a href="/team.php?id=<?php echo $item->tid; ?>"><img src="<?php echo $service_leagueInfo->logos->{$item->tid}->{'32x32'}; ?>"> <?php echo isset($service_allteam->{$item->tid}->name) ? $service_allteam->{$item->tid}->name : $item->tn; ?></a></td>
                                        <td></td>
                                        <td><?php echo $item->gp_h; ?></td>
                                        <td><?php echo $item->w_h; ?></td>
                                        <td><?php echo $item->d_h; ?></td>
                                        <td><?php echo $item->l_h; ?></td>
                                        <td><?php echo $item->gf_h; ?></td>
                                        <td><?php echo $item->ga_h; ?></td>
                                        <td><?php echo $item->plusminus_h; ?></td>
                                        <td><span class="font-gray"><?php echo $item->pts_h; ?></span></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>

                                <tbody ng-show="tableTab == 'away'">
                                <tr>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td>P</td>
                                    <td>W</td>
                                    <td>D</td>
                                    <td>L</td>
                                    <td>F</td>
                                    <td>A</td>
                                    <td>GD</td>
                                    <td>PTS</td>
                                </tr>
                                <?php foreach (__::sortBy($service_leagueInfo->stat_table, function ($n) { return $n->no_g; }) as $index => $item): ?>
                                    <tr class="<?php if ($index <= 1) echo ' bg-team-goto-champion-league'; ?><?php if ($index >= (count($service_leagueInfo->stat_table) - 3)) echo ' bg-team-degraded'; ?>">
                                        <td class="order-table"><?php echo $item->no_g; ?></td>
                                        <td class="name-team"><a href="/team.php?id=<?php echo $item->tid; ?>"><img src="<?php echo $service_leagueInfo->logos->{$item->tid}->{'32x32'}; ?>"> <?php echo isset($service_allteam->{$item->tid}->name) ? $service_allteam->{$item->tid}->name : $item->tn; ?></a></td>
                                        <td></td>
                                        <td><?php echo $item->gp_g; ?></td>
                                        <td><?php echo $item->w_g; ?></td>
                                        <td><?php echo $item->d_g; ?></td>
                                        <td><?php echo $item->l_g; ?></td>
                                        <td><?php echo $item->gf_g; ?></td>
                                        <td><?php echo $item->ga_g; ?></td>
                                        <td><?php echo $item->plusminus_g; ?></td>
                                        <td><span class="font-gray"><?php echo $item->pts_g; ?></span></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if (!empty($service_leagueInfo->resultList[0])): ?>
                <div>
                    <div class="header-match-title"><?php echo Utils::trans('Latest Match Result'); ?></div>

                    <div class="box-table">
                        <div class="section-lastest-match">
                            <table>
                                <thead>
                                <tr>
                                    <th><?php echo Utils::trans('Date'); ?></th>
<!--                                    <th><?php echo Utils::trans('League'); ?></th>-->
                                    <th><?php echo Utils::trans('Home'); ?></th>
                                    <th><?php echo Utils::trans('Goal'); ?></th>
                                    <th><?php echo Utils::trans('Away'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($service_leagueInfo->resultList as $index => $item): ?>
                                    <tr>
                                        <td  class="date-match"><?php echo $item->date; ?></td>
<!--                                        <td class="league-match">UCL</td>-->
                                        <td class="teamhome-match"><a href="/team.php?id=<?php echo $item->hid; ?>"><span class="teamhome-m"><?php echo (isset($service_allteam->{$item->hid}) && isset($service_allteam->{$item->hid}->name)) ? $service_allteam->{$item->hid}->name : $item->hn; ?></span>
                                                <img src="<?php echo isset($service_leagueInfo->logos->{$item->hid}) ? $service_leagueInfo->logos->{$item->hid}->{'32x32'} : ''; ?>"/></a></td>
                                        <td class="score-match" style="text-align: center"><?php echo $item->score; ?></td>
                                        <td class="teamaway-match"><a href="/team.php?id=<?php echo $item->aid; ?>"><img src="<?php echo isset($service_leagueInfo->logos->{$item->aid}) ? $service_leagueInfo->logos->{$item->aid}->{'32x32'} : ''; ?>"/> <span class="teamaway-m"><span class="teamhome-m"><?php echo (isset($service_allteam->{$item->aid}) && isset($service_allteam->{$item->aid}->name)) ? $service_allteam->{$item->aid}->name : $item->an; ?></span></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if (!empty($service_leagueInfo->fixtureList[0])): ?>
                <div>
                    <div class="header-match-title"><?php echo Utils::trans('Fixture'); ?></div>
                    <div class="box-table">
                        <div class="section-fixure">
                            <table>
                                <thead>
                                <tr>
                                    <th><?php echo Utils::trans('Date'); ?></th>
                                    <th><?php echo Utils::trans('League'); ?></th>
                                    <th><?php echo Utils::trans('Home'); ?></th>
                                    <th><?php echo Utils::trans('Goal'); ?></th>
                                    <th><?php echo Utils::trans('Away'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($service_leagueInfo->fixtureList as $index => $item): ?>
                                    <tr">
                                        <td class="date-match"><?php echo $item->date ?></td>
                                        <td>UCL</td>
                                        <td class="team-h"><a href="/team.php?id=<?php echo $item->hid; ?>"><?php echo isset($service_allteam->{$item->hid}->name) ? $service_allteam->{$item->hid}->name : $item->hn; ?> <img src="<?php echo $service_leagueInfo->logos->{$item->hid}->{'32x32'}; ?>"/></a></td>
                                        <td class="score-match"> -</td>
                                        <td class="team-a"><a href="/team.php?id=<?php echo $item->aid; ?>"><img src="<?php echo $service_leagueInfo->logos->{$item->aid}->{'32x32'}; ?>"/> <?php echo isset($service_allteam->{$item->aid}->name) ? $service_allteam->{$item->aid}->name : $item->an; ?></a></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

            </div>
        </div>

    </div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>