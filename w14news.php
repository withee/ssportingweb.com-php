<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ผลบอลสด อัพเดทรวดเร็วที่สุดและแม่นยำที่สุด';
$meta = '<meta name="description" content="เช็คผลบอลสด ผลบอลเมื่อคืนและผลบอลย้อนหลังได้ที่นี่ ข้อมูลแม่นยำอัพเดทผลรวดเร็ว ซึ่งจะทำให้คุณไม่พลาดทุกวินาทีสำคัญ">' . "\n";
$meta .= '<meta name="keyword" content="ผลบอลสด,ผลบอล,ผลบอลเมื่อคืน,ผลบอลย้อนหลัง,ไฮไลท์ฟุตบอล">' . "\n";

//$service_w14 = Services::getW14();
$service_news = Services::getNews();

//echo '<pre>';
//print_r($service_news);
//echo '</pre>';
//exit;

$footerScript .= '<script src="scripts/w14news.js"></script>';

use Carbon\Carbon;

require_once(__INCLUDE_DIR__ . '/header.php')
?>


<div id="news-top-slide-box" class="wrapper-slide-comment-top" style="display: none;">

    <div class="box-comment-top" ng-repeat="item in newsTopSlide">
        <a target=_blank href="/news.php?id={{ news.ontimelines[item.newsId]}}" target="_blank">
            <table>
                <tr>
                    <td><img ng-src="{{ item.imageLink}}"></td>
                    <td>
                        <b ng-bind="news.titles[item.newsId]"></b>
                        <span class="detail-news" ng-bind="news.desc[item.newsId]"></span>
                    </td>
                </tr>
            </table>
        </a>
    </div>
</div>


<div class="wrapper-content content-profile">

    <div class="banner" style="padding-left: 5px ">
        <a href="/w14index.php"><img src="images/banner.jpg"></a>
    </div>

    <div class="tab-heading-title">News World Cup 2014</div>
    <div class="wrapperNews">
        <?php foreach ($service_news->data as $item): ?>
            <?php
            $d = Carbon::createFromTimestamp($item->createDatetime)->addHours(__GMT_OFFSET__);
            $dw = $d->day . ' ' . Utils::monthOfYear($d->month) . ' ' . $d->year;
            ?>
            <div class="boxNews-woldcup">

                <img src="<?php echo $item->imageLink; ?>">

                <div class="infoNews">
                    <a target="_blank" href="/news.php?id=<?php echo $service_news->ontimelines->{$item->newsId}; ?>"> <b><?php echo $service_news->titles->{$item->newsId}; ?></b></a>
    <?php echo $service_news->desc->{$item->newsId}; ?>
                </div>
                <div class="dateNews"><img
                        src="images/icon/calendar.png"> <?php echo Utils::dayOfWeek($d->dayOfWeek); ?> <?php echo $dw; ?>
                </div>

            </div>
<?php endforeach; ?>

        <div style="clear: both;"></div>
    </div>
</div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>