<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ssporting.com ผลบอลสด ข้อมูลแม่นยำ รวดเร็วกว่าใคร';
$meta = '<meta name="description" content="ผลบอลสดทุกลีกทั่วโลก รวบรวมสถิติการแข่งขัน ไฮไลท์ฟุตบอล ข้อมูลการแข่งและทรรศนะจากเทพเซียนบอลทั้งหลาย รวมทั้งเกมทายผลฟุตบอลยอดฮิต">' . "\n";
$meta .= '<meta name="keywords" content="ผลบอล,ผลบอลสด,ทรรศนะบอล,livescore,ไฮไลท์ฟุตบอล,โปรแกรมบอลล่วงหน้า">' . "\n";

$footerScript .= '<script src="scripts/exchange-coin.js"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>
<div>
    <!--Content-->
    <div class="wrapper-content content-profile">

        <div class="boxAllCoins">
            <div class="boxDiamond">
                <table>
                    <tr>
                        <td><img src="images/icon/diamond50.png"></td>
                        <td><b ng-bind="userInfo.diamond || 0"></b><br> Diamond</td>
                    </tr>
                </table>
            </div>
            <div class="boxSgold">
                <table>
                    <tr>
                        <td><img src="images/icon/sgold100.png"></td>
                        <td><b ng-bind="userInfo.sgold || 0"></b><br> Sgold</td>
                    </tr>
                </table>
            </div>
            <div class="boxScoin">
                <table>
                    <tr>
                        <td><img src="images/icon/scoin100.png"></td>
                        <td><b ng-bind="userInfo.scoin || 0"></b><br> Scoin</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="boxExchangeCoins">
            <div class="box1">
                <div class="boxCombo">
                    <table>
                        <tr>
                            <td><img src="images/icon/combo1.png"></td>
                            <td>
                                <b ng-if="userInfo.step_combo_type === 'positive'" ng-bind="userInfo.step_combo_count || 0"></b>
                                <b ng-if="userInfo.step_combo_type !== 'positive'">0</b>
                                <br>Combo Win</td>
                            <td><button ng-click="exchangeStepBonus()" ng-disabled="userInfo.step_combo_type != 'positive'" class="btn btn-large btn-success"><img src="images/icon/diamond.png" style="width: 20px;"> แลกเป็นเพชร</button></td>
                        </tr>
                    </table>
                </div>
                <div class="boxCombo">
                    <table>
                        <tr>
                            <td><img src="images/icon/combo2.png"></td>
                            <td>
                                <b ng-if="userInfo.step_combo_type === 'negative'" ng-bind="userInfo.step_combo_count || 0"></b>
                                <b ng-if="userInfo.step_combo_type !== 'negative'">0</b>
                                <br>Combo Lose</td>
                            <td><button ng-click="exchangeStepBonus()" ng-disabled="userInfo.step_combo_type != 'negative'" class="btn btn-large btn-success"><img src="images/icon/diamond.png" style="width: 20px;"> แลกเป็นเพชร</button></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="box2">
                <table>
                    <tr>
                        <td><b>Reset Sgold</b><br>กด Reset เพื่อเปลี่ยน Sgold ให้เป็น 0
                            <br><button ng-click="resetSgold()" class="btn btn-large btn-danger"><img src="images/icon/reset.png" style="width: 30px;"> Reset</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="clear: both;"></div>

        <div class="tableExchanged">
            <table>
                <thead>
                    <tr>
                        <th>ประเภท</th>
                        <th>เงื่อนไข</th>
                        <th>แลกเปลี่ยน</th>
                        <th>ได้รับ</th>
                    </tr>
                </thead>
                <tr>
                    <td><b>แลก Sgold เป็น Diamond</b><br>(<span ng-bind="systemConfig.sgold_to_diamond || '?'"></span> Sgold = 1 Diamond)</td>
                    <td><img src="images/icon/sgold30.png"> <img src="images/icon/exchanged.png" style="width: 30px;"> <img src="images/icon/diamond30.png" style="width: 30px;"></td>
                    <td>
                        <select ng-model="exchangeSgoldToDiamond" ng-disabled="!userInfo.uid">
                            <option value="0" selected>เลือกจำนวน Scoin</option>
                            <option value="{{ systemConfig.sgold_to_diamond }}" ng-disabled="systemConfig.sgold_to_diamond > userInfo.sgold">{{ systemConfig.sgold_to_diamond }}</option>
                            <option value="{{ systemConfig.sgold_to_diamond * 2 }}" ng-disabled="(systemConfig.sgold_to_diamond * 2) > userInfo.sgold">{{ systemConfig.sgold_to_diamond * 2 }}</option>
                            <option value="{{ systemConfig.sgold_to_diamond * 5 }}" ng-disabled="(systemConfig.sgold_to_diamond * 5) > userInfo.sgold">{{ systemConfig.sgold_to_diamond * 5 }}</option>
                            <option value="{{ systemConfig.sgold_to_diamond * 10 }}" ng-disabled="(systemConfig.sgold_to_diamond * 10) > userInfo.sgold">{{ systemConfig.sgold_to_diamond * 10 }}</option>
                        </select>
                        <button ng-click="exchangeDiamond()" class="btn btn-success" ng-disabled="!exchangeSgoldToDiamond || exchangeSgoldToDiamond == 0">แลกเปลี่ยน</button>
                    </td>
                    <td><img src="images/icon/diamond30.png" style="width: 30px;"><br><b ng-bind="exchangeSgoldToDiamond / systemConfig.sgold_to_diamond || 0"></b></td>
                </tr>
                <tr>
                    <td><b>แลก Sgold เป็น Scoin</b><br>(<span ng-bind="systemConfig.exchange_rate[0][0] || '?'"></span> Sgold = <span ng-bind="systemConfig.exchange_rate[0][1] || '?'"></span> Scoin)</td>
                    <td><img src="images/icon/sgold30.png"> <img src="images/icon/exchanged.png" style="width: 30px;"> <img src="images/icon/scoin30.png" style="width: 30px;"></td>
                    <td>
                        <select ng-model="exchangeSgoldToScoin" ng-disabled="!userInfo.uid">
                            <option value="0" selected>เลือกจำนวน Sgold</option>
                            <option value="{{ systemConfig.exchange_rate[0][0] }}" ng-disabled="systemConfig.exchange_rate[0][0] > userInfo.sgold">{{ systemConfig.exchange_rate[0][0] }}</option>
                            <option value="{{ systemConfig.exchange_rate[0][0] * 10 }}" ng-disabled="(systemConfig.exchange_rate[0][0] * 10) > userInfo.sgold">{{ systemConfig.exchange_rate[0][0] * 10 }}</option>
                            <option value="{{ systemConfig.exchange_rate[0][0] * 20 }}" ng-disabled="(systemConfig.exchange_rate[0][0] * 20) > userInfo.sgold">{{ systemConfig.exchange_rate[0][0] * 20 }}</option>
                            <option value="{{ systemConfig.exchange_rate[0][0] * 50 }}" ng-disabled="(systemConfig.exchange_rate[0][0] * 50) > userInfo.sgold">{{ systemConfig.exchange_rate[0][0] * 50 }}</option>
                        </select>
                        <button ng-click="exchangeSgold2Scoin()" class="btn btn-success" ng-disabled="!exchangeSgoldToScoin || exchangeSgoldToScoin == 0">แลกเปลี่ยน</button>
                    </td>
                    <td><img src="images/icon/scoin30.png" style="width: 30px;"><br><b ng-bind="exchangeSgoldToScoin * systemConfig.exchange_rate[0][1] || 0"></b></td>
                </tr>
<!--                <tr>-->
<!--                    <td><b>แลก Scoin เป็น Sgold</b><br>(<span ng-bind="systemConfig.exchange_rate[1][1] || '?'"></span> Scoin = <span ng-bind="systemConfig.exchange_rate[1][0] || '?'"></span> Sgold)</td>-->
<!--                    <td><img src="images/icon/scoin30.png"> <img src="images/icon/exchanged.png" style="width: 30px;"> <img src="images/icon/sgold30.png" style="width: 30px;"></td>-->
<!--                    <td>-->
<!--                        <select ng-model="exchangeScoinToSgold" ng-disabled="!userInfo.uid">-->
<!--                            <option value="0" selected>เลือกจำนวน Scoin</option>-->
<!--                            <option value="{{ systemConfig.exchange_rate[1][1] }}" ng-disabled="systemConfig.exchange_rate[1][1] > userInfo.scoin">{{ systemConfig.exchange_rate[1][1] }}</option>-->
<!--                            <option value="{{ systemConfig.exchange_rate[1][1] * 10 }}" ng-disabled="(systemConfig.exchange_rate[1][1] * 10) > userInfo.scoin">{{ systemConfig.exchange_rate[1][1] * 10 }}</option>-->
<!--                            <option value="{{ systemConfig.exchange_rate[1][1] * 20 }}" ng-disabled="(systemConfig.exchange_rate[1][1] * 20) > userInfo.scoin">{{ systemConfig.exchange_rate[1][1] * 20 }}</option>-->
<!--                            <option value="{{ systemConfig.exchange_rate[1][1] * 50 }}" ng-disabled="(systemConfig.exchange_rate[1][1] * 50) > userInfo.scoin">{{ systemConfig.exchange_rate[1][1] * 50 }}</option>-->
<!--                        </select>-->
<!--                        <button ng-click="exchangeScoin2Sgold()" class="btn btn-success" ng-disabled="!exchangeScoinToSgold || exchangeScoinToSgold == 0">แลกเปลี่ยน</button>-->
<!--                    </td>-->
<!--                    <td><img src="images/icon/sgold30.png" style="width: 30px;"><br><b ng-bind="exchangeScoinToSgold / systemConfig.exchange_rate[1][1] || 0"></b></td>-->
<!--                </tr>-->
            </table>
        </div>


        <div style="clear: both;"></div>

        <div class="wrapper-box-feed-expand" style="margin: 5px 8px 3px 3px;">
            <div class="headingBox">กฏ กติกาการแลกรางวัล</div>

        </div>

    </div>

</div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>
