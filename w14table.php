<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ผลบอลสด อัพเดทรวดเร็วที่สุดและแม่นยำที่สุด';
$meta = '<meta name="description" content="เช็คผลบอลสด ผลบอลเมื่อคืนและผลบอลย้อนหลังได้ที่นี่ ข้อมูลแม่นยำอัพเดทผลรวดเร็ว ซึ่งจะทำให้คุณไม่พลาดทุกวินาทีสำคัญ">' . "\n";
$meta .= '<meta name="keyword" content="ผลบอลสด,ผลบอล,ผลบอลเมื่อคืน,ผลบอลย้อนหลัง,ไฮไลท์ฟุตบอล">' . "\n";

$service_w14 = Services::getW14();

//echo '<pre>';
//print_r($service_w14->matches);
//echo '</pre>';
//exit;

$footerScript .= '<script src="scripts/w14table.js"></script>';
use Carbon\Carbon;

require_once(__INCLUDE_DIR__ . '/header.php')
?>

    <div id="news-top-slide-box" class="wrapper-slide-comment-top" style="display: none;">

        <div class="box-comment-top" ng-repeat="item in newsTopSlide">
            <a href="/news.php?id={{ news.ontimelines[item.newsId]}}">
                <table>
                    <tr>
                        <td><img ng-src="{{ item.imageLink}}"></td>
                        <td>
                            <b ng-bind="news.titles[item.newsId]"></b>
                            <span class="detail-news" ng-bind="news.desc[item.newsId]"></span>
                        </td>
                    </tr>
                </table>
            </a>
        </div>
    </div>


    <div class="wrapper-content content-profile">

        <div class="banner" style="padding-left: 5px ">
            <a href="/w14index.php"><img src="images/banner.jpg"></a>
        </div>

        <div class="tab-heading-title">Table Group</div>

        <?php foreach ($service_w14->tables as $item): ?>
            <div class="wrapper-box-feed-expand table-group">
                <div class="headerGroup"><?php echo preg_replace("#-#", ' ',$item[0]->subLeagueNamePk); ?></div>
                <div class="table-worldcup">
                    <table>
                        <thead>
                        <tr>
                            <th>Teams</th>
                            <th>MP</th>
                            <th>W</th>
                            <th>D</th>
                            <th>L</th>
                            <th>GF</th>
                            <th>GA</th>
                            <th>GD</th>
                            <th>Pts</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($item as $match): ?>
                            <tr>
                                <td>
                                    <?php if($match->tid): ?>
                                        <img src="http://ws.1ivescore.com/worldcup/<?php echo $match->tid; ?>_.png">
                                    <?php else: ?>
                                        <img src="http://ws.1ivescore.com/teams_clean/team_default_32x32.png">
                                    <?php endif; ?>
                                    <?php echo isset($service_w14->team->{$match->tid}) ? $service_w14->team->{$match->tid}->{__LANGUAGE__} : $match->tnPk; ?>
                                </td>
                                <td><?php echo $match->gp; ?></td>
                                <td><?php echo $match->w; ?></td>
                                <td><?php echo $match->d; ?></td>
                                <td><?php echo $match->l; ?></td>
                                <td><?php echo $match->gf; ?></td>
                                <td><?php echo $match->ga; ?></td>
                                <td><?php echo $match->plusminus; ?></td>
                                <td><?php echo $match->pts; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php endforeach; ?>

    </div>


<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>