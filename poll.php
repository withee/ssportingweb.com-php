<?php
require_once(dirname(__FILE__) . '/_init_.php');

$title = 'ผลบอลสด อัพเดทรวดเร็วที่สุดและแม่นยำที่สุด';
$meta = '<meta name="description" content="เช็คผลบอลสด ผลบอลเมื่อคืนและผลบอลย้อนหลังได้ที่นี่ ข้อมูลแม่นยำอัพเดทผลรวดเร็ว ซึ่งจะทำให้คุณไม่พลาดทุกวินาทีสำคัญ">' . "\n";
$meta .= '<meta name="keyword" content="ผลบอลสด,ผลบอล,ผลบอลเมื่อคืน,ผลบอลย้อนหลัง,ไฮไลท์ฟุตบอล">' . "\n";

$footerScript .= '<script id="rankingScript" src="scripts/ranking.js" fb_uid="' . __FB_UID__ . '"></script>';

require_once(__INCLUDE_DIR__ . '/header.php')
?>


    <div class="wrapper-content content-profile">
        <div class="wrapper-box-feed-expand">
            <div class="title-poll">คิดว่าทีมไหนจะได้แชมป์พรีเมียลีกฤดูกาล 2015 - 2016</div>
            <div class="boardDescription">
                <div class="wrapper-box-votepoll">
                    <table class="option-poll-tab">
                        <tr>
                            <td><input type="checkbox"></td>
                            <td>
                                <div class="graph-outer-poll">
                                    <div class="inner-left-bar-poll" style="width: 76%;">แมนเชสเตอร์ ยูไนเต็ด
                                    </div>
                                </div>
                            </td>
                            <td>76%</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <div class="img-user-vote">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">

                                    <div class="more-user-vote">+456</div>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>

                <div class="wrapper-box-votepoll">
                    <table class="option-poll-tab">
                        <tr>
                            <td><input type="checkbox"></td>
                            <td>
                                <div class="graph-outer-poll">
                                    <div class="inner-left-bar-poll" style="width: 50%;">อาร์เซนอล</div>
                                </div>
                            </td>
                            <td>50%</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <div class="img-user-vote">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">

                                    <div class="more-user-vote">+209</div>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>

                <div class="wrapper-box-votepoll">
                    <table class="option-poll-tab">
                        <tr>
                            <td><input type="checkbox"></td>
                            <td>
                                <div class="graph-outer-poll">
                                    <div class="inner-left-bar-poll" style="width: 49%;">ลิเวอร์พูล</div>
                                </div>
                            </td>
                            <td>49%</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <div class="img-user-vote">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">

                                    <div class="more-user-vote">+259</div>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>


                <div class="wrapper-box-votepoll">
                    <table class="option-poll-tab">
                        <tr>
                            <td><input type="checkbox"></td>
                            <td>
                                <div class="graph-outer-poll">
                                    <div class="inner-left-bar-poll" style="width: 55%;">บาเซโลน่า</div>
                                </div>
                            </td>
                            <td>55%</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <div class="img-user-vote">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">

                                    <div class="more-user-vote">+259</div>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>

                <div class="wrapper-box-votepoll">
                    <table class="option-poll-tab">
                        <tr>
                            <td><input type="checkbox"></td>
                            <td>
                                <div class="graph-outer-poll">
                                    <div class="inner-left-bar-poll" style="width: 39%;">เรอัลมาดริด</div>
                                </div>
                            </td>
                            <td>39%</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <div class="img-user-vote">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">

                                    <div class="more-user-vote">+20</div>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>

                <div class="wrapper-box-votepoll">
                    <table class="option-poll-tab">
                        <tr>
                            <td><input type="checkbox"></td>
                            <td>
                                <div class="graph-outer-poll">
                                    <div class="inner-left-bar-poll" style="width: 88%;">แมนซิตี้</div>
                                </div>
                            </td>
                            <td>88%</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <div class="img-user-vote">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">
                                    <img ng-src="/images/imguser.png">

                                    <div class="more-user-vote">+390</div>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>

                <div class="wrapper-box-votepoll">
                    <table class="option-poll-tab">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <input type="text" placeholder="+ เพิ่มคำตอบ">
                                <button class="btn btn-primary">เพิ่ม</button>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>


                <!--ส่วนแสดงคอมเม้นท์ Poll-->

                <div class="box-show-comment" style="margin-top: 20px;">
                    <table>
                        <tr>
                            <td class="user-comment-img" style="display: none;">
                            </td>
                            <td class="comment-box-like-match">
                                <div ng-click="browsePicture()" class="upimages-box-match" style="display:none;"><img
                                        src="images/icon/cam15.png"
                                        title="แนบรูปภาพ"/></div>
                                <textarea placeholder="แสดงความคิดเห็น..."></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="tab-ui-post">
                                <div class="tab-upload-img"><img src="images/icon/cam15.png" title="แนบรูปภาพ"/> รูปภาพ
                                </div>
                                <span class="loading"><img src="images/icon/loading_icon.gif"></span>
                                <div class="button-post-img">โพสต์</div>
                            </td>
                        </tr>
                    </table>

                </div>


                <div class="box-show-comment">
                    <span class="remove-ment ment-1" title="ลบ">x</span>
                    <table>
                        <tr>
                            <td class="user-comment-img userCommentGame"><a href="/profile.php?id={{ comment.fb_uid}}">
                                    <img src="/images/imguser.png"/></a></td>
                            <td class="user-comment-post">
                                <span class="reportPost"><img src="images/icon/downicon.png"></span>
                                <a href="/profile.php?id={{ comment.fb_uid}}"><b>Diarylovely</b></a>
                                เราชอบบาเซโลน่า
                                <div ng-show="comment.thumbnail">
                                    <img ng-src="{{ comment.thumbnail}}"/>
                                </div>
                            <span class="time-post">
                                <ul>
                                    <li class="time-post-comment-user">a few minute</li>
                                    <li class="user-liked"><img
                                            ng-click="likeComment(comment.id, comment.match_id, index)"
                                            src="images/icon/heart.png"/> (20) <span
                                            class="reply" ng-click="openReplyBox(comment.id)">{{ replies[comment.id].length}} reply</span>
                                    </li>
                                </ul>
                            </span>
                            </td>
                        </tr>
                    </table>

                    <div class="wrapper-reply-comment">
                        <div class="wrapper-reply">
                            <div class="box-show-comment sub-user-comment">
                                <span class="remove-ment ment-2" title="ลบ">x</span>
                                <table>
                                    <tr>
                                        <td class="user-comment-img userCommentGame"><img src="/images/imguser.png"/></td>
                                        <td class="user-comment-reply">
                                            <span class="reportPost"><img src="images/icon/downicon.png"></span>
                                            <b>Diarylovely</b> เชียร์ลิเวอร์พูลเต็มที่
                                            <div class="reply-image-upload" ng-show="reply.thumbnail">
                                                <img ng-src="{{ reply.thumbnail}}"/>
                                            </div>
                                        <span class="time-post">
                                            <ul>
                                                <li>2 minute ago</li>
                                                <li><img ng-click="likeReply(reply.id, comment.id, r_index)"
                                                         src="images/icon/heart.png"/> (2)
                                                </li>
                                            </ul>
                                        </span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div style="clear: both;"></div>

                        <!-- reply comment input -->
                        <div class="box-show-comment sub-user-comment reply-box" style="display: snone;">
                            <table>
                                <tr>
                                    <td class="user-comment-img userCommentGame"><img ng-if="facebookInfo.id"
                                                                                      ng-src="https://graph.facebook.com/{{ facebookInfo.id}}/picture"/>
                                    </td>
                                    <td class="user-comment-reply">

                                        <div class="upimages-box reply-upload">
                                            <img src="images/icon/cam15.png" title="แนบรูปภาพ"/></div>

                                        <input type="text" placeholder="reply..."/>
                                        <span class="loading loading-reply"><img src="images/icon/loading_icon.gif"/></span>

                                    </td>
                                </tr>
                            </table>
                            <div class="img-post-comment">
                                <div class="removeImg">x</div>
                                <img class="new-reply-thumbnail" ng-src="{{ newReply.thumbnail}}"/>

                                <div class="tab-reply">
                                    <div class="button-post-img">โพสต์</div>
                                    <div style="clear: both;"></div>
                                </div>
                                <button style="display: none;" class="btn">
                                    โพสต์
                                </button>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>


            </div>


            <div style="clear:both;"></div>
        </div>

    </div>
<?php require_once(__INCLUDE_DIR__ . '/footer.php'); ?>